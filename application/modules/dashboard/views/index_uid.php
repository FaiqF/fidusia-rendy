<style>
	.uk-sortable .md-card { cursor: pointer;  }
</style>
<div class="uk-grid">
	<div class="uk-width-1-1">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Monitoring Fiducia</h3>
			</div>
			<?php if(@$expired_date != ""){ ?>
			<div class="md-card-content" >
				<div id="alert"></div>
				<div class="uk-width-medium-1-1 uk-row-first">
					<div class="uk-alert uk-alert-danger" data-uk-alert=""><a href="#" class="uk-alert-close uk-close"></a></p><b>Masa aktif password anda akan berakhir dalam <?= $expired_date; ?> hari. Segera lakukan perubahan password. Jika tidak, user anda akan terblokir</b></p></div>
				</div>
			</div>
			<?php } ?>
			<form id="form1">
				<div class="md-card-content">
					<div class="uk-grid">  <!-- pake ui grid iki -->
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="uk-grid uk-grid-width-large-1-3 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable">

	<a href="#">
	<div>
 		<div class="md-card">
 			<div class="md-card-content">
 				<div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_bar peity_data">5,3,9,6,5,9,7</span></div>
 				<span class="uk-text-muted uk-text-small">Total Cabang</span>
 				<h2 class="uk-margin-remove"><span class="countUpMe"></span></h2>
 			</div>
 		</div>
 	</div>
	</a>

	<a href="#">
	<div>
 		<div class="md-card">
 			<div class="md-card-content">
 				<div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_bar peity_data">5,3,9,6,5,9,7</span></div>
 				<span class="uk-text-muted uk-text-small">Total User</span>
 				<h2 class="uk-margin-remove"><span class="countUpMe"></span></h2>
 			</div>
 		</div>
 	</div>
	</a>
 </div>
 
<script>
	$(document).ready(function(){

		$(".peity_bar").peity("bar", {
			height: 28,
			width: 48,
			padding: 0.2
		});

		App.datepicker();
		App.datepicker('.datepicker-month', true);
		App.date_range('#text3', '#text4');

		dataFilter();

		$('#text1').focus(function() {
			$('#form1').find('input, select, textarea').not('#text1').val('');
		});
		$('#text2').focus(function() {
			$('#form1').find('input, select, textarea').not('#text2').val('');
		});
		$('#text3,text4').focus(function() {
			$('#form1').find('input, select, textarea').not('#text3,#text4').val('');
		});
		$('#text5').focus(function() {
			$('.form2').find('input, select, textarea').not('#text5').val('');
		});
		$('#text6').focus(function() {
			$('.form2').find('input, select, textarea').not('#text6').val('');
		});
		$('#text7,text8').focus(function() {
			$('.form2').find('input, select, textarea').not('#text7,#tex84').val('');
		});
	});

	function dataFilter(data) {
		var filterTanggal = $('input[name=tanggal]').val();
		var filterBulan = $('input[name=bulan]').val();
		var filterTanggalAwal = $('input[name=tanggal_awal]').val();
		var filterTanggalAkhir = $('input[name=tanggal_akhir]').val();

		$.ajax({
			'url': base_url + 'dashboard/get_count_user',
			'type': 'post',
			'dataType': 'json',
			'data': {
				'fTanggal':filterTanggal,
				'fBulan':filterBulan,
				'fTanggalAwal':filterTanggalAwal,
				'fTanggalAkhir':filterTanggalAkhir,
			},
			'success': function(res) {
				var total_cabang  = res.jumlah.total_cabang;
				var total_user    = res.jumlah.total_user;

				var value_data = [];
				value_data[0] = total_cabang;
				value_data[1] = total_user;
				$('.countUpMe').each(function(i, v) {
					$(this).html(value_data[i]);
				});
			}
		});
	}
</script>
