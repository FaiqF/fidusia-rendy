<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	private $table_db       = 'ed';
	private $table_prefix   = 'ed_';
	private $prefix 		= 'dashboard';
	private $name 			= 'Dashboard';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{

		$views = 'index';
		if(user_data()->user_role == '32') {
			$views = 'index_uid';
		}
		$data['name'] 	= $this->name;
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin'] = ['peity', 'kendo_ui','datatables'];
		$users = $this->m_global->get_data_all('users', null, ['user_id' => user_data()->user_id]);
		$daylen = 60*60*24;
		if(date('Y-m-d', strtotime('+14 days')) >= $users[0]->user_expired_date){
			$data['expired_date'] = (strtotime($users[0]->user_expired_date)-strtotime(date('Y-m-d')))/$daylen;
		}
		$this->template->display($views, $data);
	}

	public function get_count()
	{

		$param = $this->input->post();
		$where = null;

		$levelCabangId = '10';
        if( user_data()->user_level == $levelCabangId ) {
            $where['data_cabang_id'] = user_data()->cabang_id;
        }

		$data['jumlah']['total_data'] = $this->m_global->count_data_all('data_transaksi', null, $where);
		$data['jumlah']['terupload']  = $this->m_global->count_data_all('data_transaksi', null, $where, "data_flag IN ('1')");
		$data['jumlah']['terkirim']   = $this->m_global->count_data_all('data_transaksi', null, $where, "data_flag IN ('2')");
		$data['jumlah']['sertifikat'] = $this->m_global->count_data_all('data_transaksi', null, $where, "data_flag IN ('3','4', '5')");

		echo json_encode( $data );
	}

	public function get_count_user()
	{
		$data['jumlah']['total_cabang'] = $this->m_global->count_data_all('cabang');
		$data['jumlah']['total_user'] = $this->m_global->count_data_all('users', null, ['user_grouping_id' => '1']);

		echo json_encode( $data ); exit;
	}

	public function newtab()
	{
		$this->load->view('login/newtab');
	}

}

/* End of file Dashboard.php */
/* Location: ./application/modules/dashboard/controllers/Dashboard.php */