<form method="post" action="<?=$url.'/act_reupload'; ?>" id="form_approve" enctype="multipart/form-data">
<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-content">
		<div class="md-card">
			<div class="md-card-toolbar">
				<div class="md-card-toolbar-actions">
					<i class="md-icon material-icons md-card-toggle">&#xE316;</i>
				</div>
				<h3 class="md-card-toolbar-heading-text">
					Reupload
				</h3>
			</div>
			<div class="md-card-content filter">
            <div class="uk-grid">
				<div class="uk-width-medium-2-5 uk-row-first">
					<label>Upload Sertifikat Fidusia <span class="required">*</span></label>
						<input accept="application/pdf" name="UPD4" type="file" id="UPD4" class="uploadFile" required="true" />
				</div>
			</div>
			<div class="uk-grid">
				<div class="uk-width-medium-3-5">
					<div class="uk-grid">
						<div class="uk-width-medium-1-2 uk-row-first">
							<div class="uk-form-row">
								<div class="md-input-wrapper">
                                    <label>Nomor Kontrak: </label> <input name="no_kontrak" type="text" class="md-input form-filter select-filter" id="text3">
									<span class="md-input-bar uk-form-width-medium"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="uk-grid">
				<div class="uk-width-medium-3-5">
					<div class="uk-grid">
						<div class="uk-width-medium-1-2 uk-row-first">
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Input Nomor Sertifikat Fidusia <span class="required">*</span></label>
                                    <select name="tipe" id="user_tugas" select-style select-style-bottom>
									<option value="">Pilih Tipe</option>
                                    <option value="1">PNBP</option>
                                    <option value="2">Sertifikat</option>
								    </select>
									<span class="md-input-bar uk-form-width-medium"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="uk-text-center uk-margin-large-top">
				<input type="hidden" name="status_now" value="4">
				<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Proses</button>
				<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
			</div>
			</div>
		</div>
	</div>
</div>
</form>
<script>
$('.uploadFile').dropify({
	messages: {
		'default': 'File Uplaod',
		'replace': '',
		'remove':  'Batal',
	}
});

$('#form_approve').on('submit', function(e){
    e.preventDefault();

    var $this 	= $(this),
    url 		= $this.attr('action'),
    data 		= $this.serialize();

    var form = $('form')[0]	;
    var formData = new FormData(form);
    var err_res = 0,
    err_msg = [],
    suc_msg = [];

    $.ajax({
        url: url,
        type: 'post',
        dataType:'json',
        data: formData,
        processData: false,
        contentType: false,
        async: false,
        success : function(res) {
            if (res.sts == 0) {
                App.notif('error', res.msg, 'error');
            } else {
                App.notif('Success', res.msg, 'success');
            }
        },
        error 	: function(res) {
            App.notif('Error', res.msg , 'error');
        }
    });
});


</script>
