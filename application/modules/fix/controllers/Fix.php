<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fix extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
    }

    public function index($sstatus = false)
        {
            $data['name'] 		= "Entry Data";
            $data['url'] 		= base_url()."fix";
            $data['plugin']		= ['datatables', 'datatables_fixcolumns','kendo_ui'];
    

            $data['status'] = $sstatus;
            $this->template->display('fix_upload', $data);
        }

    public function reupload($sstatus = false)
    {
        $data['name'] 		= "Reupload";
        $data['url'] 		= base_url()."fix";
        $data['plugin']		= ['datatables', 'datatables_fixcolumns','kendo_ui'];


        $data['status'] = $sstatus;
        $data['plugin'] 	 = ['kendo_ui','dropify', 'datatables', 'datatables_fixcolumns', 'pdfobject' => 'js', 'inputmask'];
        $this->template->display('reupload', $data);
    }

    public function reDownload($sstatus = false)
    {
        $data['name'] 		= "redownload";
        $data['url'] 		= base_url()."fix/reupload";


        $data['status'] = $sstatus;
        $data['plugin'] 	 = ['kendo_ui','dropify', 'datatables', 'datatables_fixcolumns', 'pdfobject' => 'js', 'inputmask'];
        $data['status']    = $sstatus;

        $this->template->display('redownload', $data);
    }

    public function actionFix() 
    {
        $post = $this->input->post();

        // ================================================ No kontrak ================================================ //
        $this->form_validation->set_rules('no_kontrak', 'Nomor Kontrak', 'trim|required');

        if($this->form_validation->run() == FALSE){
            $output['status'] = FALSE;
            $output['code']   = 0;
            $output['msg']    = validation_errors();
        } else  if($this->form_validation->run() == TRUE){
            $id = $this->m_global->get_data_all('entry_data',null,array('ed_nomor_kontrak'=>$post['no_kontrak']),'ed_id');
            if(isset($id[0]->ed_id)) {
                $actDelete1 = $this->m_global->delete('lampiran_referensi', array('lamp_ed_id'=>$id[0]->ed_id,'lamp_tipe'=>'UPD3'));
                $actDelete2 = $this->m_global->delete('lampiran_referensi', array('lamp_ed_id'=>$id[0]->ed_id,'lamp_tipe'=>'UPD4'));
                $actDelete3 = $this->m_global->delete('lampiran', array('lamp_ed_id'=>$id[0]->ed_id,'lamp_tipe'=>'UPD3'));
                $actDelete4 = $this->m_global->delete('lampiran', array('lamp_ed_id'=>$id[0]->ed_id,'lamp_tipe'=>'UPD4'));

                $output['status'] = TRUE;
                $output['code']   = "1";
                $output['msg']    = "Data Berhasil Dirubah";
            } else {
                $output['status'] = FALSE;
                $output['code']   = "0";
                $output['msg']    = "Nomor Kontrak tidak ditemukan";
            }
        }

        echo json_encode($output);
    }

    public function act_reupload()
    {
        $post = $this->input->post();
        // header('Content-Type:Application/Json');
        $this->form_validation->set_rules('no_kontrak', 'Nomor Kontrak', 'trim|required');
        $this->form_validation->set_rules('tipe', 'Tipe Upload', 'trim|required');
        $this->form_validation->set_rules('no_kontrak', 'Nomor Kontrak', 'trim|required');
        if($this->form_validation->run() == false ) {
            $data['sts']    = 0;
            $data['msg']    = validation_errors();
        }  else {
            $getData = $this->m_global->get_data_all('entry_data', null, array('ed_nomor_kontrak'=>$post['no_kontrak']));
            if($getData) {
                $post['tipe'] === '1' ? $jenis = 'UPD3':$jenis = 'UPD4';
               $upload['ed_id']         = $getData[0]->ed_id;
               $upload['nomor_kontrak'] = $getData[0]->ed_nomor_kontrak;
               $upload['entry']         = 1;
               $doUpload                = $this->do_upload($upload, $jenis);
               if($doUpload['sts'] == '1') {
                 $data['sts']    = '1';
                 $data['msg']    = 'Data berhasil di upload';
               }
            } else {
                $data['sts'] = 0;
                $data['msg'] = 'Data dengan no kontrak '.$post['no_kontrak'].' tidak ditemukan';
            }
        }

        echo json_encode($data);
    }

    public function do_upload($entry, $datatype = false){
		$result = [];
		$post 	= $this->input->post();
		if ($entry['entry']) {
			// upload file untuk wom
			set_include_path('phpseclib');
			include('phpseclib/Net/SFTP.php');
			$sftp = new Net_SFTP('112.215.7.49');
			
			if (!$sftp->login('fidusia', 'P@ssw0rd')) {
				$this->db->trans_rollback();
				exit('Login Failed');
			}
			
			foreach($_FILES as $kfile => $cfile){
				$entry['jenis'] = $datatype;
				if($cfile['name']){
					if(!$cfile['error']){
						$new_file_name = strtolower($cfile['name']); //rename file
						$size = $cfile['size'];
						$rename = explode('.', $new_file_name);
						$replace_name = str_replace($rename[0], $entry['ed_id'].'_'.$entry['jenis'], $new_file_name);
						if ($size > 1187000) {
							$result['msg'] = $replace_name.' melebihi 11.187 kb';
							$result['sts'] = '0';
						}else{
                            /* path local */
                            // $pathcore = './assets/uploads/';

                            /* path server wom */
                            $pathcore = './assets/uploads/file/';
                            
							$kontrak  = $entry['nomor_kontrak'];
	
							if (!file_exists($pathcore)) {
								$oldmask = umask(0);
								mkdir($pathcore, 0777);
								umask($oldmask);
							}
							if (!file_exists($pathcore.'/'.$kontrak)) {
								$oldmask = umask(0);
								mkdir($pathcore.'/'.$kontrak, 0777);
								umask($oldmask);
							}
	
							$file_path = $pathcore.$kontrak.'/'.$replace_name;
							move_uploaded_file($cfile['tmp_name'], FCPATH . $file_path);

                            // local
							// $path = "/opt/lampp/htdocs/fidusia/wom_finance/assets/uploads/".$kontrak;
                            // $file = "/opt/lampp/htdocs/fidusia/wom_finance/assets/uploads/".$kontrak."/".$replace_name;
                            
                            /* wom server path */
							$path = "/opt/lampp/htdocs/fidusia/fidusia/assets/uploads/uploads/".$kontrak;
							$file = "/opt/lampp/htdocs/fidusia/fidusia/assets/uploads/uploads/".$kontrak."/".$replace_name;

							if(!$sftp->chdir($path)){
								$sftp->mkdir($path);
							}
							$sftp->chmod(16895,$path);
							if (!$sftp->put($file,FCPATH.$file_path,NET_SFTP_LOCAL_FILE)) {
								exit('Error');
							} else {
                                $result['msg']  = 'Data berhasil terupload';
                                $result['sts']  = '1';
                            }
	
						}
					}else{
						$result['msg'] = 'Data gagal ditambahakan !';
						$result['sts'] = '0';
					}
				}
			}
		}else{
			$result['msg'] = 'Data gagal ditambahakan !';
			$result['sts'] = '0';
		}
		return $result;
    }
    
    public function act_redownload() 
    {
        $this->form_validation->set_rules('no_kontrak', 'Nomor kontrak', 'trim|required');
        if($this->form_validation->run() === true) {
            /*sftp module*/
            $noKontrak = str_replace(' ', '', $this->input->post('no_kontrak'));
            $getData = $this->m_global->get_data_All('entry_data', null, ['ed_nomor_kontrak' => $noKontrak]);
            if($getData) {
                set_include_path('phpseclib');
                include('phpseclib/Net/SFTP.php');
                $sftp = new Net_SFTP('112.215.7.49');
                
                $path = FCPATH.'/assets/file/ext-wom/uploads/'.$noKontrak;
                if(!is_dir($path)) {
                    mkdir($path);
                }
                if (!$sftp->login('fidusia', 'P@ssw0rd')) {
                    exit('Login Failed');
                }
                $fileDowload = "/opt/lampp/htdocs/fidusia/wom_finance/assets/uploads/uploads/".$noKontrak.'/'.$getData[0]->ed_id.'_UPD1.pdf';
                $fileLocal   = $path.'/'.$getData[0]->ed_id.'_UPD1.pdf'; 
                if($sftp->get($fileDowload, $fileLocal)) {
                    $data['sts'] = '1';
                    $data['msg'] = 'success data berhasil terdownload';
                } else {
                    $data['sts'] = '';
                    $data['msg'] = 'data gagal terdownload';
                }
            } else {
                $data['sts'] = 0;
                $data['msg'] = 'Data tidak ditemukan';
            }
        } else {
            $data['sts'] = 0;
            $data['msg'] = validation_errors();
        }

        echo json_encode($data);
    }
}