<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	require_once APPPATH.'libraries/spout/Autoloader/autoload.php';

	// using spout class must be outside class codeigniter
	use Box\Spout\Reader\ReaderFactory;
	use Box\Spout\Writer\WriterFactory;
	use Box\Spout\Common\Type;
	use Box\Spout\Writer\Style\StyleBuilder;
	use Box\Spout\Writer\Style\Color;
	use Box\Spout\Writer\Style\Border;
	use Box\Spout\Writer\Style\BorderBuilder;


class Laporan extends MX_Controller {

	private $table_db       = 'entry_data'; //nama table
	private $table_prefix   = 'ed_'; //nama awal pada field
	private $prefix 		= 'laporan';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index($sstatus = false, $param = ''){
		
		$data['name'] 	   = "Laporan";
		$data['url'] 	   = base_url().$this->prefix;
		$data['plugin']	   = ['datatables', 'datatables_fixcolumns','kendo_ui'];
		$data['show_data'] = (!in_array(user_data()->user_level, ['0','1'])) ? 'style="display: none;"' : '';
		$data['export']	   = [ ['name' => 'Export Excel', 'link' => base_url('laporan/export_excel')] ];
		$data['region']   = $this->m_global->get_data_all('region',null,null,'region_id as key,region_name as name_of_region');
		
		$data['status']    = $sstatus;
		$data['param']     = $param;
		
		$this->template->display('index', $data);
	}

	public function select($sstatus = false, $param =''){
		$post 	 = $this->input->post();
		$length  = intval($post['length']);
		$start   = intval($post['start']);
		$sEcho	 = intval($post['draw']);
		$join    = null;
		$where   = null;
		$where_e = null;

		$levelCabangId = '10';
        if( user_data()->user_level == $levelCabangId ) {
            $where['data_cabang_id'] = user_data()->cabang_id;
        }
		$search 	= [
			"kontrak" 	 => 'data_no_kontrak',
			"lastupdate" => 'updatedat',
			'flag'		 => 'data_flag'
		];
		// pre($post); exit;
		// pre($post['lastupdate']); exit;
		if (@$post['action'] == 'filter')
		{
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = $post['lastupdate'];
						if($tmp[0] != "" && $tmp[1] != ""){
							$where_e .= "DATE(data_transaksi.createdat) BETWEEN '".date('Y-m-d', strtotime($tmp[0]))."' AND '".date('Y-m-d', strtotime($tmp[1]))."' AND ";
						}
					} else if( $key =='flag' ) {
						$where['data_flag'] = $post['flag'];
					} else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		}
		  
		$keys 		= array_keys($search);
		$order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

		$select		= '*, '.implode(',', $search);
		$where_e    = ($param != '' ? $where_e." AND 1 = 1 " : $where_e." 1 = 1 ");

		$count 		= $this->m_global->count_data_all( 'data_transaksi', $join, $where, $where_e );
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;


		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();

		$join = [
			['pdf_upload', 'data_pdf_id = pdf_id', 'left'],
			['sertifikat_upload', 'data_sertifikat_id = sertifikat_id', 'left'],
			['invoice_upload', 'data_invoice_id = invoice_id', 'left']
		];

		$data = $this->m_global->get_data_all('data_transaksi', $join, $where, $select, $where_e, $order, $start, $length);
		// echo $this->db->last_query(); exit;
		$i = 1 + $start;

		$status = [
			'0' => '<span class="uk-badge uk-badge-warning">InActive</span>',
			'1' => '<span class="uk-badge uk-badge-primary">Active</span>',
			'99' => '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
		];

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
				$rows->data_no_kontrak,
				data_flag_status($rows->data_flag),
				$rows->createdat == null ? '' : date('d-m-Y h:i', strtotime($rows->createdat)),
				$rows->sertifikat_createddate == null ? '' : date('d-m-Y h:i', strtotime($rows->sertifikat_createddate))
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

	public function _button($id, $status, $flag, $show = false)
	{
		$prev = $id;
		$id = strEncrypt($id);
		$button = '';

		if($show) {
			$c_status = '<a data-uk-tooltip title="Change Status" class="md-btn ' . (($status == '1') ? 'md-btn-success' : (($status == '99') ? 'md-btn-danger' : '')) . ' md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(1, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/'.($status == 1 ? '0' : '1' ) ).'">
						<i class="' . (($status == '1') ? 'uk-icon-check' : (($status == '99') ? 'uk-icon-warning' : 'uk-icon-ban')) . ' uk-icon-small"></i>
					  </a>';
			$edit 	= '<a data-uk-tooltip title="Edit" class="ajaxify md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="'.base_url('entry_data/show_edit').'/'.$id.'"><i class="uk-icon-pencil uk-icon-small"></i></a>';
			$delete = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(2, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/99'.($status == 99 ? '/true' : '' )).'"><i class="uk-icon-trash uk-icon-small"></i></a>';
			$preview = '<a data-uk-tooltip title="Preview" class="md-btn md-btn-primary md-btn-mini md-btn-wave-light md-btn-icon waves-effect waves-button waves-light ajaxify" href="'.base_url('entry_data/show_document').'/'.$id.'">
							<i class="uk-icon-map"></i>
						</a>';

			if ($flag > '0') {
				if (in_array(user_data()->user_level, ['1', '5'])) {
					$button = $preview . $c_status . $edit . $delete;
				} else {
					if (user_data()->user_level == '2' && $flag == '9') {
						$button = $preview . $c_status . $edit . $delete;
					} else {
						$button = $preview;
					}
				}
			} else {
				if (in_array(user_data()->user_level, ['1', '2', '5'])) {
					$button = $preview . $c_status . $edit . $delete;
				} else if (in_array(user_data()->user_level, ['3', '4'])) {
					$button = $preview;
				}
			}
		} else {
			$button = '-';
		}

		return $button;
	}

	public function export_excel()
	{
		$where_e = "1 = 1";

		$get = $this->input->get();
		if(@$get['kontrak'] != ""){
			$where_e.= " AND data_no_kontrak LIKE '%$get[kontrak]%'";
		}

		if(@$get['flag'] != ""){
			$where_e.= " AND data_flag IN($get[flag])";
		}

		if(@$get['lastupdate'][0] != "" && @$get['lastupdate'][1] != ""){
			$where_e.= " AND (DATE(updatedat) BETWEEN '".date('Y-m-d', strtotime($get['lastupdate'][0]))."' AND '".date('Y-m-d', strtotime($get['lastupdate'][1]))."')";
		}

		$join = [
			['sertifikat_upload', 'data_sertifikat_id = sertifikat_id', 'left'],
			['invoice_upload', 'data_invoice_id = invoice_id', 'left'],
			['pdf_upload', 'data_pdf_id = pdf_id', 'left']
		];
		$data = $this->m_global->get_data_all('data_transaksi', $join, null, null, $where_e);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
		$styleArray = array(
            'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
		$styleArray1 = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
		);
		
		//background
		$styleArray12 = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'rgb' => 'FFEC8B',
				),
			),
		);
		
		// header
		$objPHPExcel->getActiveSheet()->getStyle('B:B')->getNumberFormat()->setFormatCode('0');
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
    	$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan');
		// end header

		$row = 3;
		$rowawal = $row;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->applyFromArray($styleArray);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->applyFromArray($styleArray1);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->applyFromArray($styleArray12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		

		// header column
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'No');
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, 'Nomor Kontrak');
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, 'Status');
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, 'Tanggal Upload');
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, 'Tanggal Terdaftar');

		$row++;
		$objPHPExcel->getActiveSheet()->freezePane('A'.$row);
		header( "Content-type: application/vnd.ms-excel" );
		header('Content-Disposition: attachment; filename="laporan.xls"');
		header("Pragma: no-cache");
		header("Expires: 0");
		ob_end_clean();

		foreach ($data as $key => $dt) {
			
			//  here u write the xls file
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, ($row - $rowawal));
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $dt->data_no_kontrak);
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $this->flag_status_name($dt->data_flag));
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $dt->createdat == "" ? "" : date('d-m-Y h:i', strtotime($dt->createdat)));
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $dt->sertifikat_createddate == "" ? "" : date('d-m-Y h:i', strtotime($dt->sertifikat_createddate)));
			$row++;
		}

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// Write file to the browser
		$writer->save('php://output');
	}

	public function flag_status_name( $flag = '0' )
	{
		$dataFlag = [
			'0' => '',
			'1' => 'Upload',
			'2' => 'Terkirim',
			'3' => 'Sertifikat',
			'4' => 'Terdaftar'
		];
		return $dataFlag[$flag];
	}

}
/* End of file Entry_data.php */
/* Location: ./application/modules/entry_data/controllers/Entry_data.php */
