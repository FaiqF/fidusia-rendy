<style>
.dropify-wrapper{
    width: initial;
}
.center{
    text-align: center;
}
</style>
<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-content">
		<div id="error"></div>
		<form id="upload" method="post" action="<?=$url; ?>/upload_excel" enctype="multipart/form-data">
			<div class="md-card">
				<div class="md-card-toolbar">
					<div class="md-card-toolbar-actions">
						<i class="md-icon material-icons md-card-toggle">&#xE316;</i>
					</div>
					<h3 class="md-card-toolbar-heading-text">
						Upload File
					</h3>
				</div>
				<div class="md-card-content">
					<div class="uk-grid">
						<div class="uk-width-medium-4-4 uk-row-first">
							<div class="uk-grid">
								<div class="uk-width-medium-1-1 uk-row-first">
									<div class="uk-form-row text-center">
                                        <input data-allowed-file-extensions="xls xlsx" name="file_upload" type="file" id="file_upload" class="file_upload"/>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid">
						<div class="uk-width-medium-4-4 uk-row-first">
							<div class="uk-grid">
								<div class="uk-width-medium-1-1 uk-row-first">
									<div class="uk-form-row text-center">
										<div class="uk-width-1-1">
											<button name="btn-upload" value="btn-upload" type="button" id="btn-upload" class="filter-submit md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Upload</button>
											<input type="hidden" id="edit">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!-- datatable true data -->
			<table id="datatable_ajax" class="uk-table" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th width="40">No.</th>
						<th>Nomor kontrak</th>
						<th>Status</th>
						<th>Nomor akta</th>
						<th>Tanggal akta</th>
						<th>Nomor sertifikat</th>
						<th>Tanggal sertifikat</th>
						<th>Nomor mesin</th>
						<th>Nomor rangka</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		<!-- end datatable true data -->

	</div>
</div>

<script>
	// file upload dropify
		$('.file_upload').dropify({
			messages: {
				'default': 'Upload File Excel',
				'replace': '',
				'remove' :  'Batal'
			}
		});
	// end file upload dropify
	
	// action upload (data cek)
		$("#btn-upload").click(function() {
			altair_helpers.content_preloader_show('md');
			setTimeout(function () {
				$('#upload').submit();
			}, 2000);
			
		});

	// end action upload

	// handle form submit
		$('#upload').on('submit', function(e) {
			e.preventDefault();
			var table = $('#datatable_ajax').DataTable();
			var file = $('#file_upload')[0];
			$.each(file.files, function(index, files) {
				var formData   = new FormData;
				var ajaxUrl    = "<?php echo base_url('laporan/action_import_data')?>";
				var ajaxMethod = "post"
				formData.append('file_xls', files);
				$.ajax({
					method		: ajaxMethod,
					dataType	: 'json',
					url 		: ajaxUrl,
					data 		: formData,
					processData : false,
					contentType : false,
					async		: false,
					success		: function( response )
					{
						if( response.status == true ) {
							var dataTableNumber = 1;
							var data = response.data;
							$.each(data, function(index, value) {
								table.row.add([
									dataTableNumber,
									value.nomorKontrak,
									value.statusBadge,
									value.nomorAkta,
									value.tanggalAkta,
									value.nomorSertifikat,
									value.tanggalSertifikat,
									value.nomorMesin,
									value.nomorRangka
								]).draw( false );
								dataTableNumber += 1;
							});
						}

					}

				})
			});
			altair_helpers.content_preloader_hide('md');
		});
	// end handle
</script>