<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error extends MX_Controller {

	private $prefix			= 'error';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function error_404()
	{
		$data['name']  		= 'Error';
		$data['subtitle']   = 'error 404';

		$data['prefix']     = $this->prefix; // uri/controller

		$data['breadcrumb'] = [ 'Error' => $this->prefix.'/error_404' ];

		$js['js']           = null;
		$css['css']         = [ 'error' ];

		// $data['url']        = $this->input->post('url');
		// $data['url1']        = $this->input->post('url1');

		$this->template->display( $this->prefix.'_404', $data);
	}

}

/* End of file Error.php */
/* Location: ./application/modules/error/controllers/Error.php */