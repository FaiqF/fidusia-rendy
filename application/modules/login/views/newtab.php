<?php
	$x = $_SERVER['SCRIPT_NAME'];
	$y = str_replace('index.php', '', $x);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- Remove Tap Highlight on Windows Phone IE -->
		<meta name="msapplication-tap-highlight" content="no"/>

		<link rel="icon" type="image/png" href="<?=$y; ?>/assets/img/favicon-16x16.png" sizes="16x16">
		<link rel="icon" type="image/png" href="<?=$y; ?>/assets/img/favicon-32x32.png" sizes="32x32">

		<title>New Tab</title>

		<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>

		<!-- uikit -->
		<link rel="stylesheet" href="<?=$y; ?>/bower_components/uikit/css/uikit.almost-flat.min.css"/>

		<!-- altair admin error page -->
		<link rel="stylesheet" href="<?=$y; ?>/assets/css/error_page.min.css" />

		<script>
			var base_url = "<?=base_url(); ?>";
		</script>
	</head>
	<body class="error_page">

		<div class="error_page_header">
			<div class="uk-width-8-10 uk-container-center">
				Error !
			</div>
		</div>
		<div class="error_page_content">
			<div class="uk-width-8-10 uk-container-center">
				<p class="heading_b">Aplikasi telah dibuka di-jendela lain !</p>
				<!-- <p class="uk-text-large">
					The requested URL <span class="uk-text-muted">/some_url</span> was not found on this server.
				</p>
				<a href="#" onclick="history.go(-1);return false;">Go back to previous page</a> -->
			</div>
		</div>
		
		<!-- common functions -->
		<script src="<?=base_url(); ?>assets/js/common.min.js"></script>
		<!-- uikit functions -->
		<script src="<?=base_url(); ?>assets/js/uikit_custom.min.js"></script>
		<!-- altair core functions -->
		<script src="<?=base_url(); ?>assets/js/altair_admin_common.min.js"></script>

		<!-- altair login page functions -->
		<script src="<?=base_url(); ?>assets/js/pages/login.js"></script>
		<script src="<?=base_url(); ?>assets/js/core/app.js"></script>
		<script src="<?=base_url(); ?>bower_components/toastr/js/toastr.min.js"></script>
		<script>
			$(document).ready(function(){
				if(typeof(Storage) !== "undefined") {
					if (typeof(localStorage.tab) !== "undefined") {
						if (localStorage.getItem('tab') == 1 && sessionStorage.getItem('tab') == 1) {
							window.location.href = base_url+'login';
						} else {
							return true;
						}
					} else {
						var Tab = 1;

						localStorage.setItem('tab', Tab);
						sessionStorage.setItem('tab', Tab);
					}
				}
			});
		</script>
	</body>
</html>