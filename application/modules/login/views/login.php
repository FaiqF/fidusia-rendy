﻿<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Remove Tap Highlight on Windows Phone IE -->
	<meta name="msapplication-tap-highlight" content="no"/>

	<link rel="icon" type="image/png" href="<?=base_url(); ?>assets/img/BRIF/favicon.jpg" sizes="16x16">
	<link rel="icon" type="image/png" href="<?=base_url(); ?>assets/img/BRIF/favicon.jpg" sizes="32x32">

	<title>Simulasi Kredit Motor</title>

	<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>

	<!-- uikit -->
	<link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css"/>

	<!-- altair admin login page -->
	<link rel="stylesheet" href="assets/css/login_page.min.css" />
	<link rel="stylesheet" href="<?=base_url(); ?>bower_components/toastr/css/toastr.min.css" media="all">

	<script>
		var base_url = "<?=base_url(); ?>";
		window.onbeforeunload = function (e) {
			e = e || window.event;

			// For IE and Firefox prior to version 4
			if (e) {
				e.returnValue = 'Sure?';
			}

			// For Safari
			return 'Sure?';
		};
	</script>

</head>
<body class="login_page">

	<div class="login_page_wrapper">
		<div id="alert_error"></div>
		<div class="md-card" id="login_card">
			<div class="md-card-content large-padding" id="login_form">
				<div class="login_heading">
					<img style="width: 250px;" src="<?=base_url(); ?>/assets/img/BRIF/login_logo.png" alt="Logo">
					
					<!-- <div class="user_avatar"></div> -->
				</div>
				<form method="post" action="<?=base_url('login/check_login'); ?>" id="form_login">
					<div class="uk-form-row">
						<label for="login_username">Username</label>
						<input class="md-input" type="text" id="login_username" name="login_username" />
					</div>
					<div class="uk-form-row">
						<label for="login_password">Password</label>
						<input class="md-input" type="password" id="login_password" name="login_password" />
					</div>
					<div class="uk-margin-medium-top">
						<button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large">Sign In</button>
					</div>
					<div class="uk-margin-top">
						<a href="#" id="login_help_show" class="uk-float-right">Need help?</a>
						<span class="icheck-inline">
						</span>
					</div>
				</form>
			</div>
			<div class="md-card-content large-padding uk-position-relative" id="login_help" style="display: none">
				<button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
				<h2 class="heading_b uk-text-success">Can't log in?</h2>
				<p>Here’s the info to get you back in to your account as quickly as possible.</p>
				<p>First, try the easiest thing: if you remember your password but it isn’t working, make sure that Caps Lock is turned off, and that your username is spelled correctly, and then try again.</p>
				<p>If your password still isn’t working, it’s time to <b>contact your administrator</b>.</p>
			</div>
			<div class="md-card-content large-padding" id="register_form" style="display: none">
				<div style="margin-left: -20px; margin-right: -20px; margin-top: -10px;" class="uk-alert uk-alert-warning" data-uk-alert><a href="#" class="uk-alert-close uk-close"></a>
					<?php echo $notif; ?>
				</div>
                <h2 class="heading_a uk-margin-medium-bottom">Change Password</h2>
                <form method="post" action="<?=base_url('login/change_password'); ?>" id="form_change">
                	<input type="hidden" name="id_data" id="id_data">
                    <div class="uk-form-row">
                        <label for="change_password">Password</label>
                        <input class="md-input" type="password" id="change_password" name="change_password" />
                    </div>
                    <div class="uk-form-row">
                        <label for="change_password_repeat">Repeat Password</label>
                        <input class="md-input" type="password" id="change_password_repeat" name="change_password_repeat" />
                    </div>
                    <div class="uk-margin-medium-top">
						<button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large">Save & Login</button>
                    </div>
                </form>
            </div>
		</div>
		<div class="uk-margin-top uk-text-center">
            <a href="#" id="signup_form_show"></a>
        </div>
	</div>

	<!-- common functions -->
	<script src="assets/js/common.min.js"></script>
	<!-- uikit functions -->
	<script src="assets/js/uikit_custom.min.js"></script>
	<!-- altair core functions -->
	<script src="assets/js/altair_admin_common.min.js"></script>

	<!-- altair login page functions -->
	<script src="assets/js/pages/login.js"></script>
	<script src="assets/js/core/app.js"></script>
	<script src="<?=base_url(); ?>bower_components/toastr/js/toastr.min.js"></script>

	<script>
		$(document).ready(function(){
			$('#form_login').on('submit', function(e){
				e.preventDefault();

				$.ajax({
					url 	: $(this).attr('action'),
					type 	: 'post',
					dataType: 'json',
					data 	: $(this).serialize(),
					success : function(data) {
						if (data.sts == 99) {
							var error = $('#alert_error');

							App.scrollTop();
							error.html('<div class="uk-alert uk-alert-danger" data-uk-alert><a href="#" class="uk-alert-close uk-close"></a> '+data.msg+' </div>');
						} else if (data.sts == 88) {
							$('#id_data').val(data.id);
							$('#signup_form_show').trigger('click');
						} else {
							if(data.sts == 1) {
								App.notif('Success', data.msg, 'success');

								window.location.replace(base_url+'/dashboard');
							} else {
								App.notif('Error', data.msg, 'error');
							}
						}
					},
					error 	: function(data) {
						App.notif('Error', 'Internal Server Error !', 'error');
					}
				});
			});

			$('#form_change').on('submit', function(e){
				e.preventDefault();

				$.ajax({
					url 	: $(this).attr('action'),
					type 	: 'post',
					dataType: 'json',
					data 	: $(this).serialize(),
					success : function(data) {
						if (data.sts == 99) {
							var error = $('#alert_error');

							App.scrollTop();
							error.html('<div class="uk-alert uk-alert-danger" data-uk-alert><a href="#" class="uk-alert-close uk-close"></a> '+data.msg+' </div>');
						} else {
							if(data.sts == 1) {
								App.notif('Success', data.msg, 'success');

								window.location.replace(base_url+'/dashboard');
							} else {
								App.notif('Error', data.msg, 'error');
							}
						}
					},
					error 	: function(data) {
						App.notif('Error', 'Internal Server Error !', 'error');
					}
				});
			});

			checkTab();
		});

		window.onbeforeunload = function(e) {
		var dialogText = 'Dialog text here';
		e.returnValue = dialogText;
		return dialogText;
		};

		window.onbeforeunload = function() {
			if (localStorage.getItem('tab') == 1 && sessionStorage.getItem('tab') == 1) {
				localStorage.removeItem('tab');
				sessionStorage.removeItem('tab');
				return null;
			}
		};

		// check for theme
		if (typeof(Storage) !== "undefined") {
			var root = document.getElementsByTagName( 'html' )[0],
				theme = localStorage.getItem("altair_theme");
			if(theme == 'app_theme_dark' || root.classList.contains('app_theme_dark')) {
				root.className += ' app_theme_dark';
			}
		}

		function checkTab() {
			if(typeof(Storage) !== "undefined") {
				if (typeof(localStorage.tab) !== "undefined") {
					if (localStorage.getItem('tab') == 1 && sessionStorage.getItem('tab') == 1) {
						return true;
					} else {
						window.location.href = base_url+'login/newtab';
					}
				} else {
					var Tab = 1;

					localStorage.setItem('tab', Tab);
					sessionStorage.setItem('tab', Tab);
				}
			}
		}
	</script>

</body>
</html>