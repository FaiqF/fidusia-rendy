<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-toolbar">
		<h3 class="md-card-toolbar-heading-text">
			Kombinasi Password
		</h3>
	</div>
	<div class="md-card-content">
		<table id="datatable_ajax2" class="uk-table" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th width="40">No.</th>
					<th>Nama Parameter</th>
					<th>Keterangan</th>
					<th>Nilai</th>
					<th>Rule</th>
					<th>Status</th>
					<th width="152">Aksi</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</div>

<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-toolbar">
		<h3 class="md-card-toolbar-heading-text">
			Parameter Password
		</h3>
	</div>
	<div class="md-card-content">
		<table id="datatable_ajax" class="uk-table" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th width="40">No.</th>
					<th>Nama Parameter</th>
					<th>Keterangan</th>
					<th>Nilai</th>
					<th>Rule</th>
					<th>Status</th>
					<th width="152">Aksi</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</div>

<script>
	$(document).ready(function(){
		var url 	= "<?=$url; ?>/select",
			header  = [
						{ "className": "text-center" },
						null,
						null,
						null,
						null,
						{ "className": "text-center" },
						{ "className": "text-center" },
					  ],
			order 	= [
						['1', 'asc']
					  ],
			sort 	= [-1, 0];

		App.setDatatable(url, header, order, sort);

		var url2 	= "<?=$url; ?>/select2",
			header2 = [
						{ "className": "text-center" },
						null,
						null,
						null,
						null,
						{ "className": "text-center" },
						{ "className": "text-center" },
					  ],
			order2 	= [
						['1', 'asc']
					  ],
			sort2 	= [-1, 0];

		App.setDatatable(url2, header2, order2, sort2, '#datatable_ajax2');
	});
</script>
