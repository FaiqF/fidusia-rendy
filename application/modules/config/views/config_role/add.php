<div class="uk-grid">
	<div class="uk-width-medium-1-1">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Add Role</h3>
			</div>
			<div class="md-card-content">
				<div id="alert_error"></div>
				<form method="post" action="<?=$url; ?>/action_add" id="form_add">
					<div class="uk-grid">
						<div class="uk-width-medium-2-5 uk-row-first">
							<div class="uk-grid">
								<div class="uk-width-medium-1-1 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper">
											<label>Name <span class="required">*</span></label>
											<input name="name" type="text" class="md-input uk-form-width-medium">
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
									<div class="uk-form-row">
										<div class="md-input-wrapper">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Status <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1-3 uk-row-first">
													<span>
														<input checked value="1" type="radio" name="status" id="role1; ?>" input-style-checkbox />
														<label for="role1; ?>" class="inline-label">Active</label>
													</span>
													<span class="md-input-bar"></span>
												</div>
												<div class="uk-width-medium-1-3">
													<span>
														<input value="0" type="radio" name="status" id="role0; ?>" input-style-checkbox />
														<label for="role0; ?>" class="inline-label">InActive</label>
													</span>
													<span class="md-input-bar"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="uk-width-medium-3-5">
							<div class="uk-grid">
								<div class="uk-width-medium-1-1 uk-row-first">
									<h3 class="md-card-toolbar-heading-text">Menu <span class="required">*</span></h3>
								</div>
								<div class="uk-width-medium-1-1">
									<button type="button" onclick="treeSelected(true);" class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light"><i class="uk-icon-check-square-o uk-icon-small"></i> Select All</button>
									<button type="button" onclick="treeSelected(false);" class="md-btn md-btn-success md-btn-mini md-btn-wave-light waves-effect waves-button waves-light"><i class="uk-icon-square uk-icon-small"></i> Deselect All</button>
								</div>
								<div class="uk-width-medium-1-1 uk-margin-small-top">
									<div id="roles">
										<?php //print_r(menu('all')); ?>
										<?php v_tree_view(menu('all')); ?>
									</div>
									<input value="1" type="hidden" name="access" id="inputTree">
								</div>
							</div>
						</div>
					</div>
					<div class="uk-text-center uk-margin-large-top">
						<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Save</button>
						<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<a href="<?=$url; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		App.form_submit($('#form_add'));

		$("#roles").fancytree({
			checkbox: true,
			selectMode: 3,
			// imagePath: "assets/icons/others/",
			extensions: ["dnd"],
			autoScroll: true,
			select: function(event, data) {
				var selKeys2 = $.map(data.tree.getSelectedNodes(), function(node){
					var id_c = node.data.id,
						id_p = node.parent;

					if(id_p.parent !== null) {
						var id_px = id_p.data.id

						return [id_c, id_px];
					} else {
						return [id_c];
					}
				});

				var unique = $.unique(selKeys2);
			
				$("#inputTree").val(unique.join(", "));
			},
		});
	});

	function treeSelected(status) {
		$("#roles").fancytree("getTree").visit(function(node){
			node.setSelected(status);
		});
		
		return false;
	}
</script>