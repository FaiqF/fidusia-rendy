<div class="uk-grid">
	<div class="uk-width-medium-1-1">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Add Menu</h3>
			</div>
			<div class="md-card-content">
				<div id="alert_error"></div>
				<form method="post" action="<?=$url; ?>/action_add" id="form_add">
					<div class="uk-grid">
						<div class="uk-width-medium-1-5 uk-row-first"></div>
						<div class="uk-width-medium-4-5 uk-row-first">
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Name <span class="required">*</span></label>
									<input name="name" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Link <span class="required">*</span></label>
									<input name="link" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<input type="checkbox" data-switchery-size="small" data-switchery data-switchery-color="#1e88e5" id="parent_switch" name="parent_switch">
								<label for="parent_switch" class="inline-label">children</label>
							</div>
							<div style="display: none;" class="uk-form-row select_parent">
								<div class="uk-grid">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
										<label>Parent <span class="required">*</span></label>
									</div>
									<div class="uk-width-medium-1 uk-margin-small-bottom">
										<select onchange="getNumber();" name="parent" class="form-filter select-filter uk-form-width-large" select-style select-style-bottom>
											<option value="">Select Parent</option>
											<?php foreach ($parent as $key => $val): ?>
												<option value="<?=$val->menu_id; ?>"><?=$val->menu_name; ?></option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
							</div>
							<div class="uk-form-row select_parent_i">
								<div class="md-input-wrapper">
									<label>Icon <span class="required">*</span></label>
									<input name="icon" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
									<span class="uk-form-help-block">Material Icon</span>
								</div>
							</div>
							<div style="display: none;" class="uk-form-row">
								<div class="md-input-wrapper md-input-filled">
									<label>Number <span class="required">*</span></label>
									<input name="number" type="number" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
									<span class="uk-form-help-block">Sort Number</span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<div class="uk-grid">
										<div class="uk-width-medium-1-3 uk-row-first">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Status <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1-2">
													<span>
														<input checked value="1" type="radio" name="status" id="role1; ?>" input-style-checkbox />
														<label for="role1; ?>" class="inline-label">Active</label>
													</span>
													<span class="md-input-bar"></span>
												</div>
												<div class="uk-width-medium-1-2">
													<span>
														<input value="0" type="radio" name="status" id="role0; ?>" input-style-checkbox />
														<label for="role0; ?>" class="inline-label">InActive</label>
													</span>
													<span class="md-input-bar"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-text-center uk-margin-large-top">
						<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Save</button>
						<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<a href="<?=$url; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		$('#parent_switch').on('change', function(e){
			e.preventDefault()

			var status  = this.checked,
				target  = $('.select_parent'),
				target1 = $('.select_parent_i');

			App.select_val('');
			$('input[name="icon"]').val('');

			if(status) { target.show(); target1.hide(); } else { target.hide(); target1.show(); }

			getNumber();
		});

		App.form_submit($('#form_add'));
	
		getNumber();
	
	});

	function getNumber() {
		var target 	= $('input[name="number"]'),
			parent 	= $('select[name="parent"]').val(),
			p_val 	= (parent != '') ? parent : '0',
			data 	= { q : p_val };

		$.ajax({
			url 	: '<?=$url; ?>/getNumber/',
			type 	: 'post',
			dataType: 'json',
			data 	: data,
			success : function(data) {
				target.val(data);
			}
		});
	}
</script>