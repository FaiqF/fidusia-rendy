<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-content">
		<div class="md-card">
			<div class="md-card-toolbar">
				<div class="md-card-toolbar-actions">
					<i class="md-icon material-icons md-card-toggle">&#xE316;</i>
				</div>
				<h3 class="md-card-toolbar-heading-text">
					Filter
				</h3>
			</div>
			<div class="md-card-content">
				<div class="uk-grid">
					<div class="uk-width-medium-1-3 uk-row-first">
						<div class="md-input-wrapper">
							<label>Name</label>
							<input name="name" type="text" class="md-input form-filter">
							<span class="md-input-bar"></span>
						</div>
					</div>
					<div class="uk-width-medium-1-3">
						<div class="md-input-wrapper">
							<label>Link</label>
							<input name="link" type="text" class="md-input form-filter">
							<span class="md-input-bar"></span>
						</div>
					</div>
					<div class="uk-width-medium-1-3">
						<select name="status" class="form-filter select-filter" select-style select-style-bottom>
							<option value="">Status</option>
							<option value="1">Active</option>
							<option value="0">InActive</option>
							<option value="99">Soft Delete</option>
						</select>
					</div>
				</div>
				<div class="uk-grid uk-margin-medium-top uk-text-center">
					<div class="uk-width-1-1">
						<button type="button" class="filter-submit md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Submit</button>
						<button type="button" class="filter-reset md-btn md-btn-small md-btn-danger md-btn-wave-light waves-effect waves-button waves-light">Reset</button>
					</div>
				</div>
			</div>
		</div>
		<table id="datatable_ajax" class="uk-table" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th width="40">No.</th>
					<th>Name</th>
					<th>Link</th>
					<th width="70">Status</th>
					<th width="152">Actions</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
</div>

<div class="md-fab-wrapper">
	<a class="md-fab md-fab-accent ajaxify" href="<?=$url; ?>/show_add">
		<i class="material-icons">&#xE145;</i>
	</a>
</div>
<script>
	$(document).ready(function(){
		var url 	= "<?=$url; ?>/select",
			header  = [
						{ "className": "text-center" },
						null,
						null,
						{ "className": "text-center" },
						{ "className": "text-center" },
					  ],
			order 	= [
						['1', 'asc']
					  ],
			sort 	= [-1, 0];

		App.setDatatable(url, header, order, sort);
	});
</script>