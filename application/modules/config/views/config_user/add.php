<div class="uk-grid">
	<div class="uk-width-medium-1-1">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Tambah User</h3>
			</div>
			<div class="md-card-content">
				<div id="alert_error"></div>
				<form method="post" action="<?=$url; ?>/action_add" id="form_add">
					<div class="uk-grid">
						<div class="uk-width-medium-2-5 uk-row-first">
							<div class="uk-grid">
								<div class="uk-width-medium-1-1 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper">
											<label>NIK <span class="required">*</span></label>
											<input id="nik" name="nik" type="text" class="md-input uk-form-width-medium">
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
									<div class="uk-form-row">
										<div class="md-input-wrapper">
											<label>Nama <span class="required">*</span></label>
											<input id="name" name="name" type="text" class="md-input uk-form-width-medium">
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
									<div class="uk-form-row">
										<div class="md-input-wrapper">
											<label>Email </label>
											<input name="email" type="email" class="md-input uk-form-width-medium">
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
									<div class="uk-form-row">
										<div class="md-input-wrapper">
											<label>Password <span class="required">*</span></label>
											<input name="password" type="password" class="no-space md-input uk-form-width-medium">
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
									<div class="uk-form-row">
										<div class="md-input-wrapper">
											<label>Ulangi Password <span class="required">*</span></label>
											<input name="repeat_password" type="password" class="no-space md-input uk-form-width-medium">
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>

								<!-- begin Option form / Level -->
									<div class="uk-form-row">
										<div class="md-input-wrapper">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Level <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-2-3 uk-row-first">
													<select id="level" name="level" select-style select-style-bottom>
														<option data-group="" value="">Level</option>
														<?php foreach ($level as $key => $val): ?>
															<option value="<?=$val->level_id; ?>"><?=$val->level_name; ?></option>
														<?php endforeach ?>
													</select>
													<input id="target" type="hidden" name="group">
												</div>
											</div>
										</div>
									</div>
								<!-- end Option form / Level -->

								<!-- Begin Option form / Cabang -->
									<div id="cabang" style="display: none;" class="uk-form-row">
											<div class="md-input-wrapper">
												<div class="uk-grid">
													<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
														<label>Cabang <span class="required">*</span></label>
													</div>
													<div class="uk-width-medium-2-3 uk-row-first">
														<select name="cabang" select-style select-style-bottom>
															<option value="">Cabang</option>
															<?php foreach ($cabang as $key => $val): ?>
																<option value="<?=$val->cabang_id; ?>"><?=$val->cabang_name; ?></option>
															<?php endforeach ?>
														</select>
													</div>
												</div>
											</div>
										</div>
								<!-- End Option form / Cabang -->
									
								</div>
							</div>
						</div>
						<div class="uk-width-medium-3-5">
							<div class="uk-grid">
								<div class="uk-width-medium-1-1 uk-row-first">
									
									<div class="uk-form-row">
										<div class="md-input-wrapper">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Status Pegawai <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1-3 uk-row-first">
													<span>
														<input checked value="1" class="cb1" type="radio" name="status_pegawai" id="sp1" input-style-checkbox />
														<label for="sp1" class="inline-label">Kontrak</label>
													</span>
													<span class="md-input-bar"></span>
												</div>
												<div class="uk-width-medium-1-3">
													<span>
														<input value="0" class="cb1" type="radio" name="status_pegawai" id="sp0" input-style-checkbox />
														<label for="sp0" class="inline-label">Tetap</label>
													</span>
													<span class="md-input-bar"></span>
												</div>
											</div>
										</div>
									</div>
									<div class="uk-form-row cb1isi">
										<div class="md-input-wrapper">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Masa Aktif <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="expired" type="text" class="datepicker">
												</div>
											</div>
										</div>
									</div>
									<div class="uk-form-row">
										<div class="md-input-wrapper">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Role <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<select id="user_role" name="user_role" class="uk-form-width-large" select-style select-style-bottom>
														<option value="">Select Role</option>
														<?php foreach ($roles as $key => $val): ?>
															<option value="<?=$val->role_id; ?>"><?=$val->role_name; ?></option>
														<?php endforeach ?>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div id="roles"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-text-center uk-margin-large-top">
						<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Simpan</button>
						<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Kembali</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<a href="<?=$url; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		$('.cb1').on('ifChecked', function(event){
			if($("#sp1").prop('checked') == true){
				$('.cb1isi').show();
			}else if($("#sp0").prop('checked') == true){
				$('.cb1isi').hide();
			} 
		}); 
		App.form_submit($('#form_add'));

		App.datepicker();
		App.masked_input();

		$('#roles').fancytree();
		
		$('.no-space').on('keydown', function(e){
			if(e.which !== 32){
				return e.which;
			} else {
				return false;
			}
		});

		$('#nik').on('keyup', function(e){
			var regex = /^[a-zA-Z0-9]+$/,
				val   = $(this).val();

			if (regex.test(val)) {
				return;
			} else {
				e.preventDefault();
			}
		});

		$(".number_only").keydown(function (e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
					 // let it happen, don't do anything
					 return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});

		$('#user_role').on('change', function(e){
			e.preventDefault();
			$('#roles').html('');
			$('#roles').fancytree("destroy");

			$.ajax({
				url: base_url+'config/config_user/get_treeview',
				data: {id: $(this).val()},
				type: 'post',
				dataType: 'html',
				success: function(data) {
					$('#roles').html(data);
				}
			}).done(function(){
				$("#roles").fancytree({
					checkbox: false,
					selectMode: 3,
					extensions: ["dnd"],
					autoScroll: true,
				});
			});
		});
	
	// method handle on level change
		$('#level').on('change', function(e){
			e.preventDefault();

			var levelValue = $(this).val();
			var	cabangForm = $('#cabang');

			if( levelValue == 10 ) {
				cabangForm.show();
				return;
			}

			cabangForm.hide();

		});
	// end method

	});
</script>