<div class="uk-grid">
	<div class="uk-width-medium-1-1 some">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Tambah User</h3>
			</div>
			<div class="md-card-content">
				<div id="alert_error"></div>
					<form method="post" action="<?php echo $url; ?>/action_add" id="form_add">
						<div class="uk-grid">
							<div class="uk-width-large-1-2 uk-row-first">
								<div class="uk-form-row">
									<div class="md-input-wrapper">
										<label>NIK <span class="required">*</span></label>
										<input name="nik" type="text" class="md-input uk-form-width-large" required>
									</div>
								</div>
							</div>
							<div class="uk-width-large-1-2">
								<div class="uk-form-row">
									<div class="md-input-wrapper">
										<label>Nama <span class="required">*</span></label>
										<input name="nama" type="text" class="md-input uk-form-width-large" required>
									</div>
								</div>
							</div>

							<div class="uk-width-large-1-2">
								<div class="uk-form-row">
									<div class="md-input-wrapper">
										<label>Tempat Lahir<span class="required">*</span></label>
										<input name="tempat_lahir" type="text" class="md-input uk-form-width-large" required>
									</div>
								</div>
							</div>
							<div class="uk-width-large-1-2">
								<div class="uk-form-row">
									<div class="md-input-wrapper">
										<label>Tanggal Lahir <span class="required">*</span></label>
										<input name="tanggal_lahir" type="text" class="md-input uk-form-width-large" required>
									</div>
								</div>
							</div>
							<div class="uk-width-large-1-2">
								<div class="uk-form-row">
									<div class="md-input-wrapper">
										<div class="uk-grid">
											<div class="uk-width-medium-1-3 uk-margin-small-top uk-row-first">
												<label>Jenis Kelamin <span class="required">*</span></label>
											</div>
											<div class="uk-width-medium-2-4 uk-row-first">
												<select name="jenis_kelamin" select-style select-style-bottom>
													<option value=""></option>
													<option value="laki">Laki-Laki</option>
													<option value="perempuan">Perempuan</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="uk-width-large-1-2">
								<div class="uk-form-row">
									<div class="md-input-wrapper">
										<label>Alamat <span class="required">*</span></label>
										<input name="alamat" type="text" class="md-input uk-form-width-large" required>
									</div>
								</div>
							</div>
							<div class="uk-width-large-1-2">
								<div class="uk-form-row">
									<div class="md-input-wrapper">
										<label>Agama <span class="required">*</span></label>
										<input name="agama" type="text" class="md-input uk-form-width-large" required>
									</div>
								</div>
							</div>
							<div class="uk-width-large-1-2">
								<div class="uk-form-row">
									<div class="md-input-wrapper">
										<div class="uk-grid">
											<div class="uk-width-medium-1-3 uk-margin-small-top uk-row-first">
												<label>Status Pernikahan <span class="required">*</span></label>
											</div>
											<div class="uk-width-medium-2-4 uk-row-first">
												<select name="pernikahan" select-style select-style-bottom>
													<option value=""></option>
													<option value="belum_menikah">Belum Menikah</option>
													<option value="menikah">Menikah</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="uk-width-large-1-2">
								<div class="uk-form-row">
									<div class="md-input-wrapper">
										<label>Pekerjaan <span class="required">*</span></label>
										<input name="pekerjaan" type="text" class="md-input uk-form-width-large" required>
									</div>
								</div>
							</div>
							<div class="uk-width-large-1-2">
								<div class="uk-form-row">
									<div class="md-input-wrapper">
										<label>Kewarganegaraan <span class="required">*</span></label>
										<input name="kewarganegaraan" type="text" class="md-input uk-form-width-large" required>
									</div>
								</div>
							</div>
						</div>
						<div class="uk-text-center uk-margin-large-top">
							<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Simpan</button>
							<a href="<?php echo $url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Kembali</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<a href="<?php echo $url; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		App.form_submit($('#form_add'));
	});
</script>