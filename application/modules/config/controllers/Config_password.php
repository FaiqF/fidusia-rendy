<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_password extends CI_Controller {

	private $table_db       = 'password'; //nama table
	private $table_prefix   = 'pass_'; //nama awal pada field
	private $prefix 		= 'config/config_password';
	private $name 			= 'Konfigurasi Password';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$data['name'] 		= $this->name;
		$data['url'] 		= base_url().$this->prefix;
		$data['plugin']		= ['datatables', 'kendo_ui'];

		$this->template->display('config_password/index', $data);
	}

	public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 				= null;
		$where  			= null;
		$where_e 			= 'pass_type = "1"';

		$search 	= [
			"status" 	=> $this->table_prefix.'status'
		];

		
		if (@$post['action'] == 'filter')
		{
			$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = "DATE(ed_lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

					} else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			$where[$this->table_prefix.'status <>'] = '99';
		}

		$keys 		= array_keys($search);
		$order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

		$select		= '*, '.implode(',', $search);
		$count 		= $this->m_global->count_data_all($this->table_db, $join, $where, $where_e );
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


		$data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, $where_e, $order, $start, $length);
		$i = 1 + $start;

		$status = [
					'0' => '<span class="uk-badge uk-badge-warning">InActive</span>',
					'1' => '<span class="uk-badge uk-badge-primary">Active</span>',
					'99' => '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
				  ];

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
				$rows->pass_name,
				$rows->pass_desc,
				$rows->pass_value,
				$rows->pass_rule,
				$status[$rows->pass_status],
				$this->_button($rows->pass_id, $rows->pass_status, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

	public function _button($id, $status, $show = false)
	{
		$prev = $id;
		$id = strEncrypt($id);
		$button = '';

		if($show) {
			$c_status = '<a data-uk-tooltip title="Change Status" class="md-btn ' . (($status == '1') ? 'md-btn-success' : (($status == '99') ? 'md-btn-danger' : '')) . ' md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(1, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/'.($status == 1 ? '0' : '1' ) ).'">
						<i class="' . (($status == '1') ? 'uk-icon-check' : (($status == '99') ? 'uk-icon-warning' : 'uk-icon-ban')) . ' uk-icon-small"></i>
					  </a>';

			$button = $c_status;
		} else {
			$button = '-';
		}

		return $button;
	}

	public function select2()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 				= null;
		$where  			= null;
		$where_e 			= 'pass_type = "2"';

		$search 	= [
			"status" 	=> $this->table_prefix.'status'
		];

		
		if (@$post['action'] == 'filter')
		{
			$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = "DATE(ed_lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

					} else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			$where[$this->table_prefix.'status <>'] = '99';
		}

		$keys 		= array_keys($search);
		$order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

		$select		= '*, '.implode(',', $search);
		$count 		= $this->m_global->count_data_all($this->table_db, $join, $where, $where_e );
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


		$data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, $where_e, $order, $start, $length);
		$i = 1 + $start;

		$status = [
					'0' => '<span class="uk-badge uk-badge-warning">InActive</span>',
					'1' => '<span class="uk-badge uk-badge-primary">Active</span>',
					'99' => '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
				  ];

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
				$rows->pass_name,
				$rows->pass_desc,
				$rows->pass_value,
				$rows->pass_rule,
				$status[$rows->pass_status],
				$this->_button2($rows->pass_id, $rows->pass_status, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

	public function _button2($id, $status, $show = false)
	{
		$prev = $id;
		$id = strEncrypt($id);
		$button = '';

		if($show) {
			$c_status = '<a data-uk-tooltip title="Change Status" class="md-btn ' . (($status == '1') ? 'md-btn-success' : (($status == '99') ? 'md-btn-danger' : '')) . ' md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(1, this, event, '."'datatable_ajax2'".');" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/'.($status == 1 ? '0' : '1' ) ).'">
						<i class="' . (($status == '1') ? 'uk-icon-check' : (($status == '99') ? 'uk-icon-warning' : 'uk-icon-ban')) . ' uk-icon-small"></i>
					  </a>';

			$button = $c_status;
		} else {
			$button = '-';
		}

		return $button;
	}

	public function change_status_by( $id, $status, $stat = FALSE )
	{
		$this->db->trans_begin();
		if ( $stat ) {
			$arrlamp = $this->m_global->get_data_all('lampiran', null, [strEncrypt('lamp_ed_id', TRUE) => $id]);
			$locations = str_replace(base_url(), '', str_replace('/'.$arrlamp[0]->lamp_nama, '', $arrlamp[0]->lamp_file));
			foreach (glob($locations."/*") as $filename) {
				unlink($filename);
			}
			if(rmdir($locations)){
				$result = $this->m_global->delete( $this->table_db, [strEncrypt($this->table_prefix.'id', true) => $id] );
				$result = $this->m_global->delete( 'lampiran', [strEncrypt('lamp_ed_id', true) => $id] );
			}else{
				echo json_encode( 0 );
				die();
			}
		} else {
			$result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [strEncrypt($this->table_prefix.'id', true) => $id]);
		}

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$data['status'] = 0;
		}else{
			$this->db->trans_commit();
			$data['status'] = 1;
		}
		echo json_encode( $data );
	}

}

/* End of file Config_password.php */
/* Location: ./application/modules/config/controllers/Config_password.php */