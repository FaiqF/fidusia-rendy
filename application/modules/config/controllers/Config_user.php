<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_user extends MX_Controller 
{

	private $table_db       = 'users';
	private $table_prefix   = 'user_';
	private $prefix 		= 'config/config_user';
	private $name 			= 'Konfigurasi User';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$data['name'] 	= $this->name;
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables'];

		$data['role'] 	= $this->m_global->get_data_all('roles', null, ['role_status' => '1', ], 'role_id, role_name', 'role_id NOT IN (1)');

		$this->template->display('config_user/index', $data);
	}

	public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 		= [['roles', 'role_id = user_role']];
		$where 		= null;
		$where_e 	= null;

		if(user_data()->user_role == '32') {
			$where['user_grouping_id'] = '1';
		}

		$search 	= [
			"nik" 		=> $this->table_prefix.'nik',
			"name" 		=> $this->table_prefix.'name',
			"role" 		=> $this->table_prefix.'role',
			"status" 	=> $this->table_prefix.'status',
		];

		if (@$post['action'] == 'filter')
		{
			$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = "DATE(user_lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."' AND ";
					} else if($key == "role") {
						$where[$value.' LIKE '] = ''.$post[$key].'';
					} else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			$where[$this->table_prefix.'status <>'] = '99';
		}

		$keys 		= array_keys($search);
		$order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

		$select		= 'user_id, role_name, '.implode(',', $search);
		$count 		= $this->m_global->count_data_all( $this->table_db, $join, $where, $where_e );
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();

		$data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, $where_e, $order, $start, $length);

		$i = 1 + $start;

		$status = [
			'0' 	=> '<span class="uk-badge uk-badge-warning">InActive</span>',
			'1' 	=> '<span class="uk-badge uk-badge-primary">Active</span>',
			'88' 	=> '<span class="uk-badge uk-badge-danger">Banned</span>',
			'99' 	=> '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
		];

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
				$rows->user_nik,
				$rows->user_name,
				$rows->role_name,
				$status[$rows->user_status],
				$this->_button($rows->user_id, $rows->user_status, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

	public function _button($id, $status, $show = false)
	{
		$id = strEncrypt($id);
		$button = '';

		if($show) {
			$c_status = '<a data-uk-tooltip title="Change Status" class="md-btn ' . (($status == '1') ? 'md-btn-success' : (($status == '99') ? 'md-btn-danger' : '')) . ' md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(1, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/'.($status == 1 ? '0' : '1' ) ).'">
						<i class="' . (($status == '1') ? 'uk-icon-check' : (($status == '99') ? 'uk-icon-warning' : 'uk-icon-ban')) . ' uk-icon-small"></i>
					  </a>';
			$edit 	= '<a data-uk-tooltip title="Edit" class="ajaxify md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="'.base_url('config/config_user/show_edit').'/'.$id.'"><i class="uk-icon-pencil uk-icon-small"></i></a>';
			$delete = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(2, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/99/'.($status == 99 ? '/true' : '' )).'"><i class="uk-icon-trash uk-icon-small"></i></a>';

			$button = $c_status . $edit ;
		} else {
			$button = '-';
		}

		return $button;
	}

	public function show_add()
	{
		$where['role_status'] = '1';
		$where_e 			  =  'role_id NOT IN (1)';
		$where_e_level 		  = null;

		if(user_data()->user_role == '32') {
			$where_e = "role_id in ('37')";
			$where_e_level = "level_id in ('10', '11')";
		}
		
		$data['name'] 		 = $this->name;
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'Konfigurasi User' => base_url('config/config_user'), 'Tambah' => base_url('config/config_user/show_add')];
		$data['roles']		 = $this->m_global->get_data_all('roles', null, $where, 'role_id, role_name', $where_e);
		$data['plugin'] 	 = ['kendo_ui', 'fancytree'];

		$data['level'] 		 = $this->m_global->get_data_all('level', null, null, '*', $where_e_level);
		// echo $this->db->last_query(); exit;
		$data['notaris'] 	 = $this->m_global->get_data_all('notaris', null, ['notaris_status' => '1'], 'notaris_id, notaris_nama');
		$data['cabang'] 	 = $this->m_global->get_data_all('cabang', null, ['cabang_status' => '1'], 'cabang_id, cabang_name');

		$this->template->display('config_user/add', $data);
	}

	public function show_edit($id)
	{
		$data['name'] 		 = $this->name;
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'Konfigurasi User' => base_url('config/config_user'), 'Edit' => base_url('config/config_user/show_edit').'/'.$id];
		$data['users'] 		 = $this->m_global->get_data_all('users', [['roles', 'user_role = role_id']], null, 'user_id, user_nik, role_id, role_name, role_access');
		$data['roles']		 = dataHelper('roles');
		$data['plugin'] 	 = ['kendo_ui', 'fancytree'];

		$data['notaris'] 	 = $this->m_global->get_data_all('notaris', null, ['notaris_status' => '1'], 'notaris_id, notaris_nama');
		$data['level'] 		 = $this->m_global->get_data_all('level');

		$data['record']		 = $this->m_global->get_data_all('users', null, [strEncrypt('user_id', TRUE) => $id]);
		$data['cabang']		 = $this->m_global->get_data_all('cabang');
		$data['id'] 		 = $id;

		$this->template->display('config_user/edit', $data);
	}

	public function action_add()
	{
		$result = [];
		$post 	= $this->input->post();

		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('repeat_password', 'Ulangi Password', 'trim|required|matches[password]');

		$this->form_validation->set_rules('nik', 'NIK', 'trim|required|is_unique[users.user_nik]');
		$this->form_validation->set_rules('name', 'NAMA', 'trim|required');
		$this->form_validation->set_rules('status_pegawai', 'Status Pegawai', 'trim|required');
		$this->form_validation->set_rules('user_role', 'Role', 'trim|required');
		$this->form_validation->set_rules('level', 'Level', 'trim|required');


		if ( $post['status_pegawai'] == '1' ){
			$this->form_validation->set_rules('expired', 'Tanggal Masa Aktif', 'trim|required');
		}

		if( $post['level'] == "10" ){
			$this->form_validation->set_rules('cabang', 'Cabang', 'trim|required');
		}

		if ( $this->form_validation->run() == FALSE ){
			$result['msg'] = validation_errors();
			$result['sts'] = '99';

			echo json_encode($result); exit();
		}

		$user_data = [
			'user_nik' 				=> $post['nik'],
			'user_name' 			=> $post['name'],
			'user_email' 			=> @$post['email'],
			'user_password' 		=> hash('sha512', $post['password']),
			'user_password_backup'	=> json_encode(array()),
			'user_deactivated_date'	=> ((@$post['expired'] == "")?'2999-12-31':date('Y-m-d', strtotime($post['expired']))),
			'user_expired_date'		=> ((@$post['expired'] == "")?date('Y-m-d', strtotime('+'.get_config_password('PASSWORD_EXPIRY')[0]->pass_value.' days')):date('Y-m-d', strtotime($post['expired']))),
			'user_status_pegawai'	=> $post['status_pegawai'],
			'user_role'				=> $post['user_role'],
			'user_level'			=> $post['level'],
			'user_group_id'			=> $post['level'] == '10' ? $post['cabang'] : null,
			'user_created_by'		=> user_data()->user_id,
			'user_created_date'		=> date('Y-m-d H:i:s'),
			'user_lastupdate'		=> date('Y-m-d H:i:s'),
		];

		$user = $this->m_global->insert('users', $user_data);

		if(!$user) {
			$result['sts'] = '0';
			$result['msg'] = 'Data gagal ditambahkan !';

			echo json_encode($result); exit();
		}

		$result['sts'] = '1';
		$result['msg'] = 'Data berhasil ditambahkan !';

		echo json_encode($result); exit();
	}

	public function action_edit($id)
	{
		$result = [];
		$post 	= $this->input->post();
		$levelCabangId = '10';

		$this->form_validation->set_rules('password', 'Password', 'trim|matches[repeat_password]');
		$this->form_validation->set_rules('repeat_password', 'Ulangi Password', 'trim');
		$this->form_validation->set_rules('nik', 'NIK', 'trim|required');
		$this->form_validation->set_rules('name', 'NAMA', 'trim|required');
		$this->form_validation->set_rules('status_pegawai', 'Status Pegawai', 'trim|required');
		$this->form_validation->set_rules('user_role', 'Role', 'trim|required');

		if( $post['level'] == $levelCabangId ){
			$this->form_validation->set_rules('cabang', 'Cabang', 'trim|required');
		}

		if( $post['status_pegawai'] == '1' ){
			$this->form_validation->set_rules('expired', 'Tanggal Masa Aktif', 'trim|required');
		}

		if( $this->form_validation->run() == FALSE ){
			$response['sts'] = '99';
			$response['msg'] = validation_errors();

			echo json_encode( $response ); exit;
		}

		$checkUsernameExist = count($this->m_global->get_data_all('users', null, ['user_status' => '1', 'user_nik' => $post['nik'], strEncrypt('user_id', TRUE).' <> ' => $id]));
		if( $checkUsernameExist > 0 ) {
			$response['sts'] = '0';
			$response['msg'] = 'Username sudah digunakan !';

			echo json_encode( $response ); exit;
		}

		if( $post['password'] != '' ) {
			$updateDataUser['user_password'] = hash('sha512', $post['password']);
		}

		$updateDataUser['user_deactivated_date'] = ((@$post['expired'] == "")?'2999-12-31':(($post['status_pegawai']=="0")?'2999-12-31':date('Y-m-d', strtotime($post['expired']))));
		$updateDataUser['user_status_pegawai'] 	 = $post['status_pegawai'];
		$updateDataUser['user_nik'] 			 = $post['nik'];
		$updateDataUser['user_name'] 			 = $post['name'];
		$updateDataUser['user_email'] 			 = $post['email']; 
		$updateDataUser['user_role'] 			 = $post['user_role'];
		$updateDataUser['user_level'] 			 = $post['level'];
		$updateDataUser['user_group_id'] 		 = $post['level'] == '10' ? $post['cabang'] : null;

		$updateUser = $this->m_global->update('users', $updateDataUser, [strEncrypt('user_id', TRUE) => $id]);
		if( !$updateUser ) {
			$response['sts'] = '0';
			$response['msg'] = 'Data gagal dirubah';

			echo json_encode( $response ); exit;

		}

		$response['sts'] = '1';
		$response['msg'] = 'Data berhasil dirubah';

		echo json_encode( $response ); exit;
	}

	public function change_status_by( $id, $status, $stat = FALSE )
	{
		if ( $stat ) {
			$result = $this->m_global->delete( $this->table_db, [strEncrypt($this->table_prefix.'id', true) => $id] );
		} else {
			if($status == "1"){
				$result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status, $this->table_prefix.'login' => '0', $this->table_prefix.'expired' => '0', $this->table_prefix.'wrong' => '0', $this->table_prefix.'expired_date' => date('Y-m-d', strtotime('+90 days')), $this->table_prefix.'lastupdate' => date('Y-m-d')], [strEncrypt($this->table_prefix.'id', true) => $id]);
			}else{
				$result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status, $this->table_prefix.'login' => '0', $this->table_prefix.'expired' => '1'], [strEncrypt($this->table_prefix.'id', true) => $id]);
			}
		}

		if ( $result ) {
			$data['status'] = 1;
		} else {
			$data['status'] = 0;
		}

		echo json_encode( $data );
	}

	public function _check_pass($str)
	{
		$result 	= [];

		$dataResult = $this->m_global->get_data_all('password', null, ['pass_status' => '1', 'pass_type' => '2'], 'pass_code, pass_number');

		$numbers	= '/[0-9]/';
		$uppercase 	= '/[A-Z]/';
		$lowercase 	= '/[a-z]/';
		$symbol		= '/[!@#$%^&*()\-_=+{};:,<.>]/';

		$r_upper 	= (preg_match_all($uppercase, $str, $out) > 0) ? 1 : 0 ;
		$r_lower 	= (preg_match_all($lowercase, $str, $out) > 0) ? 1 : 0 ;
		$r_numbers	= (preg_match_all($numbers, $str, $out) > 0) ? 1 : 0 ;
		$r_symbol	= (preg_match_all($symbol, $str, $out) > 0) ? 1 : 0 ;

		$numberCode = (($r_upper == 1) ? 7000 : 0) + (($r_lower == 1) ? 700 : 0) + (($r_numbers == 1) ? 70 : 0) + (($r_symbol == 1) ? 7 : 0);

		$arrData 	= [
						'UPPER' 	=> $r_upper,
						'LOWER' 	=> $r_lower,
						'NUMBERS' 	=> $r_numbers,
						'SYMBOLS' 	=> $r_symbol
					  ];

		$msg 		= [
						'DEFAULT' 	=> '<p>Kombinasi password harus terdiri dari :</p>',
						'NUMBERS' 	=> '<p>- Angka (numeric)</p>',
						'UPPER' 	=> '<p>- Huruf besar (uppercase alphabetic)</p>',
						'LOWER' 	=> '<p>- Huruf kecil (lowercase alphabetic)</p>',
						'SYMBOLS' 	=> '<p>- Simbol/karakter khusus (special characters).</p>'
					  ];

		$dataMsg = $msg['DEFAULT'];
		foreach ($dataResult as $key => $val) {
			$dataMsg .= $msg[$val->pass_code];
		}

		$num = 0;
		foreach ($dataResult as $key => $val) {
			$return = $arrData[$val->pass_code];
			$num   += $val->pass_number;

			if($return !== 1) {
				return ['sts' => FALSE, 'msg' => $dataMsg];
				die();
			}
		}

		if ($num == $numberCode) {
			return ['sts' => TRUE];
		} else {
			return ['sts' => FALSE, 'msg' => $dataMsg];
		}
	}

	public function get_treeview()
	{
		$id = $this->input->post('id');

		$data = v_tree_view(menu('', get_access($id)));

		echo $data;
	}

	public function get_group()
	{
		$id = $_POST['id'];

		$data = $this->m_global->get_data_all('level', null, ['level_id' => $id], 'level_group');

		$result = ($data) ? $data[0]->level_group : '';

		echo json_encode($result);
	}

}

/* End of file Config_user.php */
/* Location: ./application/modules/config/controllers/Config_user.php */