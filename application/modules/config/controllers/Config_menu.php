<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_menu extends CI_Controller {

	private $table_db       = 'menus';
	private $table_prefix   = 'menu_';
	private $prefix 		= 'config/config_menu';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$data['name'] 	= "Config Menu";
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables'];

		$this->template->display('config_menu/index', $data);
	}

	public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 		= null;
		$where 		= null;
		$where_e 	= null;

		$search 	= [
						"name" 		=> $this->table_prefix.'name',
						"link" 		=> $this->table_prefix.'link',
						"status" 	=> $this->table_prefix.'status',
					  ];

		if (@$post['action'] == 'filter')
		{
			$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = "DATE(menu_lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

					} else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			$where[$this->table_prefix.'status <>'] = '99';
		}

		$keys 		= array_keys($search);
		$order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

		$select		= 'menu_id, '.implode(',', $search);

		$count 		= $this->m_global->count_data_all( $this->table_db, $join, $where, $where_e );
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


		$data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, $where_e, $order, $start, $length);

		$i = 1 + $start;

		$status = [
					'0' => '<span class="uk-badge uk-badge-warning">InActive</span>',
					'1' => '<span class="uk-badge uk-badge-primary">Active</span>',
					'99' => '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
				  ];

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
				$rows->menu_name,
				$rows->menu_link,
				$status[$rows->menu_status],
				$this->_button($rows->menu_id, $rows->menu_status, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

	public function _button($id, $status, $show = false)
	{
		$id = strEncrypt($id);
		$button = '';

		if($show) {
			$c_status = '<a data-uk-tooltip title="Change Status" class="md-btn ' . (($status == '1') ? 'md-btn-success' : (($status == '99') ? 'md-btn-danger' : '')) . ' md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(1, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/'.($status == 1 ? '0' : '1' ) ).'">
						<i class="' . (($status == '1') ? 'uk-icon-check' : (($status == '99') ? 'uk-icon-warning' : 'uk-icon-ban')) . ' uk-icon-small"></i>
					  </a>';
			$edit 	= '<a data-uk-tooltip title="Edit" class="ajaxify md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="'.base_url('config/config_menu/show_edit').'/'.$id.'"><i class="uk-icon-pencil uk-icon-small"></i></a>';
			$delete = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(2, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/99/'.($status == 99 ? '/true' : '' )).'"><i class="uk-icon-trash uk-icon-small"></i></a>';

			$button = $c_status . $edit . $delete;
		} else {
			$button = '-';
		}

		return $button;
	}

	public function show_add()
	{
		$data['name'] 		 = "Config Menu";
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'Config Menu' => base_url('config/config_menu'), 'Add' => base_url('config/config_menu/show_add')];
		$data['parent']		 = $this->m_global->get_data_all('menus', null, ['menu_status' => '1']);

		$this->template->display('config_menu/add', $data);
	}

	public function change_status_by( $id, $status, $stat = FALSE )
	{
		
		if ( $stat ) {
			$result = $this->m_global->delete( $this->table_db, [strEncrypt($this->table_prefix.'id', true) => $id] );
		} else {
			$result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [strEncrypt($this->table_prefix.'id', true) => $id]);
		}

		if ( $result ) {
			$data['status'] = 1;
		} else {
			$data['status'] = 0;
		}

		echo json_encode( $data );
	}

	public function action_add()
	{
		$result = [];
		$post 	= $this->input->post();

		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('link', 'Link', 'trim|required');
		$this->form_validation->set_rules('number', 'Number', 'trim|required|numeric');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');

		if(@$post['parent_switch']) {
			$this->form_validation->set_rules('parent', 'Parent', 'trim|required');
		} else {
			$this->form_validation->set_rules('icon', 'Icon', 'trim|required');
		}
		
		if ($this->form_validation->run() == TRUE){
			$menu_data 	= [
						'menu_name' 		=> $post['name'],
						'menu_link' 		=> $post['link'],
						'menu_icon'			=> $post['icon'],
						'menu_number'		=> $post['number'],
						'menu_status'		=> $post['status'],
						'menu_parent'		=> (@$post['parent']) ? $post['parent'] : '0' ,
						'menu_created_by'	=> user_data()->user_id,
						'menu_created_date'	=> time()
					  ];

			$role = $this->m_global->insert('menus', $menu_data);

			if($role) {
				$result['msg'] = 'Data berhasil ditambahkan !';
				$result['sts'] = '1';
			} else {
				$result['msg'] = 'Data gagal ditambahkan !';
				$result['sts'] = '0';
			}
		} else {
			$result['msg'] = validation_errors();
			$result['sts'] = '99';
		}

		echo json_encode($result);
	}

	public function show_edit($id)
	{
		$data['name'] 		 = "Config Menu";
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'Config Menu' => base_url('config/config_menu'), 'Edit' => base_url('config/config_menu/show_edit').'/'.$id];
		$data['record']		 = $this->m_global->get_data_all('menus', null, [strEncrypt('menu_id', TRUE) => $id]);
		$data['parent']		 = $this->m_global->get_data_all('menus', null, ['menu_status' => '1', 'menu_id <>' => $data['record'][0]->menu_id]);
		$data['id'] 		 = $id;

		$this->template->display('config_menu/edit', $data);
	}

	public function action_edit($id)
	{
		$result = [];
		$post 	= $this->input->post();

		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('link', 'Link', 'trim|required');
		$this->form_validation->set_rules('number', 'Number', 'trim|required|numeric');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');

		if(@$post['parent_switch']) {
			$this->form_validation->set_rules('parent', 'Parent', 'trim|required');
		} else {
			$this->form_validation->set_rules('icon', 'Icon', 'trim|required');
		}
		
		if ($this->form_validation->run() == TRUE){
			$menu_data 	= [
						'menu_name' 		=> $post['name'],
						'menu_link' 		=> $post['link'],
						'menu_icon'			=> $post['icon'],
						'menu_number'		=> $post['number'],
						'menu_status'		=> $post['status'],
						'menu_parent'		=> (@$post['parent']) ? $post['parent'] : '0' ,
						'menu_created_by'	=> user_data()->user_id,
					  ];

			$role = $this->m_global->update('menus', $menu_data, [strEncrypt('menu_id', TRUE) => $id]);

			if($role) {
				$result['msg'] = 'Data berhasil dirubah !';
				$result['sts'] = '1';
			} else {
				$result['msg'] = 'Data gagal dirubah !';
				$result['sts'] = '0';
			}
		} else {
			$result['msg'] = validation_errors();
			$result['sts'] = '99';
		}

		echo json_encode($result);
	}

	public function getNumber()
	{
		$q = $_POST['q'];

		$result = [];

		$parent = ($q) ? $q : '0' ;

		$where  = ['menu_status' => '1', 'menu_parent' => $parent];
		$number = $this->m_global->count_data_all('menus', null, $where);
	
		$result = ($number) ? ($number + 1) : '1' ;

		echo json_encode($result);
	}

}

/* End of file Config_menu.php */
/* Location: ./application/modules/config/controllers/Config_menu.php */