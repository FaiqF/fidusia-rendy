<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_konsumen extends MX_Controller 
{

	private $table_db       = 'master_konsumen';
	private $table_prefix   = 'konsumen_';
	private $prefix 		= 'config/config_konsumen';
	private $name 			= 'Konfigurasi Konsumen';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$data['name'] 	= $this->name;
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables'];

		$this->template->display('config_konsumen/index', $data);
	}

	public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);
		$join       = [];
		$where 		= null;
		$where_e 	= null;

		$search 	= [
			"nik" 		=> $this->table_prefix.'nik',
			"name" 		=> $this->table_prefix.'nama'
		];

		if (@$post['action'] == 'filter')
		{
			$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = "DATE(user_lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."' AND ";
					} else if($key == "role") {
						$where[$value.' LIKE '] = ''.$post[$key].'';
					} else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
		}

		$count 		= $this->m_global->count_data_all( $this->table_db, $join, $where, $where_e );
		$keys 		= array_keys($search);
		$order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

		$select		= 'konsumen_id, '.implode(',', $search);
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();

		$data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, $where_e, $order, $start, $length);

		$i = 1 + $start;

		$status = [
			'0' 	=> '<span class="uk-badge uk-badge-warning">InActive</span>',
			'1' 	=> '<span class="uk-badge uk-badge-primary">Active</span>',
			'88' 	=> '<span class="uk-badge uk-badge-danger">Banned</span>',
			'99' 	=> '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
		];

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
				$rows->konsumen_nik,
				$rows->konsumen_nama,
				$this->_button($rows->konsumen_id, null, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

	public function _button($id, $status, $show = false)
	{
		$id = strEncrypt($id);
		$button = '';

		if($show) {
			$delete = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(2, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/99/'.($status == 99 ? '/true' : '' )).'"><i class="uk-icon-trash uk-icon-small"></i></a>';

			$button = $delete;
			return $button;
		}
		return $button;
	}

	public function show_add()
	{
		$data['name'] 		 = $this->name;
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'Konfigurasi Konsumen' => base_url('config/config_konsumen'), 'Tambah' => base_url('config/config_konsumen/show_add')];

		$this->template->display('config_konsumen/add', $data);
	}

	public function action_add()
	{
		$post = $this->input->post();

		$this->form_validation->set_rules('nik', 'Nik', 'required|trim');
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required|trim');
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required|trim');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required|trim');
		$this->form_validation->set_rules('agama', 'Agama', 'required|trim');
		$this->form_validation->set_rules('pernikahan', 'Status Pernikahan', 'required|trim');
		$this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'required|trim');
		$this->form_validation->set_rules('kewarganegaraan', 'Kewarganegaraan', 'required|trim');

		if($this->form_validation->run() == False) {
			$result = [
				'sts' => '99',
				'msg' => validation_errors()
			];

			echo json_encode( $result ); exit();
		}
		$paramKonsumen = [
			'konsumen_nik' 			   => $post['nik'],
			'konsumen_nama' 		   => $post['nama'],
			'konsumen_tempat_lahir'    => $post['tempat_lahir'],
			'konsumen_tanggal_lahir'   => $post['tanggal_lahir'],
			'konsumen_kelamin'  	   => $post['jenis_kelamin'],
			'konsumen_alamat'  	   	   => $post['alamat'],
			'konsumen_agama' 		   => $post['agama'],
			'konsumen_pernikahan' 	   => $post['pernikahan'],
			'konsumen_pekerjaan' 	   => $post['pekerjaan'],
			'konsumen_kewarganegaraan' => $post['kewarganegaraan']
		];
		$this->m_global->insert('master_konsumen', $paramKonsumen);

		$result = [
			'sts' => '1',
			'msg' => 'Data berhasil ditambahkan'
		];
		echo json_encode( $result ); exit();
	}
}

/* End of file Config_user.php */
/* Location: ./application/modules/config/controllers/Config_user.php */