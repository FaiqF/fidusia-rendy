<div class="uk-grid">
	<div class="uk-width-medium-1-1">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Edit Notaris</h3>
			</div>
			<div class="md-card-content">
				<div id="alert_error"></div>
				<form method="post" action="<?=$url; ?>/action_edit/<?=$id; ?>" id="form_edit">
					<div class="uk-grid">
						<div class="uk-width-medium-1-5 uk-row-first"></div>
						<div class="uk-width-medium-4-5">
							<div class="uk-form-row">
								<div class="md-input-wrapper md-input-filled">
									<label>Nama <span class="required">*</span></label>
									<input name="nama" type="text" class="md-input uk-form-width-large" value="<?=$record[0]->kd_nama; ?>">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper md-input-filled">
									<label>Nomor <span class="required">*</span></label>
									<input name="nomor" type="text" class="md-input uk-form-width-large" value="<?=$record[0]->kd_nomor; ?>">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="uk-grid">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
										<label>Tanggal <span class="required">*</span></label>
									</div>
									<div class="uk-width-medium-1 uk-margin-small-bottom">
										<input name="tanggal" type="text" class="datepicker" value="<?=$record[0]->kd_tanggal; ?>">
									</div>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper md-input-filled">
									<label>Tempat Lahir <span class="required">*</span></label>
									<input name="tempat_lahir" type="text" class="md-input uk-form-width-large" value="<?=$record[0]->kd_tempat_lahir; ?>">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="uk-grid">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
										<label>Tanggal Lahir <span class="required">*</span></label>
									</div>
									<div class="uk-width-medium-1 uk-margin-small-bottom">
										<input name="tanggal_lahir" type="text" class="datepicker" value="<?=$record[0]->kd_tanggal_lahir; ?>">
									</div>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper md-input-filled">
									<label>Pekerjaan <span class="required">*</span></label>
									<input name="pekerjaan" type="text" class="md-input uk-form-width-large" value="<?=$record[0]->kd_pekerjaan; ?>">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper md-input-filled">
									<label>Alamat <span class="required">*</span></label>
									<input name="alamat" type="text" class="md-input uk-form-width-large" value="<?=$record[0]->kd_alamat; ?>">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper md-input-filled">
									<label>NIK <span class="required">*</span></label>
									<input name="nik" type="text" class="md-input uk-form-width-large" value="<?=$record[0]->kd_nik; ?>">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<div class="uk-grid">
										<div class="uk-width-medium-1 uk-row-first">
											<label>Cabang <span class="required">*</span></label>
										</div>
										<div class="uk-width-medium-1">
											<select multiple name="cabang[]" class="uk-form-width-large" select-style select-style-bottom>
												<option value="">Cabang</option>
												<?php foreach ($cabang as $key => $val): ?>
													<option <?= (in_array($val->cabang_id, $selectedarr))?"selected":""; ?> value="<?=$val->cabang_id; ?>"><?=$val->cabang_name; ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-text-center uk-margin-large-top">
						<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Simpan</button>
						<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Kembali</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<a href="<?=$url; ?>/show_edit/<?=$id; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		App.form_submit($('#form_edit'));
		App.datepicker();
	});
</script>