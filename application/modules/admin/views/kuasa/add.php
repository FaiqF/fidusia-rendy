<div class="uk-grid">
	<div class="uk-width-medium-1-1">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Tambah Kuasa</h3>
			</div>
			<div class="md-card-content">
				<div id="alert_error"></div>
				<form method="post" action="<?=$url; ?>/action_add" id="form_add">
					<div class="uk-grid">
						<div class="uk-width-medium-1-5 uk-row-first"></div>
						<div class="uk-width-medium-4-5">
						
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Panggilan <span class="required">*</span></label>
									<input name="panggilan" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Nama <span class="required">*</span></label>
									<input name="nama" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Tempat Lahir <span class="required">*</span></label>
									<input name="tempat_lahir" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="uk-grid">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
										<label>Tanggal Lahir <span class="required">*</span></label>
									</div>
									<div class="uk-width-medium-1 uk-margin-small-bottom">
										<input name="tanggal_lahir" type="text" class="datepicker">
									</div>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Pekerjaan <span class="required">*</span></label>
									<input name="pekerjaan" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Alamat <span class="required">*</span></label>
									<input name="alamat" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Provinsi <span class="required">*</span></label>
									<input name="provinsi" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Kabupaten <span class="required">*</span></label>
									<input name="kabupaten" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Kecamatan <span class="required">*</span></label>
									<input name="kecamatan" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Kelurahan <span class="required">*</span></label>
									<input name="kelurahan" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Kewarganegaraan <span class="required">*</span></label>
									<input name="kewarganegaraan" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Jenis identitas <span class="required">*</span></label>
									<input name="jenis_identitas" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>NIK <span class="required">*</span></label>
									<input name="nik" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>No kuasa <span class="required">*</span></label>
									<input name="nomor" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="uk-grid">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
										<label>Tanggal Surat Kuasa Pembebanan (SKPJF)<span class="required">*</span></label>
									</div>
									<div class="uk-width-medium-1 uk-margin-small-bottom">
										<input name="tanggal" type="text" class="datepicker">
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="uk-text-center uk-margin-large-top">
						<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Simpan</button>
						<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Kembali</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<a href="<?=$url; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		App.form_submit($('#form_add'));
		App.datepicker();
		App.datepicker('.years', null, true);
		App.masked_input();
		$('.masked_input').inputmask();
	});
</script>