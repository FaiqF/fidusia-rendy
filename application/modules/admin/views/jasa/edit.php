<div class="uk-grid">
	<div class="uk-width-medium-1-1">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Tambah Biaya Jasa</h3>
			</div>
			<div class="md-card-content">
				<div id="alert_error"></div>
				<form method="post" action="<?=$url; ?>/action_edit/<?=$id; ?>" id="form_edit">
					<div class="uk-grid">
						<div class="uk-width-medium-1-5 uk-row-first"></div>
						<div class="uk-width-medium-4-5">
							<div class="uk-form-row">
								<div class="md-input-wrapper md-input-filled">
									<label>Max <span class="required">*</span></label>
									<input value="<?=$record[0]->jasa_max; ?>" name="max" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper md-input-filled">
									<label>Harga <span class="required">*</span></label>
									<input value="<?=$record[0]->jasa_harga; ?>" name="harga" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-text-center uk-margin-large-top">
						<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Simpan</button>
						<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Kembali</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<a href="<?=$url; ?>/show_edit/<?=$id; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		App.form_submit($('#form_edit'));
	});
</script>