<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-content">
		<div class="md-card">
			<div class="md-card-toolbar">
				<div class="md-card-toolbar-actions">
					<i class="md-icon material-icons md-card-toggle">&#xE316;</i>
				</div>
				<h3 class="md-card-toolbar-heading-text">
					Filter
				</h3>
			</div>
			<div class="md-card-content">
				<div class="uk-grid">
					<div class="uk-width-medium-1-3">
						<select name="region" class="form-filter select-filter" select-style select-style-bottom>
							<option value="">Region</option>
							<?php foreach ($region as $key => $val): ?>
								<option value="<?=$val->region_id; ?>"><?=$val->region_name; ?></option>
							<?php endforeach ?>							
						</select>
					</div>
					<div class="uk-width-medium-1-3 uk-row-first">
						<div class="md-input-wrapper">
							<label>Nama</label>
							<input name="nama" type="text" class="md-input form-filter">
							<span class="md-input-bar"></span>
						</div>
					</div>
					<div class="uk-width-medium-1-3">
						<select name="status" class="form-filter select-filter" select-style select-style-bottom>
							<option value="">Status</option>
							<option value="1">Aktif</option>
							<option value="0">Tidak Aktif</option>
							<!-- <option value="99">Hapus</option> -->
						</select>
					</div>
				</div>
				<div class="uk-grid uk-margin-medium-top uk-text-center">
					<div class="uk-width-1-1">
						<button type="button" class="filter-submit md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Cari</button>
						<button type="button" class="filter-reset md-btn md-btn-small md-btn-danger md-btn-wave-light waves-effect waves-button waves-light">Reset</button>
					</div>
				</div>
			</div>
		</div>
		<table id="datatable_ajax" class="uk-table" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Max</th>
					<th>Harga</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
</div>

<div class="md-fab-wrapper">
	<a class="md-fab md-fab-accent ajaxify" href="<?=$url; ?>/show_add">
		<i class="material-icons">&#xE145;</i>
	</a>
</div>
<script>
	$(document).ready(function(){
		var url 	= "<?=$url; ?>/select",
			header  = [
						{ "className": "text-center" },
						{ "className": "text-center" },
						{ "className": "text-center" },
					  ],
			order 	= [
						['1', 'asc']
					  ],
			sort 	= [-1, 0];

		App.setDatatable(url, header, order, sort);
	});
</script>