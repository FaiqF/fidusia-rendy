<div class="uk-grid">
	<div class="uk-width-medium-1-1">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Edit Notaris</h3>
			</div>
			<div class="md-card-content">
				<div id="alert_error"></div>
				<form method="post" action="<?=$url; ?>/action_edit/<?=$id; ?>" id="form_edit">
					<div class="uk-grid">
						<div class="uk-width-medium-1-5 uk-row-first"></div>
						<div class="uk-width-medium-4-5">
							<div class="uk-form-row">
								<div class="md-input-wrapper md-input-filled">
									<label>Nama <span class="required">*</span></label>
									<input value="<?=$record[0]->saksi_nama; ?>" name="nama" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="uk-grid md-input-filled">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
										<label>KTP <span class="required">*</span></label>
									</div>
									<div class="uk-width-medium-1 uk-margin-small-bottom">
										<input value="<?=$record[0]->saksi_ktp; ?>" name="ktp" type="text" class="masked" format="0000000000000000">
									</div>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper md-input-filled">
									<label>Tempat Lahir <span class="required">*</span></label>
									<input value="<?=$record[0]->saksi_tempat_lahir	; ?>" name="tempat_lahir" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="uk-grid md-input-filled">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
										<label>Tanggal Lahir <span class="required">*</span></label>
									</div>
									<div class="uk-width-medium-1 uk-margin-small-bottom">
										<input value="<?=$record[0]->saksi_tanggal_lahir; ?>" name="tanggal_lahir" type="text" class="datepicker">
									</div>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper md-input-filled">
									<label>Alamat <span class="required">*</span></label>
									<input value="<?=$record[0]->saksi_alamat; ?>" name="alamat" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							
						</div>
					</div>
					<div class="uk-text-center uk-margin-large-top">
						<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Simpan</button>
						<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Kembali</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<a href="<?=$url; ?>/show_edit/<?=$id; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		App.form_submit($('#form_edit'));
		App.datepicker();
		App.datepicker('.years', null, true);
		App.masked_input();
		$('.masked_input').inputmask();
	});
</script>