<div class="uk-grid">
	<div class="uk-width-medium-1-1">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Edit Notaris</h3>
			</div>
			<div class="md-card-content">
				<div id="alert_error"></div>
				<form method="post" action="<?=$url; ?>/action_edit/<?=$id; ?>" id="form_edit">
					<div class="uk-grid">
						<div class="uk-width-medium-1-5 uk-row-first"></div>
						<div class="uk-width-medium-4-5">
							<!-- <div class="uk-form-row">
								<div class="md-input-wrapper">
									<div class="uk-grid">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
											<label>Cabang <span class="required">*</span></label>
										</div>
										<div class="uk-width-medium-1 uk-margin-small-bottom">
											<select multiple name="cabang[]" class="form-filter select-filter uk-form-width-large" select-style select-style-bottom>
												<option value="">Cabang</option>
												<?php
													foreach ($cabang as $key => $val):
													$x = explode(',', $record[0]->wn_cabang_id);
												?>
													<option <?=(in_array($val->cabang_id, $x)) ? 'selected' : ''; ?> value="<?=$val->cabang_id; ?>"><?=$val->cabang_name; ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
								</div>
							</div> -->
							<div class="uk-form-row">
								<div class="md-input-wrapper md-input-filled">
									<label>Nama Wilayah <span class="required">*</span></label>
									<input value="<?=$record[0]->wn_name; ?>" name="name" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<div class="uk-grid">
										<div class="uk-width-medium-1-3 uk-row-first">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Status <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1-2">
													<span>
														<input <?=($record[0]->wn_status == '1') ? 'checked' : ''; ?> value="1" type="radio" name="status" id="role1; ?>" input-style-checkbox />
														<label for="role1; ?>" class="inline-label">Active</label>
													</span>
													<span class="md-input-bar"></span>
												</div>
												<div class="uk-width-medium-1-2">
													<span>
														<input <?=($record[0]->wn_status == '0') ? 'checked' : ''; ?> value="0" type="radio" name="status" id="role0; ?>" input-style-checkbox />
														<label for="role0; ?>" class="inline-label">InActive</label>
													</span>
													<span class="md-input-bar"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-text-center uk-margin-large-top">
						<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Simpan</button>
						<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Kembali</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<a href="<?=$url; ?>/show_edit/<?=$id; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		App.form_submit($('#form_edit'));
	});
</script>