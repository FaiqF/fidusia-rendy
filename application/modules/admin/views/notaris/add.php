<div class="uk-grid">
	<div class="uk-width-medium-1-1">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Tambah Notaris</h3>
			</div>
			<div class="md-card-content">
				<div id="alert_error"></div>
				<form method="post" action="<?=$url; ?>/action_add" id="form_add">
					<div class="uk-grid">
						<div class="uk-width-medium-1-5 uk-row-first"></div>
						<div class="uk-width-medium-4-5">
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Nama <span class="required">*</span></label>
									<input name="nama" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Nomor SK </label>
									<input name="izin" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<div class="uk-grid">
										<div class="uk-width-medium-1 uk-row-first">
											<label>Admin <span class="required">*</span></label>
										</div>
										<div class="uk-width-medium-1">
											<select multiple name="win_user_id[]" class="uk-form-width-large" select-style select-style-bottom>
												<option value="">Admin</option>
												<?php foreach ($users as $key => $val): ?>
													<option value="<?=$val->user_id; ?>"><?=$val->user_name; ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<div class="uk-grid">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
											<label>Wilayah Notaris <span class="required">*</span></label>
										</div>
										<div class="uk-width-medium-1 uk-margin-small-bottom">
											<select name="wilayah_kerja_id" class="form-filter select-filter uk-form-width-large" select-style select-style-bottom>
												<option value="">Wilayah Notaris</option>
												<?php foreach ($wilayah_notaris as $key => $val): ?>
													<option value="<?=$val->wn_id; ?>"><?=$val->wn_name; ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<div class="uk-grid">
										<div class="uk-width-medium-1 uk-row-first">
											<label>Cabang <span class="required">*</span></label>
										</div>
										<div class="uk-width-medium-1">
											<select multiple name="cabang[]" class="uk-form-width-large" select-style select-style-bottom>
												<option value="">Cabang</option>
												<?php foreach ($cabang as $key => $val): ?>
													<option value="<?=$val->cabang_id; ?>"><?=$val->cabang_name; ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<div class="uk-grid">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
											<label>Kuasa Direksi 1 <span class="required">*</span></label>
										</div>
										<div class="uk-width-medium-1 uk-margin-small-bottom">
											<select name="kd_id1" class="form-filter select-filter uk-form-width-large" select-style select-style-bottom>
												<option value="">Kuasa Direksi</option>
												<?php foreach ($kuasa_direksi as $key => $val): ?>
													<option value="<?=$val->kd_id; ?>"><?=$val->kd_nama; ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							<!--<div class="uk-form-row">
								<div class="md-input-wrapper">
									<div class="uk-grid">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
											<label>Kuasa Direksi 2 <span class="required">*</span></label>
										</div>
										<div class="uk-width-medium-1 uk-margin-small-bottom">
											<select name="kd_id2" class="form-filter select-filter uk-form-width-large" select-style select-style-bottom>
												<option value="">Kuasa Direksi</option>
												<?php foreach ($kuasa_direksi as $key => $val): ?>
													<option value="<?=$val->kd_id; ?>"><?=$val->kd_nama; ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
								</div>
							</div>-->
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<div class="uk-grid">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
											<label>Saksi 1 <span class="required">*</span></label>
										</div>
										<div class="uk-width-medium-1 uk-margin-small-bottom">
											<select name="saksi_id1" class="form-filter select-filter uk-form-width-large" select-style select-style-bottom>
												<option value="">Saksi</option>
												<?php foreach ($saksi as $key => $val): ?>
													<option value="<?=$val->saksi_id; ?>"><?=$val->saksi_nama; ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<div class="uk-grid">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
											<label>Saksi 2 <span class="required">*</span></label>
										</div>
										<div class="uk-width-medium-1 uk-margin-small-bottom">
											<select name="saksi_id2" class="form-filter select-filter uk-form-width-large" select-style select-style-bottom>
												<option value="">Saksi</option>
												<?php foreach ($saksi as $key => $val): ?>
													<option value="<?=$val->saksi_id; ?>"><?=$val->saksi_nama; ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							
							
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<div class="uk-grid">
										<div class="uk-width-medium-1-3 uk-row-first">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Status <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1-2">
													<span>
														<input checked value="1" type="radio" name="status" id="role1; ?>" input-style-checkbox />
														<label for="role1; ?>" class="inline-label">Active</label>
													</span>
													<span class="md-input-bar"></span>
												</div>
												<div class="uk-width-medium-1-2">
													<span>
														<input value="0" type="radio" name="status" id="role0; ?>" input-style-checkbox />
														<label for="role0; ?>" class="inline-label">InActive</label>
													</span>
													<span class="md-input-bar"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-text-center uk-margin-large-top">
						<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Simpan</button>
						<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Kembali</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<a href="<?=$url; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		App.form_submit($('#form_add'));
	});
</script>