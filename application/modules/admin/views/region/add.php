<div class="uk-grid">
	<div class="uk-width-medium-1-1">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Tambah Region</h3>
			</div>
			<div class="md-card-content">
				<div id="alert_error"></div>
				<form method="post" action="<?=$url; ?>/action_add" id="form_add">
					<div class="uk-grid">
						<div class="uk-width-medium-1-5 uk-row-first"></div>
						<div class="uk-width-medium-4-5">
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Nama <span class="required">*</span></label>
									<input name="name" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<div class="uk-grid">
										<div class="uk-width-medium-1-3 uk-row-first">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Status <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1-2">
													<span>
														<input checked value="1" type="radio" name="status" id="role1; ?>" input-style-checkbox />
														<label for="role1; ?>" class="inline-label">Active</label>
													</span>
													<span class="md-input-bar"></span>
												</div>
												<div class="uk-width-medium-1-2">
													<span>
														<input value="0" type="radio" name="status" id="role0; ?>" input-style-checkbox />
														<label for="role0; ?>" class="inline-label">InActive</label>
													</span>
													<span class="md-input-bar"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-text-center uk-margin-large-top">
						<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Simpan</button>
						<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Kembali</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<a href="<?=$url; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		App.form_submit($('#form_add'));
	});
</script>