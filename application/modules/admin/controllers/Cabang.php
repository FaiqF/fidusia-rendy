<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cabang extends MX_Controller 
{

	private $table_db       = 'cabang';
	private $table_prefix   = 'cabang_';
	private $prefix 		= 'admin/cabang';
	private $name 			= 'Cabang';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$data['name'] 	= $this->name;
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables'];

		$data['region'] = $this->m_global->get_data_all('region', null, ['region_status' => '1']);

		$this->template->display('cabang/index', $data);
	}

	public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 		= [];
		$where 		= null;
		$where_e 	= null;

		$search 	= [
			"nama" 			=> $this->table_prefix.'name',
			"status" 		=> $this->table_prefix.'status',
		];

		if (@$post['action'] == 'filter')
		{
			$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = "DATE(cabang_lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

					} else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			$where[$this->table_prefix.'status <>'] = '99';
		}

		$keys 		= array_keys($search);
		$order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

		$select		= 'cabang_id, '.implode(',', $search);

		$count 		= $this->m_global->count_data_all( $this->table_db, $join, $where, $where_e );
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


		$data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, $where_e, $order, $start, $length);

		$i = 1 + $start;

		$status = [
			'0' => '<span class="uk-badge uk-badge-warning">InActive</span>',
			'1' => '<span class="uk-badge uk-badge-primary">Active</span>',
			'99' => '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
		];

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
				$rows->cabang_name,
				$status[$rows->cabang_status],
				$this->_button($rows->cabang_id, $rows->cabang_status, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

	public function _button($id, $status, $show = false)
	{
		$id = strEncrypt($id);
		$button = '';

		if($show) {
			$c_status = '<a data-uk-tooltip title="Change Status" class="md-btn ' . (($status == '1') ? 'md-btn-success' : (($status == '99') ? 'md-btn-danger' : '')) . ' md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(1, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/'.($status == 1 ? '0' : '1' ) ).'">
						<i class="' . (($status == '1') ? 'uk-icon-check' : (($status == '99') ? 'uk-icon-warning' : 'uk-icon-ban')) . ' uk-icon-small"></i>
					  </a>';
			$edit 	= '<a data-uk-tooltip title="Edit" class="ajaxify md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="'.base_url('admin/cabang/show_edit').'/'.$id.'"><i class="uk-icon-pencil uk-icon-small"></i></a>';
			$delete = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(2, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/99/'.($status == 99 ? '/true' : '' )).'"><i class="uk-icon-trash uk-icon-small"></i></a>';

			$button = $c_status . $edit;
		} else {
			$button = '-';
		}

		return $button;
	}

	public function show_add()
	{
		$data['name'] 		 = $this->name;
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'Cabang' => base_url('admin/cabang'), 'Tambah' => base_url('admin/cabang/show_add')];

		$data['region'] = $this->m_global->get_data_all('region', null, ['region_status' => '1']);
		
		$this->template->display('cabang/add', $data);
	}

	public function show_edit($id)
	{
		$data['name'] 		 = $this->name;
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'Cabang' => base_url('admin/cabang'), 'Edit' => base_url('admin/cabang/show_edit').'/'.$id];
		$data['record']		 = $this->m_global->get_data_all('cabang', null, [strEncrypt('cabang_id', TRUE) => $id]);
		$data['id'] 		 = $id;

		$this->template->display('cabang/edit', $data);
	}

	public function action_add()
	{
		$result = [];
		$post 	= $this->input->post();

		$this->form_validation->set_rules('name', 'Name', 'trim|required|is_unique[cabang.cabang_name]');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');

		if ( $this->form_validation->run() == FALSE ){
			$response['msg'] = validation_errors();
			$response['sts'] = '99';

			echo json_encode( $response ); exit;

		}

		$data 	= [
			'cabang_name' 			=> $post['name'],
			'cabang_status'			=> $post['status'],
			'cabang_created_by'		=> user_data()->user_id,
			'cabang_created_date'	=> date('Y-m-d H:i:s')
		];
		$res = $this->m_global->insert('cabang', $data);

		if( !$res ) {
			$response['msg'] = 'Data gagal ditambahkan !';
			$response['sts'] = '0';

			echo json_encode( $response ); exit;

		}

		$result['msg'] = 'Data berhasil ditambahkan !';
		$result['sts'] = '1';

 
		echo json_encode($result); exit();
	}

	public function action_edit($id)
	{
		$result = [];
		$post 	= $this->input->post();

		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');

		if ( $this->form_validation->run() == FALSE ){
			$response['sts'] = '99';
			$response['msg'] = validation_errors();

			echo json_encode( $response ); exit;
		}

		$sameNameCabang = count($this->m_global->get_data_all('cabang', null, [strEncrypt('cabang_id', TRUE) . ' <> ' => $id, 'cabang_name' => $post['name']]));

		if( $sameNameCabang > 0 ){
			$response['sts'] = '0';
			$response['msg'] = 'Nama sudah ada !';

			echo json_encode( $response ); exit;
		}

		$data 	= [
			'cabang_name' 			=> $post['name'],
			'cabang_status'			=> $post['status']
		];
		$res = $this->m_global->update('cabang', $data, [strEncrypt('cabang_id', TRUE) => $id]);

		if( !$res ) {
			$response['sts'] = '0';
			$response['msg'] = 'Data gagal dirubah';

			echo json_encode( $response ); exit;
		}

		$response['sts'] = '1';
		$response['msg'] = 'Data berhasil dirubah !';

		echo json_encode( $response ); exit;	

	}

	public function change_status_by( $id, $status, $stat = FALSE )
	{
		
		if ( $stat ) {
			$result = $this->m_global->delete( $this->table_db, [strEncrypt($this->table_prefix.'id', true) => $id] );
		} else {
			$result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [strEncrypt($this->table_prefix.'id', true) => $id]);
		}

		if ( $result ) {
			$data['status'] = 1;
		} else {
			$data['status'] = 0;
		}

		echo json_encode( $data );
	}

}

/* End of file Cabang.php */
/* Location: ./application/modules/admin/controllers/Cabang.php */