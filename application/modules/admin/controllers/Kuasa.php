<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kuasa extends MX_Controller {

	private $table_db       = 'kuasa_direksi';
	private $table_prefix   = 'kd_';
	private $prefix 		= 'admin/kuasa';
	private $name 			= 'Kuasa';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$data['name'] 	= $this->name;
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables'];

		$data['region'] = $this->m_global->get_data_all('kuasa_direksi', null, null);

		$this->template->display('kuasa/index', $data);
	}

	public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 		= null;
		$where 		= null;
		$where_e 	= null;

		$search 	= [
						"nama" 			=> $this->table_prefix.'nama',
						"nomor" 		=> $this->table_prefix.'nomor',
						"tanggal" 		=> $this->table_prefix.'tanggal',
					  ];

		if (@$post['action'] == 'filter')
		{
			$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = "DATE(kuasa_direksi_lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

					} else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			//$where[$this->table_prefix.'status <>'] = '99';
		}

		$keys 		= array_keys($search);
		$order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

		$select		= 'kd_id, kd_tanggal, '.implode(',', $search);

		$count 		= $this->m_global->count_data_all( $this->table_db, $join, $where, $where_e );
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


		$data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, $where_e, $order, $start, $length);

		$i = 1 + $start;

		$status = [
					'0' => '<span class="uk-badge uk-badge-warning">InActive</span>',
					'1' => '<span class="uk-badge uk-badge-primary">Active</span>',
					'2' => '<span class="uk-badge uk-badge-danger">Used</span>',
					'99' => '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
				  ];

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
				$rows->kd_nama,
				$rows->kd_nomor,
				$rows->kd_tanggal,
				$this->_button($rows->kd_id, 1, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

	public function _button($id, $status, $show = false)
	{
		$id = strEncrypt($id);
		$button = '';

		if($show) {
			$c_status = '<a data-uk-tooltip title="Change Status" class="md-btn ' . (($status == '1') ? 'md-btn-success' : (($status == '99') ? 'md-btn-danger' : '')) . ' md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(1, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/'.($status == 1 ? '0' : '1' ) ).'">
						<i class="' . (($status == '1') ? 'uk-icon-check' : (($status == '99') ? 'uk-icon-warning' : 'uk-icon-ban')) . ' uk-icon-small"></i>
					  </a>';
			$edit 	= '<a data-uk-tooltip title="Edit" class="ajaxify md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="'.base_url('admin/kuasa/show_edit').'/'.$id.'"><i class="uk-icon-pencil uk-icon-small"></i></a>';
			$delete = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(2, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/99/'.($status == 99 ? '/true' : '' )).'"><i class="uk-icon-trash uk-icon-small"></i></a>';

			$button = $c_status . $edit . $delete;
		} else {
			$button = '-';
		}

		return $button;
	}

	public function show_add()
	{
		$data['name'] 		 = $this->name;
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'kuasa_direksi' => base_url('admin/kuasa'), 'Tambah' => base_url('admin/kuasa/show_add')];
		$data['plugin'] 	 = ['kendo_ui','dropify', 'inputmask'];
		$select = 'cabang_id,cabang_name';
		$data['cabang']		 = $this->m_global->get_data_all('cabang',null,null,$select);

		$this->template->display('kuasa/add', $data);
	}

	public function show_edit($id)
	{
		$data['selectedarr'] = array();


		$data['name'] 		 = $this->name;
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'kuasa_direksi' => base_url('admin/kuasa_direksi'), 'Edit' => base_url('admin/kuasa/show_edit').'/'.$id];
		$data['record']		 = $this->m_global->get_data_all('kuasa_direksi', null, [strEncrypt('kd_id', TRUE) => $id]);
		$data['id'] 		 = $id;
		$data['plugin'] 	 = ['kendo_ui','dropify', 'inputmask'];
		$data['region'] 	 = $this->m_global->get_data_all('region', null, ['region_status' => '1']);
		$select = 'cabang_id,cabang_name';
		$data['cabang']		 =$this->m_global->get_data_all('cabang',null,null,$select);
		$select = 'kc_cabang';
		$id = $data['record'][0]->kd_id;
		$where = 'kuasa_cabang.kc_pejabat ='.$id;
		$cabang = $this->m_global->get_data_all('kuasa_cabang',null,$where,$select);
		foreach ($cabang as $key => $value) {
			$data['selectedarr'][]= $value->kc_cabang;
		}


		$this->template->display('kuasa/edit', $data);
	}

	public function action_add()
	{
		$post 	= $this->input->post();

		$this->form_validation->set_rules('panggilan', 'Panggilan','trim|required|html_escape');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir','trim|required|html_escape');
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir','trim|required|html_escape');
		$this->form_validation->set_rules('pekerjaan', 'Pekerjaan','trim|required|html_escape');
		$this->form_validation->set_rules('provinsi', 'Provinsi','trim|required|html_escape');
		$this->form_validation->set_rules('kabupaten', 'Kabupaten / Kota','trim|required|html_escape');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan','trim|required|html_escape');
		$this->form_validation->set_rules('kelurahan', 'Kelurahan','trim|required|html_escape');
		$this->form_validation->set_rules('kewarganegaraan', 'Kewarganegaraan','trim|required|html_escape');
		$this->form_validation->set_rules('jenis_identitas', 'Jenis Identitas','trim|required|html_escape');
		$this->form_validation->set_rules('nik', 'NIK','trim|required|html_escape');
		$this->form_validation->set_rules('nomor', 'Nomor Kuasa','trim|required|html_escape');
		$this->form_validation->set_rules('tanggal', 'Tanggal Kuasa','trim|required|html_escape');
		
		if ($this->form_validation->run() == FALSE){
			$result['sts'] = '99';
			$result['msg'] = validation_errors();

			echo json_encode($result); exit();
		}

		$data = [
			'kd_panggilan'	   => $post['panggilan'],
			'kd_nama'		   => $post['nama'],
			'kd_tempat_lahir'  => $post['tempat_lahir'],
			'kd_tanggal_lahir' => date('Y-m-d', strtotime($post['tanggal_lahir'])),
			'kd_pekerjaan'	   => $post['pekerjaan'],
			'kd_alamat'		   => $post['alamat'],
			'kd_provinsi'	   => $post['provinsi'],
			'kd_kabupaten'	   => $post['kabupaten'],
			'kd_kecamatan'	   => $post['kecamatan'],
			'kd_kelurahan'     => $post['kelurahan'],
			'kd_nik'		   => $post['nik'],
			'kd_tanggal'	   => date('Y-m-d', strtotime($post['tanggal'])),
			'kd_nomor' 		   => $post['nomor']
		];
		$res = $this->m_global->insert('kuasa_direksi', $data);

		if(!$res) {
			$result['sts'] = '0';
			$result['msg'] = 'Data gagal ditambahkan !';
			echo json_encode($result); exit;
		}

		$result['sts'] = '1';
		$result['msg'] = 'Data berhasil ditambahkan';
		// echo $this->db->last_query(); exit();
		// pre($result); exit();
		echo json_encode($result); exit;
	}

	public function action_edit($id)
	{
		$result = [];
		$post 	= $this->input->post();

		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			$check_name = $this->m_global->get_data_all('kuasa_direksi', null, [strEncrypt('kd_id', TRUE) . ' <> ' => $id, 'kd_nama' => $post['nama']]);
			$id = $this->m_global->get_data_all('kuasa_direksi', null, [strEncrypt('kd_id', TRUE)  => $id],'kd_id');
			$id = $id[0]->kd_id;

			if ($check_name) {
				$result['msg'] = 'Nama sudah ada !';
				$result['sts'] = '0';
			} else {
				$delete = $this->m_global->delete('kuasa_cabang','kc_pejabat ='.$id);
				$ins = array();
					foreach ($post['cabang'] as $key => $value) {
							$ins[]= array('kc_pejabat'=>$id,
										  'kc_cabang'=>$value);
				}
				$insert = $this->m_global->many_insert('kuasa_cabang',$ins);

				$data 	= [
						'kd_nama'		=> $post['nama'],
						'kd_nomor' 		=> $post['nomor'],
						'kd_tanggal'	=> date('Y-m-d', strtotime($post['tanggal'])),
						'kd_tempat_lahir'	=> $post['tempat_lahir'],
						'kd_tanggal_lahir'	=> date('Y-m-d', strtotime($post['tanggal_lahir'])),
						'kd_pekerjaan'	=> $post['pekerjaan'],
						'kd_alamat'	=> $post['alamat'],
						'kd_nik'	=> $post['nik'],
					  ];

				$res = $this->m_global->update('kuasa_direksi', $data, [strEncrypt('kd_id', TRUE) => $id]);

				if($res) {
					$result['msg'] = 'Data berhasil dirubah !';
					$result['sts'] = '1';
				} else {
					$result['msg'] = 'Data gagal dirubah !';
					$result['sts'] = '0';
				}
			}
		} else {
			$result['msg'] = validation_errors();
			$result['sts'] = '99';
		}

		echo json_encode($result);
	}

	public function change_status_by( $id, $status, $stat = FALSE )
	{
		
		if ( $stat ) {
			$result = $this->m_global->delete( $this->table_db, [strEncrypt($this->table_prefix.'id', true) => $id] );
		} else {
			$result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [strEncrypt($this->table_prefix.'id', true) => $id]);
		}

		if ( $result ) {
			$data['status'] = 1;
		} else {
			$data['status'] = 0;
		}

		echo json_encode( $data );
	}

}

/* End of file kuasa_direksi.php */
/* Location: ./application/modules/admin/controllers/kuasa_direksi.php */