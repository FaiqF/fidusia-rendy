<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jasa	 extends MX_Controller {

	private $table_db       = 'jasa';
	private $table_prefix   = 'jasa_';
	private $prefix 		= 'admin/jasa';
	private $name 			= 'Jasa';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$data['name'] 	= $this->name;
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables'];
		$this->template->display('jasa/index', $data);
	}

	public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 		= null;
		$where 		= null;
		$where_e 	= null;

		$search 	= [
						"max" 		=> $this->table_prefix.'max',
						"harga" 			=> $this->table_prefix.'harga',
					  ];

		if (@$post['action'] == 'filter')
		{
			$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = "DATE(cabang_lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

					} else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			//$where[$this->table_prefix.'status <>'] = '99';
		}

		$keys 		= array_keys($search);
		$order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

		$select		= 'jasa_max, jasa_harga';

		$count 		= $this->m_global->count_data_all( $this->table_db, $join, $where, $where_e );
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


		$data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, $where_e, $order, $start, $length);

		$i = 1 + $start;

		$status = [
					'0' => '<span class="uk-badge uk-badge-warning">InActive</span>',
					'1' => '<span class="uk-badge uk-badge-primary">Active</span>',
					'99' => '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
				  ];

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$rows->jasa_max,
				$rows->jasa_harga,
				$this->_button($rows->jasa_max, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

	public function _button($id, $show = false)
	{
		$id = strEncrypt($id);
		$button = '';

		if($show) {
			$edit 	= '<a data-uk-tooltip title="Edit" class="ajaxify md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="'.base_url('admin/jasa/show_edit').'/'.$id.'"><i class="uk-icon-pencil uk-icon-small"></i></a>';
			$delete = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(2, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/99/true').'"><i class="uk-icon-trash uk-icon-small"></i></a>';

			$button = $edit.$delete;
		} else {
			$button = '-';
		}

		return $button;
	}

	public function show_add()
	{
		$data['name'] 		 = $this->name;
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'Jasa' => base_url('admin/jasa'), 'Tambah' => base_url('admin/jasa/show_add')];

		//$data['region'] = $this->m_global->get_data_all('region', null, ['region_status' => '1']);
		
		$this->template->display('jasa/add', $data);
	}

	public function show_edit($id)
	{
		$data['name'] 		 = $this->name;
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'Jasa' => base_url('admin/jasa'), 'Edit' => base_url('admin/jasa/show_edit').'/'.$id];
		$data['record']		 = $this->m_global->get_data_all('jasa', null, [strEncrypt('jasa_max', TRUE) => $id]);
		$data['id'] 		 = $id;
		$this->template->display('jasa/edit', $data);
	}

	public function action_add()
	{
		$result = [];
		$post 	= $this->input->post();

		$this->form_validation->set_rules('max', 'Max', 'trim|required');
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			$data 	= [
						'jasa_max'				=> $post['max'],
						'jasa_harga' 			=> $post['harga']
					  ];

			$res = $this->m_global->insert('jasa', $data);

			if($res) {
				$result['msg'] = 'Data berhasil ditambahkan !';
				$result['sts'] = '1';
			} else {
				$result['msg'] = 'Data gagal ditambahkan !';
				$result['sts'] = '0';
			}
		} else {
			$result['msg'] = validation_errors();
			$result['sts'] = '99';
		}

		echo json_encode($result);
	}

	public function action_edit($id)
	{
		$result = [];
		$post 	= $this->input->post();

		$this->form_validation->set_rules('max', 'Max', 'trim|required');
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			$data 	= [
					'jasa_max'		=> $post['max'],
					'jasa_harga' 			=> $post['harga'],
				  ];

			$res = $this->m_global->update('jasa', $data, [strEncrypt('jasa_max', TRUE) => $id]);

			if($res) {
				$result['msg'] = 'Data berhasil dirubah !';
				$result['sts'] = '1';
			} else {
				$result['msg'] = 'Data gagal dirubah !';
				$result['sts'] = '0';
			}
		} else {
			$result['msg'] = validation_errors();
			$result['sts'] = '99';
		}

		echo json_encode($result);
	}

	public function change_status_by( $id, $status, $stat = FALSE )
	{
		
		if ( $stat ) {
			$result = $this->m_global->delete( $this->table_db, [strEncrypt($this->table_prefix.'max', true) => $id] );
		} else {
			$result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [strEncrypt($this->table_prefix.'id', true) => $id]);
		}

		if ( $result ) {
			$data['status'] = 1;
		} else {
			$data['status'] = 0;
		}

		echo json_encode( $data );
	}

}

/* End of file Cabang.php */
/* Location: ./application/modules/admin/controllers/Cabang.php */