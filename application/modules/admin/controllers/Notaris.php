<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notaris extends MX_Controller {

	private $table_db       = 'notaris';
	private $table_prefix   = 'notaris_';
	private $prefix 		= 'admin/notaris';
	private $name 			= 'Notaris';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$data['name'] 	= $this->name;
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables'];

		$this->template->display('notaris/index', $data);
	}

	public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 		= null;
		$where 		= null;
		$where_e 	= null;

		$search 	= [
						"nama" 			=> $this->table_prefix.'nama',
						"status" 		=> $this->table_prefix.'status',
					  ];

		if (@$post['action'] == 'filter')
		{
			$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = "DATE(notaris_lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

					} else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			$where[$this->table_prefix.'status <>'] = '99';
		}

		$keys 		= array_keys($search);
		@$order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

		$select		= 'notaris_id, '.implode(',', $search);

		$count 		= $this->m_global->count_data_all( $this->table_db, $join, $where, $where_e );
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


		$data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, $where_e, $order, $start, $length);
		$i = 1 + $start;
		$status = [
					'0' => '<span class="uk-badge uk-badge-warning">InActive</span>',
					'1' => '<span class="uk-badge uk-badge-primary">Active</span>',
					'99' => '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
				  ];

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
				$rows->notaris_nama,
				$status[$rows->notaris_status],
				$this->_button($rows->notaris_id, $rows->notaris_status, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

	public function _button($id, $status, $show = false)
	{
		$id = strEncrypt($id);
		$button = '';

		if($show) {
			$c_status = '<a data-uk-tooltip title="Change Status" class="md-btn ' . (($status == '1') ? 'md-btn-success' : (($status == '99') ? 'md-btn-danger' : '')) . ' md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(1, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/'.($status == 1 ? '0' : '1' ) ).'">
						<i class="' . (($status == '1') ? 'uk-icon-check' : (($status == '99') ? 'uk-icon-warning' : 'uk-icon-ban')) . ' uk-icon-small"></i>
					  </a>';
			$edit 	= '<a data-uk-tooltip title="Edit" class="ajaxify md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="'.base_url('admin/notaris/show_edit').'/'.$id.'"><i class="uk-icon-pencil uk-icon-small"></i></a>';
			$delete = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(2, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/99/'.($status == 99 ? '/true' : '' )).'"><i class="uk-icon-trash uk-icon-small"></i></a>';

			$button = $c_status . $edit . $delete;
		} else {
			$button = '-';
		}

		return $button;
	}

	public function show_add()
	{
		
		$data['name'] 		 	 = $this->name;
		$data['url'] 		 	 = base_url().$this->prefix;
		$data['breadcrumbs'] 	 = ['Dashboard' => base_url('dashboard'), 'Notaris' => base_url('admin/notaris'), 'Tambah' => base_url('admin/notaris/show_add')];
		$data['users'] 		 	 = $this->m_global->get_data_all('users', null, null, 'user_id, user_name','user_role = 24');
		$data['cabang'] 		 = $this->m_global->get_data_all('cabang', null, null, 'cabang_id, cabang_name');
		$data['wilayah_notaris'] = $this->m_global->get_data_all('wilayah_notaris', null, null, 'wn_id, wn_name');
		$data['kuasa_direksi'] 	 = $this->m_global->get_data_all('kuasa_direksi', null, null, 'kd_id, kd_nama');
		$data['saksi'] 		 	 = $this->m_global->get_data_all('saksi', null, null, 'saksi_id, saksi_nama');

		$this->template->display('notaris/add', $data);
	}

	public function show_edit($id)
	{
		$data['selectedadmin'] = array();
		$data['name'] 		 	 = $this->name;
		$data['url'] 		     = base_url().$this->prefix;
		$data['breadcrumbs'] 	 = ['Dashboard' => base_url('dashboard'), 'Notaris' => base_url('admin/notaris'), 'Edit' => base_url('admin/notaris/show_edit').'/'.$id];
		$data['record']		 	 = $this->m_global->get_data_all('notaris', null, [strEncrypt('notaris_id', TRUE) => $id]);
		$data['id'] 		 	 = $id;
		$data['users'] 		 	 = $this->m_global->get_data_all('users', null, null, 'user_id, user_name','user_role = 24');
		$data['cabang'] 		 = $this->m_global->get_data_all('cabang', null, null, 'cabang_id, cabang_name');
		$data['wilayah_notaris'] = $this->m_global->get_data_all('wilayah_notaris', null, null, 'wn_id, wn_name');
		$data['kuasa_direksi'] 	 = $this->m_global->get_data_all('kuasa_direksi', null, null, 'kd_id, kd_nama');
		$data['saksi'] 		 	 = $this->m_global->get_data_all('saksi', null, null, 'saksi_id, saksi_nama');
		$data['arrcabang'] 		 = explode('|', $data['record'][0]->notaris_cabang_id);
		$data['adminwin']		 = $this->m_global->get_data_all('notaris_admin',null,[strEncrypt('na_notaris',TRUE) =>$id,],'na_admin');
		foreach ($data['adminwin'] as $val) {
			$data['selectedadmin'][] = $val->na_admin;
		}

		$this->template->display('notaris/edit', $data);
	}

	public function action_add()
	{
		$result = [];
		$post 	= $this->input->post();
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|is_unique[notaris.notaris_nama]');
		$this->form_validation->set_rules('win_user_id[]', 'WIN User ID', 'trim|required');
		$this->form_validation->set_rules('wilayah_kerja_id', 'Wilayah Kerja', 'trim|required');
		$this->form_validation->set_rules('kd_id1', 'Kuasa Direksi 1', 'trim|required');
		//$this->form_validation->set_rules('kd_id2', 'Kuasa Direksi 2', 'trim|required');
		$this->form_validation->set_rules('saksi_id1', 'Saksi 1', 'trim|required');
		$this->form_validation->set_rules('saksi_id2', 'Saksi 2', 'trim|required');
		
	
		if ($this->form_validation->run() == TRUE){
			$data 	= [
						'notaris_nama' 			   => $post['nama'],
						'notaris_izin' 			   => $post['izin'],
						'notaris_wilayah_kerja_id' => $post['wilayah_kerja_id'],
						'notaris_cabang_id'		   => join('|', $post['cabang']),
						'notaris_kd_id1'		   => $post['kd_id1'],
						'notaris_saksi_id1'		   => $post['saksi_id1'],
						'notaris_saksi_id2'		   => $post['saksi_id2'],
						'notaris_status'		   => $post['status']
					  ];

			$res = $this->m_global->insert('notaris', $data);

			if($res) {
			$where['notaris_nama'] = $post['nama'];
			$id = $this->m_global->get_data_all('notaris',null,$where,'notaris_id');
			foreach($post['win_user_id'] as $val) {
				$insertadmin[] = array('na_notaris'=>$id[0]->notaris_id,'na_admin'=>$val);
			}

			$this->m_global->many_insert('notaris_admin',$insertadmin);

				$result['msg'] = 'Data berhasil ditambahkan !';
				$result['sts'] = '1';
			} else {
				$result['msg'] = 'Data gagal ditambahkan !';
				$result['sts'] = '0';
			}
		} else {
			$result['msg'] = validation_errors();
			$result['sts'] = '99';
		}

		echo json_encode($result);
	}

	public function action_edit($id)
	{
		$result = [];
		$post 	= $this->input->post();
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('win_user_id[]', 'WIN User ID', 'trim|required');
		$this->form_validation->set_rules('wilayah_kerja_id', 'Wilayah Kerja', 'trim|required');
		$this->form_validation->set_rules('kd_id1', 'Kuasa Direksi 1', 'trim|required');
		//$this->form_validation->set_rules('kd_id2', 'Kuasa Direksi 2', 'trim|required');
		$this->form_validation->set_rules('saksi_id1', 'Saksi 1', 'trim|required');
		$this->form_validation->set_rules('saksi_id2', 'Saksi 2', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			$check_name = $this->m_global->get_data_all('notaris', null, [strEncrypt('notaris_id', TRUE) . ' <> ' => $id, 'notaris_nama' => $post['nama']]);

			if ($check_name) {
				$result['msg'] = 'Nama sudah ada !';
				$result['sts'] = '0';
			} else {
				$data 	= [
						'notaris_nama' 			=> $post['nama'],
						'notaris_izin' 		=> $post['izin'],
						'notaris_win_user_id'	=> 0,
						'notaris_wilayah_kerja_id'			=> $post['wilayah_kerja_id'],
						'notaris_cabang_id'			=> join('|', $post['cabang']),
						'notaris_kd_id1'			=> $post['kd_id1'],
						'notaris_saksi_id1'			=> $post['saksi_id1'],
						'notaris_saksi_id2'			=> $post['saksi_id2'],
						'notaris_status'		=> $post['status']
					  ];
				$res = $this->m_global->update('notaris', $data, [strEncrypt('notaris_id', TRUE) => $id]);

				if($res) {
					$id = $this->m_global->get_data_all('notaris', null, [strEncrypt('notaris_id', TRUE) => $id],'notaris_id');
					foreach($post['win_user_id'] as $val){
						$insertadmin[] = array('na_notaris'=>$id[0]->notaris_id,'na_admin'=>$val);
					}
					$this->m_global->delete('notaris_admin',array('na_notaris'=>$id[0]->notaris_id));
					$this->m_global->many_insert('notaris_admin',$insertadmin);
					$result['msg'] = 'Data berhasil dirubah !';
					$result['sts'] = '1';
				} else {
					$result['msg'] = 'Data gagal dirubah !';
					$result['sts'] = '0';
				}
			}
		} else {
			$result['msg'] = validation_errors();
			$result['sts'] = '99';
		}

		echo json_encode($result);
	}

	public function change_status_by( $id, $status, $stat = FALSE )
	{
		
		if ( $stat ) {
			$result = $this->m_global->delete( $this->table_db, [strEncrypt($this->table_prefix.'id', true) => $id] );
		} else {
			$result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [strEncrypt($this->table_prefix.'id', true) => $id]);
		}

		if ( $result ) {
			$data['status'] = 1;
		} else {
			$data['status'] = 0;
		}

		echo json_encode( $data );
	}

}

/* End of file Notaris.php */
/* Location: ./application/modules/admin/controllers/Notaris.php */