<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saksi extends MX_Controller {

	private $table_db       = 'saksi';
	private $table_prefix   = 'saksi_';
	private $prefix 		= 'admin/saksi';
	private $name 			= 'Saksi';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$data['name'] 	= $this->name;
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables'];

		$data['region'] = $this->m_global->get_data_all('saksi', null, ['saksi_status' => '1']);

		$this->template->display('saksi/index', $data);
	}

	public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 		= null;
		$where 		= null;
		$where_e 	= null;

		$search 	= [
						"nama" 		=> $this->table_prefix.'nama',
						"tempat_lahir" 			=> $this->table_prefix.'tempat_lahir',
						"tanggal_lahir" 		=> $this->table_prefix.'tanggal_lahir',
						"alamat" 		=> $this->table_prefix.'alamat',
						"ktp" 		=> $this->table_prefix.'ktp',
						"status" 		=> $this->table_prefix.'status',
					  ];

		if (@$post['action'] == 'filter')
		{
			$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = "DATE(saksi_lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

					} else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			$where[$this->table_prefix.'status <>'] = '99';
		}

		$keys 		= array_keys($search);
		$order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

		$select		= 'saksi_id, '.implode(',', $search);

		$count 		= $this->m_global->count_data_all( $this->table_db, $join, $where, $where_e );
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


		$data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, $where_e, $order, $start, $length);

		$i = 1 + $start;

		$status = [
					'0' => '<span class="uk-badge uk-badge-warning">InActive</span>',
					'1' => '<span class="uk-badge uk-badge-primary">Active</span>',
					'2' => '<span class="uk-badge uk-badge-danger">Used</span>',
					'99' => '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
				  ];

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
				$rows->saksi_nama,
				$rows->saksi_ktp,
				$rows->saksi_alamat,
				$status[$rows->saksi_status],
				$this->_button($rows->saksi_id, $rows->saksi_status, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

	public function _button($id, $status, $show = false)
	{
		$id = strEncrypt($id);
		$button = '';

		if($show) {
			$c_status = '<a data-uk-tooltip title="Change Status" class="md-btn ' . (($status == '1') ? 'md-btn-success' : (($status == '99') ? 'md-btn-danger' : '')) . ' md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(1, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/'.($status == 1 ? '0' : '1' ) ).'">
						<i class="' . (($status == '1') ? 'uk-icon-check' : (($status == '99') ? 'uk-icon-warning' : 'uk-icon-ban')) . ' uk-icon-small"></i>
					  </a>';
			$edit 	= '<a data-uk-tooltip title="Edit" class="ajaxify md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="'.base_url('admin/saksi/show_edit').'/'.$id.'"><i class="uk-icon-pencil uk-icon-small"></i></a>';
			$delete = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(2, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/99/'.($status == 99 ? '/true' : '' )).'"><i class="uk-icon-trash uk-icon-small"></i></a>';

			$button = $c_status . $edit . $delete;
		} else {
			$button = '-';
		}

		return $button;
	}

	public function show_add()
	{
		$data['name'] 		 = $this->name;
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'saksi' => base_url('admin/saksi'), 'Tambah' => base_url('admin/saksi/show_add')];
		$data['plugin'] 	 = ['kendo_ui','dropify', 'inputmask'];
		$this->template->display('saksi/add', $data);
	}

	public function show_edit($id)
	{
		$data['name'] 		 = $this->name;
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'saksi' => base_url('admin/saksi'), 'Edit' => base_url('admin/saksi/show_edit').'/'.$id];
		$data['record']		 = $this->m_global->get_data_all('saksi', null, [strEncrypt('saksi_id', TRUE) => $id]);
		$data['plugin'] 	 = ['kendo_ui','dropify', 'inputmask'];
		$data['id'] 		 = $id;

		$data['region'] = $this->m_global->get_data_all('region', null, ['region_status' => '1']);

		$this->template->display('saksi/edit', $data);
	}

	public function action_add()
	{
		$result = [];
		$post 	= $this->input->post();

		//$this->form_validation->set_rules('region', 'Region', 'trim|required');
		//$this->form_validation->set_rules('name', 'Name', 'trim|required|is_unique[saksi.saksi_name]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			$data 	= [
						'saksi_nama'		=> $post['nama'],
						'saksi_ktp' 			=> $post['ktp'],
						'saksi_tanggal_lahir'			=> $post['tanggal_lahir'],
						'saksi_tempat_lahir'			=> $post['tempat_lahir'],
						'saksi_alamat'			=> $post['alamat'],
						'saksi_status'			=> $post['status']
					  ];

			$res = $this->m_global->insert('saksi', $data);

			if($res) {
				$result['msg'] = 'Data berhasil ditambahkan !';
				$result['sts'] = '1';
			} else {
				$result['msg'] = 'Data gagal ditambahkan !';
				$result['sts'] = '0';
			}
		} else {
			$result['msg'] = validation_errors();
			$result['sts'] = '99';
		}

		echo json_encode($result);
	}

	public function action_edit($id)
	{
		$result = [];
		$post 	= $this->input->post();

		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		if ($this->form_validation->run() == TRUE){
			$check_name = $this->m_global->get_data_all('saksi', null, [strEncrypt('saksi_id', TRUE) . ' <> ' => $id, 'saksi_nama' => $post['nama']]);

			if ($check_name) {
				$result['msg'] = 'Nama sudah ada !';
				$result['sts'] = '0';
			} else {
				$data 	= [
						'saksi_nama'		=> $post['nama'],
						'saksi_ktp' 			=> $post['ktp'],
						'saksi_tanggal_lahir'			=> date('Y-m-d', strtotime($post['tanggal_lahir'])),
						'saksi_tempat_lahir'			=> $post['tempat_lahir'],
						'saksi_alamat'			=> $post['alamat'],
					  ];

				$res = $this->m_global->update('saksi', $data, [strEncrypt('saksi_id', TRUE) => $id]);
				if($res) {
					$result['msg'] = 'Data berhasil dirubah !';
					$result['sts'] = '1';
				} else {
					$result['msg'] = 'Data gagal dirubah !';
					$result['sts'] = '0';
				}
			}
		} else {
			$result['msg'] = validation_errors();
			$result['sts'] = '99';
		}

		echo json_encode($result);
	}

	public function change_status_by( $id, $status, $stat = FALSE )
	{
		
		if ( $stat ) {
			$result = $this->m_global->delete( $this->table_db, [strEncrypt($this->table_prefix.'id', true) => $id] );
		} else {
			$result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [strEncrypt($this->table_prefix.'id', true) => $id]);
		}

		if ( $result ) {
			$data['status'] = 1;
		} else {
			$data['status'] = 0;
		}

		echo json_encode( $data );
	}

}

/* End of file saksi.php */
/* Location: ./application/modules/admin/controllers/saksi.php */