<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-content">
		<div class="md-card">
			<div class="md-card-toolbar">
				<div class="md-card-toolbar-actions">
					<i class="md-icon material-icons md-card-toggle">&#xE316;</i>
				</div>
				<h3 class="md-card-toolbar-heading-text">
					Filter
				</h3>
			</div>
			<div class="md-card-content filter">
				<div class="uk-grid">
					<div class="uk-width-medium-1-3 uk-margin-small-bottom uk-row-first">
						<label>Tanggal Awal: </label> <input name="periode[0]" type="text" class="md-input form-filter select-filter" id="text3">
					</div>
					<div class="uk-width-medium-1-3 uk-margin-small-bottom uk-row-first">
						<label>Tanggal Akhir: </label> <input name="periode[1]" type="text" class="md-input form-filter select-filter" id="text4">
					</div>
					<div class="uk-width-medium-1-3 uk-margin-small-bottom uk-row-first">
						<select name="flag" class="form-filter select-filter" select-style select-style-bottom>
							<option value="''">Status</option>
							<option value="'3'">Proses Ahu</option>
							<option value="'4'">Proses Sertifikat</option>
							<option value="'5'">Terdaftar</option>
						</select>
					</div>
				</div>
				<div class="uk-grid uk-margin-medium-top uk-text-center">
					<div class="uk-width-1-1">
						<a href="<?= $link ?>" class="export_excel"><button type="button" class="filter-submit md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Cari</button></a>
						<button type="button" class="filter-reset md-btn md-btn-small md-btn-danger md-btn-wave-light waves-effect waves-button waves-light">Reset</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		App.datepicker();
		App.datepicker('.datepicker-month', true);
		App.date_range('#text3', '#text4');
		$('#text3,text4').focus(function() {
			$('#form1').find('input, select, textarea').not('#text3,#text4').val('');
		});
		$('.export_excel').on('click', function(e){
			e.preventDefault();

			var url = $(this).attr('href'),
			    data = $('.filter').find('input.form-filter, select.form-filter').serialize();

			window.open(url+'?'+data,'_blank' );
		});
	});
</script>
