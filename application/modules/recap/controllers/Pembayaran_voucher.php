<?php
defined('BASEPATH') OR exit('No direct script access allowed');

   require_once APPPATH.'libraries/spout/Autoloader/autoload.php';

	// using spout class must be outside class codeigniter
	use Box\Spout\Reader\ReaderFactory;
	use Box\Spout\Writer\WriterFactory;
	use Box\Spout\Common\Type;
	use Box\Spout\Writer\Style\StyleBuilder;
	use Box\Spout\Writer\Style\Color;
	use Box\Spout\Writer\Style\Border;
	use Box\Spout\Writer\Style\BorderBuilder;

class Pembayaran_voucher extends CI_Controller {

	private $table_db       = 'entry_data'; //nama table
	private $table_prefix   = 'ed_'; //nama awal pada field
	private $prefix 		= 'recap/pembayaran_voucher';
	private $name 			= 'Pembayaran Voucher';

	public function __construct(){
		parent::__construct();
		//Do your magic here
	}

	public function index(){
		$data['name'] 	= "Pembayaran Voucher";
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables', 'datatables_fixcolumns','kendo_ui'];

		$data['link']	= base_url('recap/pembayaran_voucher/export_excel2');

		$this->template->display('recap/pembayaran_voucher/index', $data);
	}
	
	public function export_excel(){
		ob_end_clean();

		$get = $this->input->get();
		$where = "";
		if(in_array(user_data()->role_name, ['Adminwin1'])){
			$arrcabangnotaris = $this->m_global->get_data_all('notaris', [['notaris_admin','notaris_admin.na_notaris = notaris.notaris_id','left']], ['na_admin' => user_data()->user_id, 'notaris_status' => '1']);
			foreach($arrcabangnotaris as $a => $b){
				$arrcabangid[$a] = $b->notaris_cabang_id;
			}
			$cabangid = join('|', $arrcabangid);
			$where .=" AND ed.ed_cabang IN('".str_replace("|","', '",$cabangid)."') "; 
			
		}
		if ($get['periode']) {
			if (!empty($get['periode'][0]) && !empty($get['periode'][1])) {
				$where .= "AND DATE(led.log_ed_date) BETWEEN ".$this->db->escape(date('Y-m-d',strtotime($get['periode'][0])))." AND ".$this->db->escape(date('Y-m-d',strtotime($get['periode'][1])));
			}
			else if (!empty($get['periode'][0]) && empty($get['periode'][1])) {
				$where .= "AND DATE(led.log_ed_date) BETWEEN ".$this->db->escape(date('Y-m-d',strtotime($get['periode'][0])))." AND '2999-12-12'";
			} else if (empty($get['periode'][0]) && !empty($get['periode'][1])) {
				$where .= "AND DATE(led.log_ed_date) BETWEEN '2017-01-01 ' AND ".$this->db->escape(date('Y-m-d',strtotime($get['periode'][1])));
				
			}
		}
		$query = "SELECT ed.ed_id,reg.region_name AS region,cb.cabang_name AS cabang,ed.ed_nomor_kontrak AS nomor_kontrak,kon.kon_nama AS nama_konsumen,nor.notaris_nama AS nama_notaris,ak.akta_waktu AS tanggal_akta_fidusia,ak.akta_nomor AS no_akta,pemb.pemb_tanggal AS tgl_voucher,ak.akta_voucher AS nomor_voucher,pemb.pemb_pnbp AS pnbp,ed.ed_ntpn AS ntpn,led.log_ed_action as status FROM entry_data ed INNER JOIN cabang cb ON cb.cabang_id = ed.ed_cabang INNER JOIN akta ak ON ak.ed_id = ed.ed_id INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id INNER JOIN region reg ON reg.region_id = cb.cabang_region_id INNER JOIN konsumen kon ON kon.kon_id = ed.ed_konsumen_id INNER JOIN notaris nor ON nor.notaris_id = ak.notaris_id INNER JOIN pembiayaan pemb ON pemb.pemb_id = ed.ed_pembiayaan_id WHERE led.log_ed_flag = ".$get['flag']." AND ed.ed_flag = ".$get['flag']."".$where;
		$data = $this->db->query($query);
		include('application/libraries/PHPExcel.php'); 
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="pembayaran_voucher.xls"');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
		$styleArray = array(
                    'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                  );
		$styleArray1 = array(
                     'borders' => array(
                    'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ),
                     ),
                    );
                    //background
                    $styleArray12 = array(
                    'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array(
                    'rgb' => 'FC1919',
                    ),
                    ),
				);
		$objPHPExcel->getActiveSheet()->setCellValue('A2', 'LAMPIRAN SURAT NOMOR :');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'TANGGAL :'.$get['periode'][0]);
		$objPHPExcel->getActiveSheet()->setCellValue('A4', 'SAMPAI :'.$get['periode'][1]);
		$objPHPExcel->getActiveSheet()->setCellValue('A5', 'JUMLAH VOUCHER :'.$data->num_rows());
		$objPHPExcel->getActiveSheet()->setCellValue('A6', 'JUMLAH PEMBAYARAN :');
		$objPHPExcel->getActiveSheet()->getStyle('D:D')->getNumberFormat()->setFormatCode('0');
		$objPHPExcel->getActiveSheet()->getStyle('J:J')->getNumberFormat()->setFormatCode('0');

		$row = 8;
		$rowawal = $row;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':M'.$row)->applyFromArray($styleArray);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':M'.$row)->applyFromArray($styleArray1);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':M'.$row)->applyFromArray($styleArray12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
    	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(50);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'No');
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, 'Region');
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, 'Cabang');
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, 'No. Kontrak');
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, 'Nama Konsumen');
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, 'Nama Notaris');
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, 'Tgl. Akta Fidusia');
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, 'No. Akta Fidusia');
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, 'Tgl. Voucher');
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, 'Nomor Voucher');
		$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, 'PNBP');
		$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, 'NTPN');
		$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, 'Status');
		$row++;
		$objPHPExcel->getActiveSheet()->freezePane('A'.$row);
		foreach ($data->result() as $key => $dt) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, ($row - $rowawal));
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $dt->region);
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $dt->cabang);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $dt->nomor_kontrak);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $dt->nama_konsumen);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $dt->nama_notaris);  
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, date('d-m-Y', strtotime($dt->tanggal_akta_fidusia)));
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $dt->no_akta);
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, $dt->nomor_voucher);
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, $dt->pnbp);
			$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, $dt->ntpn);
			$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, 'Sertifikat');
			$row++;
		}

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// Write file to the browser
		$writer->save('php://output');
	}
	public function export_excel2()
	{
		ob_end_clean();

		$get = $this->input->get();
		if ($get['flag'] == null) {
			$get['flag'] = "' '";
		}
		$where = "";

		if(in_array(user_data()->role_name, ['Adminwin1'])){
			$arrcabangnotaris = $this->m_global->get_data_all('notaris', [['notaris_admin','notaris_admin.na_notaris = notaris.notaris_id','left']], ['na_admin' => user_data()->user_id, 'notaris_status' => '1']);
			foreach($arrcabangnotaris as $a => $b){
				$arrcabangid[$a] = $b->notaris_cabang_id;
			}
			$cabangid = join('|', $arrcabangid);
			$where .=" AND ed.ed_cabang IN('".str_replace("|","', '",$cabangid)."') "; 
			
		}
		if ($get['periode']) {
			if (!empty($get['periode'][0]) && !empty($get['periode'][1])) {
				$where .= "AND DATE(led.log_ed_date) BETWEEN ".$this->db->escape(date('Y-m-d',strtotime($get['periode'][0])))." AND ".$this->db->escape(date('Y-m-d',strtotime($get['periode'][1])));
			}
			else if (!empty($get['periode'][0]) && empty($get['periode'][1])) {
				$where .= "AND DATE(led.log_ed_date) BETWEEN ".$this->db->escape(date('Y-m-d',strtotime($get['periode'][0])))." AND '2999-12-12'";
			} else if (empty($get['periode'][0]) && !empty($get['periode'][1])) {
				$where .= "AND DATE(led.log_ed_date) BETWEEN '2017-01-01 ' AND ".$this->db->escape(date('Y-m-d',strtotime($get['periode'][1])));
				
			}
		}
		$query = "SELECT @rownum:=@rownum+1 as nomer,reg.region_name AS region,cb.cabang_name AS cabang,ed.ed_nomor_kontrak AS nomor_kontrak,kon.kon_nama AS nama_konsumen,nor.notaris_nama AS nama_notaris,ak.akta_waktu AS tanggal_akta_fidusia,ak.akta_nomor AS no_akta,pemb.pemb_tanggal AS tgl_voucher,ak.akta_voucher AS nomor_voucher,pemb.pemb_pnbp AS pnbp,ed.ed_ntpn AS ntpn,ed.ed_nomor_fidusia,ed.ed_tanggal_fidusia,led.log_ed_action as status FROM (SELECT @rownum := 0) as rownum,entry_data ed INNER JOIN cabang cb ON cb.cabang_id = ed.ed_cabang INNER JOIN akta ak ON ak.ed_id = ed.ed_id INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id INNER JOIN region reg ON reg.region_id = cb.cabang_region_id INNER JOIN konsumen kon ON kon.kon_id = ed.ed_konsumen_id INNER JOIN notaris nor ON nor.notaris_id = ak.notaris_id INNER JOIN pembiayaan pemb ON pemb.pemb_id = ed.ed_pembiayaan_id WHERE led.log_ed_flag = ".$get['flag']." AND ed.ed_flag = ".$get['flag']." ".$where;
		$data = $this->db->query($query);

		$border = (new BorderBuilder())
			       ->setBorderBottom( Border::WIDTH_THICK)
			       ->setBorderTOP( Border::WIDTH_THICK)
			       ->setBorderRIGHT( Border::WIDTH_THICK)
			       ->setBorderLEFT( Border::WIDTH_THICK)
			       ->build();

		$style = (new StyleBuilder())
           ->setFontBold()
           ->setFontSize(8)
           ->setShouldWrapText()
           ->setBorder($border)
           ->setBackgroundColor(Color::WHITE)
           ->build();

         	$style2 = (new StyleBuilder())
           ->setFontBold()
           ->setFontSize(8)
           ->setShouldWrapText()
           ->setBorder($border)
           ->setBackgroundColor(Color::YELLOW)
           ->build();
           // style adding border
        $stylecoloumn = (new StyleBuilder())
           ->setFontSize(8)
           ->setShouldWrapText()
           ->setBorder($border)
           ->build();

 		$write = WriterFactory::create(Type::XLSX);

 		// set header and info excel
 		$info 		 = ['LAMPIRAN SURAT NOMOR','TANGGAL','SAMPAI','JUMLAH VOUCHER','JUMLAH PEMBAYARAN'];
 		$infoContent = ['',$get['periode'][0],$get['periode'][0],$data->num_rows(),''];
 		$title 		 = ['No', 'Region', 'Cabang', 'No.Kontrak', 'Nama Konsumen', 'Nama Notaris',
                  'Tgl.Akta Fidusia','No. Akta Fidusia', 'Tgl. Voucher', 'Nomor Voucher', 'PNBP', 'NTPN','Nomor Sertifikat','Tanggal Sertifikat', 'Status'];
        $break 		 = ['',''];
       $write->openToBrowser('pembayaran_voucher.xlsx');
       $write->addRowWithStyle($info,$style2);
       $write->addRowWithStyle($infoContent,$style2);
       $write->addRow($break);
       $write->addRowWithStyle($title,$style2);

       foreach ($data->result_array() as $key => $value) {

       		$write->addRowWithStyle($value,$style);
       }

       $write->close();
	}

}

/* End of file Invoice.php */
/* Location: ./application/modules/invoice/controllers/Invoice.php */