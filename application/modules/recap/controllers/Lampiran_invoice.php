<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lampiran_invoice extends CI_Controller {

	private $table_db       = 'entry_data'; //nama table
	private $table_prefix   = 'ed_'; //nama awal pada field
	private $prefix 		= 'recap/rekap_laporan';
	private $name 			= 'Rekap laporan';

	public function __construct(){
		parent::__construct();
		//Do your magic here
	}

	public function index(){
		$data['name'] 	= "Rekap Laporan";
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables', 'datatables_fixcolumns','kendo_ui'];

		$data['link']	= base_url('recap/lampiran_invoice/export_excel');

		$this->template->display('recap/lampiran_invoice/index', $data);
	}
	
	public function export_excel(){
		ob_clean();
		$get = $this->input->get();
		if(@$get['invoice'] != ""){
			$where[] = "invoice_nomor = '$get[invoice]'";
		}
		$where[] = "1 = 1";
		if(@$where != "") @$where = " ".join(" AND ",  $where);
		$data = $this->db->query("SELECT * FROM v_entry_data a LEFT JOIN cabang b ON b.cabang_id = a.ed_cabang LEFT JOIN akta c ON c.ed_id = a.ed_id LEFT JOIN region e ON e.region_id = b.cabang_region_id LEFT JOIN notaris f ON f.notaris_id = c.notaris_id LEFT JOIN referensi g ON CONCAT(g.referensi_id, '|', g.referensi_tipe) = CONCAT(a.ed_flag, '|FLAG') LEFT JOIN ed_invoice h ON h.ed_id = a.ed_id LEFT JOIN invoice i ON i.invoice_id = h.invoice_id LEFT JOIN pembiayaan j ON j.pemb_id = a.ed_pembiayaan_id WHERE ed_flag IN('7') AND ".@$where." ORDER BY a.ed_cabang");
		
		include('application/libraries/PHPExcel.php');											
		// We'll be outputting an excel file                                                    
		header('Content-type: application/vnd.ms-excel');                                       
                                                                                                
		// It will be called file.xls
		header('Content-Disposition: attachment; filename="rekap_laporan.xls"');

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
		$styleArray = array(
                    'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                  );
		$styleArray1 = array(
                     'borders' => array(
                    'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ),
                     ),
                    );
                    //background
                    $styleArray12 = array(
                    'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array(
                    'rgb' => 'FFEC8B',
                    ),
                    ),
									);
		$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
    	$objPHPExcel->getActiveSheet()->getStyle('A2:N2')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:N2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', 'SENTRALISASI PENDAFTARAN FIDUSIA PT WAHANA OTTOMITRA MULTIARTHA, Tbk');
		$objPHPExcel->getActiveSheet()->setCellValue('A4', 'LAMPIRAN INVOICE');
		$objPHPExcel->getActiveSheet()->setCellValue('L4', 'e-WIN  Fidusia');
		$objPHPExcel->getActiveSheet()->setCellValue('A5', 'NOMOR   : '.@$get['invoice'][0]);
		$objPHPExcel->getActiveSheet()->setCellValue('A5', 'TANGGAL : ');
		$row = 7;
		$rowawal = $row;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':N'.$row)->applyFromArray($styleArray);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':N'.$row)->applyFromArray($styleArray1);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':N'.$row)->applyFromArray($styleArray12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'No');
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, 'Region');
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, 'Cabang');
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, 'No. Kontrak');
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, 'Nama Konsumen');
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, 'Nama Notaris');
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, 'Tgl. Akta Fidusia');
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, 'No. Akta Fidusia');
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, 'Tgl. Sertifikat');
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, 'No. Sertifikat');
		$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, 'Biaya PNBP');
		$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, 'Biaya Jasa');
		$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, 'Total Biaya');
		$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row, 'Status');
		$row++;
		$objPHPExcel->getActiveSheet()->freezePane('A'.$row);
		foreach ($data->result() as $key => $dt) {
			//$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':P'.$row)->applyFromArray($styleArray);
      		//$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':P'.$row)->applyFromArray($styleArray1);
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, ($row - $rowawal));
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $dt->region_name);
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $dt->cabang_name);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $dt->ed_nomor_kontrak);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $dt->kon_nama);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $dt->notaris_nama);
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, date('d-m-Y', strtotime($dt->akta_waktu)));
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $dt->akta_nomor);
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, date('d-m-Y', strtotime($dt->ed_tanggal_fidusia)));
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, $dt->ed_nomor_fidusia);
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, $dt->pemb_pnbp);
			$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row, $dt->referensi_nama);
			$row++;
		
		}
		$row++;

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// Write file to the browser
		$writer->save('php://output');
	}

}

/* End of file Invoice.php */
/* Location: ./application/modules/invoice/controllers/Invoice.php */