<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_minuta extends CI_Controller {

	private $table_db       = 'entry_data'; //nama table
	private $table_prefix   = 'ed_'; //nama awal pada field
	private $prefix 		= 'recap/daftar_minuta';
	private $name 			= 'Daftar Minuta';

	public function __construct(){
		parent::__construct();
		//Do your magic here
	}

	public function index(){
		$data['name'] 	= "Daftar Minuta";
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables', 'datatables_fixcolumns','kendo_ui'];

		$data['link']	= base_url('recap/daftar_minuta/export_excel');

		$this->template->display('recap/daftar_minuta/index', $data);
	}
	
	public function export_excel(){
		ob_end_clean();
		$where = "";
		$get = $this->input->get();
		if ($get['periode']) {
			if (!empty($get['periode'][0]) && !empty($get['periode'][1])) {
				$where .= "AND DATE(led.log_ed_date) BETWEEN ".$this->db->escape(date('Y-m-d',strtotime($get['periode'][0])))." AND ".$this->db->escape(date('Y-m-d',strtotime($get['periode'][1])));
			}
			else if (!empty($get['periode'][0]) && empty($get['periode'][1])) {
				$where .= "AND DATE(led.log_ed_date) BETWEEN ".$this->db->escape(date('Y-m-d',strtotime($get['periode'][0])))." AND '2999-12-12'";
			} else if (empty($get['periode'][0]) && !empty($get['periode'][1])) {
				$where .= "AND DATE(led.log_ed_date) BETWEEN '2017-01-01 ' AND ".$this->db->escape(date('Y-m-d',strtotime($get['periode'][1])));
				
			}
		}
		$quer = "SELECT ed.ed_id, reg.region_name AS region, cb.cabang_name AS cabang, ed.ed_nomor_kontrak AS nomor_kontrak, kon.kon_nama AS nama_konsumen, nor.notaris_nama AS nama_notaris, ak.akta_waktu AS waktu_akta, ak.akta_nomor AS nomor_akta FROM entry_data ed INNER JOIN cabang cb ON cb.cabang_id = ed.ed_cabang INNER JOIN akta ak ON ak.ed_id = ed.ed_id INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id INNER JOIN region reg ON reg.region_id = cb.cabang_region_id INNER JOIN notaris nor ON nor.notaris_id = ak.notaris_id INNER JOIN referensi ref ON ref.referensi_id = ed.ed_flag INNER JOIN konsumen kon ON kon.kon_id = ed.ed_konsumen_id WHERE led.log_ed_flag = '3' ".$where." ORDER BY ed.ed_cabang ASC";
		$data = $this->db->query($quer);
		include('application/libraries/PHPExcel.php');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="daftar_minuta.xls"');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
		$styleArray = array(
                    'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                  );
		$styleArray1 = array(
                     'borders' => array(
                    'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ),
                     ),
                    );
                    //background
                    $styleArray12 = array(
                    'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array(
                    'rgb' => 'FFEC8B',
                    ),
                    ),
									);
		$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
    	$objPHPExcel->getActiveSheet()->getStyle('A2:K2')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:K2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', 'SENTRALISASI PENDAFTARAN FIDUSIA PT WAHANA OTTOMITRA MULTIARTHA, Tbk');
		$objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);
    	$objPHPExcel->getActiveSheet()->getStyle('A3:K3')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->mergeCells('A3:K3');
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'DAFTAR MINUTA AKTA JAMINAN FIDUSIA');
		$objPHPExcel->getActiveSheet()->setCellValue('A4', 'JUMLAH KATA :');
		$objPHPExcel->getActiveSheet()->setCellValue('A5', 'TANGGAL :');
		//$objPHPExcel->getActiveSheet()->setCellValue('A5', 'PERIODE : '.@$get['periode'][0].' - '.@$get['periode'][1]);
		$row = 7;
		$rowawal = $row;
		$objPHPExcel->getActiveSheet()->getStyle('D:D')->getNumberFormat()->setFormatCode('0');
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.($row+1))->applyFromArray($styleArray);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.($row+1))->applyFromArray($styleArray1);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.($row+1))->applyFromArray($styleArray12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(60);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':A'.($row+1));
		$objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':B'.($row+1));
		$objPHPExcel->getActiveSheet()->mergeCells('C'.$row.':C'.($row+1));
		$objPHPExcel->getActiveSheet()->mergeCells('D'.$row.':D'.($row+1));
		$objPHPExcel->getActiveSheet()->mergeCells('E'.$row.':E'.($row+1));
		$objPHPExcel->getActiveSheet()->mergeCells('F'.$row.':F'.($row+1));
		$objPHPExcel->getActiveSheet()->mergeCells('G'.$row.':G'.($row+1));
		$objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':H'.($row+1));
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'No');
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, 'Region');
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, 'Cabang');
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, 'No. Kontrak');
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, 'Nama Konsumen');
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, 'Nama Notaris');
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, 'Tgl. Akta Fidusia');
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, 'No. Akta Fidusia');
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, 'WOM');
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, 'WIN');
		$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, 'NOTARIS	');
		$row++;
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, 'Tgl :');
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, 'Tgl :');
		$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, 'Tgl :');
		$row++;
		$objPHPExcel->getActiveSheet()->freezePane('A'.$row);
		$num = 0;
		foreach ($data->result() as $key => $dt) {
			$num++;
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $num);
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $dt->region);
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $dt->cabang);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $dt->nomor_kontrak);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $dt->nama_konsumen);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $dt->nama_notaris);  
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, date('d-m-Y', strtotime($dt->waktu_akta)));
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $dt->nomor_akta);
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, '');
			$row++;
		}

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// Write file to the browser
		$writer->save('php://output');
	}
	
}

/* End of file Invoice.php */
/* Location: ./application/modules/invoice/controllers/Invoice.php */