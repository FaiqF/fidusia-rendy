<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_invoice extends CI_Controller {

	private $table_db       = 'entry_data'; //nama table
	private $table_prefix   = 'ed_'; //nama awal pada field
	private $prefix 		= 'recap/rekap_invoice';
	private $name 			= 'Rekap Invoice';

	public function __construct(){
		parent::__construct();	
		//Do your magic here
	}

	public function index(){
		$data['name'] 	= "Rekap Invoice";
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables', 'datatables_fixcolumns','kendo_ui'];

		$data['link']	= base_url('recap/rekap_invoice/export_excel');

		$this->template->display('recap/rekap_invoice/index', $data);
	}
	
	public function export_excel(){
		ob_clean();
		$get = $this->input->get();
		if(@$get['invoice'] != ""){
			$where[] = "invoice_nomor = '$get[invoice]'";
		}
		$where[] = "1 = 1";
		if(@$where != "") @$where = " ".join(" AND ",  $where);
		$data = $this->db->query("SELECT COUNT(a.ed_id) AS 'total', e.region_name, b.cabang_name FROM v_entry_data a LEFT JOIN cabang b ON b.cabang_id = a.ed_cabang LEFT JOIN akta c ON c.ed_id = a.ed_id LEFT JOIN region e ON e.region_id = b.cabang_region_id LEFT JOIN notaris f ON f.notaris_id = c.notaris_id LEFT JOIN referensi g ON CONCAT(g.referensi_id, '|', g.referensi_tipe) = CONCAT(a.ed_flag, '|FLAG') LEFT JOIN ed_invoice h ON h.ed_id = a.ed_id LEFT JOIN invoice i ON i.invoice_id = h.invoice_id WHERE ed_flag IN('7') AND ".@$where." GROUP BY a.ed_cabang ORDER BY a.ed_cabang");
		// echo $this->db->last_query();exit();
		include('application/libraries/PHPExcel.php');											
		// We'll be outputting an excel file                                                    
		header('Content-type: application/vnd.ms-excel');                                       
                                                                                                
		// It will be called file.xls
		header('Content-Disposition: attachment; filename="rekap_invoice.xls"');

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
		$styleArray = array(
                    'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                  );
		$styleArray1 = array(
                     'borders' => array(
                    'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ),
                     ),
                    );
                    //background
                    $styleArray12 = array(
                    'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array(
                    'rgb' => 'FFEC8B',
                    ),
                    ),
									);
		$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
    	$objPHPExcel->getActiveSheet()->getStyle('A2:N2')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->mergeCells('A2:N2');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', 'SENTRALISASI PENDAFTARAN FIDUSIA PT WAHANA OTTOMITRA MULTIARTHA, Tbk');
		$objPHPExcel->getActiveSheet()->setCellValue('A4', 'REKAP INVOICE');
		$objPHPExcel->getActiveSheet()->setCellValue('L4', 'e-WIN  Fidusia');
		$objPHPExcel->getActiveSheet()->setCellValue('A5', 'NOMOR   : '.@$get['invoice'][0]);
		$objPHPExcel->getActiveSheet()->setCellValue('A5', 'TANGGAL : ');
		$row = 7;
		$rowawal = $row;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($styleArray);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($styleArray1);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($styleArray12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'No');
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, 'Region');
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, 'Nama Cabang');
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, 'Jumlah Akta Fidusia');
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, 'Jumlah Sertifikat');
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, 'Biaya PNBP');
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, 'Biaya Jasa');
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, 'Total Biaya');
		$row++;
		$objPHPExcel->getActiveSheet()->freezePane('A'.$row);
		$total = 0;
		foreach ($data->result() as $key => $dt) {
			
			//$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':P'.$row)->applyFromArray($styleArray);
      		//$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':P'.$row)->applyFromArray($styleArray1);
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, ($row - $rowawal));
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $dt->region_name);
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $dt->cabang_name);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $dt->total);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $dt->total);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, '');
			$row++;
			$total += $dt->total;
		}
		$row++;
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, 'JUMLAH TOTAL');
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $total);
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $total);
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, '');
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, '');
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, '');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// Write file to the browser
		$writer->save('php://output');
	}

}

/* End of file Invoice.php */
/* Location: ./application/modules/invoice/controllers/Invoice.php */