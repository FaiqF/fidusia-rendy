<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends MX_Controller {

	private $table_db       = 'entry_data';
	private $table_prefix   = 'ed_';
	private $prefix 		= 'report';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$data['name'] 	= "Report";
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin'] = ['datatables', 'datatables_fixcolumns','kendo_ui'];

		$data['export']		= [
								['name' => 'Export Excel', 'link' => base_url('report/export_excel')],
								['name' => 'Export PDF', 'link' => base_url('report/export_pdf')],
							  ];
		$data['cabang'] = $this->m_global->get_data_all('cabang',null,null,'cabang_id,cabang_name');

		$this->template->display('index', $data);
	}

	public function bypass($id)
	{
		$update = array('ed_flag' => '3');
		$entry['bypass'] = $this->m_global->update('entry_data', $update, [strEncrypt('ed_id', TRUE) => $id]);
		redirect(base_url()."report/show_preview/".$id);

	}

	public function rollBackDiterima($id)
	{
		$update = array('ed_flag' => '1');
		$entry['bypass'] = $this->m_global->update('entry_data', $update, [strEncrypt('ed_id', TRUE) => $id]);
		redirect(base_url()."report/show_preview/".$id);

	}
	
	public function bypassTerkirim($id)
	{
		$flag = '2';
		$note = '';
		$action = 'Akte Fidusia';
		$param_id = $this->m_global->get_data_all('entry_data', null, [strEncrypt('ed_id', TRUE) => $id]);
		$entry_data = array('log_ed_id' 		   => $param_id[0]->ed_id, 
							'log_ed_nomor_kontrak' => $param_id[0]->ed_nomor_kontrak, 
							'log_ed_note' 		   => $note,
							'log_ed_action'		   => $action, 
							'log_ed_user_id' 	   => user_data()->user_id, 
							'log_ed_user_role' 	   => user_data()->user_role, 
							'log_ed_flag' 		   => $flag);
		$entry['entry'] = $this->m_global->insert('log_entry_data', $entry_data);

		$update = array('ed_flag' => $flag);
		$entry['bypass'] = $this->m_global->update('entry_data', $update, [strEncrypt('ed_id', TRUE) => $id]);
		redirect(base_url()."report/show_preview/".$id);

	}
	public function bypassDiterima($id)
	{
		$flag = '3';
		$note = '';
		$action = 'Proses Ahu';
		$param_id = $this->m_global->get_data_all('entry_data', null, [strEncrypt('ed_id', TRUE) => $id]);
		$entry_data = array('log_ed_id' 		   => $param_id[0]->ed_id, 
							'log_ed_nomor_kontrak' => $param_id[0]->ed_nomor_kontrak, 
							'log_ed_note' 		   => $note,
							'log_ed_action'		   => $action, 
							'log_ed_user_id' 	   => user_data()->user_id, 
							'log_ed_user_role' 	   => user_data()->user_role, 
							'log_ed_flag' 		   => $flag);
		$entry['entry'] = $this->m_global->insert('log_entry_data', $entry_data);

		$update = array('ed_flag' => $flag);
		$entry['bypass'] = $this->m_global->update('entry_data', $update, [strEncrypt('ed_id', TRUE) => $id]);
		redirect(base_url()."report/show_preview/".$id);

	}
	public function export_word2($id)
	{

		include_once(APPPATH.'libraries/PHPWord/bootstrap.php');
		$select = "	`a`.*, `b`.*, `c`.*, `d`.*, `e`.saksi_nama AS saksi_nama1,
					`e`.saksi_ktp AS saksi_nik1,
					`e`.saksi_tempat_lahir AS saksi_tempat_lahir1,
					`e`.saksi_tanggal_lahir AS saksi_tanggal_lahir1,
					`e`.saksi_alamat AS saksi_alamat1,
					`f`.*,`g`.saksi_nama AS saksi_nama2,
					`g`.saksi_ktp AS saksi_nik2,
					`g`.saksi_tempat_lahir AS saksi_tempat_lahir2,
					`g`.saksi_tanggal_lahir AS saksi_tanggal_lahir2,
					`g`.saksi_alamat AS saksi_alamat2,
					`h`.*,
					`i`.wn_name as wilayah_kerja_notaris,
					`j`.pemb_nilai_benda_fidusia AS objek_benda,
					`v_entry_data`.*";
		$data = $this->m_global->get_data_all('v_entry_data', [['akta a', 'a.ed_id = v_entry_data.ed_id', 'left'],
															   ['notaris b', 'b.notaris_id = a.notaris_id','left'], 
															   ['wilayah_notaris c','c.wn_id = b.notaris_wilayah_kerja_id ','left'],
															   ['kuasa_direksi d','d.kd_id = b.notaris_kd_id1','left'],
															   ['saksi e','e.saksi_id = a.saksi_id1','left'],
															   ['daerah f','f.daerah_id = a.daerah_id','left'],
															   ['saksi g','g.saksi_id = a.saksi_id2','left'],
															   ['data_kendaraan h','h.dk_id = v_entry_data.ed_data_kendaraan_id','left'],
															   ['wilayah_notaris i','i.wn_id = b.notaris_wilayah_kerja_id','inner'],
															   ['pembiayaan j','j.pemb_id = v_entry_data.ed_data_kendaraan_id','inner']
															//    ['konsumen k','v_entry_data.ed_konsumen_id = k.kon_id','inner']
															   ]
															   , [strEncrypt('v_entry_data.ed_id', TRUE) => $id], $select);

		@$date = explode(" ", $data[0]->akta_waktu);
		@$tanggal = $date[0];
		@$waktu = $date[1];
		@$dknote = explode("-", @$data[0]->dk_covernote);
		@$dknote = @$dknote[2]."-".@$dknote[1]."-".@$dknote[0];
		// tanggal akta`
		@$var[0] = $this->change_tanggal($tanggal);
		@$var[1] = $this->change_time($waktu);
		@$var[2] = $this->change_tanggal($data[0]->akta_tanggal_lahir_penerima);
		@$var[3] = $this->change_tanggal($data[0]->akta_tanggal_penerima);
		@$var[4] = $this->change_tanggal($data[0]->akta_tanggal_lahir_nasabah);
		@$var[5] = $this->change_tanggal($data[0]->akta_tanggal_kuasa);
		@$var[6] = $this->change_tanggal($data[0]->akta_tanggal_pk);
		@$var[7] = $this->change_tanggal($data[0]->akta_perjanjian_kredit);
		@$var[8] = $this->change_tanggal($dknote);
		@$var[9] = $this->get_day(date("D",strtotime($data[0]->akta_waktu)));
		@$tanggal_lahir_saksi[1] = $this->change_tanggal($data[0]->saksi_tanggal_lahir1);
		@$tanggal_lahir_saksi[2] = $this->change_tanggal($data[0]->saksi_tanggal_lahir2);
		$terbilang_hutang = Terbilang($data[0]->akta_hutang_nasabah);
		$terbilang_jaminan = Terbilang($data[0]->akta_nilai_jaminan);
		$terbilang_objek = Terbilang($data[0]->objek_benda);
		$terbilang_hutang_pokok = Terbilang($data[0]->akta_hutang_pokok);
		@$tanggal_lahir_pasangan = $this->change_tanggal($data[0]->kon_tanggal_lahir_pasangan);

		// $day = change
		$notaris = explode(".", $data[0]->notaris_nama);
		$notaris = "( ".$notaris[0].", SH.,M.Kn. )"; 
		$d1 = $data[0]->akta_nomor;
		$d2 = $var[9];
		$d3 = $var[0];
		$d4 = $var[1];
		$d5 = $data[0]->notaris_nama;
		$d6 = $data[0]->wn_name;
		$d7 = $data[0]->akta_nama_penerima;
		$d8 = $data[0]->akta_tempat_lahir_penerima;
		$d9 = $var[2];
		$d10 = $data[0]->akta_pekerjaan_penerima;
		$d11 = $data[0]->akta_alamat_penerima;
		$d12 = $data[0]->akta_nik_penerima;
		$d13 = $var[3];
		$d14 = $data[0]->akta_nama_pembeban;
		$d15 = $data[0]->akta_tempat_lahir_nasabah;
		$d16 = $var[4];
		$d17 = $data[0]->akta_pekerjaan_nasabah;
		$d18 = $data[0]->daerah_kota.", ".$data[0]->akta_alamat_nasabah;
		$d19 = $data[0]->akta_nik_nasabah;
		$d20 = $var[5];
		$d21 = $data[0]->akta_nomor_kuasa;
		$d22 = $data[0]->kd_nama;
		$d23 = $data[0]->akta_nomor_pk;
		$d24 = $var[6];
		$d25 = $var[6];
		$d26 = $var[7];
		$d27 = number_format($data[0]->akta_hutang_pokok,null,"",".") . ", - ($terbilang_hutang_pokok rupiah)";
		$d28 = number_format($data[0]->akta_nilai_jaminan,null,"",".") . ", - ($terbilang_jaminan rupiah)";
		$d29 = "RODA 2";
		$d30 = $data[0]->dk_merk;
		$d31 = $data[0]->dk_tipe;
		$d32 = $data[0]->dk_tipe;
		$d33 = $data[0]->dk_tahun_pembuatan;
		$d34 = $data[0]->dk_tahun_pembuatan;
		$d35 = $data[0]->dk_no_rangka;
		$d36 = $data[0]->dk_no_mesin;
		$d37 = $data[0]->dk_no_bpkb;
		$d38 = number_format($data[0]->objek_benda,null,"",".").",- ($terbilang_objek rupiah)";
		$d39 = $data[0]->saksi_nama1;
		$d40 = $data[0]->saksi_tempat_lahir1;
		$d41 = $tanggal_lahir_saksi[1];
		$d42 = $data[0]->saksi_alamat1;
		$d43 = $data[0]->saksi_nama2;
		$d44 = $data[0]->saksi_tempat_lahir2;
		$d45 = $tanggal_lahir_saksi[2];
		$d46 = $data[0]->saksi_alamat2;
		$d47 = $data[0]->akta_pengadilan;
		$d48 = $data[0]->wilayah_kerja_notaris;
		$d49 = $notaris;
		$d50 = $data[0]->saksi_nik1;
		$d51 = $data[0]->saksi_nik2;
		// pasangan konsumen
		$d52 = $data[0]->kon_status_pernikahan;
		$d53 = $data[0]->kon_nama_pasangan;
		$d54 = $data[0]->kon_tempat_lahir_pasangan;
		$d55 = $tanggal_lahir_pasangan;
		$d56 = $data[0]->kon_alamat_pasangan;
		$d57 = $data[0]->kon_pekerjaan_pasangan;
		$d58 = $data[0]->kon_ktp_pasangan;
		if($data[0]->kon_status_pernikahan == '0') {
			$d59 = 'Suaminya';
			$d60 = 'Tuan';
		} else if($data[0]->kon_status_pernikahan == '1') {
			$d59 = 'Istrinya';
			$d50 = 'Nyonya';
		}

		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->getCompatibility()->setOoxmlVersion(14);
		$phpWord->getCompatibility()->setOoxmlVersion(15);
		$phpWord->setDefaultFontName('Courier New');
		$phpWord->setDefaultFontSize(12);
		$phpWord->setDefaultParagraphStyle(
		    array(
		    	'align'=> 'both',
		        // 'spacing'    => 170,
		        'indentation' => array('left'=>1000,'right'=>0,'firstLine'=>0),
		    	'space' => array('before' => 0, 'after' => 0, 'line' => 380, 'rule' => 'single'),
		    )
		);

		$targetFile = "./global/uploads/";
		$filename = 'akta_'.$data[0]->ed_nomor_kontrak.'.docx';


		// add style settings for the title and paragraph
		$paragraphStyleName = 'pStyle';
		$phpWord->addParagraphStyle($paragraphStyleName, array('indentation'=>array('left'=>2500,'right'=>0),'align'=>'center'));

		$pstyle1 = 'pStyle1';
		$phpWord->addParagraphStyle($pstyle1, array('indentation'=>array('left'=>1780,'right'=>-850),'align'=>'both'));
		$pstyle2 = 'pStyle2';
		$phpWord->addParagraphStyle($pstyle2, array('indentation'=>array('left'=>2100,'right'=>-850),'align'=>'both'));
		$pstyle3 = 'pStyle3';
		$phpWord->addParagraphStyle($pstyle3, array('indentation'=>array('left'=>2500,'right'=>-850),'align'=>'both'));
		$pstyle4 = 'pStyle4';
		$phpWord->addParagraphStyle($pstyle4, array('indentation'=>array('left'=>2750,'right'=>-850),'align'=>'both'));
		$pstyle5 = 'pStyle5';
		$phpWord->addParagraphStyle($pstyle5, array('indentation'=>array('left'=>2900,'right'=>-850),'align'=>'both'));


		$section = $phpWord->addSection(array('marginTop' =>700));

		$textrun = $section->addTextRun($paragraphStyleName);
		$textrun->addText("AKTA JAMINAN FIDUSIA",array('bold'=>true));
		$textrun->addTextBreak();
		$textrun->addText("Nomor : ".$d1.".",null);
		$textrun->addTextBreak();
		$textrun = $section->addTextRun($pstyle1);
		$textrun->addText("-Pada hari ini, ".$d2.",tanggal ".$d3."--------------------<w:br/>-Pukul ".$d4.".-------------------------------------");		
		$textrun->addText("<w:br/>-Hadir dihadapan saya, ");
		$textrun->addText($d5,array('bold'=>true));
		$textrun->addText(", Notaris di Daerah ".$d6.", dengan dihadiri saksi-saksi yang telah dikenal oleh saya, Notaris dan nama-namanya akan disebutkan pada bagian akhir akta ini.-------------------------------<w:br/>");
		$textrun->addText("-".$d7."",array('bold'=>true));
		$textrun->addText(", lahir di ".$d8.",------------",null);
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("pada tanggal ".$d9." Warga Negara Indonesia ".$d10.", bertempat tinggal di ".$d11.", pemegang Kartu Tanda Penduduk Nomor :-----------",null);
		$textrun->addText($d12.";---------------------------------",null);
		$textrun = $section->addTextRun($pstyle1);
			$textrun->addText("- menurut keterangannya dalam hal ini bertindak : ",null);
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("a. Berdasarkan ",null);
		$textrun->addText("Surat Kuasa Pembebanan Jaminan Fidusia",array('bold'=>true));
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText("yang dibuat dibawah tangan, bermeterai cukup, tertanggal ".$d13,array('bold'=>true),array('indentation'=>array('left'=>275,'right'=>-800)));
		$textrun->addText(", oleh dan karenanya sah bertindak untuk dan atas nama :------------------------",null,array('indentation'=>array('left'=>275,'right'=>-800)));
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText("- ".$d14.", Lahir di ".$d15." pada tanggal ",null);
		$textrun = $section->addTextRun($pstyle4);
		$textrun->addText($d16.", Warga Negara Indonesia, ".$d17.", bertempat tinggal di ".$d18.", pemegang Kartu Tanda Penduduk Nomor: ".$d19.";",null);
		// if status pernikahaan suami(0) or istri(1)
		if(in_array($d52, ['0','1'])){
			$textrun = $section->addTextRun($pstyle4);
			$textrun->addText("- dan menurut keteranganya dalam melakukan perbuatan hukum dibawah ini telah mendapat persetujuan dan diketahui oleh ".$d59. " yaitu:",null);
			$textrun = $section->addTextRun($pstyle4);
			$textrun->addText("- ".$d53." Lahir di ".$d54." pada tanggal ".$d55." Warga Negara Indonesia,".$d57." bertempat tinggal di, ".$d56." Pemegang Kartu Tanda Penduduk (NIK):".$d58,null);

		}
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText("- untuk selanjutnya disebut : ",null);
		$textrun->addText("''PIHAK PERTAMA''",array('bold'=>true));
		$textrun->addText(" atau",null);
		$textrun->addText("''PEMBERI FIDUSIA''.",array('bold'=>true));
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("b. Berdasarkan ");
		$textrun->addText("Surat Kuasa yang dibuat dibawah tangan",array('bold'=>true));
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText("bermaterai cukup,tertanggal ".$d20." bernomor : ".$d21,array('bold'=>true),array('indentation'=>array('left'=>275,'right'=>-800)));
		$textrun->addText(", selaku kuasa dari dari Tuan ANTHONY Y PANGGABEAN dan Tuan  ZACHARIA SUSANTA DIREDJA dalam jabatannya berturut-turut selaku para Direktur dari Perseroan yang akan disebut, oleh dan karenanya sah tertindak untuk dan atas nama Direksi Perseroan PT. WAHANA OTTOMITRA MULTIARTHA Tbk. (Terbuka), berkedudukan di Jakarta, yang perubahan seluruh anggaran dasarnya dimuat dalam akta tertanggal dua belas Agustus dua ribu delapan (12-08-2008), Nomor:  54 (lima puluh empat), dibuat dihadapan Nyonya POERBANINGSIH ADI WARSITO, Sarjana Hukum, Notaris di Jakarta, akta perubahan seluruh anggaran dasar mana telah mendapatkan persetujuan dari Menteri Hukum dan Hak Asasi Manusia Republik Indonesia, sebagaimana ternyata dalam Surat Keputusan tertanggal Sembilan belas Agustus dua ribu delapan (19-08-2008), Nomor : AHU-52847.AH.01.02.
Tahun 2008; ",null,array('indentation'=>array('left'=>275,'right'=>-800)));
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText("- Perubahan susunan pengurus terakhir dimuat dalam akta tertanggal tiga puluh satu Mei dua ribu tujuh belas (31-05-2017), Nomor : 30, yang dibuat dihadapan Nyonya RINI YULIANTI, Sarjana Hukum, Notaris di Jakarta, akta mana telah diterima dan dicatatkan dalam database Sisminbakum Departemen Hukum dan Hak Asasi Manusia Republik Indonesia, sebagaimana ternyata dalam Penerimaan Pemberitahuan Perubahan Data Perseroan tujuh juni dua ribu tujuh belas (07-06-2017), Nomor :");
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText("AHU-AH.01.03-0143578;----------------------------");
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText("- untuk selanjutnya disebut :");
		$textrun->addText("''PIHAK KEDUA'",array('bold'=>true));
		$textrun->addText(" atau ");
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText("''PENERIMA FIDUSIA'';------------------------------",array('bold'=>true));
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("-Penghadap dikenal oleh saya, Notaris.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("-Penghadap bertindak sebagaimana tersebut diatas menerangkan terlebih dahulu, sebagai berikut :");
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText("A. bahwa, Pemberi Fidusia selaku Debitur telah");
		$textrun = $section->addTextRun($pstyle5);
		$textrun->addText("dan/atau akan memperoleh fasilitas pembiayaan dari PT. WAHANA OTTOMITRA MULTIARTHA Tbk. (Terbuka) yang bertindak selaku pemberi pembiayaan (''Kreditur'') telah dibuat dan ditanda tangani ");
		$textrun->addText("perjanjian pembiayaan konsumen yang dibuat dibawah tangan bermeterai cukup, Nomor : ".$d23.", tertanggal ".$d24,array('bold'=>true));
		$textrun->addText(", Jangka waktu Perjanjian dari tanggal : ".$d25." sampai dengan tanggal : ".$d26." yang fotokopinya dilekatkan pada minuta akta ini (selanjutnya Perjanjian Pembiayaan Konsumen tersebut berikut perubahan, penambahan dan perpanjangannya disebut:------------------------------------");
		$textrun = $section->addTextRun($pstyle5);
		$textrun->addText("''PERJANJIAN PEMBIAYAAN KONSUMEN'';-------------",array('bold'=>true));
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText("B. bahwa, untuk lebih menjamin dan menanggung");
		$textrun = $section->addTextRun($pstyle5);
		$textrun->addText("terbayarnya dengan baik segala sesuatu yang terhutang dan dibayar oleh Pemberi Fidusia yang juga selaku Debitur sebagaimana ditentukan dalam Perjanjian Pembiayaan Konsumen, Pemberi Fidusia diwajibkan untuk memberikan jaminan fidusia atas kendaraan bermotor milik Pemberi Fidusia untuk kepentingan Penerima Fidusia, sebagaimana yang akan diuraikan di bawah ini --------------------");
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText("C .bahwa, untuk memenuhi ketentuan tentang pemberian");
		$textrun = $section->addTextRun($pstyle5);
		$textrun->addText("jaminan yang ditentukan dalam Perjanjian Pembiayaan Konsumen, Pemberi Fidusia dan Penerima Fidusia telah sepakat dan setuju, mengadakan perjanjian sebagaimana yang dimaksud dalam Undang-undang nomor 42 Tahun 1999 (seribu sembilan ratus sembilan puluh sembilan), yaitu perjanjian tentang Jaminan Fidusia sebagaimana yang hendak dinyatakan sekarang dalam akta ini.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Selanjutnya, penghadap bertindak dalam kedudukannya tersebut menerangkan bahwa untuk menjamin terbayr-nya dengan baik segala sesuatu yang terhutang dan wajib dibayar oleh Debitur kepada Penerima Fidusia, baik karena hutang pokok, bunga dan biaya-biaya lainnya yang timbul berdasarkan Perjanjian Pembiayaan Konsumen, dengan ");
		$textrun->addText("jumlah hutang sebesar Rp. ".$d27.", ",array('bold'=>true));
		$textrun->addText("atau sejumlah uang yang ditentukan dikemudian hari berdasarkan perjanjian pembiayaan konsumen, Maka Pihak Pertama yang bertindak selaku Pemberi Fidusia menerangkan dengan ini, memberikan jaminan fidusia kepada Penerima Fidusia. Kemudian atas nama Pihak Kedua selaku Penerima Fidusia, menerangkan dengan ini menerima jaminan fidusia dari Pemberi Fidusia, sampai dengan nilai penjaminan sebesar RP. ".$d28.", atas Obyek Jaminan Fidusia berupa 1 (satu) unit kendaraan bermotor roda ".$d29.", yaitu :");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Merk --------------------	: ".$d30.";");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Type --------------------	: ".$d31.";");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Jenis/Model -------------	: ".$d32.";");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Tahun Pembuatan ---------	: ".$d33.";");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Tahun Perakitan ---------	: ".$d34.";");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Nomor Rangka/NIK --------	: ".$d35.";");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Nomor Mesin -------------	: ".$d36.";");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Nomor Polisi ");
		$textrun->addText("dan Nomor Bukti Kepemilikan Kendaraan Bermotor (BPKB) ".$d37,array('bold'=>true));
		$textrun->addText(", dengan data-data sebagaimana lebih lanjut tercantum dalam daftar kendaraan yang dikeluarkan oleh Pemberi Fidusia, (selanjutnya daftar kendaraan tersebut berikut segala, pembaharuan dan penggantiannya disebut ''Daftar Kendaraan'') yang bernilai pada saat ini RP. ".$d38.", (selanjutnya disebut ''Obyek Jaminan Fidusia''), dengan persyaratan dan ketentuan sebagai berikut:");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("----------------------- Pasal 1 -------------------");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Dengan ditandatanganinya akta ini, Obyek Jaminan Fidusia beralih hak kepemilikannya atas dasar kepercayaan kepada Penerima Fidusia, sedangkan Obyek Jaminan Fidusia tersebut tetap berada pada dan dalam kekuasaan Pemberi Fidusia selaku peminjam pakai, untuk  itu surat-surat bukti hak milik atas Obyek Jaminan Fidusia serta surat-surat lain yang bersangkutan diserahkan kepada Penerima Fidusia.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("----------------------- Pasal 2 -------------------");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Obyek Jaminan Fidusia hanya dapat dipergunakan oleh Pemberi Fidusia menurut sifat dan peruntukannya, dengan tidak ada kewajiban bagi Pemberi Fidusia untuk membayar biaya atau ganti rugi berupa apapun untuk pinjam pakai tersebut kepada Penerima Fidusia. Namun Pemberi Fidusia berkewajiban untuk memelihara Obyek Jaminan Fidusia tersebut dengan sebaik-baiknya sebagaimana biasa dalam memelihara barang-barang yang dipercayakan kepadanya termasuk melakukan semua tindakan yang diperlukan untuk pemeliharaan dan perbaikan atas Obyek Jaminan Fidusia atas biaya dan tanggungan Pemberi Fidusia, serta membayar pajak dan beban lainnya yang bersangkutan dengan itu. Risiko tentang kerugian atau kerusakan seluruh atau sebagian Obyek Jaminan Fidusia atau adanya tuntutan dari Polisi, aparat hukum atau pihak lainnya mengenai Obyek Jaminan Fidusia tetap merupakan tanggungjawab Pemberi Fidusia sepenuhnya, dimanapun Obyek Jaminan Fidusia berada. Penerima Fidusia dibebaskan dari segala tuntutan atau tagihan dari pihak manapun juga. - Apabila untuk penggunaan atas Obyek Jaminan Fidusia diperlukan suatu kuasa khusus, maka Pemberi Fidusia dengan ini memberi kuasa kepada Penerima Fidusia untuk melakukan tindakan-tindakan yang diperlukan dalam rangka pinjam pakai Obyek Jaminan Fidusia.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("----------------------- Pasal 3 -------------------");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Penerima Fidusia atau wakilnya yang sah setiap waktu berhak untuk memeriksa tentang adanya dan keadaan Obyek Jaminan Fidusia tersebut, dan sehubungan dengan hal tersebut Penerima Fidusia atau wakilnya yang sah dengan ini telah diberi kuasa dengan hak substitusi oleh Pemberi Fidusia untuk memasuki gedung, rumah, gudang, bangunan, ruangan dimana Obyek Jaminan Fidusia disimpan atau berada.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Pemberi Fidusia dengan ini menyatakan bahwa tindakan tersebut tidak merupakan tindakan memasuki tempat dan atau bangunan tanpa izin (huisvredebreuk).");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("----------------------- Pasal 4 -------------------");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Apabila bagian dari Obyek Jaminan Fidusia atau diantara Obyek Jaminan Fidusia ada yang tidak dapat dipergunakan lagi maka maka Pemberi Fidusia dengan ini  berjanji dan karenanya mengikat diri untuk mengganti bagian dari atau Obyek Jaminan Fidusia yang tidak dapat dipergunakan tersebut dengan Obyek Jaminan Fidusia lainnya yang sejenis yang nilainya setara dengan yang digantikan serta yang dapat disetujui Penerima Fidusia dan pengganti Obyek Jaminan Fidusia tersebut termasuk dalam Jaminan Fidusia yang dinyatakan dalam akta ini.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("----------------------- Pasal 5 -------------------");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Pemberi Fidusia menyatakan dan menjamin kepada Penerima Fidusia sebagai berikut :");
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText("a. Obyek Jaminan Fidusia adalah milik/hak Pemberi");
		$textrun = $section->addTextRun($pstyle4);
		$textrun->addText("Fidusia dan tidak ada orang/pihak lain yang turut memiliki atau mempunyai hak apapun, oleh karena itu Pemberi Fidusia mempunyai kewenangan hukum untuk memberikan Obyek Jaminan Fidusia kepada Penerima Fidusia berdasarkan akta ini;");
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText("b.Obyek Jaminan Fidusia belum pernah dijual atau ");
		$textrun = $section->addTextRun($pstyle4);
		$textrun->addText("dialihkan haknya dengan cara bagaimanapun dan kepada siapapun juga;");
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText("c.Obyek Jaminan Fidusia saat ini tidak sedang ");
		$textrun = $section->addTextRun($pstyle4);
		$textrun->addText("diagunkan dengan cara bagaimanapun dan kepada siapapun, kecuali kepada Penerima Fidusia;");
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText("d.Obyek Jaminan Fidusia tidak sedang tersangkut");
		$textrun = $section->addTextRun($pstyle4);
		$textrun->addText("perkara/sengketa dan tidak sedang dalam sitaan;");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Pemberi Fidusia dengan ini membebaskan dan melepaskan Penerima Fidusia dan semua dan setiap tuntutan, gugatan atau tagihan yang diajukan oleh siapapun mengenai atau yang berhubungan dengan hal yang dinyatakan dan dijamin oleh Pemberi Fidusia tersebut diatas. Semua tuntutan/gugatan yang ada atau mungkin ada sepenuhnya merupakan beban dan tanggung jawab Pemberi Fidusia dan atas permintaan pertama dari Penerima Fidusia, Pemberi Fidusia wajib mengurus, menyelesaikan dan (jika) perlu membayar tuntutan atau gugatan tersebut.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("----------------------- Pasal 6 -------------------");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Pemberi Fidusia tidak berhak untuk melakukan Fidusia ulang atas Obyek Jaminan Fidusia.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Pemberi Fidusia tidak diperkenankan untuk mengadaikan atau mengalihkan atau menyewakan dengan cara apapun Obyek Jaminan Fidusia kepada pihak lain tanpa persetujuan tertulis terlebih dahulu dari Penerima Fidusia. Bilamana Pemberi Fidusia selaku Debitur tidak memenuhi sebagaimana telah ditentukan dalam akta ini atau Pemberi Fidusia selaku Debitur tidak memenuhi kewajibannya berdasarkan Perjanjian Pembiayaan Konsumen maka lewatnya waktu yang ditentukan untuk memenuhi kewajiban tersebut saja sudah cukup membuktikan tentang adanya pelanggaran atau kelalaian Penerima Fidusia selaku Debitur dalam memenuhi kewajiban tersebut, dalam hal mana hak Pemberi Fidusia untuk meminjam pakai Obyek Jaminan Fidusia tersebut menjadi berakhir dan karenanya Obyek Jaminan Fidusia wajib segera diserahkan oleh Pemberi Fidusia kepada Penerima Fidusia setelah adanya  pemberitahuan secara tertulis dari Penerima Fidusia.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("----------------------- Pasal 7 -------------------");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Pemberi Fidusia berjanji karenanya mengikat diri untuk mengasuransikan Obyek Jaminan Fidusia tersebut pada perusahaan asuransi yang ditunjuk atau disetujui oleh Penerima Fidusia terhadap risiko kehilangan serta risiko/bahaya lainnya dan untuk suatu jumlah pertanggungan dengan persyaratan yang dipandang baik oleh Penerima Fidusia. Dalam polis asuransi tersebut harus dicantumkan klausula bahwa dalam hal terjadi kerugian, maka uang pengganti kerugian, tersebut harus dibayarkan terlebih dahulu kepada Penerima Fidusia, untuk diperhitungkan dengan jumlah yang masih yang harus dibayarkan oleh Pemberi Fidusia kepada Penerima Fidusia. Berdasarkan Perjanjian Pembiayaan Konsumen, sedangkan sisanya jika masih ada harus dikendalikan Penerima Fidusia kepada Pemberi Fidusia dengan tidak ada kewajiban bagi Penerima Fidusia untuk membayar bunga atau ganti kerugian berupa apapun kepada Pemberi Fidusia. Semua uang premi asuransi menjadi beban dan wajib dibayar oleh Pemberi Fidusia atau Debitur.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Apabila Pemberi Fidusia atau Debitur lalai dan atau tidak mengasuransikan Obyek Jaminan Fidusia, maka Penerima Fidusia berhak (namun tidak berkewajiban) dan seberapa perlu dengan ini kepadanya oleh Pemberi Fidusia diberi kuasa untuk mengasuransikan Obyek Jaminan Fidusia dengan ketentuan bahwa premi asuransinya tetap wajib dibayar oleh Pemberi Fidusia atau Debitur. Asli polis asuransi dan perpanjangannya di kemudian hari serta kuitansi pembayaran premi asuransi tersebut wajib diserahkan untuk disimpan oleh Penerima Fidusia segera setelah Pemberi Fidusia memperoleh dokumen dokumen tersebut dari perusahaan asuransi.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("----------------------- Pasal 8 -------------------");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Dalam hal Penerima Fidusia lalai berdasarkan akta ini dan/atau berdasarkan Perjanjian Pembiayaan Konsumen, maka tanpa diperlukan lagi surat teguran juru sita atau surat lain yang serupa dengan itu, Penerima Fidusia atas kekuasaannya sendiri berhak untuk menjual Obyek Jaminan Fidusia tersebut atas dasar titel eksekutorial atau melalui pelelangan dimuka umum atau melalui penjualan dibawah tangan yang dilakukan berdasarkan kesepakatan Pemberi Fidusia dan Penerima Fidusia.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Untuk keperluan penjualan tersebut, Penerima Fidusia berhak menghadap dimana perlu, membuat atau suruh membuat serta menandatangani semua surat, akta serta dokumen lain yang diperlukan, menerima uang hasil penjualan dan memberikan tanda penerimaan untuk itu menyerahkan apa yang dijual itu kepada pembelinya, memperhitungkan uang hasil bersih penjualan yang diterimanya itu dengan semua yang wajib dibayar oleh Pemberi Fidusia kepada Penerima Fidusia berdasarkan Perjanjian Pembiayaan Konsumen, akan tetapi dengan kewajiban bagi Penerima Fidusia untuk menyerahkan sisa uang hasil bersih penjualannya jika masih ada kepada Pemberi Fidusia, dengan tidak ada kewajiban bagi Penerima Fidusia untuk membayar bunga atau ganti kerugian berupa apapun juga kepada Pemberi Fidusia mengenai sisa uang hasil bersih penjualan itu dan selanjutnya Penerima Fidusia juga berhak untuk melakukan segala sesuatu yang dipandang perlu dan berguna dalam rangka penjualan Obyek Jaminan Fidusia tersebut dengan tidak ada yang dikecualikan.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("----------------------- Pasal 9 -------------------");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Dalam hal Penerima Fidusia mempergunakan hak-hak yang diberikan kepadanya seperti diuraikan di atas, Pemberi Fidusia wajib dan mengikat diri, sekarang ini untuk nanti pada waktunya, menyerahkan Obyek Jaminan Fidusia dalam keadaan terpelihara baik kepada Penerima Fidusia atas permintaan atau teguran pertama dari Penerima Fidusia dalam waktu sebagaimana ditentukan dalam surat permintaan atau teguran tersebut. Dalam hal Pemberi Fidusia tidak memenuhi ketentuan tersebut maka Pemberi Fidusia dianggap telah lalai sehingga tidak diperlukan lagi surat teguran juru sita atau surat lain yang serupa dengan itu. Dalam hal Pemberi Fidusia tidak menyerahkan Obyek Jaminan Fidusia pada waktu eksekusi dilaksanakan, Penerima Fidusia atau kuasanya yang sah berhak mengambil Obyek Jaminan Fidusia dan apabila perlu dapat meminta bantuan pihak yang berwenang, dengan ketentuan bahwa semua biaya yang berkaitan dengan itu menjadi beban dan wajib dibayar oleh Pemberi Fidusia atau Debitur. ");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("----------------------- Pasal 10 -------------------");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Pembebanan terhadap Obyek Jaminan Fidusia ini dilakukan oleh Pemberi Fidusia kepada Penerima Fidusia dengan syarat-syarat yang memutuskan (onder de ontbindende voorwaarden) yakni sampai dengan Debitur telah memenuhi/membayar lunas semua yang wajib dibayar oleh Debitur kepada Kreditur sebagaimana ditentukan dalam Perjanjian Pembiayaan Konsumen.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("----------------------- Pasal 11 -------------------");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Sepanjang diperlukan, Pemberi Fidusia dengan ini memberikan kuasa dengan hak substitusi kepada Penerima Fidusia yang menyatakan menerima kuasa dari Pemberi Fidusia untuk melaksanakan pendaftaran jaminan fidusia tersebut, untuk keperluan tersebut berhak menghadap di hadapan pejabat atau instansi yang berwenang (termasuk kantor pendaftaran fidusia), memberikan keterangan, menandatangani surat/formulir, mendaftarkan jaminan fidusia atas Obyek Jaminan Fidusia tersebut dengan melampirkan pernyataan pendaftaran jaminan fidusia, serta untuk mengajukan permohonan pendaftaran atas perubahan dalam hal terjadi perubahan atas data yang tercantum dalam sertipikat jaminan fidusia, selanjutnya menerima sertipikat jaminan fidusia dan/atau Pernyataan Perubahan, serta dokumen-dokumen lain yang berkaitan untuk keperluan itu, membayar semua biaya dan menerima semua kuitansi segala pembayaran serta selanjutnya melakukan segala tindakan yang perlu dan berguna untuk melaksanakan ketentuan dari akta ini.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Akta ini merupakan bagian yang tidak dapat dipisahkan dari Perjanjian Pembiayaan Konsumen, demikian pula kuasa yang diberikan dalam akta ini merupakan bagian yang tidak terpisahkan dari akta ini yang tanpa adanya akta ini dan kuasa tersebut, niscaya Perjanjian Pembiayaan Konsumen demikian pula akta ini yang tanpa adanya akta ini dan kuasa tersebut, tidak akan diterima dan dilangsungkan diantara para pihak yang bersangkutan, oleh karenanya akta ini tidak dapat ditarik kembali atau dibatalkan selama kewajiban Debitur berdasarkan Perjanjian Pembiayaan Konsumen belum terpenuhi dan kuasa tersebut tidak akan batal atau berakhir karena sebab yang dapat mengakhiri pemberian sesuatu kuasa, termasuk sebab yang tercantum dalam pasal 1813, 1814, dan 1816 Kitab Undang-Undang Hukum Perdata Indonesia.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("----------------------- Pasal 12 -------------------");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("-Penerima Fidusia berhak dan dengan ini diberi kuasa dengan hak subtitusi oleh Pemberi Fidusia untuk melakukan perubahan akta penyesuaian atas ketentuan dalam akta ini, dalam hal perubahan atau penyesuaian tersebut dalam rangka memenuhi ketentuan dalam peraturan pemerintah tentang pendaftaran fidusia maupun ketentuan dalam Undang-Undang nomor 42 Tahun 1999 tentang jaminan fidusia.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("----------------------- Pasal 13 -------------------");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Biaya akta ini dan biaya lainnya yang berkenaan dengan pembuatan akta ini maupun dalam melaksanakan ketentuan dalam akta ini menjadi beban dan wajib dibayar oleh---- Pemberi Fidusia, demikian pula pendaftaran fidusia ini di Kantor Pendaftaran Fidusia.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("----------------------- Pasal 14 -------------------");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Segala perselisihan yang mungkin timbul di antara para pihak mengenai akta ini yang tidak dapat diselesaikan diantara para pihak sendiri, maka para pihak akan memilih domisili hukum yang tetap dan seumumnya di Kantor Panitera PENGADILAN NEGERI JAKARTA.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("- Segala Pemilihan domisili hukum tersebut dilakukan dengan tidak mengurangi hak dari Penerima Fidusia untuk mengajukan tuntutan hukum terhadap Pemberi Fidusia berdasarkan jaminan fidusia atas Obyek Jaminan Fidusia tersebut dihadapan Pengadilan lainnya dalam Wilayah Republik Indonesia, yaitu pada pengadilan negeri yang mempunyai yurisdiksi atas diri dari Pemberi Fidusia atau atas Obyek Jaminan Fidusia tersebut.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("-----------------DEMIKIAN AKTA INI------------------",array('bold'=>true));
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("Dibuat sebagai minuta dan dilangsungkan di ".$d48.", pada hari dan tanggal tersebut pada bagian awal akta ini, dengan dihadiri oleh :");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("1.".$d39.", lahir di ".$d40.", pada tanggal");
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText($d41.", bertempat tinggal di ".$d42.", pemegang Kartu Tanda Penduduk Nomor : $d50;");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("2.".$d43.", lahir di ".$d44.", pada tanggal");
		$textrun = $section->addTextRun($pstyle3);
		$textrun->addText($d45.", bertempat tinggal di ".$d46.", pemegang Kartu Tanda Penduduk Nomor : $d51;");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("-keduanya pegawai Kantor Notaris, sebagai saksi-saksi.");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("-Atas permintaan penghadap, akta ini tidak dibacakan oleh saya, Notaris, akan tetapi penghadap dan para saksi sudah mengerti maksud dari isi akta ini, maka pada ketika itu juga penghadap, para saksi dan saya, Notaris menandatanganinya. ");
		$textrun = $section->addTextRun($pstyle2);
		$textrun->addText("-Dilangsungkan Dengan tanpa perubahan");
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save($filename);

		redirect(base_url($filename));
	}

	
	public function export_word($id){

		$select = "	`a`.*, `b`.*, `c`.*, `d`.*, `e`.saksi_nama AS saksi_nama1,
					`e`.saksi_ktp AS saksi_nik1,
					`e`.saksi_tempat_lahir AS saksi_tempat_lahir1,
					`e`.saksi_tanggal_lahir AS saksi_tanggal_lahir1,
					`e`.saksi_alamat AS saksi_alamat1,
					`f`.*,`g`.saksi_nama AS saksi_nama2,
					`g`.saksi_ktp AS saksi_nik2,
					`g`.saksi_tempat_lahir AS saksi_tempat_lahir2,
					`g`.saksi_tanggal_lahir AS saksi_tanggal_lahir2,
					`g`.saksi_alamat AS saksi_alamat2,
					`h`.*,
					`i`.wn_name as wilayah_kerja_notaris,
					`j`.pemb_nilai_benda_fidusia AS objek_benda";
		$data = $this->m_global->get_data_all('v_entry_data', [['akta a', 'a.ed_id = v_entry_data.ed_id', 'left'],
															   ['notaris b', 'b.notaris_id = a.notaris_id','left'], 
															   ['wilayah_notaris c','c.wn_id = b.notaris_wilayah_kerja_id ','left'],
															   ['kuasa_direksi d','d.kd_id = b.notaris_kd_id1','left'],
															   ['saksi e','e.saksi_id = a.saksi_id1','left'],
															   ['daerah f','f.daerah_id = a.daerah_id','left'],
															   ['saksi g','g.saksi_id = a.saksi_id2','left'],
															   ['data_kendaraan h','h.dk_id = v_entry_data.ed_data_kendaraan_id','left'],
															   ['wilayah_notaris i','i.wn_id = b.notaris_wilayah_kerja_id','inner'],
															   ['pembiayaan j','j.pemb_id = v_entry_data.ed_data_kendaraan_id','inner']]
															   , [strEncrypt('v_entry_data.ed_id', TRUE) => $id], $select);

		@$date = explode(" ", $data[0]->akta_waktu);
		@$tanggal = $date[0];
		@$waktu = $date[1];
		$dknote = explode("-", $data[0]->dk_covernote);
		$dknote = $dknote[2]."-".$dknote[1]."-".$dknote[0];
		// tanggal akta`
		@$var[0] = $this->change_tanggal($tanggal);
		@$var[1] = $this->change_time($waktu);
		@$var[2] = $this->change_tanggal($data[0]->akta_tanggal_lahir_penerima);
		@$var[3] = $this->change_tanggal($data[0]->akta_tanggal_penerima);
		@$var[4] = $this->change_tanggal($data[0]->akta_tanggal_lahir_nasabah);
		@$var[5] = $this->change_tanggal($data[0]->akta_tanggal_kuasa);
		@$var[6] = $this->change_tanggal($data[0]->akta_tanggal_pk);
		@$var[7] = $this->change_tanggal($data[0]->akta_perjanjian_kredit);
		@$var[8] = $this->change_tanggal($dknote);
		@$var[9] = $this->get_day(date("D",strtotime($data[0]->akta_waktu)));
		@$tanggal_lahir_saksi[1] = $this->change_tanggal($data[0]->saksi_tanggal_lahir1);
		@$tanggal_lahir_saksi[2] = $this->change_tanggal($data[0]->saksi_tanggal_lahir2);
		$terbilang_hutang = Terbilang($data[0]->akta_hutang_nasabah);
		$terbilang_jaminan = Terbilang($data[0]->akta_nilai_jaminan);
		$terbilang_objek = Terbilang($data[0]->objek_benda);
		$terbilang_hutang_pokok = Terbilang($data[0]->akta_hutang_pokok);

		// $day = change
		$notaris = explode(".", $data[0]->notaris_nama);
		$notaris = "( ".$notaris[0].", SH.,M.Kn. )"; 
		$d1 = $data[0]->akta_nomor;
		$d2 = $var[9];
		$d3 = $var[0];
		$d4 = $var[1];
		$d5 = $data[0]->notaris_nama;
		$d6 = $data[0]->wn_name;
		$d7 = $data[0]->akta_nama_penerima;
		$d8 = $data[0]->akta_tempat_lahir_penerima;
		$d9 = $var[2];
		$d10 = $data[0]->akta_pekerjaan_penerima;
		$d11 = $data[0]->akta_alamat_penerima;
		$d12 = $data[0]->akta_nik_penerima;
		$d13 = $var[3];
		$d14 = $data[0]->akta_nama_pembeban;
		$d15 = $data[0]->akta_tempat_lahir_nasabah;
		$d16 = $var[4];
		$d17 = $data[0]->akta_pekerjaan_nasabah;
		$d18 = $data[0]->daerah_kota.", ".$data[0]->akta_alamat_nasabah;
		$d19 = $data[0]->akta_nik_nasabah;
		$d20 = $var[5];
		$d21 = $data[0]->akta_nomor_kuasa;
		$d22 = $data[0]->kd_nama;
		$d23 = $data[0]->akta_nomor_pk;
		$d24 = $var[6];
		$d25 = $var[6];
		$d26 = $var[7];
		$d27 = number_format($data[0]->akta_hutang_pokok,null,"",".") . ", - ($terbilang_hutang_pokok rupiah)";
		$d28 = number_format($data[0]->akta_nilai_jaminan,null,"",".") . ", - ($terbilang_jaminan rupiah)";
		$d29 = "RODA 2";
		$d30 = $data[0]->dk_merk;
		$d31 = $data[0]->dk_tipe;
		$d32 = $data[0]->dk_tipe;
		$d33 = $data[0]->dk_tahun_pembuatan;
		$d34 = $data[0]->dk_tahun_pembuatan;
		$d35 = $data[0]->dk_no_rangka;
		$d36 = $data[0]->dk_no_mesin;
		$d37 = $data[0]->dk_no_bpkb;
		$d38 = number_format($data[0]->objek_benda,null,"",".").",- ($terbilang_objek rupiah)";
		$d39 = $data[0]->saksi_nama1;
		$d40 = $data[0]->saksi_tempat_lahir1;
		$d41 = $tanggal_lahir_saksi[1];
		$d42 = $data[0]->saksi_alamat1;
		$d43 = $data[0]->saksi_nama2;
		$d44 = $data[0]->saksi_tempat_lahir2;
		$d45 = $tanggal_lahir_saksi[2];
		$d46 = $data[0]->saksi_alamat2;
		$d47 = $data[0]->akta_pengadilan;
		$d48 = $data[0]->wilayah_kerja_notaris;
		$d49 = $notaris;
		$d50 = $data[0]->saksi_nik1;
		$d51 = $data[0]->saksi_nik2;

		header("Content-type: application/vnd.ms-word");
		header("Content-Disposition: attachment;Filename=akta_fidusia.doc");
		echo "<html>";
		echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
		echo "<head>";
		echo "<style>";
		echo "body {
			font-family:Courier New;
			word-spacing: 0px;
			margin-left:3cm;
		}";
		echo "<head>";
		echo "<body>";
		echo "	<div width='310px' align='center'><b>AKTA JAMINAN FIDUSIA</b></div><br>
	<div width='310px' align='center'>Nomor : ".$d1."</div><br>
	<div>
		- Pada hari ini, ".$d2.", tanggal ".$d3.".<br> 
		-Pukul ".$d4.".<br>
		-Hadir dihadapan saya,<b> ".$d5."</b>, Notaris di Daerah ".$d6.",
		dengan dihadiri saksi-saksi yang telah dikenal oleh saya,
		Notaris dan nama-namanya akan disebutkan pada bagian akhir akta ini.<br>
		- ".$d7.", lahir di ".$d8.", pada tanggal ".$d9.", Warga Negara Indonesia ".$d10.", bertempat tinggal di ".$d11.", pemegang 
		Kartu Tanda Penduduk Nomor : ".$d12.";<br>
		-menurut keterangannya dalam hal ini bertindak : <br>
		a.Berdasarkan Surat Kuasa Pembebanan Jaminan Fidusia yang dibuat dibawah tangan, bermeterai cukup, tertanggal ".$d13.", oleh dan karenanya sah bertindak untuk dan atas nama : ".$d14.", Lahir di ".$d15."<br> pada tanggal ".$d16.",<br> Warga Negara Indonesia, ".$d17.",<br> bertempat tinggal di ".$d18.", pemegang Kartu Tanda Penduduk Nomor: ".$d19."; untuk selanjutnya disebut : ''PIHAK PERTAMA'' atau ''PEMBERI FIDUSIA''. <br>
		b.Berdasarkan Surat Kuasa yang dibuat dibawah tangan bermaterai cukup,tertanggal ".$d20.", bernomor : ".$d21.", selaku kuasa dari dari Tuan ANTHONY Y PANGGABEAN dan Tuan  ZACHARIA SUSANTA DIREDJA.dalam jabatannya berturut-turut selaku para Direktur dari Perseroan yang akan disebut, oleh dan karenanya sah tertindak untuk dan atas nama Direksi Perseroan PT. WAHANA OTTOMITRA MULTIARTHA Tbk. (Terbuka), berkedudukan di Jakarta, yang perubahan seluruh anggaran dasarnya dimuat dalam akta tertanggal dua belas Agustus dua ribu delapan (12-08-2008), Nomor:  54 (lima puluh empat), dibuat dihadapan Nyonya POERBANINGSIH ADI WARSITO, Sarjana Hukum, Notaris di Jakarta, akta perubahan seluruh anggaran dasar mana telah mendapatkan persetujuan dari Menteri Hukum dan Hak Asasi Manusia Republik Indonesia, sebagaimana ternyata dalam Surat Keputusan tertanggal Sembilan belas Agustus dua ribu delapan (19-08-2008),<br> Nomor : AHU-52847.AH.01.02.Tahun 2008; <br>
			- Perubahan susunan pengurus terakhir dimuat dalam akta tertanggal tiga puluh satu Mei dua ribu tujuh belas (31-05-2017), Nomor : 30, yang dibuat dihadapan Nyonya RINI YULIANTI, Sarjana Hukum, Notaris di Jakarta, akta mana telah diterima dan dicatatkan dalam database Sisminbakum Departemen Hukum dan Hak Asasi Manusia Republik Indonesia, sebagaimana ternyata dalam Penerimaan Pemberitahuan Perubahan Data Perseroan tujuh juni dua ribu tujuh belas (07-06-2017), Nomor : AHU-AH.01.03-0143578;
		- untuk selanjutnya disebut : ''PIHAK KEDUA'' atau ''PENERIMA FIDUSIA'';

		- Penghadap dikenal oleh saya, Notaris. Penghadap bertindak sebagaimana tersebut diatas menerangkan terlebih dahulu, sebagai berikut :<br> 
		A.bahwa, Pemberi Fidusia selaku Debitur telah dan/atau akan memperoleh fasilitas pembiayaan dari PT. WAHANA OTTOMITRA MULTIARTHA Tbk. (Terbuka) yang bertindak selaku pemberi pembiayaan (''Kreditur'') telah dibuat dan ditanda tangani perjanjian pembiayaan konsumen yang dibuat dibawah tangan bermeterai cukup, Nomor : ".$d23.", tertanggal <br> ".$d24.", , Jangka waktu Perjanjian dari tanggal : ".$d25." sampai dengan tanggal : ".$d26." yang fotokopinya dilekatkan pada minuta akta ini (selanjutnya Perjanjian Pembiayaan Konsumen tersebut berikut perubahan, penambahan dan perpanjangannya disebut :<br>''PERJANJIAN PEMBIAYAAN KONSUMEN'';<br>
		B.bahwa, untuk lebih menjamin dan menanggung terbayarnya dengan baik segala sesuatu yang terhutang dan dibayar oleh Pemberi Fidusia yang juga selaku Debitur sebagaimana ditentukan dalam Perjanjian Pembiayaan Konsumen, Pemberi Fidusia diwajibkan untuk memberikan jaminan fidusia atas kendaraan bermotor milik Pemberi Fidusia untuk kepentingan Penerima Fidusia, sebagaimana yang akan diuraikan di bawah ini ;<br>
		C.bahwa, untuk memenuhi ketentuan tentang pemberian jaminan yang ditentukan dalam Perjanjian Pembiayaan Konsumen, Pemberi Fidusia dan Penerima Fidusia telah sepakat dan setuju, mengadakan perjanjian sebagaimana yang dimaksud dalam Undang-undang nomor 42 Tahun 1999 (seribu sembilan ratus sembilan puluh sembilan), yaitu perjanjian tentang Jaminan Fidusia sebagaimana yang hendak dinyatakan sekarang dalam akta ini.<br><br>
		- Selanjutnya, penghadap bertindak dalam kedudukannya tersebut menerangkan bahwa untuk menjamin terbayr-nya dengan baik segala sesuatu yang terhutang dan wajib dibayar oleh Debitur kepada Penerima Fidusia, baik karena hutang pokok, bunga dan biaya-biaya lainnya yang timbul berdasarkan Perjanjian Pembiayaan Konsumen, dengan jumlah hutang sebesar Rp. ".$d27.", atau sejumlah uang yang ditentukan dikemudian hari berdasarkan perjanjian pembiayaan konsumen, Maka Pihak Pertama yang bertindak selaku Pemberi Fidusia menerangkan dengan ini, memberikan jaminan fidusia kepada Penerima Fidusia. Kemudian atas nama Pihak Kedua selaku Penerima Fidusia, menerangkan dengan ini menerima jaminan fidusia dari Pemberi Fidusia, sampai dengan nilai penjaminan sebesar RP. ".$d28.", atas Obyek Jaminan Fidusia berupa 1 (satu) unit kendaraan bermotor roda ".$d29.", yaitu : <br>
		- Merk -------------------- &nbsp;: ".$d30."; <br>
		- Type -------------------- &nbsp;: ".$d31."; <br>
		- Jenis Sepeda Motor/Model &nbsp; : ".$d32.";<br> 
		- Tahun Pembuatan ---------&nbsp; : ".$d33.";<br> 
		- Tahun Perakitan --------- &nbsp;: ".$d34.";<br>
		- Nomor Rangka/NIK -------- &nbsp;: ".$d35.";<br>
		- Nomor Mesin ------------- &nbsp;: ".$d36.";<br>
		- Nomor Polisi dan Nomor Bukti Kepemilikan Kendaraan Bermotor (BPKB) ".$d37.", dengan data-data sebagaimana lebih lanjut tercantum dalam daftar kendaraan yang dikeluarkan oleh Pemberi Fidusia, (selanjutnya daftar kendaraan tersebut berikut segala, pembaharuan dan penggantiannya disebut ''Daftar Kendaraan'') yang bernilai pada saat ini RP. ".$d38.", (selanjutnya disebut ''Obyek Jaminan Fidusia''), dengan persyaratan dan ketentuan sebagai berikut: <br><br>
		<div width='310px' align='center'>-------------- Pasal 1 ----------------</div><br>
		- Dengan ditandatanganinya akta ini, Obyek Jaminan Fidusia beralih hak kepemilikannya atas dasar kepercayaan kepada Penerima Fidusia, sedangkan Obyek Jaminan Fidusia tersebut tetap berada pada dan dalam kekuasaan Pemberi Fidusia selaku peminjam pakai, untuk  itu surat-surat bukti hak milik atas Obyek Jaminan Fidusia serta surat-surat lain yang bersangkutan diserahkan kepada Penerima Fidusia.<br>
		<div width='310px' align='center'>-------------- Pasal 2 ----------------</div><br>
		- Obyek Jaminan Fidusia hanya dapat dipergunakan oleh Pemberi Fidusia menurut sifat dan peruntukannya, dengan tidak ada kewajiban bagi Pemberi Fidusia untuk membayar biaya atau ganti rugi berupa apapun untuk pinjam pakai tersebut kepada Penerima Fidusia. Namun Pemberi Fidusia berkewajiban untuk memelihara Obyek Jaminan Fidusia tersebut dengan sebaik-baiknya sebagaimana biasa dalam memelihara barang-barang yang dipercayakan kepadanya termasuk melakukan semua tindakan yang diperlukan untuk pemeliharaan dan perbaikan atas Obyek Jaminan Fidusia atas biaya dan tanggungan Pemberi Fidusia, serta membayar pajak dan beban lainnya yang bersangkutan dengan itu. Risiko tentang kerugian atau kerusakan seluruh atau sebagian Obyek Jaminan Fidusia atau adanya tuntutan dari Polisi, aparat hukum atau pihak lainnya mengenai Obyek Jaminan Fidusia tetap merupakan tanggungjawab Pemberi Fidusia sepenuhnya, dimanapun Obyek Jaminan Fidusia berada. Penerima Fidusia dibebaskan dari segala tuntutan atau tagihan dari pihak manapun juga. 
		- Apabila untuk penggunaan atas Obyek Jaminan Fidusia diperlukan suatu kuasa khusus, maka Pemberi Fidusia dengan ini memberi kuasa kepada Penerima Fidusia untuk melakukan tindakan-tindakan yang diperlukan dalam rangka pinjam pakai Obyek Jaminan Fidusia. <br>

		<div width='310px' align='center'>-------------- Pasal 3 ----------------</div><br>
		- Penerima Fidusia atau wakilnya yang sah setiap waktu berhak untuk memeriksa tentang adanya dan keadaan Obyek Jaminan Fidusia tersebut, dan sehubungan dengan hal tersebut Penerima Fidusia atau wakilnya yang sah dengan ini telah diberi kuasa dengan hak substitusi oleh Pemberi Fidusia untuk memasuki gedung, rumah, gudang, bangunan, ruangan dimana Obyek Jaminan Fidusia disimpan atau berada.
		- Pemberi Fidusia dengan ini menyatakan bahwa tindakan tersebut tidak merupakan tindakan memasuki tempat dan atau bangunan tanpa izin (huisvredebreuk).

		<div width='310px' align='center'>-------------- Pasal 4 ----------------</div><br>
		- Apabila bagian dari Obyek Jaminan Fidusia atau diantara Obyek Jaminan Fidusia ada yang tidak dapat dipergunakan lagi maka maka Pemberi Fidusia dengan ini  berjanji dan karenanya mengikat diri untuk mengganti bagian dari atau Obyek Jaminan Fidusia yang tidak dapat dipergunakan tersebut dengan Obyek Jaminan Fidusia lainnya yang sejenis yang nilainya setara dengan yang digantikan serta yang dapat disetujui Penerima Fidusia dan pengganti Obyek Jaminan Fidusia tersebut termasuk dalam Jaminan Fidusia yang dinyatakan dalam akta ini.

		<div width='310px' align='center'>-------------- Pasal 5 ----------------</div><br>
		- Pemberi Fidusia menyatakan dan menjamin kepada Penerima Fidusia sebagai berikut : <br>
		  a.Obyek Jaminan Fidusia adalah milik/hak Pemberi Fidusia dan tidak ada orang/pihak lain yang turut memiliki atau mempunyai hak apapun, oleh karena itu Pemberi Fidusia mempunyai kewenangan hukum untuk memberikan Obyek Jaminan Fidusia kepada Penerima Fidusia berdasarkan akta ini; <br>
		  b.Obyek Jaminan Fidusia belum pernah dijual atau dialihkan haknya dengan cara bagaimanapun dan kepada siapapun juga; <br>
		  c.Obyek Jaminan Fidusia saat ini tidak sedang diagunkan dengan cara bagaimanapun dan kepada siapapun, kecuali kepada Penerima Fidusia; <br> 
		  d.Obyek Jaminan Fidusia tidak sedang tersangkut perkara/sengketa dan tidak sedang dalam sitaan;
		- Pemberi Fidusia dengan ini membebaskan dan melepaskan Penerima Fidusia dan semua dan setiap tuntutan, gugatan atau tagihan yang diajukan oleh siapapun mengenai atau yang berhubungan dengan hal yang dinyatakan dan dijamin oleh Pemberi Fidusia tersebut diatas. Semua tuntutan/gugatan yang ada atau mungkin ada sepenuhnya merupakan beban dan tanggung jawab Pemberi Fidusia dan atas permintaan pertama dari Penerima Fidusia, Pemberi Fidusia wajib mengurus, menyelesaikan dan (jika) perlu membayar tuntutan atau gugatan tersebut.

		<div width='310px' align='center'>-------------- Pasal 6 ----------------</div><br>
		- Pemberi Fidusia tidak berhak untuk melakukan Fidusia ulang atas Obyek Jaminan Fidusia. 
		- Pemberi Fidusia tidak diperkenankan untuk mengadaikan atau mengalihkan atau menyewakan dengan cara apapun Obyek Jaminan Fidusia kepada pihak lain tanpa persetujuan tertulis terlebih dahulu dari Penerima Fidusia. Bilamana Pemberi Fidusia selaku Debitur tidak memenuhi sebagaimana telah ditentukan dalam akta ini atau Pemberi Fidusia selaku Debitur tidak memenuhi kewajibannya berdasarkan Perjanjian Pembiayaan Konsumen maka lewatnya waktu yang ditentukan untuk memenuhi kewajiban tersebut saja sudah cukup membuktikan tentang adanya pelanggaran atau kelalaian Penerima Fidusia selaku Debitur dalam memenuhi kewajiban tersebut, dalam hal mana hak Pemberi Fidusia untuk meminjam pakai Obyek Jaminan Fidusia tersebut menjadi berakhir dan karenanya Obyek Jaminan Fidusia wajib segera diserahkan oleh Pemberi Fidusia kepada Penerima Fidusia setelah adanya  pemberitahuan secara tertulis dari Penerima Fidusia.

		<div width='310px' align='center'>-------------- Pasal 7 ----------------</div><br>		
		- Pemberi Fidusia berjanji karenanya mengikat diri untuk mengasuransikan Obyek Jaminan Fidusia tersebut pada perusahaan asuransi yang ditunjuk atau disetujui oleh Penerima Fidusia terhadap risiko kehilangan serta risiko/bahaya lainnya dan untuk suatu jumlah pertanggungan dengan persyaratan yang dipandang baik oleh Penerima Fidusia. Dalam polis asuransi tersebut harus dicantumkan klausula bahwa dalam hal terjadi kerugian, maka uang pengganti kerugian, tersebut harus dibayarkan terlebih dahulu kepada Penerima Fidusia, untuk diperhitungkan dengan jumlah yang masih yang harus dibayarkan oleh Pemberi Fidusia kepada Penerima Fidusia. Berdasarkan Perjanjian Pembiayaan Konsumen, sedangkan sisanya jika masih ada harus dikendalikan Penerima Fidusia kepada Pemberi Fidusia dengan tidak ada kewajiban bagi Penerima Fidusia untuk membayar bunga atau ganti kerugian berupa apapun kepada Pemberi Fidusia. Semua uang premi asuransi menjadi beban dan wajib dibayar oleh Pemberi Fidusia atau Debitur.
		- Apabila Pemberi Fidusia atau Debitur lalai dan atau tidak mengasuransikan Obyek Jaminan Fidusia, maka Penerima Fidusia berhak (namun tidak berkewajiban) dan seberapa perlu dengan ini kepadanya oleh Pemberi Fidusia diberi kuasa untuk mengasuransikan Obyek Jaminan Fidusia dengan ketentuan bahwa premi asuransinya tetap wajib dibayar oleh Pemberi Fidusia atau Debitur. Asli polis asuransi dan perpanjangannya di kemudian hari serta kuitansi pembayaran premi asuransi tersebut wajib diserahkan untuk disimpan oleh Penerima Fidusia segera setelah Pemberi Fidusia memperoleh dokumen dokumen tersebut dari perusahaan asuransi.

		<div width='310px' align='center'>-------------- Pasal 8 ----------------</div><br>		
		- Dalam hal Penerima Fidusia lalai berdasarkan akta ini dan/atau berdasarkan Perjanjian Pembiayaan Konsumen, maka tanpa diperlukan lagi surat teguran juru sita atau surat lain yang serupa dengan itu, Penerima Fidusia atas kekuasaannya sendiri berhak untuk menjual Obyek Jaminan Fidusia tersebut atas dasar titel eksekutorial atau melalui pelelangan dimuka umum atau melalui penjualan dibawah tangan yang dilakukan berdasarkan kesepakatan Pemberi Fidusia dan Penerima Fidusia.
		- Untuk keperluan penjualan tersebut, Penerima Fidusia berhak menghadap dimana perlu, membuat atau suruh membuat serta menandatangani semua surat, akta serta dokumen lain yang diperlukan, menerima uang hasil penjualan dan memberikan tanda penerimaan untuk itu menyerahkan apa yang dijual itu kepada pembelinya, memperhitungkan uang hasil bersih penjualan yang diterimanya itu dengan semua yang wajib dibayar oleh Pemberi Fidusia kepada Penerima Fidusia berdasarkan Perjanjian Pembiayaan Konsumen, akan tetapi dengan kewajiban bagi Penerima Fidusia untuk menyerahkan sisa uang hasil bersih penjualannya jika masih ada kepada Pemberi Fidusia, dengan tidak ada kewajiban bagi Penerima Fidusia untuk membayar bunga atau ganti kerugian berupa apapun juga kepada Pemberi Fidusia mengenai sisa uang hasil bersih penjualan itu dan selanjutnya Penerima Fidusia juga berhak untuk melakukan segala sesuatu yang dipandang perlu dan berguna dalam rangka penjualan Obyek Jaminan Fidusia tersebut dengan tidak ada yang dikecualikan.

		<div width='310px' align='center'>-------------- Pasal 9 ----------------</div><br>
		- Dalam hal Penerima Fidusia mempergunakan hak-hak yang diberikan kepadanya seperti diuraikan di atas, Pemberi Fidusia wajib dan mengikat diri, sekarang ini untuk nanti pada waktunya, menyerahkan Obyek Jaminan Fidusia dalam keadaan terpelihara baik kepada Penerima Fidusia atas permintaan atau teguran pertama dari Penerima Fidusia dalam waktu sebagaimana ditentukan dalam surat permintaan atau teguran tersebut. Dalam hal Pemberi Fidusia tidak memenuhi ketentuan tersebut maka Pemberi Fidusia dianggap telah lalai sehingga tidak diperlukan lagi surat teguran juru sita atau surat lain yang serupa dengan itu. Dalam hal Pemberi Fidusia tidak menyerahkan Obyek Jaminan Fidusia pada waktu eksekusi dilaksanakan, Penerima Fidusia atau kuasanya yang sah berhak mengambil Obyek Jaminan Fidusia dan apabila perlu dapat meminta bantuan pihak yang berwenang, dengan ketentuan bahwa semua biaya yang berkaitan dengan itu menjadi beban dan wajib dibayar oleh Pemberi Fidusia atau Debitur. 

		<div width='310px' align='center'>-------------- Pasal 10 ----------------</div><br>
		- Pembebanan terhadap Obyek Jaminan Fidusia ini dilakukan oleh Pemberi Fidusia kepada Penerima Fidusia dengan syarat-syarat yang memutuskan (onder de ontbindende voorwaarden) yakni sampai dengan Debitur telah memenuhi/membayar lunas semua yang wajib dibayar oleh Debitur kepada Kreditur sebagaimana ditentukan dalam Perjanjian Pembiayaan Konsumen. 

		<div width='310px' align='center'>-------------- Pasal 11 ----------------</div><br>
		- Sepanjang diperlukan, Pemberi Fidusia dengan ini memberikan kuasa dengan hak substitusi kepada Penerima Fidusia yang menyatakan menerima kuasa dari Pemberi Fidusia untuk melaksanakan pendaftaran jaminan fidusia tersebut, untuk keperluan tersebut berhak menghadap di hadapan pejabat atau instansi yang berwenang (termasuk kantor pendaftaran fidusia), memberikan keterangan, menandatangani surat/formulir, mendaftarkan jaminan fidusia atas Obyek Jaminan Fidusia tersebut dengan melampirkan pernyataan pendaftaran jaminan fidusia, serta untuk mengajukan permohonan pendaftaran atas perubahan dalam hal terjadi perubahan atas data yang tercantum dalam sertipikat jaminan fidusia, selanjutnya menerima sertipikat jaminan fidusia dan/atau Pernyataan Perubahan, serta dokumen-dokumen lain yang berkaitan untuk keperluan itu, membayar semua biaya dan menerima semua kuitansi segala pembayaran serta selanjutnya melakukan segala tindakan yang perlu dan berguna untuk melaksanakan ketentuan dari akta ini. 
		- Akta ini merupakan bagian yang tidak dapat dipisahkan dari Perjanjian Pembiayaan Konsumen, demikian pula kuasa yang diberikan dalam akta ini merupakan bagian yang tidak terpisahkan dari akta ini yang tanpa adanya akta ini dan kuasa tersebut, niscaya Perjanjian Pembiayaan Konsumen demikian pula akta ini yang tanpa adanya akta ini dan kuasa tersebut, tidak akan diterima dan dilangsungkan diantara para pihak yang bersangkutan, oleh karenanya akta ini tidak dapat ditarik kembali atau dibatalkan selama kewajiban Debitur berdasarkan Perjanjian Pembiayaan Konsumen belum terpenuhi dan kuasa tersebut tidak akan batal atau berakhir karena sebab yang dapat mengakhiri pemberian sesuatu kuasa, termasuk sebab yang tercantum dalam pasal 1813, 1814, dan 1816 Kitab Undang-Undang Hukum Perdata Indonesia.

		<div width='310px' align='center'>-------------- Pasal 12 ----------------</div><br>
		-Penerima Fidusia berhak dan dengan ini diberi kuasa dengan hak subtitusi oleh Pemberi Fidusia untuk melakukan perubahan akta penyesuaian atas ketentuan dalam akta ini, dalam hal perubahan atau penyesuaian tersebut dalam rangka memenuhi ketentuan dalam peraturan pemerintah tentang pendaftaran fidusia maupun ketentuan dalam Undang-Undang nomor 42 Tahun 1999 tentang jaminan fidusia. 

		<div width='310px' align='center'>-------------- Pasal 13 ----------------</div><br>
		- Biaya akta ini dan biaya lainnya yang berkenaan dengan pembuatan akta ini maupun dalam melaksanakan ketentuan dalam akta ini menjadi beban dan wajib dibayar oleh---- Pemberi Fidusia, demikian pula pendaftaran fidusia ini di Kantor Pendaftaran Fidusia.

		<div width='310px' align='center'>-------------- Pasal 14 ----------------</div><br>
		- Segala perselisihan yang mungkin timbul di antara para pihak mengenai akta ini yang tidak dapat diselesaikan diantara para pihak sendiri, maka para pihak akan memilih domisili hukum yang tetap dan seumumnya di Kantor Panitera ".$d47.".
		- Segala Pemilihan domisili hukum tersebut dilakukan dengan tidak mengurangi hak dari Penerima Fidusia untuk mengajukan tuntutan hukum terhadap Pemberi Fidusia berdasarkan jaminan fidusia atas Obyek Jaminan Fidusia tersebut dihadapan Pengadilan lainnya dalam Wilayah Republik Indonesia, yaitu pada pengadilan negeri yang mempunyai yurisdiksi atas diri dari Pemberi Fidusia atau atas Obyek Jaminan Fidusia tersebut.

		<div width='310px' align='center'>DEMIKIAN AKTA INI</div><br>
		Dibuat sebagai minuta dan dilangsungkan di ".$d48.", pada hari dan tanggal tersebut pada bagian awal akta ini, dengan dihadiri oleh : <br>
		1.".$d39.", lahir di ".$d40.", pada tanggal ".$d41.", bertempat tinggal di ".$d42.", pemegang Kartu Tanda Penduduk Nomor : $d50; <br>
		2.".$d43.", lahir di ".$d44.", pada tanggal ".$d45.", bertempat tinggal di ".$d46.", pemegang Kartu Tanda Penduduk Nomor : $d51; <br>
		- keduanya pegawai Kantor Notaris, sebagai saksi-saksi.<br>
		- Atas permintaan penghadap, akta ini tidak dibacakan oleh saya, Notaris, akan tetapi penghadap dan para saksi sudah mengerti maksud dari isi akta ini, maka pada ketika itu juga penghadap, para saksi dan saya, Notaris menandatanganinya. Dilangsungkan dengan tanpa ada perubahan. Asli minuta akta ini telah ditandatanganinya<br>
		<br>
		-- Dilangsungkan Dengan
		<br>
		<br>
		<br>

		-- Penghadap; ------------------------------------<br>


		<br>
		".$d7."<br>
		<br>
		<br>

		---Saksi-saksi ; ---------------------------------<br>



		<div><p>1.".$d39."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.".$d43."</p></div>
		<br>
		<br>     				
		<br>
		<div style='margin-left:5cm;'>Notaris ".$d6.".</div>
		<br>
		<br>
		<br>
		<br>
		<div style='margin-left:5cm;'><b>".$d49."<b></div> </div>
	</div>
";
		echo "</body>";
		echo "</html>";
	}
	
	public function export_excel(){
		ob_clean();

		$get = $this->input->get();

		if(@$get['kontrak'] != ""){
			$where[] = "ed_nomor_kontrak LIKE '%$get[kontrak]%'";
		}
		if(@$get['status'] != ""){
			$where[] = "ed_status = '$get[status]'";
		}

		if(@$where != "") @$where = " AND ".join(" AND ",  $where);
		$data = $this->db->query("SELECT * FROM v_entry_data WHERE ed_status = '1' ".@$where);

		include('application/libraries/PHPExcel.php');
		// We'll be outputting an excel file
		header('Content-type: application/vnd.ms-excel');

		// It will be called file.xls
		header('Content-Disposition: attachment; filename="entry_data.xls"');

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
		$styleArray = array(
                    'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                  );
		$styleArray1 = array(
                     'borders' => array(
                    'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ),
                     ),
                    );
                    //background
                    $styleArray12 = array(
                    'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array(
                    'rgb' => 'FFEC8B',
                    ),
                    ),
									);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
    	$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->mergeCells('A1:P1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'EXPORT DATA');
		$row = 3;
		$rowawal = $row;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':P'.$row)->applyFromArray($styleArray);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':P'.$row)->applyFromArray($styleArray1);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':P'.$row)->applyFromArray($styleArray12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'No');
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, 'No Order');
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, 'Nama Konsumen');
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, 'No Mesin');
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, 'No Rangka');
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, 'Tipe Kendaraan');
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, 'OTR');
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, 'Pokok');
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, 'Bunga');
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, 'Biaya Notaris');
		$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, 'Status Reason');
		$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, 'Nama Cabang');
		$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, 'No AKTA');
		$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row, 'No Resi');
		$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, 'No Sertifikat');
		$objPHPExcel->getActiveSheet()->SetCellValue('P'.$row, 'Tgl Terima');
		$row++;
		$objPHPExcel->getActiveSheet()->freezePane('A'.$row);
		foreach ($data->result() as $key => $dt) {
			//$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':P'.$row)->applyFromArray($styleArray);
      		//$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':P'.$row)->applyFromArray($styleArray1);
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, ($row - $rowawal));
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $dt->kon_nama);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $dt->dk_no_mesin);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $dt->dk_no_rangka);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $dt->dk_tipe);
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row, $dt->pemb_no);
			$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('P'.$row, '');
			$row++;
		}

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// Write file to the browser
		$writer->save('php://output');
	}

	public function export_pdf($perbank = false){
      $limit = 12;
      $this->load->library('fpdf');
      $panjang = 200;
      $lebar = 200;
      $margin = 10;
			$post = $this->input->post();
			if(@$post['nama_konsumen'] != ""){
				$where[] = "kon_nama LIKE '%$post[nama_konsumen]%'";
			}
			if(@$post['alamat_konsumen'] != ""){
				$where[] = "kon_alamat LIKE '%$post[alamat_konsumen]%'";
			}
			if(@$post['ktp_konsumen'] != ""){
				$where[] = "kon_ktp LIKE '%$post[ktp_konsumen]%'";
			}
			if(@$post['status'] != ""){
				$where[] = "ed_status = '$post[status]'";
			}
			if(@$where != "") @$where = " AND ".join(" AND ",  $where);
			$data = $this->db->query("SELECT * FROM v_entry_data WHERE ed_status = '1' ".@$where);
			$option['jenis_kelamin'] = array('1' => 'Laki-Laki', '2' => 'Perempuan');
			$option['status_pernikahan'] = array('' => 'Belum Kawin', '1' => 'Suami', '2' => 'Istri');
			$pdf = new FPDF('P', 'mm', 'A4');
      $pdf->AddPage();
      $pdf->SetAutoPageBreak(false, 0);
      $t = $panjang;
      $pdf->SetFont('Arial', 'B', 12);
      $pdf->Cell($t, 7, 'EXPORT DATA ', 0, 0, 'C');
      $pdf->Ln();
      //$pdf->Cell($t - $margin, 7, 'TANGGAL ', 0, 0, 'C');
      $pdf->Ln();
      $pdf->Ln();
      $pdf->SetFont('Arial', '', 7);
      $x = $pdf->GetX();
      $y = $pdf->GetY();
      $mleft = $x;
      $w = 0;
      $l1 = 12;
      $l2 = 35;
      $l3 = 46;
      $l4 = 70;
      $l5 = 15;
			$l6 = 55;
      $h1 = 7;
      $h2 = 7;
      $h3 = 7;



			foreach ($data->result() as $key => $dt) {
				if ($pdf->GetY() > ($lebar - $margin)) {
          $pdf->AddPage();
					$pdf->SetY($pdf->GetY());

												$pdf->SetFont('Arial', 'B', 12);
									      $pdf->Cell($t, 7, 'LAPORAN ', 0, 0, 'C');
									      $pdf->Ln();
									      //$pdf->Cell($t - $margin, 7, 'TANGGAL ', 0, 0, 'C');
									      $pdf->Ln();
									      $pdf->Ln();
									      $pdf->SetFont('Arial', '', 7);
												  $y = $pdf->GetY();
				}
				$x = $mleft;
				$yy = $y;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l2, $h1 * 2, 'DATA KONSUMEN', 0, 'L');
				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nama', 0, 'L');
				$pdf->SetXY($x= $w, $y);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->kon_nama, 0, 'L');
				$xx = $l3 + $l4;

				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nomor KTP', 0, 'L');
				$pdf->SetXY($x= $w, $y);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->kon_ktp, 0, 'L');
				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Jenis Kelamin', 0, 'L');
				$pdf->SetXY($x= $w, $y);
	      $pdf->MultiCell($w = $l4, $h1 * 2,	@$option['jenis_kelamin'][$dt->kon_jenis_kelamin], 0, 'L');
				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Status Pernikahan', 0, 'L');
				$pdf->SetXY($x= $w, $y);
	      $pdf->MultiCell($w = $l4, $h1 * 2, @$option['status_pernikahan'][$dt->kon_status_pernikahan], 0, 'L');
				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Alamat Konsumen', 0, 'L');
				$pdf->SetXY($x= $w, $y);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->kon_alamat, 0, 'L');
				$y += $h1;
				$y += $h1;
				$pdf->MultiCell($w = $l2, $h1 * 2, 'DATA PEMBIAYAAN', 0, 'L');
				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nomor', 0, 'L');
				$pdf->SetXY($x= $w, $y);
		      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->pemb_no, 0, 'L');
				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Tanggal', 0, 'L');
				$pdf->SetXY($x= $w, $y);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->pemb_tanggal, 0, 'L');
				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nilai Penjaminan', 0, 'L');
				$pdf->SetXY($x= $w, $y);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->pemb_nilai_penjaminan, 0, 'L');
				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nilai Benda Fidusia', 0, 'L');
				$pdf->SetXY($x= $w, $y);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->pemb_nilai_benda_fidusia, 0, 'L');


				$chky = $pdf->GetY();


				$xx = $l3 + $l4;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'DATA PEMILIK KENDARAAN', 0, 'L');

				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nama', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->pem_nama, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nomor KTP', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->pem_ktp, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Jenis Kelamin', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, @$option['jenis_kelamin'][$dt->pem_jenis_kelamin], 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Alamat Pemilik', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->pem_alamat, 0, 'L');
				$yy += $h1;
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'DATA KENDARAAN', 0, 'L');

				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Merk', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->dk_merk, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Tipe', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->dk_tipe, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Tahun Pembuatan', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->dk_tahun_pembuatan, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nomor Rangka', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->dk_no_rangka, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nomor Mesin', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->dk_no_mesin, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Cover Note Dealer', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->dk_covernote, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nomor Polisi', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->dk_no_polisi, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nomor BPKB', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->dk_no_bpkb, 0, 'L');

				$chkyy = $pdf->GetY();
				if($chkyy > $chky){
					$y = $chkyy;
				}else{
					$y = $chky;
				}
				$y = $y - 10;
				$pdf->SetY($y);
				$pdf->MultiCell(($l4 * 4), $h1 * 2, '----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------', 0, 'L');
				$y += $h1;

			}




			$y += $h1;
      $x = $mleft;
      $x += $l1 + $l2 + $l3;
			$pdf->Output('Laporan', 'I');
    }

	public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 				= [['pembiayaan b', 'v_entry_data.ed_pembiayaan_id = b.pemb_id','inner']];
		$where_e 			= "ed_flag IN('1', '2', '3', '4', '5', '6')";

		// if ((!in_array(user_data()->user_level, ['1', '5']))) {
		// 	$where['ed_cabang'] = user_data()->cabang_id;
		// } else {
		// 	$where 	= null;
		// }
			
		if(in_array(user_data()->user_role, ['24'])){
			$arrcabangnotaris = $this->m_global->get_data_all('notaris', [['notaris_admin','notaris_admin.na_notaris = notaris.notaris_id','left']], ['na_admin' => user_data()->user_id, 'notaris_status' => '1']);
			foreach($arrcabangnotaris as $a => $b){
				$arrcabangid[$a] = $b->notaris_cabang_id;
			}
			$cabangid = join('|', $arrcabangid);
			$where_e = $where_e." AND ed_cabang IN('".str_replace("|","', '",$cabangid)."') "; 
			
		}
		
		$search 	= [
			"kontrak" 	=> $this->table_prefix.'nomor_kontrak',
			"status" 	=> $this->table_prefix.'status',
			'cabang' 	=> $this->table_prefix.'cabang'
		];

		if (@$post['action'] == 'filter')
		{
			$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = $where_e." AND DATE(ed_lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."' AND '".$this->db->escape_str($tmp[2])."'";

					} else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			$where[$this->table_prefix.'status <>'] = '99';
		}

		$keys 		= array_keys($search);
		$order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

		$select		= '*, '.implode(',', $search);
		$count 		= $this->m_global->count_data_all( 'v_'.$this->table_db, $join, $where, $where_e );
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


		$data = $this->m_global->get_data_all('v_'.$this->table_db, $join, $where, $select, $where_e, $order, $start, $length);
		$i = 1 + $start;

		$status = [
					'0' => '<span class="uk-badge uk-badge-warning">InActive</span>',
					'1' => '<span class="uk-badge uk-badge-primary">Active</span>',
					'99' => '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
				  ];

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
				$rows->ed_nomor_kontrak,
				$rows->cabang_name,
				flag_status($rows->ed_flag),
				$rows->pemb_note ? '<span class="uk-badge uk-badge-danger">Berkas Hardcopy</span>':'<span class="uk-badge uk-badge-primary">Berkas SoftCopy</span>' ,
				$rows->kon_nama,
				$rows->kon_alamat,
				$rows->kon_ktp,
				$rows->pem_nama,
				$rows->pem_alamat,
				$rows->pem_ktp,
				uang($rows->pemb_nilai_penjaminan,null),
				uang($rows->pemb_nilai_benda_fidusia,null),
				$rows->dk_merk,
				$rows->dk_tipe,
				$rows->dk_tahun_pembuatan,
				$rows->dk_no_rangka,
				$rows->dk_no_mesin,
				$rows->dk_no_polisi,
				$rows->dk_no_bpkb,
				$rows->dk_covernote,
				date('d-m-Y', strtotime($rows->ed_tanggal_jual)),
				$this->_button($rows->ed_id, $rows->ed_status, $rows->ed_flag, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

	public function _button($id, $status, $flag, $show = false)
	{
		$id = strEncrypt($id);
		$button = '';

		if($show) {
			$cek 	= '<a data-uk-tooltip title="Cek Data" class="ajaxify md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="'.base_url('report/show_preview').'/'.$id.'"><i class="uk-icon-warning uk-icon-small"></i></a>';

			$button = $cek;
		} else {
			$button = '-';
		}

		return $button;
	}

	public function show_preview($id)
	{
		$data['lampiran'] 	 = $this->m_global->get_data_all('lampiran', null, [strEncrypt('lamp_ed_id', TRUE) => $id, 'lamp_tipe' => 'UPD1']);
		$data['name'] 		   = "Config User";
		$data['url'] 		 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'Laporan' => base_url('report'), 'Cek Data' => base_url('report/show_preview').'/'.$id];
		$data['plugin'] 	   = ['kendo_ui','dropify', 'datatables', 'datatables_fixcolumns', 'pdfobject' => 'js', 'inputmask'];
		$data['option']		   = array();
		$data['option']['jenis_kelamin'] = array('1' => 'Laki-Laki', '2' => 'Perempuan');
		$data['option']['status_pernikahan'] = array('0' => 'Belum Menikah', '1' => 'Suami', '2' => 'Istri');
		// $data['prop']		 = dataHelper('prop');
		$data['upload'] 	 = array('satu' => 'Satu', 'dua' => 'Dua', 'tiga' => 'Tiga', 'empat' => 'Empat', 'lima' => 'Lima', 'enam' => 'Enam');
		$kond = "Notaris.notaris_cabang_id LIKE '%CONCAT(' | ', entry_data.ed_cabang, ' | ')%'";
		$data['record']		 = $this->m_global->get_data_all('v_entry_data a', [['pembiayaan b','a.ed_pembiayaan_id = b.pemb_id','inner']], [strEncrypt('ed_id', TRUE) => $id]);

		$data['akta']		 = $this->m_global->get_data_all('akta', [['daerah a', 'a.daerah_id = akta.daerah_id', 'left']], [strEncrypt('ed_id', TRUE) => $id]);
		$data['report']		 = $this->m_global->get_data_all('v_entry_data', [['negara a', 'a.negara_id = kon_negara', 'left'], ['negara b', 'b.negara_id = pem_negara', 'left'], ['cabang c', 'c.cabang_id = ed_cabang', 'left']], [strEncrypt('ed_id', TRUE) => $id], '*, a.negara_name kon_nama_negara, b.negara_name pem_nama_negara');
		$idcabang = $data['report'][0]->ed_cabang;
		$data['pejabat'] = $this->m_global->get_data_all('kuasa_cabang a',[['kuasa_direksi b','a.kc_pejabat = b.kd_id','inner']],['a.kc_cabang'=>$idcabang]);

		$data['report'][0]->jumlah_hutang = $data['report'][0]->pemb_nilai_penjaminan;

		$data['report'][0]->pemb_nilai_penjaminan = uang($data['report'][0]->pemb_nilai_penjaminan,null);
		$data['report'][0]->pemb_nilai_benda_fidusia = uang($data['report'][0]->pemb_nilai_benda_fidusia,null);

		$data['file']		 = $this->m_global->get_data_all('lampiran_referensi', [['referensi', 'lamp_tipe = referensi_tipe AND lampiran_referensi.referensi_id = referensi.referensi_id', 'left']], [strEncrypt('lamp_ed_id', TRUE) => $id, 'lamp_tipe' => 'UPD1']);
		$data['upload']		 = $this->m_global->get_data_all('referensi', null, ['referensi_tipe' => 'UPD1']);
		foreach($data['file'] as $a => $b){
			$data['arrfile'][$b->referensi_id] = true;
		}


		$kondnotaris = "CONCAT('|' ,a.notaris_cabang_id,'|') LIKE CONCAT('%|',(SELECT v_entry_data.ed_cabang FROM `v_entry_data` WHERE md5(concat('wom_finance_demo_v.0.1',ed_id)) = '".$id."'),'|%') AND a.notaris_status <> '99'";
		$selectnotaris = "a.notaris_id,a.notaris_nama,a.notaris_kd_id1,kd1.*";

		$data['notaris'] = $this->m_global->get_data_all('notaris a', [['kuasa_direksi kd1','kd1.kd_id = a.notaris_kd_id1','left'],['kuasa_direksi kd2','kd2.kd_id = a.notaris_kd_id1','left']], $kondnotaris,$selectnotaris);

		// $provselect = "`daerah`.`daerah_id` AS `daerah_id`,
		// 			   `daerah`.`daerah_kode_pos` AS `daerah_kode_pos`,
		// 			   `daerah`.`daerah_kelurahan` AS `daerah_kelurahan`,
		// 			   `daerah`.`daerah_kecamatan` AS `daerah_kecamatan`,
		// 			   `daerah`.`daerah_jenis_kota` AS `daerah_jenis_kota`,
		// 			   `daerah`.`daerah_kota` AS `daerah_kota`,
		// 			   `daerah`.`daerah_provinsi` AS `daerah_provinsi`";

		// $data['provinsi'] = $this->m_global->get_data_all('daerah', null, null,$provselect,null,null,null,null,'daerah.daerah_provinsi');
		$data['provinsi'] = array();

		if(@$data['akta'][0]->daerah_kota != ""){
			$data['kota'] = $this->m_global->get_data_all('v_kota', null, ['daerah_provinsi' => $data['akta'][0]->daerah_provinsi]);
			$data['kecamatan'] = $this->m_global->get_data_all('v_kecamatan', null, ['daerah_provinsi' => $data['akta'][0]->daerah_provinsi, 'CONCAT(daerah_jenis_kota, " ", daerah_kota) =' => $data['akta'][0]->daerah_jenis_kota.' '.$data['akta'][0]->daerah_kota]);
			$data['kelurahan'] = $this->m_global->get_data_all('daerah', null, ['daerah_provinsi' => $data['akta'][0]->daerah_provinsi, 'CONCAT(daerah_jenis_kota, " ", daerah_kota) =' => $data['akta'][0]->daerah_jenis_kota.' '.$data['akta'][0]->daerah_kota, 'daerah_kecamatan' => $data['akta'][0]->daerah_kecamatan]);
			
		}
		$data['id'] 		 = $id;
		$data['negara'] 	 = $this->m_global->get_data_all('negara');
		$data['user_tugas']  = 'admin';
		// $this->act_redownload($data['report'][0]->ed_nomor_kontrak, $data['lampiran'][0]);
		
		$pathPdfFile = explode('/', $data['lampiran'][0]->lamp_lokasi_file);
		$data['pathPdfFile'] = $pathPdfFile[0].'/uploads/'.$pathPdfFile[2].'/';

		$this->template->display('entry_data_preview', $data);
	}
	
	public function get_kota(){
		$id = @$_POST['provinsi_id'];
		$html = '<option value=""></option>';
		if(@$id != ""){
			$select = "`daerah`.`daerah_id` AS `daerah_id`,
					   `daerah`.`daerah_kode_pos` AS `daerah_kode_pos`,
					   `daerah`.`daerah_kelurahan` AS `daerah_kelurahan`,
					   `daerah`.`daerah_kecamatan` AS `daerah_kecamatan`,
					   `daerah`.`daerah_jenis_kota` AS `daerah_jenis_kota`,
					   `daerah`.`daerah_kota` AS `daerah_kota`,
					   `daerah`.`daerah_provinsi` AS `daerah_provinsi`";
			$group = "concat(
				      `daerah`.`daerah_jenis_kota`,
					  ' ',
					  `daerah`.`daerah_kota`)";

			$arrdata = $this->m_global->get_data_all('daerah', null, ['daerah_provinsi' => $id],$select,null,null,null,null,$group);
			foreach($arrdata as $a=>$b){
				$html = $html.'<option value="'.$b->daerah_jenis_kota.' '.$b->daerah_kota.'">'.$b->daerah_jenis_kota.' '.$b->daerah_kota.'</option>';
			}
		}
		echo $html;
	}
	
	public function get_kecamatan(){
		$provinsi_id = @$_POST['provinsi_id'];
		$id = @$_POST['kota_id'];
		$html = '<option value=""></option>';
		if(@$id != ""){

			$select = "`daerah`.`daerah_id` AS `daerah_id`,
					   `daerah`.`daerah_kode_pos` AS `daerah_kode_pos`,
					   `daerah`.`daerah_kelurahan` AS `daerah_kelurahan`,
					   `daerah`.`daerah_kecamatan` AS `daerah_kecamatan`,
					   `daerah`.`daerah_jenis_kota` AS `daerah_jenis_kota`,
					   `daerah`.`daerah_kota` AS `daerah_kota`,
					   `daerah`.`daerah_provinsi` AS `daerah_provinsi`";
			$group = " `daerah`.`daerah_kecamatan`,
						CONCAT(
						`daerah`.`daerah_jenis_kota`,
						' ',
						`daerah`.`daerah_kota`
						),
						`daerah`.`daerah_provinsi`";
			$arrdata = $this->m_global->get_data_all('daerah', null, ['daerah_provinsi' => $provinsi_id, 'CONCAT(daerah_jenis_kota, " ", daerah_kota) =' => $id],$select,null,null,null,null,$group);
			foreach($arrdata as $a=>$b){
				$html = $html.'<option value="'.$b->daerah_kecamatan.'">'.$b->daerah_kecamatan.'</option>';
			}
		}
		echo $html;
	}
	
	public function get_kelurahan(){
		$provinsi_id = @$_POST['provinsi_id'];
		$kota_id = @$_POST['kota_id'];
		$id = @$_POST['kecamatan_id'];
		$html = '<option value=""></option>';
		if(@$id != ""){
			$arrdata = $this->m_global->get_data_all('daerah', null, ['daerah_kecamatan' => $id, 'daerah_provinsi' => $provinsi_id, 'CONCAT(daerah_jenis_kota, " ", daerah_kota) =' => $kota_id]);
			foreach($arrdata as $a=>$b){
				$html = $html.'<option value="'.$b->daerah_id.'">'.$b->daerah_kelurahan.'</option>';
			}
		}
		echo $html;
	}
	
	public function get_asd(){ 
		$id = @$_POST['daerah_id'];
		$html = '<option value=""></option>';
		if(@$id != ""){
			$arrdata = $this->m_global->get_data_all('daerah', null, ['daerah_id' => $id]);
			$arr = $arrdata[0]->daerah_kode_pos.'|'.$arrdata[0]->daerah_pengadilan;
		}
		echo $arr;
	}

	public function log($id)
	{

	  $post 		= $this->input->post();
	  $length 	= intval($post['length']);
	  $start  	= intval($post['start']);
	  $sEcho		= intval($post['draw']);
	  $join 				= [['roles', 'log_ed_user_role = role_id', 'left'], ['referensi', "'FLAG' = referensi_tipe AND log_ed_flag = referensi.referensi_id", 'left']];
	  $where['log_ed_id'] 	= $id;
	  $where_e 			= null;

	  $search 	= [
	          "log_ed_user_role" 		=> 'log_ed_user_role',
	          "log_ed_action" 		=> 'log_ed_action',
	          "log_ed_flag" 			=> 'log_ed_flag',
	          "log_ed_date" 				=> 'log_ed_date'
	          ];

	  if (@$post['action'] == 'filter')
	  {
	    $where = [];
	    foreach ( $search as $key => $value )
	    {
	      if ( $post[$key] != '' )
	      {
	        if ( $key == 'lastupdate' )
	        {
	          $tmp = explode(' ', $post[$key]);
	          $where_e = "DATE(ed_lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

	        } else {
	          $where[$value.' LIKE '] = '%'.$post[$key].'%';
	        }
	      }
	    }
	  } else {
	    $where['log_ed_id ='] = $id;
	  }

	  $keys 		= array_keys($search);
	  $order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

	  $select		= 'log_ed_id, log_ed_action, role_name, referensi_nama, log_ed_date, '.implode(',', $search);
	  $count 		= $this->m_global->count_data_all('log_entry_data', $join, $where, $where_e );
	  $length 	= $length < 0 ? $count : $length;
	  $end 		= $start + $length;
	  $end 		= $end > $count ? $count : $end;

	  $result['iTotalRecords'] 			= $count;
	  $result['iTotalDisplayRecords'] 	= $length;

	  $records 			= array();
	  $records["data"]	= array();


	  $data = $this->m_global->get_data_all('log_entry_data', $join, $where, $select, $where_e, $order, $start, $length);

	  $i = 1 + $start;

	  $status = [
	        '0' => '<span class="uk-badge uk-badge-warning">InActive</span>',
	        '1' => '<span class="uk-badge uk-badge-primary">Active</span>',
	        '99' => '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
	        ];

	  foreach ( $data as $rows ) {
	    $records["data"][] = array(
	      $i,
	      $rows->log_ed_action,
	      $rows->role_name,
	      $rows->referensi_nama,
	      $rows->log_ed_date
	    );
	    $i++;
	  }

	  $records["draw"]            = $sEcho;
	  $records["recordsTotal"]    = $count;
	  $records["recordsFiltered"] = $count;

	  echo json_encode($records);
	}

	public function view_lampiran($ed_id){
		$file    	= $this->m_global->get_data_all('lampiran', null, [strEncrypt('lamp_ed_id', TRUE) => $ed_id], 'lamp_lokasi_file')[0]->lamp_lokasi_file;
		$nama_file  = $this->m_global->get_data_all('lampiran', null, [strEncrypt('lamp_ed_id', TRUE) => $ed_id], 'lamp_nama')[0]->lamp_nama;
		$file = $file."/".$nama_file;

		$file = str_replace('system/', '', str_replace('\\', '/',BASEPATH)).str_replace('./', '',str_replace(base_url(), '', $file));
		$imginfo = getimagesize($file);
		header("Content-type: $imginfo[mime]");
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
		ob_clean();
        flush();
        readfile($file);
        exit;

	}

	public function action_approve()
	{
		$result = [];
		$post 	= $this->input->post();
		$search = array(' ',".",'Rp');
		$id = $post['id'];
				$this->db->trans_begin();
				$param_id = $this->m_global->get_data_all('v_entry_data', null, [strEncrypt('ed_id', TRUE) => $id]);
				$note = '';
				if($post['status_now'] == "1"){
					if($post['aksi'] == '1'){
						// update konsumen 
						$konsumen 	= [
							'kon_ktp' 					 => $post['nomor_ktp_konsumen'],
							'kon_alamat' 				 => $post['alamat_konsumen'],
							'kon_jenis_kelamin' 		 => $post['jk_konsumen'],
							'kon_status_pernikahan'		 => $post['pernikahan_konsumen'],
							'kon_pekerjaan' 			 => $post['pekerjaan_konsumen'],
							'kon_tanggal_lahir' 		 => date("Y-m-d", strtotime($post['tanggal_lahir_konsumen'])),
							'kon_nama_pasangan'			 => $post['nama_pasangan'],
							'kon_tempat_lahir_pasangan'  => $post['tempat_lahir_pasangan'],
							'kon_tanggal_lahir_pasangan' => date("Y-m-d", strtotime($post['tanggal_lahir_nasabah'])),
							'kon_alamat_pasangan'		 => $post['alamat_pasangan'],
							'kon_pekerjaan_pasangan'     => $post['pekerjaan_pasangan'],
							'kon_rw'     				 => $post['rw_konsumen'],
							'kon_rt'     				 => $post['rt_konsumen'],
							'kon_telp'    				 => $post['telp_konsumen'],
							'kon_kode_pos'  		     => $post['kode_pos_konsumen'],
							'daerah_id'    				 => $post['daerah_id'],
							'kon_ktp_pasangan'			 => $post['nomor_ktp_pasangan']
						];
					
						$pemilik = isset($post['ktp_pemilik']);
						if($pemilik) {
							$pemilik = [
								'pem_ktp' 			=> $post['ktp_pemilik'],
								'pem_alamat' 		=> $post['alamat_pemilik'],
								'pem_jenis_kelamin' => $post['jk_pemilik'],
								'pem_pekerjaan' 	=> $post['pekerjaan_pemilik'],
								'pem_tempat_lahir' 	=> $post['tempat_lahir_pemilik'],
								'pem_tanggal_lahir' => date("Y-m-d", strtotime($post['tanggal_lahir_pemilik'])),
							];

						}
					
						if($post['nilai_penjaminan'] <= '50000000'){
							$pnbp = '50000';
						}else if($post['nilai_penjaminan'] <= '100000000'){
							$pnbp = '100000';
						}else if($post['nilai_penjaminan'] <= '250000000'){
							$pnbp = '200000';
						}else if($post['nilai_penjaminan'] <= '500000000'){
							$pnbp = '400000';
						}else if($post['nilai_penjaminan'] <= '1000000000'){
							$pnbp = '5000000';
						}else if($post['nilai_penjaminan'] <= '100000000000'){
							$pnbp = '1800000';
						}

						$pembiayaan 	= [
							'pemb_tanggal' 				=> date('Y-m-d', strtotime($post['tgl_pembiayaan'])),
							'pemb_nilai_penjaminan' 	=> $post['nilai_penjaminan'],
							'pemb_pnbp' 				=> $pnbp,
							'pemb_ttd_debitur' 				=> $post['pemb_ttd_debitur'],
							'pemb_jangka_tanggal'	=>date('Y-m-d', strtotime($post['pemb_jangka_tanggal']))
							,
							'pemb_sampai_tanggal'	=>date('Y-m-d', strtotime($post['pemb_sampai_tanggal'])),
						];

						if(@$post['tanggal_dokumen']){
							$pembiayaan['pemb_date'] = $post['tanggal_dokumen'];
						}
					
						$kendaraan 	= [
							'dk_no_polisi' 			=> $post['nomor_polisi'],
							'dk_no_bpkb' 			=> $post['nomor_bpkb'],
							'dk_golongan' 			=> $post['tipe_roda'],
						];
						

						$entry_data['ed_lastupdate'] = date('Y-m-d H:i:s');
						$entry['entry'] = $this->m_global->update('entry_data', $entry_data, [strEncrypt('ed_id', TRUE) => $id]);

						$param_id = $this->m_global->get_data_all('entry_data', null, [strEncrypt('ed_id', TRUE) => $id]);
						
						$this->m_global->update('konsumen', $konsumen, ['kon_id' => $param_id[0]->ed_data_kendaraan_id]);
						$this->db->query("UPDATE kosumen set kon_tempat _lahir = ".$post['tempat_lahir_konsumen']." WHERE kon_id = ".$param_id[0]->ed_data_kendaraan_id);

						if($pemilik) {
							$this->m_global->update('pemilik', $pemilik, ['pem_id' => $param_id[0]->ed_pemilik_id]);
						}
						
						$this->m_global->update('pembiayaan', $pembiayaan, ['pemb_id' => $param_id[0]->ed_pembiayaan_id]);
						
						$this->m_global->update('data_kendaraan', $kendaraan, ['dk_id' => $param_id[0]->ed_data_kendaraan_id]);

						$entry['ed_id'] = $param_id[0]->ed_id;
						$update = array('ed_flag' => '2');
						$result['inp'] = '2';
						$action = 'Akte Fidusia';
					}else{
						$note = '(Notaris) '.$post['note'];
						$update = array('ed_flag' => '9');
						$result['inp'] = '2';
						$action = 'Tolak';
					}
					$entry['entry'] = $this->m_global->update('entry_data', $update, [strEncrypt('ed_id', TRUE) => $id]);
					$result['msg'] = 'Data Berhasil Dimasukkan';
				}else if($post['status_now'] == "2"){
					if($post['voucher'] == ""){
						$update = array('ed_flag' => '2');
					}else{
						$update = array('ed_flag' => '3');
					}
					$paramnotaris = $this->m_global->get_data_all('notaris', null, ['notaris_id' => $post['notaris_id']]);
					$idd1 = [
							'ed_id' 		=> $param_id[0]->ed_id
							];							
					$this->m_global->delete('akta', $idd1);
					$post['hutang_nasabah'] = (int)str_replace($search, "", $post['hutang_nasabah']);
					$post['nilai_jaminan'] = (int)str_replace($search, "", $post['nilai_jaminan']);
					$akta = array('akta_nomor' => $post['nomor'],
								  'akta_voucher' => $post['voucher'],
								  'akta_waktu' => $post['tanggal'],
								  'akta_nama_penerima' => $post['nama_penerima'],
								  'akta_tempat_lahir_penerima' => $post['tempat_lahir_penerima'],
								  'akta_tanggal_lahir_penerima' => $post['tanggal_lahir_penerima'],
								  'akta_pekerjaan_penerima' => $post['pekerjaan_penerima'],
								  'akta_alamat_penerima' => $post['alamat_penerima'],
								  'akta_nik_penerima' => $post['nik_penerima'],
								  'akta_tanggal_penerima' => $post['tanggal_penerima'],
								  'akta_nama_nasabah' => $post['nama_nasabah'],
								  'akta_kode_pos_nasabah' => $post['kode_pos_nasabah'],
								  'akta_tempat_lahir_nasabah' => $post['tempat_lahir_nasabah'],
								  'akta_tanggal_lahir_nasabah' => $post['tanggal_lahir_nasabah'],
								  'akta_pekerjaan_nasabah' => $post['pekerjaan_nasabah'],
								  'akta_alamat_nasabah' => $post['alamat_nasabah'],
								  'akta_telp_nasabah' => $post['telepon_nasabah'],
								  'akta_nik_nasabah' => $post['nik_nasabah'],
								  'akta_tanggal_kuasa' => $post['tanggal_kuasa'],
								  'akta_nomor_kuasa' => $post['nomor_kuasa'],
								  'akta_nomor_pk' => $post['nomor_pk'],
								  'akta_tanggal_pk' => $post['tanggal_pk'],
								  'akta_hutang_nasabah' => $post['hutang_nasabah'],
								  'akta_nilai_jaminan' => $post['nilai_jaminan'],
								  'notaris_id' => $post['notaris_id'],
								  'saksi_id1' => $paramnotaris[0]->notaris_saksi_id1,
								  'saksi_id2' => $paramnotaris[0]->notaris_saksi_id2,
								  'daerah_id' => $post['daerah_id'],
								  'akta_pengadilan' => $post['pengadilan'],
								  'akta_perjanjian_kredit' => $post['perjanjian_kredit'],
								  'ed_id' => $param_id[0]->ed_id,
								  'akta_nama_pembeban'=>$post['nama_pembeban'],
								  'akta_hutang_pokok'=>$post['hutang_pokok']
								  );

					$this->m_global->insert('akta', $akta);
					$result['inp'] = '2';
					$action = 'Proses AHU';
					$entry['entry'] = $this->m_global->update('entry_data', $update, [strEncrypt('ed_id', TRUE) => $id]);
					$result['msg'] = 'Data Berhasil Dimasukkan';
					$result['key'] = '1'; 
				}else if($post['status_now'] == "3"){

					$this->form_validation->set_rules('no_akta','nomor ntpn','required');

					if ($this->form_validation->run() == true) {
						$update = array('ed_flag' => '4',
									'ed_ntpn' => $post['no_akta']);
						$entry['entry'] = $this->m_global->update('entry_data', $update, [strEncrypt('ed_id', TRUE) => $id]);
						$result['msg'] = 'Data Berhasil Dimasukkan';
						$result['inp'] = '2';
						if ($entry['entry']) {
						$entry['ed_id'] = $param_id[0]->ed_id;
						$entry['nomor_kontrak'] = $param_id[0]->ed_nomor_kontrak;
						$result['uploadmsg'] = $this->do_upload($entry);

						$result['sts'] = '3';
					}
					$action = 'PNBP';
					} else if($this->form_validation->run() == false) {
						$valid = validation_errors();
						$result['sts'] = '5';
						$result['alert'] = $result['alert'] = '<div class="uk-alert uk-alert-danger" data-uk-alert="">
									   <a href="#" class="uk-alert-close uk-close"></a>'.$valid.'</div>';
						echo json_encode($result);
						exit();
					}


				}else if($post['status_now'] == "4"){
					$this->form_validation->set_rules('nomor','nomor sertifikat fidusia','required');
					$this->form_validation->set_rules('tanggal','tanggal sertifikat fidusia','required');

					if ($this->form_validation->run() == true) {
						$result['msg'] = 'Data Berhasil Dimasukkan';
						$result['inp'] = '2';
						$update = array('ed_flag' => '5',
									'ed_nomor_fidusia' => $post['nomor'],
									'ed_tanggal_fidusia' => date('Y-m-d', strtotime($post['tanggal'])));
						$entry['entry'] = $this->m_global->update('entry_data', $update, [strEncrypt('ed_id', TRUE) => $id]);
						if ($entry['entry']) {
							$entry['ed_id'] = $param_id[0]->ed_id;
							$entry['nomor_kontrak'] = $param_id[0]->ed_nomor_kontrak;
							$result['uploadmsg'] = $this->do_upload($entry);

							$result['sts'] = '3';
							$action = 'Selesai';
						}
					} else if($this->form_validation->run() == false) {
						$valid = validation_errors();
						$result['sts'] = '5';
						$result['alert'] = $result['alert'] = '<div class="uk-alert uk-alert-danger" data-uk-alert="">
									   <a href="#" class="uk-alert-close uk-close"></a>'.$valid.'</div>';
						echo json_encode($result);
						exit();
					}


				}  else if($post['status_now'] == "5") {

					$akta_id = [
						'ed_id' 		=> $post['akta_id']
					]; 

					$akta = [
						'akta_nomor' => $post['nomor'],
						'akta_waktu' => $post['tanggal'],
						'ed_id'			 => $post['akta_id'],
					];

					$arg_entry_data = [
						'ed_nomor_fidusia' => $post['sertifikat']
					];

					$this->m_global->update('entry_data', $arg_entry_data, $akta_id);
					$this->m_global->delete('akta', $akta_id);
					$this->m_global->insert('akta', $akta);
					$this->db->trans_commit();
					$result['msg'] = 'Data Berhasil Dimasukkan';
					$result['sts'] = '1';
					$result['inp'] = '2';
					echo json_encode($result); exit();
				}
				
				$entry_data = array('log_ed_id' 		   => $param_id[0]->ed_id, 
									'log_ed_nomor_kontrak' => $param_id[0]->ed_nomor_kontrak, 
									'log_ed_note' 		   => $note, 'log_ed_action' => $action, 
									'log_ed_user_id' 	   => user_data()->user_id, 
									'log_ed_user_role' 	   => user_data()->user_role, 
									'log_ed_flag' 		   => $update['ed_flag']);
				$entry['entry'] = $this->m_global->insert('log_entry_data', $entry_data);

				if($this->db->trans_status() === false){
				        $this->db->trans_rollback();
								$result['msg'] = 'Data Gagal Dimasukan';
								$result['sts'] = '0';
				}else{
				        $this->db->trans_commit();
								$result['msg'] = 'Data Berhasil Dimasukkan';
								$result['sts'] = '1';
				}


		echo json_encode($result);
	}

	public function do_upload($entry, $datatype = false){
		$result = [];
		$post 	= $this->input->post();
		if ($entry['entry']) {
			// upload file untuk wom
			// set_include_path('phpseclib');
			// include('phpseclib/Net/SFTP.php');
			// $sftp = new Net_SFTP('112.215.7.49');
			
			// if (!$sftp->login('fidusia', 'P@ssw0rd')) {
			// 	exit('Login Failed');
			// }
			
			foreach($_FILES as $kfile => $cfile){
				$entry['jenis'] = $kfile;
				if($cfile['name']){
					if(!$cfile['error']){
						$new_file_name = strtolower($cfile['name']); //rename file
						$size = $cfile['size'];
						$rename = explode('.', $new_file_name);
						$replace_name = str_replace($rename[0], $entry['ed_id'].'_'.$entry['jenis'], $new_file_name);
						if ($size > 1187000) {
							$result['msg'] = $replace_name.' melebihi 11.187 kb';
							$result['sts'] = '0';
						}else{
							$pathcore = './assets/uploads/';
							$kontrak  = $entry['nomor_kontrak'];
							if (!file_exists($pathcore)) {
								$oldmask = umask(0);
								mkdir($pathcore, 0777);
								umask($oldmask);
							}
							if (!file_exists($pathcore.'/'.$kontrak)) {
								$oldmask = umask(0);
								mkdir($pathcore.'/'.$kontrak, 0777);
								umask($oldmask);
							}
	
							$file_path = $pathcore.$kontrak.'/'.$replace_name;
							move_uploaded_file($cfile['tmp_name'], FCPATH . $file_path);


							$path = "/opt/lampp/htdocs/fidusia/fidusia/assets/uploads/uploads/".$kontrak;
							$file = "/opt/lampp/htdocs/fidusia/fidusia/assets/uploads/uploads/".$kontrak."/".$replace_name;

							// if(!$sftp->chdir($path)){
							// 	$sftp->mkdir($path);
							// }
							// $sftp->chmod(16895,$path);
							// if (!$sftp->put($file,FCPATH.$file_path,NET_SFTP_LOCAL_FILE)) {
							// 	exit('Failed put Error');
							// }
	
							$save_path = base_url().$file_path;
	
							$url_file 	= base_url().'assets/uploads/'.$kontrak.'/'.$replace_name;
							$lokasi 	= 'assets/uploads/'.$kontrak;
							$lampiran_data = [
							'lamp_ed_id'		=> $entry['ed_id'],
							'lamp_nama'			=> $replace_name,
							'lamp_lokasi_file' 	=> $lokasi,
							'lamp_tipe' 	=> $entry['jenis']
							];
							$idd = [
							'lamp_ed_id' 		=> $entry['ed_id'],
							'lamp_tipe' 	=> $entry['jenis'],
							'referensi_id' 	=> $entry['jenis']."01"
							];
							//$this->db->where('lamp_tipe', $entry['jenis']);
							//$this->m_global->delete('lampiran', $idd);
							
							$lampiran = $this->m_global->insert('lampiran', $lampiran_data);
							//$this->m_global->delete('lampiran_referensi', $idd);
							$lampiran = $this->m_global->insert('lampiran_referensi', $idd);
							if($lampiran) {
								$result['msg'] = 'Dokumen Berhasil ditambahkan!';
								$result['sts'] = '1';
							} else {
								$result['msg'] = 'Data gagal ditambahakan !';
								$result['sts'] = '0';
							}
						}
					}else{
						$result['msg'] = 'Data gagal ditambahakan !';
						$result['sts'] = '0';
					}
				}
			}
		}else{
			$result['msg'] = 'Data gagal ditambahakan !';
			$result['sts'] = '0';
		}
		return $result;
	}

	public function change_status_by( $id, $status, $stat = FALSE )
	{

		if ( $stat ) {
			$result = $this->m_global->delete( $this->table_db, [strEncrypt($this->table_prefix.'id', true) => $id] );
		} else {
			$result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [strEncrypt($this->table_prefix.'id', true) => $id]);
		}

		if ( $result ) {
			$data['status'] = 1;
		} else {
			$data['status'] = 0;
		}

		echo json_encode( $data );
	}

	public function tugaskan()
	{
		$post 	= $this->input->post();
		$result = [];

		if ($post['val'] !== '') {
			$data = [
				'tugas_ed_id' 		=> $post['id'],
				'tugas_user_id'		=> $post['val'],
				'tugas_created_by'	=> user_data()->user_id,
				'tugas_created_date'=> date('Y-m-d')
			];

			$return = $this->m_global->insert('tugas', $data);

			if ($return) {
				$update = ['ed_flag' => '2'];

					$upd = $this->m_global->update('entry_data', $update, ['ed_id' => $post['id']]);

					if ($upd) {
						$result['msg'] = 'Berhasil ditugaskan !';
						$result['sts'] = '1';
					} else {
						$result['msg'] = 'Gagal ditugaskan !';
						$result['sts'] = '0';
					}
			} else {
				$result['msg'] = 'Gagal ditugaskan !';
				$result['sts'] = '0';
			}
		} else {
			$result['msg'] = 'User harus diisi !';
			$result['sts'] = '0';
		}

		echo json_encode($result);
	}
	function change_tanggal($tanggal)
	{

		$tanggal = explode("-", $tanggal);

		$tahun = $tanggal[0];
		$bulan = $tanggal[1];
		$tgl   = $tanggal[2];

		$year =  $this->change_year($tahun);
		$month = $this->change_month($bulan);
		$date = $this->change_date($tgl);
		@$data = $date." ".$month." ".$year." (".$tgl."-".$bulan."-".$tahun.")";

		return $data;
	}
	public function change_year($tahun = '')
	{
		$split = str_split($tahun);
		$year = $split[0].$split[1];
		$date = $split[2].$split[3];
		if ($year == "00") {
			$ret = "Kosong";
		} else {

		if ($year == "10") {
			$data[0] = "seribu";
		} else if ($year == "19") {
			$data[0] = "Seribu Sembilan Ratus";
		} else if ($year == "20") {
			$data[0] = "Dua Ribu";
		} 

		if ($date == "01") {
			$data[1] = "Satu";
		} else if($date == "02") {
			$data[1] = "Dua";
		} else if($date == "03") {
			$data[1] = "Tiga";
		} else if($date == "04") {
			$data[1] = "Empat";
		} else if($date == "05") {
			$data[1] = "Lima";
		} else if($date == "06") {
			$data[1] = "Enam";
		} else if($date == "07" ) {
			$data[1] = "Tujuh";
		} else if($date == "08") {
			$data[1] = "Delapan";
		} else if($date == "09") {
			$data[1] = "Sembilan";
		} else if($date == "10" ) {
			$data[1] = "Sepuluh";
		} else if($date == "11" ) {
			$data[1] = "Sebelas";
		} else if($date == "12" ) {
			$data[1] = "Dua Belas";
		} else if($date == "13") {
			$data[1] = "Tiga Belas";
		} else if($date == "14") {
			$data[1] = "Empat Belas";
		} else if($date == "15") {
			$data[1] = "Lima Belas";
		} else if($date == "16") {
			$data[1] = "Enam Belas";
		} else if($date == "17") {
			$data[1] = "Tujuh Belas";
		} else if($date == "18") {
			$data[1] = "Delapan Belas";
		} else if($date == "19") {
			$data[1] = "Sembilan Belas";
		} else if($date == "20") {
			$data[1] = "Dua Puluh";
		} else if($date == "21") {
			$data[1] = "Dua Puluh Satu";
		} else if($date == "22") {
			$data[1] = "Dua Puluh Dua";
		} else if($date == "23") {
			$data[1] = "Dua Puluh Tiga";
		} else if($date == "24") {
			$data[1] = "Dua Puluh Empat";
		} else if($date == "25") {
			$data[1] = "Dua Puluh Lima";
		} else if($date == "26") {
			$data[1] = "Dua Puluh Enam";
		} else if($date == "27") {
			$data[1] = "Dua Puluh Tujuh";
		} else if($date == "28") {
			$data[1] = "Dua Puluh Delapan";
		} else if($date == "29") {
			$data[1] = "Dua Puluh Sembilan";
		} else if($date == "30") {
			$data[1] = "Tiga Puluh";
		} else if($date == "31") {
			$data[1] = "Tiga Puluh Satu";
		} else if($date == "32") {
			$data[1] = "Tiga Puluh Dua";
		} else if($date == "33") {
			$data[1] = "Tiga Puluh Tiga";
		} else if($date == "34") {
			$data[1] = "Tiga Puluh Empat";
		} else if($date == "35") {
			$data[1] = "Tiga Puluh Lima";
		} else if($date == "35") {
			$data[1] = "Tiga Puluh Lima";
		} else if($date == "36") {
			$data[1] = "Tiga Puluh Enam";
		} else if($date == "37") {
			$data[1] = "Tiga Puluh Tujuh";
		} else if($date == "38") {
			$data[1] = "Tiga Puluh Delapan";
		} else if($date == "39") {
			$data[1] = "Tiga Puluh Sembilan";
		} else if($date == "40") {
			$data[1] = "Empat Puluh";
		} else if($date == "41") {
			$data[1] = "Empat Puluh Satu";
		} else if($date == "42") {
			$data[1] = "Empat Puluh Dua";
		} else if($date == "43") {
			$data[1] = "Empat Puluh Tiga";
		} else if($date == "44") {
			$data[1] = "Empat Puluh Empat";
		} else if($date == "45") {
			$data[1] = "Empat Puluh Lima";
		} else if($date == "46") {
			$data[1] = "Empat Puluh Enam";
		} else if($date == "47") {
			$data[1] = "Empat Puluh Tujuh";
		} else if($date == "48") {
			$data[1] = "Empat Puluh Delapan";
		} else if($date == "49") {
			$data[1] = "Empat Puluh Sembilan";
		} else if($date == "50") {
			$data[1] = "Lima Puluh";
		} else if($date == "51") {
			$data[1] = "Lima Puluh Satu";
		} else if($date == "52") {
			$data[1] = "Lima Puluh Dua";
		}  else if($date == "53") {
			$data[1] = "Lima Puluh Tiga";
		} else if($date == "54") {
			$data[1] = "Lima Puluh Empat";
		} else if($date == "55") {
			$data[1] = "Lima Puluh Lima";
		} else if($date == "56") {
			$data[1] = "Lima Puluh Enam";
		} else if($date == "57") {
			$data[1] = "Lima Puluh Tujuh";
		} else if($date == "58") {
			$data[1] = "Lima Puluh Delapan";
		} else if($date == "59") {
			$data[1] = "Lima Puluh Sembilan";
		} else if($date == "60") {
			$data[1] = "Enam Puluh";
		} else if($date == "61") {
			$data[1] = "Enam Puluh Satu";
		}  else if($date == "62") {
			$data[1] = "Enam Puluh Dua";
		}  else if($date == "63") {
			$data[1] = "Enam Puluh Tiga";
		} else if($date == "64") {
			$data[1] = "Enam Puluh Empat";
		}  else if($date == "65") {
			$data[1] = "Enam Puluh Lima";
		} else if($date == "66") {
			$data[1] = "Enam Puluh Enam";
		} else if($date == "67") {
			$data[1] = "Enam Puluh Tujuh";
		}  else if($date == "68") {
			$data[1] = "Enam Puluh Delapan";
		} else if($date == "69") {
			$data[1] = "Enam Puluh Sembilan";
		} else if($date == "70") {
			$data[1] = "Tujuh Puluh";
		} else if($date == "71") {
			$data[1] = "Tujuh Puluh Satu";
		} else if($date == "72") {
			$data[1] = "Tujuh Puluh Dua";
		} else if($date == "73") {
			$data[1] = "Tujuh Puluh Tiga";
		} else if($date == "74") {
			$data[1] = "Tujuh Puluh Empat";
		} else if($date == "75") {
			$data[1] = "Enam Puluh Lima";
		} else if($date == "76") {
			$data[1] = "Enam Puluh Enam";
		} else if($date == "77") {
			$data[1] = "Enam Puluh Tujuh";
		} else if($date == "78") {
			$data[1] = "Enam Puluh Delapan";
		} else if($date == "79") {
			$data[1] = "Enam Puluh Sembilan";
		} else if($date == "80") {
			$data[1] = "Delapan Puluh";
		} else if($date == "81") {
			$data[1] = "Delapan Puluh Satu";
		} else if($date == "82") {
			$data[1] = "Delapan Puluh Dua";
		} else if($date == "83") {
			$data[1] = "Delapan Puluh Tiga";
		} else if($date == "84") {
			$data[1] = "Delapan Puluh Empat";
		} else if($date == "85") {
			$data[1] = "Delapan Puluh Lima";
		} else if($date == "86") {
			$data[1] = "Delapan Puluh Enam";
		} else if($date == "87") {
			$data[1] = "Delapan Puluh Tujuh";
		} else if($date == "89") {
			$data[1] = "Delapan Puluh Sembilan";
		} else if($date == "90") {
			$data[1] = "Sembilan Puluh";
		} else if($date == "91") {
			$data[1] = "Sembilan Puluh Satu";
		} else if($date == "92") {
			$data[1] = "Sembilan Puluh Dua";
		} else if($date == "93") {
			$data[1] = "Sembilan Puluh Tiga";
		} else if($date == "94") {
			$data[1] = "Sembilan Puluh Empat";
		} else if($date == "95") {
			$data[1] = "Sembilan Puluh Lima";
		} else if($date == "96") {
			$data[1] = "Sembilan Puluh Enam";
		} else if($date == "97") {
			$data[1] = "Sembilan Puluh Tujuh";
		} else if($date == "98") {
			$data[1] = "Sembilan Puluh";
		} else if($date == "99") {
			$data[1] = "Sembilan Puluh";
		}
		 else if($date == "00") {
			$data[1] = "Kosong";
		}
			$ret = $data[0]." ".$data[1];			
		}


		return $ret;
	}
	public function change_month($month)
	{
		if ($month == "01") {
			$ret = "Januari";
		} else if ($month == "02" ) {
			$ret = "Februari";
		} else if ($month == "03" ) {
			$ret = "Maret";
		} else if ($month == "04" ) {
			$ret = "April";
		} else if ($month == "05" ) {
			$ret = "Mei";
		} else if ($month == "06" ) {
			$ret = "Juni";
		} else if ($month == "07" ) {
			$ret = "Juli"; 
		} else if ($month == "08" ) {
			$ret = "Agustus";
		} else if ($month == "09" ) {
			$ret = "September";
		} else if ($month == "10" ) {
			$ret = "Oktober";
		} else if ($month == "11" ) {
			$ret = "November";
		} else if ($month == "12" ) {
			$ret = "Desember";
		} else if($month == "00") {
			$ret = "Kosong";
		} 
		return $ret;
	}
	public function change_date($date)
	{
		if ($date == "01") {
			$ret = "Satu";
		} else if($date == "02") {
			$ret = "Dua";
		} else if($date == "03") {
			$ret = "Tiga";
		} else if($date == "04") {
			$ret = "Empat";
		} else if($date == "05") {
			$ret = "Lima";
		} else if($date == "06") {
			$ret = "Enam";
		} else if($date == "07" ) {
			$ret = "Tujuh";
		} else if($date == "08") {
			$ret = "Delapan";
		} else if($date == "09") {
			$ret = "Sembilan";
		} else if($date == "10" ) {
			$ret = "Sepuluh";
		} else if($date == "11" ) {
			$ret = "Sebelas";
		} else if($date == "12" ) {
			$ret = "Dua Belas";
		} else if($date == "13") {
			$ret = "Tiga Belas";
		} else if($date == "14") {
			$ret = "Empat Belas";
		} else if($date == "15") {
			$ret = "Lima Belas";
		} else if($date == "16") {
			$ret = "Enam Belas";
		} else if($date == "17") {
			$ret = "Tujuh Belas";
		} else if($date == "18") {
			$ret = "Delapan Belas";
		} else if($date == "19") {
			$ret = "Sembilan Belas";
		} else if($date == "20") {
			$ret = "Dua Puluh";
		} else if($date == "21") {
			$ret = "Dua Puluh Satu";
		} else if($date == "22") {
			$ret = "Dua Puluh Dua";
		} else if($date == "23") {
			$ret = "Dua Puluh Tiga";
		} else if($date == "24") {
			$ret = "Dua Puluh Empat";
		} else if($date == "25") {
			$ret = "Dua Puluh Lima";
		} else if($date == "26") {
			$ret = "Dua Puluh Enam";
		} else if($date == "27") {
			$ret = "Dua Puluh Tujuh";
		} else if($date == "28") {
			$ret = "Dua Puluh Delapan";
		} else if($date == "29") {
			$ret = "Dua Puluh Sembilan";
		} else if($date == "30") {
			$ret = "Tiga Puluh";
		} else if($date == "31") {
			$ret = "Tiga Puluh Satu";
		} else if($date == "32") {
			$ret = "Tiga Puluh Dua";
		} else if($date == "33") {
			$ret = "Tiga Puluh Tiga";
		} else if($date == "34") {
			$ret = "Tiga Puluh Empat";
		} else if($date == "35") {
			$ret = "Tiga Puluh Lima";
		} else if($date == "00") {
			$ret = "Kosong";
		}

		return $ret;
	}
	public function change_time($waktu)
	{
	 $var = explode(":", $waktu);
	 $data[0] = $this->change_latin($var[0]);
	 $data[1] = $this->change_latin($var[1]);
	 $data[2] = $this->change_latin($var[2]);

	 if ($data[1] === "Kosong Kosong") {
		 $ret = $var[0].".".$var[1]." WIB (".$data[0]." Lewat ".$data[1]." Menit Waktu Indonesia Barat)";
	 } else {
	 	$ret = $var[0].".".$var[1]." WIB (".$data[0]." Lewat ".$data[1]." Menit Waktu Indonesia Barat)";
	 }
	 return $ret;
	}
	public function change_latin($param)
	{

		if ($param == "01") {
			$ret = "Satu";
		} else if($param == "02") {
			$ret = "Dua";
		} else if($param == "03") {
			$ret = "Tiga";
		} else if($param == "04") {
			$ret = "Empat";
		} else if($param == "05") {
			$ret = "Lima";
		} else if($param == "06") {
			$ret = "Enam";
		} else if($param == "07" ) {
			$ret = "Tujuh";
		} else if($param ==="08") {
			$ret = "Delapan";
		} else if($param == "09") {
			$ret = "Sembilan";
		} else if($param == "10" ) {
			$ret = "Sepuluh";
		} else if($param == "11" ) {
			$ret = "Sebelas";
		} else if($param == "12" ) {
			$ret = "Dua Belas";
		} else if($param == "13") {
			$ret = "Tiga Belas";
		} else if($param == "14") {
			$ret = "Empat Belas";
		} else if($param == "15") {
			$ret = "Lima Belas";
		} else if($param == "16") {
			$ret = "Enam Belas";
		} else if($param == "17") {
			$ret = "Tujuh Belas";
		} else if($param == "18") {
			$ret = "Delapan Belas";
		} else if($param == "19") {
			$ret = "Sembilan Belas";
		} else if($param == "20") {
			$ret = "Dua Puluh";
		} else if($param == "21") {
			$ret = "Dua Puluh Satu";
		} else if($param == "22") {
			$ret = "Dua Puluh Dua";
		} else if($param == "23") {
			$ret = "Dua Puluh Tiga";
		} else if($param == "24") {
			$ret = "Dua Puluh Empat";
		} else if($param == "25") {
			$ret = "Dua Puluh Lima";
		} else if($param == "26") {
			$ret = "Dua Puluh Enam";
		} else if($param == "27") {
			$ret = "Dua Puluh Tujuh";
		} else if($param == "28") {
			$ret = "Dua Puluh Delapan";
		} else if($param == "29") {
			$ret = "Dua Puluh Sembilan";
		} else if($param == "30") {
			$ret = "Tiga Puluh";
		} else if($param == "31") {
			$ret = "Tiga Puluh Satu";
		} else if($param == "32") {
			$ret = "Tiga Puluh Dua";
		} else if($param == "33") {
			$ret = "Tiga Puluh Tiga";
		} else if($param == "34") {
			$ret = "Tiga Puluh Empat";
		} else if($param == "35") {
			$ret = "Tiga Puluh Lima";
		} else if($param == "36") {
			$ret = "Tiga Puluh Enam";
		} else if($param == "37") {
			$ret = "Tiga Puluh Tujuh";
		} else if($param == "38") {
			$ret = "Tiga Puluh Delapan";
		} else if($param == "39") {
			$ret = "Tiga Puluh Sembilan";
		} else if($param == "40") {
			$ret = "Empat Puluh";
		} else if($param == "41") {
			$ret = "Empat Puluh Satu";
		} else if($param == "42") {
			$ret = "Empat Puluh Dua";
		} else if($param == "43") {
			$ret = "Empat Puluh Tiga";
		} else if($param == "44") {
			$ret = "Empat Puluh Empat";
		} else if($param == "45") {
			$ret = "Empat Puluh Lima";
		} else if($param == "46") {
			$ret = "Empat Puluh Enam";
		} else if($param == "47") {
			$ret = "Empat Puluh Tujuh";
		} else if($param == "48") {
			$ret = "Empat Puluh Delapan";
		} else if($param == "49") {
			$ret = "Empat Puluh Sembilan";
		} else if($param == "50") {
			$ret = "Lima Puluh";
		} else if($param == "51") {
			$ret = "Lima Puluh Satu";
		} else if($param == "52") {
			$ret = "Lima Puluh Dua";
		} else if($param == "53") {
			$ret = "Lima Puluh Tiga";
		} else if($param == "54") {
			$ret = "Lima Puluh Empat";
		} else if($param == "55") {
			$ret = "Lima Puluh Lima";
		} else if($param == "56") {
			$ret = "Lima Puluh Enam";
		} else if($param == "57") {
			$ret = "Lima Puluh Tujuh";
		} else if($param == "58") {
			$ret = "Lima Puluh Delapan";
		} else if($param == "59") {
			$ret = "Lima Puluh Sembilan";
		} else if($param === '00') {
			$ret = "Kosong Kosong";
		}
		return $ret;
	}
	public function get_day($day)
	{
		if ($day == "Sun") {
			$ret = "Minggu";
		} else if ($day == "Mon") {
			$ret = "Senin";
		} else if ($day == "Tue") {
			$ret = "Selasa";
		} else if ($day == "Wed") {
			$ret = "Rabu";
		} else if ($day == "Thu") {
			$ret = "Kamis";
		} else if ($day == "Fri") {
			$ret = "Jumat";
		} else if ($day == "Sat") {
			$ret = "Sabtu";
		}
		return $ret;
	}

	public function act_redownload($no_kontrak = false, $lampiran)
	{
		if($no_kontrak == false ) {
			exit('missing no kontrak');
		} else {
			$dataTransaction = $this->m_global->get_data_all('entry_data', null,['ed_nomor_kontrak' => $no_kontrak]);
			if(empty($dataTransaction)) {
				exit('Data not found');
			} else {
				$idTrans 	     = $dataTransaction[0]->ed_id;
				$kontrakTrans    = $dataTransaction[0]->ed_nomor_kontrak;

				set_include_path('phpseclib');
				include('phpseclib/Net/SFTP.php');
				$sftp = new Net_SFTP('112.215.7.49');
				
				$path = FCPATH.'/assets/uploads/file/'.$kontrakTrans;
				if(!is_dir($path)) {
					mkdir($path);
				}
				if (!$sftp->login('fidusia', 'P@ssw0rd')) {
					exit('Login Failed');
				}
				// method when login is success
				$fileLocal   = $path.'/'.$lampiran->lamp_nama; 
				$fileDownload = '/opt/lampp/htdocs/fidusia/fidusia/assets/uploads/uploads/'.$kontrakTrans.'/'.$lampiran->lamp_nama;
					$sftp->get($fileDownload, $fileLocal);
			}
		}
	}

	public function checkFile()
	{
		$path = FCPATH.'assets/uploads/file/801900063781/';
		$pathFile = FCPATH.'assets/uploads/file/801900063781/127_UPD1.pdf';

		if(is_dir($path)) {
			echo 'some dir';
		}

		if(is_file($pathFile)) {
			echo 'some file';
		}
	}
	
}

/* End of file Config_user.php */
/* Location: ./application/modules/config/controllers/Config_user.php */
