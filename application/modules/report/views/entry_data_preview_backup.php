<style>
.dropify-wrapper{
	height:100px;
	font-size:10px;
}
.kendoDatePicker {
	margin: 4px 4px 0px 0px;
}
</style>
<?php $data = $record[0];

//\\print_r($data); ?>
<div class="uk-grid">
	<div class="uk-width-medium-1">
		<div class="uk-row-first">
			<form method="post" action="<?=$url; ?>/action_approve" id="form_approve" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?= $id ?>">
				<?php if((@$report[0]->ed_flag=="3") || (@$report[0]->ed_flag=="4") || (@$report[0]->ed_flag=="5")){ ?>
				<div class="md-card">
					<div class="md-card-toolbar">
						<h3 class="md-card-toolbar-heading-text">Edit Entry Data <?= $report[0]->ed_flag; ?></h3>
					</div>
					<div class="md-card-content">
						<?php if(@$report[0]->ed_flag == "3"){ ?>
						<div class="uk-grid">
							<div class="uk-width-medium-5-5 uk-row-first">
								<div class="uk-grid">
									<div class="uk-width-medium-1-1 uk-row-first">
										<div class="uk-form-row text-center">
											<div id="alert"></div>
											<h3>Dokumen Kelengkapan Pembiayaan</h3>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>PNBP <span class="required">*</span></label>
								<input accept="application/pdf" name="UPD3" type="file" id="UPD3" class="uploadFile"/>
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-3-5">
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Input NTPN dari PNBP <span class="required">*</span></label>
												<input name="no_akta" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-2-5 uk-row-first">
										<label>Nomor Akta <span class="required">*</span></label>
										<input value="<?= @$akta[0]->akta_nomor; ?>" name="nomor" type="text" class="md-input uk-form-width-medium" required>
										<input value="<?= @$report[0]->ed_id; ?>" name="akta_id" type="hidden" class="md-input uk-form-width-medium">
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-2-5 uk-row-first">
										<label>Tanggal <span class="required">*</span></label><br>
										<input value="<?=@$akta[0]->akta_waktu;?>" name="tanggal" type="text" class="" id="waktu" required>
									</div>
								</div>
								<div <?php echo ((user_data()->user_level == '0' && $report[0]->ed_flag == '2') || (user_data()->user_level == '2' && user_role()['position'] == '1' && $report[0]->ed_flag == '2')) ? '' : 'style="display: none;"'; ?> class="uk-text-center uk-margin-large-top">
									<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Setuju</button>
									<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
								</div>
								<div <?php echo ((user_data()->user_level == '0' && $report[0]->ed_flag == '1') || (user_data()->user_level == '2' && user_role()['position'] == '0' && $report[0]->ed_flag == '1')) ? '' : 'style="display: none;"'; ?> id="tugas">
									<hr>
									<div class="uk-grid">
										<div class="uk-width-medium-1-3 uk-row-first"></div>
										<div class="uk-width-medium-1-3">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-row-first">
													<label>User <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1">
													<select name="user_tugas" id="user_tugas" select-style select-style-bottom>
														<option value="">Pilih User</option>
														<?php foreach ($user_tugas as $key => $val): ?>
															<option value="<?=$val->user_id; ?>"><?=$val->user_nik; ?></option>
														<?php endforeach ?>
													</select>
													<span class="uk-form-help-block">User yang ditugaskan ( Per-Cabang ) !</span>
												</div>
												<div class="uk-width-medium-1 uk-margin-medium-top">
													<center><button onclick="tugaskan('<?=$report[0]->ed_id; ?>');" type="button" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Tugaskan</button></center>
												</div>
											</div>
										</div>
										<div class="uk-text-center uk-margin-large-top">
											<input type="hidden" name="status_now" value="3">
											<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Proses</button>
											<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
										</div>
									</div>
								</div>
							</div>
						</div>	
						<div class="uk-text-center uk-margin-large-top">
							<input type="hidden" name="status_now" value="3">
							<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light" id="btn-proses">Proses</button>
							<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
						</div>

					<?php }elseif (@$report[0]->ed_flag=="4") { ?>
						<div id="alert"></div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Upload Sertifikat Fidusia <span class="required">*</span></label>
								<input accept="application/pdf" name="UPD4" type="file" id="UPD4" class="uploadFile" required="true" />
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-3-5">
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Input Nomor Sertifikat Fidusia <span class="required">*</span></label>
												<input name="nomor" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-3-5">
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Input Tanggal Sertifikat Fidusia <span class="required">*</span></label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="uk-grid">
							<div class="uk-width-medium-3-5">
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<input name="tanggal" type="text" class="datepicker">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="uk-text-center uk-margin-large-top">
							<input type="hidden" name="status_now" value="4">
							<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Proses</button>
							<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
						</div>

					<?php }elseif (@$report[0]->ed_flag=="5") { ?>
						<div id="alert"></div>
							<div class="uk-grid">
								<div class="uk-width-medium-2-5 uk-row-first">
									<label>Nomor Akta <span class="required">*</span></label>
									<input value="<?= @$akta[0]->akta_nomor; ?>" name="nomor" type="text" class="md-input uk-form-width-medium" required>
									<input value="<?= @$report[0]->ed_id; ?>" name="akta_id" type="hidden" class="md-input uk-form-width-medium">
								</div>
							</div>
							<div class="uk-grid">
								<div class="uk-width-medium-2-5 uk-row-first">
									<label>Tanggal <span class="required">*</span></label><br>
									<input value="<?=@$akta[0]->akta_waktu;?>" name="tanggal" type="text" class="" id="waktu" required>
								</div>
							</div>
							<div class="uk-text-center uk-margin-large-top">
								<input type="hidden" name="status_now" value="5">
								<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Proses</button>
								<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
							</div>
				<?php } ?>
					</div>
					
				</div>
				<?php } ?>

				<div class="md-card">
					<div class="md-card-content">
						<div id="alert"></div>
						<div class="uk-width-medium-1-1 uk-row-first">
							<div class="uk-form-row text-center">
								<h3>Data Kelengkapan Pembiayaan</h3>
							</div>
						</div>
					</div>
				</div>

				<div class="uk-grid uk-grid-medium uk-grid-width-medium-1-2">
					<div class="uk-row-first">
						<div class="md-card md-card-primary">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Konsumen</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Nama </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->kon_nama; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Nomor KTP </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->kon_ktp; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Pekerjaan </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->kon_pekerjaan; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Jenis Kelamin </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$option['jenis_kelamin'][@$report[0]->kon_jenis_kelamin]; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Negara </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->kon_nama_negara; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Status Pernikahan </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$option['status_pernikahan'][@$report[0]->kon_status_pernikahan]; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Alamat </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->kon_alamat; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Kode Pos </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->kon_kode_pos; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Provinsi </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->kon_provinsi; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Kabupaten </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->kon_kabupaten; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Kecamatan </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->kon_kecamatan; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Kelurahan </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->kon_kelurahan; ?> </label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div>
						<div class="md-card md-card-success" style="padding-bottom: 15px;">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Pemilik Kendaraan</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Nama </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->pem_nama; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Nomor KTP </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->pem_ktp; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Pekerjaan </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->pem_pekerjaan; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Jenis Kelamin </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$option['jenis_kelamin'][@$report[0]->pem_jenis_kelamin]; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Negara </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->pem_nama_negara; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Alamat </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->pem_alamat; ?> </label>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>

					<div class="uk-grid-margin uk-row-first">
						<div class="md-card md-card-warning">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Pembiayaan1</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Cabang </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->cabang_name; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Nomor </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->ed_nomor_kontrak; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Tanggal </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->pemb_tanggal; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<!-- <label>Nilai Penjaminan </label> -->
											<label>Nilai Harga Hutang </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->pemb_nilai_penjaminan; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Nilai Benda Fidusia </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->pemb_nilai_benda_fidusia; ?> </label>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Daftar Data Scan </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<?php foreach($file as $key => $value){ ?>
											<label>- <?= @$value->referensi_nama; ?> </label><br>
											<?php } ?>
										</div>
									</div>
								</div>
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>Jangka Waktu </label>
										</div>
									</div>
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
											<label>: <?= @$report[0]->pemb_jangka_tanggal; ?>  - <?php echo @$report[0]->pemb_sampai_tanggal; ?></label><br>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="uk-grid-margin">
						<div class="md-card md-card-danger">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Kendaraan</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid">
									<div class="uk-width-medium-1-1 uk-row-first">
										<div class="uk-grid">
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>Merk </label>
												</div>
											</div>
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>: <?= @$report[0]->dk_merk; ?> </label>
												</div>
											</div>
										</div>
										<div class="uk-grid">
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>Tahun Pembuatan </label>
												</div>
											</div>
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>: <?= @$report[0]->dk_tahun_pembuatan; ?> </label>
												</div>
											</div>
										</div>
										<div class="uk-grid">
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>Nomor Rangka </label>
												</div>
											</div>
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>: <?= @$report[0]->dk_no_rangka; ?> </label>
												</div>
											</div>
										</div>
										<div class="uk-grid">
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>Nomor Mesin </label>
												</div>
											</div>
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>: <?= @$report[0]->dk_no_mesin; ?> </label>
												</div>
											</div>
										</div>
										<div class="uk-grid">
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>Cover Note Dealer Baru </label>
												</div>
											</div>
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>: <?= @$report[0]->dk_covernote; ?> </label>
												</div>
											</div>
										</div>
										<div class="uk-grid">
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>Nomor Polisi </label>
												</div>
											</div>
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>: <?= @$report[0]->dk_no_polisi; ?> </label>
												</div>
											</div>
										</div>
										<div class="uk-grid">
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>Nomor BPKB </label>
												</div>
											</div>
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>: <?= @$report[0]->dk_no_bpkb; ?> </label>
												</div>
											</div>
										</div>
										<div class="uk-grid">
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>Tipe Roda</label>
												</div>
											</div>
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>: Roda <?= @$data->dk_golongan; ?> </label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="uk-grid-margin uk-row-first">
						<div class="md-card md-card-info">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Debitur <?php //print_r($data) ?></h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-1">
									<!-- <div> -->
										<div class="uk-grid">
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>Tipe </label>
												</div>
											</div>
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>: <?= @$data->pemb_ttd_debitur; ?> </label>
												</div>
											</div>
										</div>
										
										<div class="uk-grid">
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>Kode Pos</label>
												</div>
											</div>
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>: <?= @$data->kon_kode_pos; ?> </label>
												</div>
											</div>
										</div>
										<div class="uk-grid">
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>Alamat</label>
												</div>
											</div>
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>: <?= @$data->kon_alamat; ?> </label>
												</div>
											</div>
										</div>
										
										<div class="uk-grid">
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>Telepon</label>
												</div>
											</div>
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>:  <?= @$data->kon_telp; ?> </label>
												</div>
											</div>
										</div>
										<div class="uk-grid">
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>RW</label>
												</div>
											</div>
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>:  <?= @$data->kon_rw; ?> </label>
												</div>
											</div>
										</div>
										<div class="uk-grid">
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>RT</label>
												</div>
											</div>
											<div class="uk-width-medium-1-2 uk-row-first">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
													<label>:  <?= @$data->kon_rt; ?> </label>
												</div>
											</div>
										</div>
										
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="md-card md-card-primary">
					<div class="md-card-toolbar">
						<h3 class="md-card-toolbar-heading-text">Catatan</h3>
					</div>
					<div class="md-card-content">
						<div class="uk-grid">
							<div class="uk-width-medium-1-4 uk-row-first">
								<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
									<label>Catatan </label>
								</div>
							</div>
							<div class="uk-width-medium-1-4 uk-row-first">
								<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
									<?php echo ($data->pemb_note?"<label>:<span class='uk-badge uk-badge-danger'>Data Dikirimkan Melalui Hard copy</span></label>":"<label>:<span class='uk-badge uk-badge-info'>Data Dikirimkan Melalui SoftCopy</span></label>"); ?>
								</div>
							</div>
						</div>

						<div class="uk-grid">
							<div class="uk-width-medium-1-4 uk-row-first">
								<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
									<label>Tanggal Diterimanya berkas</label>
								</div>
							</div>
							<div class="uk-width-medium-1-4 uk-row-first">
								<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
									<label>:<?php echo $data->pemb_date; ?></label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="md-card">
					<div class="md-card-content">
						<div style="height: 500px;" id="my-container"></div>
					</div>
				</div>
				
				<!-- ---------------------------------------------------------------------------------------------------------------------------------------------------- -->

				<?php if(@$report[0]->ed_flag == "1"){ ?>

				<div class="uk-grid uk-grid-medium uk-grid-width-medium-1-2">
					<div class="uk-row-first">
						<div class="md-card md-card-primary">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Konsumen</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Pekerjaan <span class="required">*</span></label>
												<input value="<?=$data->kon_pekerjaan; ?>" name="pekerjaan_konsumen" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="pasangan-form">
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Nama Pasangan <span class="required">*</span></label>
												<input value="<?=$data->kon_nama_pasangan; ?>" name="nama_pasangan" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Tempat Lahir Pasangan <span class="required">*</span></label>
												<input value="<?=$data->kon_tempat_lahir_pasangan; ?>" name="tempat_lahir_pasangan" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Tanggal Lahir pasangan <span class="required">*</span></label>
												<input value="<?=date('Y-m-d', strtotime(!empty($data->kon_tanggal_lahir_pasangan) ? $data->kon_tanggal_lahir_pasangan : date('Y-M-d'))); ?>" name="tanggal_lahir_nasabah" type="text" class="datepicker">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Alamat Pasangan <span class="required">*</span></label>
												<input value="<?=$data->kon_alamat_pasangan; ?>" name="alamat_pasangan" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Pekerjaan Pasangan <span class="required">*</span></label>
												<input value="<?=$data->kon_pekerjaan_pasangan; ?>" name="pekerjaan_pasangan" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Nomor KTP Pasangan<span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input value="<?=$data->kon_ktp_pasangan; ?>" name="nomor_ktp_pasangan" type="text" class="masked" format="0000000000000000">
												</div>
											</div>
										</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Nomor KTP <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input value="<?=$data->kon_ktp; ?>" name="nomor_ktp_konsumen" type="text" class="masked" format="0000000000000000">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Jenis Kelamin <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<p>
														<input <?=$data->kon_jenis_kelamin == '1' ? 'checked' : ''; ?> value="1" type="radio" name="jk_konsumen" id="jk_konsumen1" input-style-checkbox />
														<label for="jk_konsumen1" class="inline-label">Pria</label>
													</p>
													<p>
														<input <?=$data->kon_jenis_kelamin == '2' ? 'checked' : ''; ?> value="2" type="radio" name="jk_konsumen" id="jk_konsumen2" input-style-checkbox />
														<label for="jk_konsumen2" class="inline-label">Wanita</label>
													</p>
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Status Pernikahan <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<p>
														<input <?=$data->kon_status_pernikahan == '0' ? 'checked' : ''; ?> id="pernikahan1" value="0" type="radio" name="pernikahan_konsumen" input-style-checkbox />
														<label for="pernikahan1" class="inline-label">Suami</label>
													</p>
													<p>
														<input <?=$data->kon_status_pernikahan == '1' ? 'checked' : ''; ?> id="pernikahan2" value="1" type="radio" name="pernikahan_konsumen" input-style-checkbox />
														<label for="pernikahan2" class="inline-label">Istri</label>
													</p>
													<p>
														<input <?=$data->kon_status_pernikahan == '2' ? 'checked' : ''; ?> id="pernikahan3" value="2" type="radio" name="pernikahan_konsumen" input-style-checkbox />
														<label for="pernikahan3" class="inline-label">Belum Menikah</label>
													</p>
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Tempat Lahir <span class="required">*</span></label>
												<input value="<?php //$data->kon_tempat _lahir; ?>" name="tempat_lahir_konsumen" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Tanggal Lahir <span class="required">*</span></label><br>
												<input value="<?=date('Y-m-d', strtotime(!empty($data->kon_tanggal_lahir) ? $data->kon_tanggal_lahir : date('Y-M-d'))); ?>" name="tanggal_lahir_konsumen" type="text" class="datepicker">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div>
						<div class="md-card md-card-success" style="padding-bottom: 15px;">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Pemilik Kendaraan</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Nomor KTP <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input value="<?=$data->pem_ktp; ?>" name="ktp_pemilik" type="text" class="masked" format="0000000000000000">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Jenis Kelamin <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<p>
														<input <?=$data->pem_jenis_kelamin == '1' ? 'checked' : ''; ?> id="jk_pemilik1" value="1" type="radio" name="jk_pemilik" input-style-checkbox />
														<label for="jk_pemilik1" class="inline-label">Pria</label>
													</p>
													<p>
														<input <?=$data->pem_jenis_kelamin == '2' ? 'checked' : ''; ?> id="jk_pemilik2" value="2" type="radio" name="jk_pemilik" input-style-checkbox />
														<label for="jk_pemilik2" class="inline-label">Wanita</label>
													</p>
												</div>
											</div>
										</div>
										<div class="uk-form-row" style="margin-bottom: 15px;">
											<div class="md-input-wrapper md-input-filled">
												<label>Alamat <span class="required">*</span></label>
												<textarea name="alamat_pemilik" class="md-input uk-form-width-large"><?=$data->pem_alamat; ?></textarea>
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Pekerjaan <span class="required">*</span></label>
												<input value="<?=$data->pem_pekerjaan; ?>" name="pekerjaan_pemilik" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Tempat Lahir <span class="required">*</span></label>
												<input value="<?=$data->pem_tempat_lahir; ?>" name="tempat_lahir_pemilik" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Tanggal Lahir <span class="required">*</span></label><br>
												<input value="<?=date('Y-m-d', strtotime(!empty($data->pem_tanggal_lahir) ? $data->pem_tanggal_lahir : date('Y-M-d'))); ?>" name="tanggal_lahir_pemilik" type="text" class="datepicker">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="uk-grid-margin uk-row-first">
						<div class="md-card md-card-info">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Debitur <?php //2print_r($data) ?></h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-1">
									<!-- <div> -->
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tipe <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<p>
														<input <?=$data->pemb_ttd_debitur == 'pemilik' ? 'checked' : ''; ?> value="pemilik" type="radio" name="pemb_ttd_debitur" id="pemb_ttd_debitur1" input-style-checkbox />
														<label for="pemb_ttd_debitur1" class="inline-label">Pemilik</label>
													</p>
													<p>
														<input <?=$data->pemb_ttd_debitur == 'konsumen' ? 'checked' : ''; ?> value="konsumen" type="radio" name="pemb_ttd_debitur" id="pemb_ttd_debitur2" input-style-checkbox />
														<label for="jenis_debitur2" class="inline-label">Konsumen</label>
													</p>
												</div>
											</div>
										</div>

									<!-- </div> -->
								</div>
							</div>
						</div>
					</div>
					</div>

					<div class="uk-grid-margin uk-row-first">
						<div class="md-card md-card-warning">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Pembiayaan</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-1">
									<div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Pembiayaan <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input value="<?=date('d-m-Y', strtotime($data->pemb_tanggal)); ?>" name="tgl_pembiayaan" type="text" class="datepicker">
												</div>
											</div>
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Nilai Penjaminan <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input value="<?=$data->pemb_nilai_penjaminan; ?>" name="nilai_penjaminan" type="text" class="md-input uk-form-width-medium masked_input" data-inputmask="'alias': 'currency', 'groupSeparator': '.', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': 'Rp. ', 'placeholder': '0'" data-inputmask-showmaskonhover="false">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>

											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Jangka Waktu <?php //print_r($data); ?><span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input value="<?=date('d-m-Y', strtotime($data->pemb_jangka_tanggal)); ?>" name="pemb_jangka_tanggal" type="text" class="datepicker">
													Sampai
													<input value="<?=date('d-m-Y', strtotime($data->pemb_sampai_tanggal)); ?>" name="pemb_sampai_tanggal" type="text" class="datepicker">
												</div>
											</div>
											
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>

					<div class="uk-grid-margin">
						<div class="md-card md-card-danger">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Kendaraan</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Nomor Polisi <span class="required">( Motor Lama )</span></label>
												<input value="<?=$data->dk_no_polisi; ?>" name="nomor_polisi" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Nomor BPKB <span class="required">( Motor Lama )</span></label>
												<input value="<?=$data->dk_no_bpkb; ?>" name="nomor_bpkb" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tipe Roda Kendaraan <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<p>
														<input <?=@$data->dk_golongan == '2' ? 'checked' : ''; ?> value="2" type="radio" name="tipe_roda" id="tipe_roda1" input-style-checkbox />
														<label for="tipe_roda1" class="inline-label">Roda 2</label>
													</p>
													<p>
														<input <?=@$data->dk_golongan == '4' ? 'checked' : ''; ?> value="4" type="radio" name="tipe_roda" id="tipe_roda2" input-style-checkbox />
														<label for="tipe_roda2" class="inline-label">Roda 4</label>
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php } ?>

				<!-- -------------------------------------------------------------------------------------------------------------------------------------------------- -->
				<?php if(@$report[0]->ed_flag == "1"){ ?>
				<div class="md-card">
					<div class="md-card-toolbar">
						<h3 class="md-card-toolbar-heading-text">Edit Entry Data <?= @$report[0]->ed_flag; ?></h3>
					</div>
					<div class="md-card-content">

						<!-- condition if note exist -->
						<?php if ($data->pemb_note): ?>
							
						<div class="uk-grid">
							<div class="uk-width-medium-1-4 uk-row-first">
								<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
									<label>Catatan </label>
								</div>
							</div>
							<div class="uk-width-medium-1-4 uk-row-first">
								<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
									<label>:<span class="uk-badge uk-badge-danger">Data Dikirimkan Melalui Hard copy</span></label>
								</div>
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-1-4 uk-row-first">
								<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
									<label>Tanggal diterimanya dokumen</label>
								</div>
							</div>
							<div class="uk-width-medium-1-4 uk-row-first">
								<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
									<label><input value="" name="tanggal_dokumen" type="text" class="kendoDatePicker" id="tanggal_dokumen"></label>
								</div>
							</div>
						</div><br><br>
						<?php endif ?>
						<!-- end condition -->
						

						<div class="uk-width-medium-1 uk-margin-small-bottom">
							<span class="icheck-inline">
								<input value="1" type="radio" name="aksi" id="gender1" input-style-checkbox class="cb1"/>
								<label for="gender1" class="inline-label">Setuju</label>
							</span>
							<span class="md-input-bar"></span>
							<span class="icheck-inline">
								<input value="2" type="radio" name="aksi" id="gender2" input-style-checkbox class="cb1"/>
								<label for="gender2" class="inline-label">Tolak</label>
							</span>
							<span class="md-input-bar"></span>
						</div>
						<div class="uk-form-row cb1div" style="display:none;">
							<div class="md-input-wrapper">
								<label>Catatan <span class="required">*</span></label>
								<textarea name="note" class="md-input uk-form-width-large"></textarea>
								<span class="md-input-bar uk-form-width-large"></span>
							</div>
						</div>
						<div class="uk-text-center uk-margin-large-top">
							<input type="hidden" name="status_now" value="1">
							<a href="<?=$url.'/bypass/'.$id; ?>" ><button type="button" class="md-btn md-btn-small md-btn-success md-btn-wave-light waves-effect waves-button waves-light">Bypass</button></a>&nbsp; 
							<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Proses</button>
							<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Batal</a>
						</div>
					</div>
				</div>
				<?php }elseif (@$report[0]->ed_flag == "2") { ?>
				<div class="md-card">
					<div class="md-card-toolbar">
						<h3 class="md-card-toolbar-heading-text">Edit Entry Data <?= @$report[0]->ed_flag; ?></h3>
					</div>
					<div class="md-card-content">
						<div class="uk-grid">
							<div class="uk-width-medium-5-5 uk-row-first">
								<div class="uk-grid">
									<div class="uk-width-medium-1-1 uk-row-first">
										<div class="uk-form-row text-center">
											<h3>Dokumen Akta</h3>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="uk-form-row">
							<div class="md-input-wrapper">
								<div class="uk-grid">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
										<label>Provinsi <span class="required">*</span></label>
									</div>
									<div class="uk-width-medium-1 uk-margin-small-bottom">
										<select id="provinsi" name="provinsi" class="form-filter select-filter uk-form-width-large" select-style select-style-bottom>
											<option value=""></option>
											<?php foreach ($provinsi as $key => $val): ?>
												<option <?= ((@$akta[0]->daerah_provinsi == $val->daerah_provinsi)?'selected':''); ?> value="<?=$val->daerah_provinsi; ?>"><?=$val->daerah_provinsi; ?></option>
											<?php endforeach ?>
										</select>
										<!--<select id="test">
											<option value=""></option>
										</select>-->
									</div>
								</div>
							</div>
						</div>
						<div class="uk-form-row">
							<div class="md-input-wrapper">
								<div class="uk-grid">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
										<label>Kabupaten/Kota <span class="required">*</span></label>
									</div>
									<div class="uk-width-medium-1 uk-margin-small-bottom">
										<select id="kota" name="kota" class="form-filter select-filter uk-form-width-large">
											<option value=""></option>
											<?php foreach (@$kota as $key => $val): ?>
												<option <?= ((@$akta[0]->daerah_jenis_kota.' '.@$akta[0]->daerah_kota == $val->daerah_jenis_kota.' '.$val->daerah_kota)?'selected':''); ?> value="<?=$val->daerah_jenis_kota.' '.$val->daerah_kota; ?>"><?=$val->daerah_jenis_kota.' '.$val->daerah_kota; ?></option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="uk-form-row">
							<div class="md-input-wrapper">
								<div class="uk-grid">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
										<label>Kecamatan <span class="required">*</span></label>
									</div>
									<div class="uk-width-medium-1 uk-margin-small-bottom">
										<select id="kecamatan" name="kecamatan" class="form-filter select-filter uk-form-width-large">
											<option value=""></option>
											<?php foreach (@$kecamatan as $key => $val): ?>
												<option <?= ((@$akta[0]->daerah_kecamatan == $val->daerah_kecamatan)?'selected':''); ?> value="<?=$val->daerah_kecamatan; ?>"><?=$val->daerah_kecamatan; ?></option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="uk-form-row">
							<div class="md-input-wrapper">
								<div class="uk-grid">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
										<label>Kelurahan <span class="required">*</span></label>
									</div>
									<div class="uk-width-medium-1 uk-margin-small-bottom">
										<select id="kelurahan" name="daerah_id" class="form-filter select-filter uk-form-width-large">
											<option value=""></option>
											<?php foreach (@$kelurahan as $key => $val): ?>
												<option <?= ((@$akta[0]->daerah_kelurahan == $val->daerah_kelurahan)?'selected':''); ?> value="<?=$val->daerah_kelurahan; ?>"><?=$val->daerah_kelurahan; ?></option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Nama pembeban <span class="required">*</span></label><br>
								<input value="<?= @$akta[0]->akta_nama_pembeban; ?>" name="nama_pembeban" type="text" class="md-input uk-form-width-medium">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Jangka Waktu Perjanjian Kredit <span class="required">*</span></label><br>
								<input value="<?= @$akta[0]->akta_perjanjian_kredit; ?>" name="perjanjian_kredit" type="text" class="kendoDatePicker" id="jangka_waktu_pk">
								<!--<input value="<?= @$akta[0]->akta_perjanjian_kredit; ?>" name="perjanjian_kredit" type="text" class="md-input uk-form-width-medium"> Tahun-->
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Daerah Pengadilan <span class="required">*</span></label>
								<input value="<?= @$akta[0]->akta_pengadilan; ?>" id="daerah_pengadilan" name="pengadilan" type="text" class="md-input uk-form-width-medium">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Nomor Akta <span class="required">*</span></label>
								<input value="<?= @$akta[0]->akta_nomor; ?>" name="nomor" type="text" class="md-input uk-form-width-medium">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Tanggal <span class="required">*</span></label><br>
								<input value="<?=@$akta[0]->akta_waktu;?>" name="tanggal" type="text" class="" id="waktu">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Nama Penerima Kuasa Fidusia <span class="required">*</span></label>
								<input value="<?= (is_null(@$akta[0]->akta_nama_penerima) ? $notaris[0]->kd_nama:$akta[0]->akta_nama_penerima); ?>" name="nama_penerima" type="text" class="md-input uk-form-width-medium">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Tempat Lahir Penerima Kuasa Fidusia <span class="required">*</span></label><br>
								<input value="<?= @$notaris[0]->kd_tempat_lahir; ?>" name="tempat_lahir_penerima" type="text" class="md-input uk-form-width-medium" style="color:red;" readonly>
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Tanggal Lahir Penerima Kuasa Fidusia <span class="required">*</span></label><br>
								<input value="<?= @$notaris[0]->kd_tanggal_lahir; ?>" name="tanggal_lahir_penerima" type="text" class="kendoDatePicker" style="color:red;" readonly>
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Pekerjaan Penerima Kuasa Fidusia <span class="required">*</span></label>
								<input value="<?= @$notaris[0]->kd_pekerjaan; ?>" name="pekerjaan_penerima" type="text" class="md-input uk-form-width-medium" style="color:red;" readonly>
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Alamat Penerima Kuasa Fidusia <span class="required">*</span></label>
								<input value="<?= @$notaris[0]->kd_alamat; ?>" name="alamat_penerima" type="text" class="md-input uk-form-width-large" style="color:red;" readonly>
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>NIK Penerima Kuasa Fidusia <span class="required">*</span></label>
								<input value="<?= @$notaris[0]->kd_nik; ?>" name="nik_penerima" type="text" class="md-input uk-form-width-medium" style="color:red;" readonly>
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Tanggal Kuasa Fidusia dari Nasabah <span class="required">*</span></label><br>
								<input value="<?= @$akta[0]->akta_tanggal_penerima; ?>" name="tanggal_penerima" type="text" class="kendoDatePicker">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Nama Nasabah <span class="required">*</span></label>
								<input value="<?= (@$akta[0]->akta_nama_nasabah=="")?@$report[0]->kon_nama:@$akta[0]->akta_nama_nasabah; ?>" name="nama_nasabah" type="text" class="md-input uk-form-width-medium">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Tempat Lahir Nasabah <span class="required">*</span></label>
								<input value="<?= @$akta[0]->akta_tempat_lahir_nasabah; ?>" name="tempat_lahir_nasabah" type="text" class="md-input uk-form-width-medium">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Tanggal Lahir Nasabah <span class="required">*</span></label><br>
								<input value="<?= @$akta[0]->akta_tanggal_lahir_nasabah; ?>" name="tanggal_lahir_nasabah" type="text" class="kendoDatePicker">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Pekerjaan Nasabah <span class="required">*</span></label>
								<input value="<?= (@$akta[0]->akta_pekerjaan_nasabah=="")?@$report[0]->kon_pekerjaan:@$akta[0]->akta_pekerjaan_nasabah; ?>" name="pekerjaan_nasabah" type="text" class="md-input uk-form-width-medium">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Alamat Nasabah <span class="required">*</span></label>
								<input value="<?= (@$akta[0]->akta_alamat_nasabah=="")?@$report[0]->kon_alamat:@$akta[0]->akta_alamat_nasabah; ?>" name="alamat_nasabah" type="text" class="md-input uk-form-width-medium">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Telepon Nasabah <span class="required">*</span></label>
								<input value="<?= @$akta[0]->akta_telp_nasabah; ?>" name="telepon_nasabah" type="text" class="md-input uk-form-width-medium">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Kode Pos Nasabah <span class="required">*</span></label>
								<input value="<?= @$akta[0]->akta_kode_pos_nasabah; ?>" id="kode_pos_nasabah" name="kode_pos_nasabah" type="text" class="md-input uk-form-width-medium">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>NIK Nasabah <span class="required">*</span></label>
								<input value="<?= (@$akta[0]->akta_nik_nasabah=="")?@$report[0]->kon_ktp:@$akta[0]->akta_nik_nasabah; ?>" name="nik_nasabah" type="text" class="md-input uk-form-width-medium">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Tanggal Kuasa Direksi <span class="required">*</span></label><br>
								<input value="<?= @$notaris[0]->kd_tanggal; ?>" name="tanggal_kuasa" type="text" class="kendoDatePicker" style="color:red;" readonly>
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Nomor Kuasa Direksi <span class="required">*</span></label>
								<input style="color:red;" value="<?= @$notaris[0]->kd_nomor; ?>" name="nomor_kuasa" type="text" class="md-input uk-form-width-medium" style="color:red;" readonly>
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Nomor Perjanjian Kredit <span class="required">*</span></label>
								<input value="<?= (@$akta[0]->akta_nomor_pk=="")?@$report[0]->ed_nomor_kontrak:@$akta[0]->akta_nomor_pk; ?>" name="nomor_pk" type="text" class="md-input uk-form-width-medium">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Tanggal Perjanjian Kredit <span class="required">*</span></label><br>
								<input value="<?= (@$akta[0]->akta_tanggal_pk=="")?@$data->pemb_tanggal:@$akta[0]->akta_tanggal_pk; ?>" name="tanggal_pk" type="text" class="kendoDatePicker">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Jumlah Hutang Pokok <span class="required">*</span></label>
								<input value="<?= @$akta[0]->akta_hutang_pokok; ?>" name="hutang_pokok" type="number" class="md-input uk-form-width-medium">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Jumlah Hutang Nasabah <span class="required">*</span></label>
								<input value="<?= @$report[0]->pemb_nilai_penjaminan; ?>" name="hutang_nasabah" type="text" class="md-input uk-form-width-medium">
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Nilai Jaminan Fidusia <span class="required">*</span></label>
								<input value="<?= @$report[0]->pemb_nilai_penjaminan; ?>" name="nilai_jaminan" type="text" class="md-input uk-form-width-medium">
							</div>
						</div>
						<div class="uk-form-row">
							<div class="md-input-wrapper">
								<div class="uk-grid">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
										<label>Notaris <span class="required">*</span></label>
									</div>
									<div class="uk-width-medium-1 uk-margin-small-bottom">
										<input type="text" name="nama_notaris" value="<?=$notaris[0]->notaris_nama?>" class="md-input uk-form-width-large" style="color:red;" readonly>
										<input type="hidden" name="notaris_id" value="<?=$notaris[0]->notaris_id?>">
										<!-- <select name="notaris_id" class="form-filter select-filter uk-form-width-large" select-style select-style-bottom >
											<option value=""></option>
											<?php foreach ($notaris as $key => $val): ?>
												<option <?= ((@$akta[0]->notaris_id == $val->notaris_id)?'selected':''); ?> value="<?=$val->notaris_id; ?>"><?=$val->notaris_nama; ?></option>
											<?php endforeach ?>
										</select> -->


									</div>
								</div>
							</div>
						</div>
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<label>Nomor Voucher </label>
								<input value="<?= @$akta[0]->akta_voucher; ?>" name="voucher" type="text" class="md-input uk-form-width-medium">
							</div>
						</div>
						<div class="uk-text-center uk-margin-large-top">
							<input type="hidden" name="status_now" value="2">
							<a href="<?=$url.'/bypass/'.$id; ?>" ><button type="button" class="md-btn md-btn-small md-btn-success md-btn-wave-light waves-effect waves-button waves-light">Bypass</button></a>&nbsp; 
							<a href="<?=$url.'/export_word2/'.$id; ?>" target="_blank"><button type="button" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">print</button></a>&nbsp; 
							<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Proses</button>
							<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
						</div>
					</div>
				</div>
				<?php } ?>		

		</form>
	</div>
</div>
</div>
<a href="<?=$url; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		$('#waktu').kendoDateTimePicker({
			format: "yyyy-MM-dd hh:mm:ss"
		});
		$('.kendoDatePicker').kendoDatePicker({
			format: "yyyy-MM-dd"
		});
					//e.preventDefault();
					$('#provinsi').on('change',function(){
						//e.preventDefault();
						var id = $(this).val();
						if(id){
							$.ajax({
								type:'POST',
								url: base_url+'report/get_kota',
								data:'provinsi_id='+id,
								success:function(htmla){
									//$('#test').html('<option value="">Select country first</option>');
									$('#kota').html(htmla);
									$('#kecamatan').html('<option value=""></option>'); 
									$('#kelurahan').html('<option value=""></option>'); 
								}
							}); 
						}else{
							$('#kecamatan').html('<option value=""></option>');
							$('#kelurahan').html('<option value=""></option>'); 
							$('#kota').html('<option value=""></option>'); 
						}
					});
					$('#kota').on('change',function(){
						//e.preventDefault();
						var provinsi_id = $('#provinsi').val();
						var id = $(this).val();
						if(id){
							$.ajax({
								type:'POST',
								url: base_url+'report/get_kecamatan',
								data:{provinsi_id: provinsi_id, kota_id: id},
								success:function(htmla){
									$('#kecamatan').html(htmla); 
									$('#kelurahan').html('<option value=""></option>'); 
								}
							}); 
						}else{
							$('#kecamatan').html('<option value=""></option>'); 
							$('#kelurahan').html('<option value=""></option>'); 
						}
					});
					$('#kecamatan').on('change',function(){
						//e.preventDefault();
						var provinsi_id = $('#provinsi').val();
						var kota_id = $('#kota').val();
						var id = $(this).val();
						if(id){
							$.ajax({
								type:'POST',
								url: base_url+'report/get_kelurahan',
								data:{provinsi_id: provinsi_id, kota_id: kota_id, kecamatan_id: id},
								success:function(htmla){
									$('#kelurahan').html(htmla); 
								}
							}); 
						}else{
							$('#kelurahan').html('<option value=""></option>'); 
						}
					});
					$('#kelurahan').on('change',function(){
						var id = $(this).val();
						if(id){
							$.ajax({
								type:'POST',
								url: base_url+'report/get_asd',
								data:'daerah_id='+id,
								success:function(htmla){
									htmlb = htmla.split("|");
									$('#kode_pos_nasabah').val(htmlb[0]);
									$('#daerah_pengadilan').val(htmlb[1]);
								}
							}); 
						}else{
							
						}
					});
					var urlPDF = "<?php echo base_url($pathPdfFile.$lampiran[0]->lamp_nama); ?>";
					PDFObject.embed(urlPDF, "#my-container");

					$('.cb1').on('ifChecked', function(event){
						if($("#gender2").prop('checked') == true){
							$('.cb1div').show();
						}else if($("#gender1").prop('checked') == true){
							$('.cb1div').hide();
						}
					});
					$('#gender1').on('click', function() {
					    //	$('#row1').css('display', this.checked ? 'none' : 'inline');
					});
					var url 	= "<?=$url; ?>/log/<?=$report[0]->ed_id; ?>",
					header  = [
					{ "className": "text-center" },
					null,
					null,
					null,
					null,
					],
					order 	= [
					['1', 'asc']
					],
					sort 	= [-1, 0];
					App.setDatatable(url, header, order, sort);
					$('#form_approve').on('submit', function(e){
						e.preventDefault();

						var $this 	= $(this),
						url 		= $this.attr('action'),
						data 		= $this.serialize();

						var form = $('form')[0]	;
						var formData = new FormData(form);
						var err_res = 0,
						err_msg = [],
						suc_msg = [];

						$.ajax({
							url: url,
							type: 'post',
							dataType:'json',
							data: formData,
							processData: false,
							contentType: false,
							async: false,
							success : function(res) {
								if (res.sts == 5) {
									$('#alert').html(res.alert)
									App.scrollTop();
								}
								if (res.sts == 0) {
									App.notif('error', res.msg, 'error');
								}
								else if (res.inp == 2) {
									App.notif('Success', res.msg, 'success');
									$('.reload').trigger('click');
									if (res.key == 1) {
										window.location = "<?=$url.'/export_word2/'.$id; ?>";
									}
								}
								if (res.sts == 3 && res.inp == 2) {
									suc_res = '';
									var chk = 0;
									$.each(res, function( key, val ) {
										if(val['sts'] == 1) {
											suc_res = 1;
											suc_msg.push(val['msg']);
										} else {
											if(typeof val['msg'] !== "undefined") chk = 1;
											err_msg.push(val['msg']);
										}
									});
									if(suc_res == 1){
										App.notif('Success', suc_msg.join('<br>') , 'success');
										if(chk === 1){
											App.notif('Error', err_msg.join('<br>'), 'error');
										}
										$('.reload').trigger('click');
									}else{
										App.notif('Success', suc_msg.join('<br>') , 'success');
										if(chk === 1){
											App.notif('Error', err_msg.join('<br>'), 'error');
										}
										$('.reload').trigger('click');
									}
								}
							},
							error 	: function(res) {
								App.notif('Error', res.msg , 'error');
							}
						});
					});

					App.datepicker();
					App.masked_input();
				});

$('.uploadFile').dropify({
	messages: {
		'default': 'File Uplaod',
		'replace': '',
		'remove':  'Batal',
	}
});


function forceLower(ele) {
	var lc = $(ele).val().toLowerCase();

	$(ele).val(lc);
}


</script>