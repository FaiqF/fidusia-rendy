<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_profile extends MX_Controller {

	private $table_db       = 'users';
	private $table_prefix   = 'user_';
	private $prefix 		= 'my_profile';
	private $name 			= '';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$id = strEncrypt(user_data()->user_id);

		$data['name'] 		 = $this->name;
		$data['url'] 		 = base_url().$this->prefix;
		$data['plugin']		 = ['kendo_ui', 'fancytree'];
		$data['breadcrumbs'] = [];
		$data['record']		 = $this->m_global->get_data_all('users', [['level', 'level_id = user_level']], [strEncrypt('user_id', TRUE) => $id]);

		$data['role']		 = $this->m_global->get_data_all('roles', null, ['role_id' => $data['record'][0]->user_role]);

		$data['position'] 	 = $this->_getPosition($data['record']);
		$data['id'] 		 = $id;

		$this->template->display('index', $data);
	}

	public function action_edit($id)
	{
		$result = [];
		$post 	= $this->input->post();

		$MIN_PSWD_CHAR = get_config_password('MIN_PSWD_CHAR') ? '|min_length['.get_config_password('MIN_PSWD_CHAR')[0]->pass_value.']' : '';
		$MAX_PSWD_CHAR = get_config_password('MAX_PSWD_CHAR') ? '|max_length['.get_config_password('MAX_PSWD_CHAR')[0]->pass_value.']' : '';

		$configData = 'trim|matches[repeat_password]|differs[nik]'.$MIN_PSWD_CHAR.$MAX_PSWD_CHAR;
		$configData2 = 'trim'.$MIN_PSWD_CHAR.$MAX_PSWD_CHAR;

		$this->form_validation->set_rules('password_lama', 'Password Lama', 'trim'.$MIN_PSWD_CHAR.$MAX_PSWD_CHAR);
		$this->form_validation->set_rules('password', 'Password', $configData);
		$this->form_validation->set_rules('repeat_password', 'Verifikasi Password', $configData2);

		//$this->form_validation->set_rules('nik', 'NIK', 'trim|required');
		//$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		// $this->form_validation->set_rules('user_status', 'Status', 'trim|required');
		if ($this->form_validation->run() == TRUE){
			$nik = $post['nik'];

			$check_username = $this->m_global->get_data_all('users', null, ['user_status' => '1', 'user_nik' => $nik, strEncrypt('user_id', TRUE).' <> ' => $id]);

			if ($check_username) {
				$result['msg'] = 'Username sudah digunakan !';
				$result['sts'] = '0';
			} else {
				$checkPassword['sts'] = true;
				$go 		   = true;
				if($post['password_lama'] !== '') {
					$get_pass = $this->m_global->get_data_all('users', null, ['user_password' => hash('sha512', $post['password_lama']), 'user_status' => '1', strEncrypt('user_id', TRUE) => $id]);

					if ($get_pass) {
						if($post['password'] !== '') {
							$user_data['user_password'] = hash('sha512', $post['password']);
							$checkPassword 				= $this->_check_pass($post['password']);
							$go 						= check_update_password($id, $post['password'], true);
						} else {
							$result['msg'] = 'Password harus diisi !';
							$result['sts'] = '0';

							echo json_encode($result);
							exit;
						}
					} else {
						$result['msg'] = 'Password Lama yang anda masukan salah !';
						$result['sts'] = '0';

						echo json_encode($result);
						exit;
					}
				}

				if ($checkPassword['sts'] == false) {
					$msg = $checkPassword['msg'];

					$result['msg'] = $msg;
					$result['sts'] = '99';

					echo json_encode($result);
					exit;
				} else if ($checkPassword['sts'] == true) {
					if ($go === false) {
						$result['msg'] = 'Password sudah pernah digunakan !';
						$result['sts'] = '99';

						echo json_encode($result);
						exit;
					}
				}
				
				$user_data['user_nik']			= $post['nik'];
				$user_data['user_email']		= @$post['email'];
				$user_data['user_expired_date']		= date('Y-m-d', strtotime('+90 days'));
				// $user_data['user_status']		= $post['user_status'];
				$user = $this->m_global->update('users', $user_data, [strEncrypt('user_id', TRUE) => $id]);

				if($user) {
					/*$profile_data = [
										'profile_fullname'	=> $post['fullname'],
										'profile_gender' 	=> $post['gender'],
										'profile_birthdate'	=> date('Y-m-d', strtotime($post['birthdate'])),
										'profile_address'	=> $post['address'],
										'profile_phone'		=> $post['phone'],
										'profile_email'		=> $post['email']
									];

					$profile = $this->m_global->update('profiles', $profile_data, [strEncrypt('profile_user_id', TRUE) => $id]);

					if($profile) {
						
					} else {
						$result['msg'] = 'Data gagal dirubah !';
						$result['sts'] = '0';
					}*/
					$result['msg'] = 'Data berhasil dirubah !';
					$result['sts'] = '1';
				} else {
					$result['msg'] = 'Data gagal dirubah !';
					$result['sts'] = '0';
				}
			}
		} else {
			$result['msg'] = validation_errors();
			$result['sts'] = '99';
		}

		echo json_encode($result);
	}

	public function _check_pass($str)
	{
		$result 	= [];

		$dataResult = $this->m_global->get_data_all('password', null, ['pass_status' => '1', 'pass_type' => '2'], 'pass_code, pass_number');

		$numbers	= '/[0-9]/';
		$uppercase 	= '/[A-Z]/';
		$lowercase 	= '/[a-z]/';
		$symbol		= '/[!@#$%^&*()\-_=+{};:,<.>]/';

		$r_upper 	= (preg_match_all($uppercase, $str, $out) > 0) ? 1 : 0 ;
		$r_lower 	= (preg_match_all($lowercase, $str, $out) > 0) ? 1 : 0 ;
		$r_numbers	= (preg_match_all($numbers, $str, $out) > 0) ? 1 : 0 ;
		$r_symbol	= (preg_match_all($symbol, $str, $out) > 0) ? 1 : 0 ;

		$numberCode = (($r_upper == 1) ? 7000 : 0) + (($r_lower == 1) ? 700 : 0) + (($r_numbers == 1) ? 70 : 0) + (($r_symbol == 1) ? 7 : 0);

		$arrData 	= [
						'UPPER' 	=> $r_upper,
						'LOWER' 	=> $r_lower,
						'NUMBERS' 	=> $r_numbers,
						'SYMBOLS' 	=> $r_symbol
					  ];

		$msg 		= [
						'DEFAULT' 	=> '<p>Kombinasi password harus terdiri dari :</p>',
						'NUMBERS' 	=> '<p>- Angka (numeric)</p>',
						'UPPER' 	=> '<p>- Huruf besar (uppercase alphabetic)</p>',
						'LOWER' 	=> '<p>- Huruf kecil (lowercase alphabetic)</p>',
						'SYMBOLS' 	=> '<p>- Simbol/karakter khusus (special characters).</p>'
					  ];

		$dataMsg = $msg['DEFAULT'];
		foreach ($dataResult as $key => $val) {
			$dataMsg .= $msg[$val->pass_code];
		}

		$num = 0;
		foreach ($dataResult as $key => $val) {
			$return = $arrData[$val->pass_code];
			$num   += $val->pass_number;

			if($return !== 1) {
				return ['sts' => FALSE, 'msg' => $dataMsg];
				die();
			}
		}

		if ($num == $numberCode) {
			return ['sts' => TRUE];
		} else {
			return ['sts' => FALSE, 'msg' => $dataMsg];
		}
	}

	public function _getPosition($data)
	{
		$group  = @$data[0]->level_group;
		$id     = @$data[0]->user_group_id;
		$result = '-';

		if ($group == 1) {
			$data = $this->m_global->get_data_all('notaris', null, ['notaris_id' => $id], 'notaris_id, notaris_nama');

			if($data) {
				$result = $data[0]->notaris_nama;
			}
		} else {
			$data = $this->m_global->get_data_all('cabang', null, ['cabang_id' => $id], 'cabang_id, cabang_name');

			if($data) {
				$result = 'Cabang - '.$data[0]->cabang_name;
			}
		}

		return $result;
	}

}

/* End of file My_profile.php */
/* Location: ./application/modules/my_profile/controllers/My_profile.php */