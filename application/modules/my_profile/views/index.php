<?php $data = @$record[0]; ?>
<form action="<?=$url; ?>/action_edit/<?=$id; ?>" class="uk-form-stacked" id="user_edit_form">
	<div class="uk-grid" data-uk-grid-margin>
		<div class="uk-width-large-7-10">
			<div class="md-card">
				<div class="user_heading" data-uk-sticky="{ top: 48, media: 960 }">
					<div class="user_heading_avatar fileinput fileinput-new" data-provides="fileinput">
						<div class="fileinput-new thumbnail">
							<img src="<?=base_url(); ?>assets/img/avatars/user.png" alt="user avatar"/>
						</div>
						<div class="fileinput-preview fileinput-exists thumbnail"></div>
					</div>
					<div class="user_heading_content">
						<h2 class="heading_b"><span class="uk-text-truncate" id="user_edit_uname"><?=user_data()->user_nik; ?></span><span class="sub-heading" id="user_edit_position"><?=user_data()->role_name; ?></span></h2>
					</div>
				</div>
				<div class="user_content">
					<div id="alert_error"></div>
					<h4 class="heading_c uk-margin-medium-bottom">User</h4>
					<div class="uk-margin-large-bottom">
						<div class="uk-grid">
							<div class="uk-width-medium-1-1 uk-row-first">
								<div class="uk-grid">
									<div class="uk-width-medium-1-2 uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>NIK <span class="required">*</span></label>
												<input <?=(!in_array(user_data()->user_level, ['1'])) ? 'disabled' : ''; ?> value="<?=$data->user_nik; ?>" id="nik" name="n" type="text" class="md-input uk-form-width-medium">
												<input value="<?=$data->user_nik; ?>" id="nik" name="nik" type="hidden" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Email </label>
												<input value="<?=$data->user_email; ?>" name="email" type="email" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div class="uk-width-medium-1-2">
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Password Lama</label>
												<input name="password_lama" type="password" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
												<span class="uk-form-help-block">Kosongkan jika tidak ada perubahan !</span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Password Baru</label>
												<input name="password" type="password" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
												<span class="uk-form-help-block">Kosongkan jika tidak ada perubahan !</span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Verifikasi Password</label>
												<input name="repeat_password" type="password" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
												<span class="uk-form-help-block">Kosongkan jika tidak ada perubahan !</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-text-center uk-margin-medium-top">
						<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Simpan</button>
						<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-danger md-btn-wave-light waves-effect waves-button waves-light ajaxify">Reset</a>
					</div>
				</div>
			</div>
		</div>
		<div class="uk-width-large-3-10">
			<div class="md-card">
				<div class="md-card-content">
					<h3 class="heading_c uk-margin-small-bottom">Information</h3>
					<hr>
					<table style="width: 100%; text-align: left;">
						<tr>
							<th width="40px">Level</th>
							<th>:</th>
							<td><?=$data->level_name; ?></td>
						</tr>
						<tr>
							<th>Role</th>
							<th>:</th>
							<td><?=$role[0]->role_name; ?></td>
						</tr>
						<tr>
							<th>Posisi</th>
							<th>:</th>
							<td>
								<?php echo $position; ?>
							</td>
						</tr>
						<tr>
							<th>Akses</th>
							<th>:</th>
							<td></td>
						</tr>
					</table>
					<div id="roles">
						<?php v_tree_view(menu(''), $role[0]->role_access); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<a href="<?=$url; ?>" class="ajaxify reload"></a>

<script>
	$(document).ready(function(){
		App.form_submit($('#user_edit_form'));

		App.datepicker();
		App.masked_input();

		$('#roles').fancytree();
		
		$('.no-space').on('keydown', function(e){
			if(e.which !== 32){
				return e.which;
			} else {
				return false;
			}
		});

		$('#nik').on('keyup', function(e){
			var regex = /^[a-zA-Z0-9]+$/,
				val   = $(this).val();

			if (regex.test(val)) {
				return;
			} else {
				e.preventDefault();
			}
		});

		$('#level').on('change', function(e){
			e.preventDefault();

			var $this    = $(this).val(),
				not_view = $('#notaris'),
				cab_view = $('#cabang'),
				target 	 = $('#target');

			$.post(base_url+'config/config_user/get_group', {id: $this}, function(data, textStatus, xhr) {
				if (data == 1) {
					not_view.show();
					cab_view.hide();

					target.val(data);
				}

				if (data == 2) {
					not_view.hide();
					cab_view.show();

					target.val(data);
				}

				if (data == '' || data == 0) {
					not_view.hide();
					cab_view.hide();
				}
			}, 'json');
		});

		$(".number_only").keydown(function (e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
					 // let it happen, don't do anything
					 return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});

		$('#user_role').on('change', function(e){
			e.preventDefault();
			$('#roles').html('');
			$('#roles').fancytree("destroy");

			$.ajax({
				url: base_url+'config/config_user/get_treeview',
				data: {id: $(this).val()},
				type: 'post',
				dataType: 'html',
				success: function(data) {
					$('#roles').html(data);
				}
			}).done(function(){
				$("#roles").fancytree({
					checkbox: false,
					selectMode: 3,
					extensions: ["dnd"],
					autoScroll: true,
				});
			});
		});
	});
</script>