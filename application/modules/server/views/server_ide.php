
<style>
	table {
		border-collapse: collapse;
		border-spacing: 0;
		border: 1px solid #ddd;
	}

	th, td {
		text-align: left;
		padding: 8px;
	}

	tr:nth-child(even){background-color: #f2f2f2}
</style>

<form action="<?php echo base_url('server/IDE'); ?>" method="POST">
	<center>
		<table border="1" width="60%">
			<tr>
				<td colspan="2"><center>IDE Mysql</center></td>
			</tr>
			<tr>
				<td>Tipe Proses</td>
				<td>
					<select class="ctnTipe" style="width:25%;" name="ctnTipe">
						<option value="" disabled selected>Select your option</option>
						<option value="1">Menampilkan Data</option>
					</select>
				</td>
			</tr>
			<tr class="ctnTable" style="display:none;">
				<td>Table</td>
				<td><div id="bdTable"></div></td>
			</tr>
			<tr class="ctnField" style="display:none;">
				<td>Field</td>
				<td><div id="bdField"></div></td>
			</tr>
			<tr>
				<td colspan="2">
					<center><button>Prosess</button></center>
				</td>
			</tr>
		</table>
	</center>
</form>

<div id="body"></div>


<script src="<?=base_url(); ?>assets/js/common.min.js"></script>
<script>
	$(document).ready(function(){
		$("select[name$='ctnTipe']").change(function(){
			var val = $(this).val();
			if(val == '1'){
				$('.ctnTable').fadeIn('slow');
				
				var xurl = "<?php echo base_url('server/IDE/getTable'); ?>";
				$.post(xurl,null,function(res){
					$('#bdTable').html(res);
				}).done(function(){
					$(".fTable").change(function(){
						var val = $(this).val();
						
						if(val != ''){
							$('.ctnField').fadeIn('slow');
							
							var xurl = '<?php echo base_url('server/IDE/getField'); ?>';
							var xdta = { 'table' : val };
							$.post(xurl,xdta,function(res){
								$('#bdField').html(res);
							});
						}else{
							$('.ctnField').fadeOut('slow');
						}
					});
				});
				
			}else{
				$('.ctnTable').fadeOut('slow');
			}
		});
		
		
	});
</script>