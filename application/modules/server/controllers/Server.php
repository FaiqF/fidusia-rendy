	<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
    Author :
        - Irfan Isma Somantri || irfan.isma@gmail.com || 08973950031
        - Faiq Fajrullah
*/

class Server extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
    }
    

    public function index(){
        
		// system load
		$coreCount = 2;
        $interval  = 1;

        $rs       = sys_getloadavg();
        $interval = $interval >= 1 && 3 <= $interval ? $interval : 1;
        $load     = $rs[$interval];
        $hasil    = round(($load * 100) / $coreCount,2);
		
		//number system of core
		$cmd = "uname";
		$OS = strtolower(trim(shell_exec($cmd)));
	 
		switch($OS) {
		   case('linux'):
			  $cmd = "cat /proc/cpuinfo | grep processor | wc -l";
			  break;
		   case('freebsd'):
			  $cmd = "sysctl -a | grep 'hw.ncpu' | cut -d ':' -f2";
			  break;
		   default:
			  unset($cmd);
		}
	 
		if ($cmd != '') {
		   $cpuCoreNo = intval(trim(shell_exec($cmd)));
		}
		
		// memory usage (ram)
		$free 	  	  = shell_exec('free');
		$free 	  	  = (string)trim($free);
		$free_arr 	  = explode("\n", $free);
		$mem 	  	  = explode(" ", $free_arr[1]);
		$mem 	  	  = array_filter($mem);
		$mem 	      = array_merge($mem);
		$memory_usage = $mem[2] / $mem[1] * 100;
		
		// disk usage
		$disktotal = disk_total_space ('/');
		$diskfree  = disk_free_space  ('/');
		$diskuse   = round (100 - (($diskfree / $disktotal) * 100)) .'%';
		
		// Server uptime
		$uptime = floor(preg_replace ('/\.[0-9]+/', '', file_get_contents('/proc/uptime')) / 86400);
		
		// Kernel Version
		$kernel = explode(' ', file_get_contents('/proc/version'));
		$kernel = $kernel[2];
		
		// number of proses
		$proc_count = 0;
		$dh 		= opendir('/proc');
		
		while ($dir = readdir($dh)) {
			if (is_dir('/proc/' . $dir)) {
				if (preg_match('/^[0-9]+$/', $dir)) {
					$proc_count ++;
				}
			}
		}
		
		// current memory usage
		$mem = memory_get_usage(true);
	
		if ($mem < 1024) {
			$$memory = $mem .' B'; 
		} elseif ($mem < 1048576) {
			$memory = round($mem / 1024, 2) .' KB';
		} else {
			$memory = round($mem / 1048576, 2) .' MB';
		}
	
		echo 'Jumlah Memory ' . memory_get_usage() . '<br>';
        echo 'system load as percentage '. $hasil . '% <br>';
		echo 'number system of core '.(empty($cpuCoreNo) ? 1 : $cpuCoreNo) . ' % <br>';
		echo 'memory usage ' . $memory_usage . '% <br>';
		echo 'disk usage ' . $diskuse . '<br>';
		echo 'server uptime ' . $uptime . '% <br>';
		echo 'kernel version ' . $kernel . '<br>';
		echo 'number of proses ' . $proc_count . '% <br>';
		echo 'current memory usage ' . $memory;
		
    }
    public function cekSftp()
    {
    	set_include_path('phpseclib');
		include('phpseclib/Net/SFTP.php');
		$sftp = new Net_SFTP('112.215.7.49');
			
		if (!$sftp->login('fidusia', 'P@ssw0rd')) {
				exit('Login Failed');
		} else {
			echo "success";
		}
	}
	function dump_db(){
        include ('phpseclib/Dumper/dumper.php');

        $world_dumper = Shuttle_Dumper::create(array(
            'host'     => '112.215.7.49',
            'username' => 'root',
            'password' => 'Password123',
            'db_name'  => 'wom_finance_new',
        ));
        // dump the database to gzipped file
        $world_dumper->dump('wom_finance.sql.gz');
    }

}

/* End of file Controllername.php */

?>