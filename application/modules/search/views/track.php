<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-toolbar">
		<h3 class="md-card-toolbar-heading-text">
			Tracking Data
		</h3>
	</div>
	<div class="md-card-content">
		<table id="datatable_ajax" class="uk-table">
			<thead>
				<tr>
					<th width="40">No.</th>
					<th width="130">No Kontrak</th>
					<th width="100">Status</th>
					<th>Catatan</th>
					<th width="130">User</th>
					<th width="150">Tanggal</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 1; foreach ($record['log'] as $key => $val): ?>
					<tr>
						<td><?=$i; ?></td>
						<td><?=$val->log_ed_nomor_kontrak; ?></td>
						<td><?=flag_status($val->log_ed_flag); ?></td>
						<td><?=$val->log_ed_note ? $val->log_ed_note: '-' ; ?></td>
						<td><?=$val->user_nik; ?></td>
						<td><?=date_format_indo($val->log_ed_date); ?></td>
					</tr>
				<?php $i++; endforeach; ?>
			</tbody>
		</table>
		<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
		<?php if(in_array(user_data()->user_level, ['1', '9'])){?>
		<a href="<?=$url.'/bypassTerdaftar/'.$id; ?>" ><button type="button" class="md-btn md-btn-small md-btn-success md-btn-wave-light waves-effect waves-button waves-light">Terdaftar</button></a>&nbsp;
		<a href="<?=$url.'/bypassTerkirim/'.$id; ?>" ><button type="button" class="md-btn md-btn-small md-btn-info md-btn-wave-light waves-effect waves-button waves-light">Terkirim</button></a>&nbsp;
		<?php }?>
	</div>
</div>