<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-content">
		<div class="md-card">
			<div class="md-card-toolbar">
				<div class="md-card-toolbar-actions">
					<i class="md-icon material-icons md-card-toggle">&#xE316;</i>
				</div>
				<h3 class="md-card-toolbar-heading-text">
					Filter
				</h3>
			</div>
			<div class="md-card-content filter">
				<div class="uk-grid">
					<!--<div class="uk-width-medium-1-2 uk-row-first"> 
						<div class="uk-grid">
							<div class="uk-width-medium-1-3 uk-margin-small-bottom uk-row-first">
								<label>Tanggal Awal: </label> <input name="lastupdate[0]" type="text" class="md-input form-filter select-filter" id="text3">
							</div>
							<div class="uk-width-medium-1-3 uk-margin-small-bottom uk-row-first">
								<label>Tanggal Akhir: </label> <input name="lastupdate[1]" type="text" class="md-input form-filter select-filter" id="text4">
							</div>
						</div>
					</div>-->
					<div class="uk-width-medium-1-3 uk-row-first">
						<div class="md-input-wrapper">
							<label>Nomor Kontrak</label>
							<input name="kontrak" type="text" class="md-input form-filter">
							<span class="md-input-bar"></span>
						</div>
					</div>
					<div class="uk-width-medium-1-3">
						<select name="flag" class="form-filter select-filter" select-style select-style-bottom>
							<option value="">Status</option>
							<option value="'0'">Upload</option>
							<option value="'8'">Siap Kirim</option>
							<option value="'1'">Terkirim</option>
							<option value="'2', '3', '4'">Diterima</option>
							<option value="'5'">Terdaftar</option>
							<option value="'7'">Terbayar</option>
							<option value="'9'">Reject</option>
						</select>
						<input type="hidden" class="form-filter select-filter" name="cabang_name">
						<input type="hidden" class="form-filter select-filter" name="pem_nama">
						<input type="hidden" class="form-filter select-filter" name="nilai_penjaminan">
						<input type="hidden" class="form-filter select-filter" name="nilai_benda_fidusia">
						<input type="hidden" class="form-filter select-filter" name="merk">
						<input type="hidden" class="form-filter select-filter" name="tipe">
						<input type="hidden" class="form-filter select-filter" name="tahun_pembuatan">
						<input type="hidden" class="form-filter select-filter" name="no_rangka">
						<input type="hidden" class="form-filter select-filter" name="no_mesin">
						<input type="hidden" class="form-filter select-filter" name="covernote">
						<input type="hidden" class="form-filter select-filter" name="tanggal_jual">
					</div>
					<div class="uk-width-medium-1-3">
						<select name="region" class="form-filter select-filter" select-style select-style-bottom>
							<option value="">Region</option>
						 <?php foreach($region as $key => $value){?>
							<option value="<?php echo $value->key;?>"><?php echo $value->name_of_region; ?></option>	
						 <?php }?>
						</select>
					</div>
				</div>
				<div class="uk-grid uk-margin-medium-top uk-text-center">
					<div class="uk-width-1-1">
						<button type="button" class="filter-submit md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Cari</button>
						<button type="button" class="filter-reset md-btn md-btn-small md-btn-danger md-btn-wave-light waves-effect waves-button waves-light">Reset</button>
					</div>
				</div>
			</div>
		</div>
		<table id="datatable_ajax" class="uk-table" cellspacing="0" width="100%" fixedColumns="true" leftColumns="1" rightColumns="1">
			<thead>
				<tr>
					<th width="40">No.</th>
					<th>No Kontrak</th>
					<th>Cabang</th>
					<th>Status</th>
					<th>Nama Pemilik</th>
					<th>Nilai Penjaminan</th>
					<th>Nilai Benda Fidusia</th>
					<th>Merk Kendaraan</th>
					<th>Tipe Kendaraan</th>
					<th>Tahun Pembuatan</th>
					<th>No Rangka Kendaraan</th>
					<th>No Mesin Kendaraan</th>
					<th>Cover Note</th>
					<th>Tanggal Jual</th>
					<th width="152">Aksi</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</div>

<script>
	$(document).ready(function(){
		var url 	= "<?=$url; ?>/select",
			header  = [
						{ "className": "text-center" },
						{ "className": "text-right" },
						null,
						{ "className": "text-center" },
						null,
						null,
						null,
						{ "className": "text-right" },
						null,
						null,
						null,
						null,
						null,
						{ "className": "text-center" },
						{ "className": "text-center" },
					  ],
			order 	= [
						['1', 'asc']
					  ],
			sort 	= [-1, 0];

		App.setDatatable(url, header, order, sort); //paling akhir tambahkan sendiri jika tabel lebih dari satu
		App.datepicker();
		App.datepicker('.datepicker-month', true);
		App.date_range('#text3', '#text4');
		$('#text3,text4').focus(function() {
			$('#form1').find('input, select, textarea').not('#text3,#text4').val('');
		});
	});
</script>
