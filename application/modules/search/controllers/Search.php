<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	require_once APPPATH.'libraries/spout/Autoloader/autoload.php';

	// using spout class must be outside class codeigniter
	use Box\Spout\Reader\ReaderFactory;
	use Box\Spout\Writer\WriterFactory;
	use Box\Spout\Common\Type;
	use Box\Spout\Writer\Style\StyleBuilder;
	use Box\Spout\Writer\Style\Color;
	use Box\Spout\Writer\Style\Border;
	use Box\Spout\Writer\Style\BorderBuilder;

class Search extends MX_Controller {

	private $table_db       = 'entry_data'; //nama table
	private $table_prefix   = 'ed_'; //nama awal pada field
	private $prefix 		= 'search';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$data['name'] 	= "Search";
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables', 'datatables_fixcolumns','kendo_ui'];
		$data['export']	= [['name' => 'Export Excel', 'link' => base_url('search/expo')]];
		$data['region']   = $this->m_global->get_data_all('region',null,null,'region_id as key,region_name as name_of_region');

		$this->template->display('index', $data);
	}

	public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 				= null;
		$where  			= null;
		$where_e 			= null;
		$where = [];
		
		if(in_array(user_data()->role_name, ['CUSTODY STAFF', 'BACK OFFICE 2 SPV', 'BACK OFFICE SPV', 'BOH'])){
			$where['ed_cabang'] = user_data()->cabang_id;
		}
		$search 	= [
			"kontrak" 				=> $this->table_prefix.'nomor_kontrak',
			"cabang_name" 			=> 'cabang_name',
			"flag" 					=> $this->table_prefix.'flag',
			"pem_nama" 				=> 'pem_nama',
			"nilai_penjaminan" 		=> 'pemb_nilai_penjaminan',
			"nilai_benda_fidusia" 	=> 'pemb_nilai_benda_fidusia',
			"merk" 					=> 'dk_merk',
			"tipe" 					=> 'dk_tipe',
			"tahun_pembuatan" 		=> 'dk_tahun_pembuatan',
			"no_rangka" 			=> 'dk_no_rangka',
			"no_mesin" 				=> 'dk_no_mesin',
			"covernote" 			=> 'dk_covernote',
			"tanggal_jual" 			=> $this->table_prefix.'tanggal_jual',
			"region"				=> "cabang_region_id"
		];

		if (@$post['action'] == 'filter')
		{
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = $post[$key];
						if($tmp[0] != "" && $tmp[1] != ""){
							$where_e = $where_e."DATE(ed_lastupdate) BETWEEN '".date('Y-m-d', strtotime($tmp[0]))."' AND '".date('Y-m-d', strtotime($tmp[1]))."' AND ";
						}
					} else if($key == 'flag') {
						$where_e = $where_e." ".$this->table_prefix.$key." IN($post[$key]) AND  ";
					} else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			//$where[$this->table_prefix.'status <>'] = '99';
		}

		$keys 		= array_keys($search);
		@$order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

		$select		= '*, '.implode(',', $search);
		$where_e = $where_e." 1 = 1";
		$count 		= $this->m_global->count_data_all( 'v_'.$this->table_db, $join, $where, $where_e );
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


		$data = $this->m_global->get_data_all('v_'.$this->table_db, $join, $where, $select, $where_e, $order, $start, $length);
		$i = 1 + $start;

		$status = [
					'0' => '<span class="uk-badge uk-badge-warning">InActive</span>',
					'1' => '<span class="uk-badge uk-badge-primary">Active</span>',
					'99' => '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
				  ];

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
				$rows->ed_nomor_kontrak,
				$rows->cabang_name,
				flag_status($rows->ed_flag),
				
				$rows->pem_nama,
				
				uang($rows->pemb_nilai_penjaminan,null),
				uang($rows->pemb_nilai_benda_fidusia,null),
				$rows->dk_merk,
				$rows->dk_tipe,
				$rows->dk_tahun_pembuatan,
				$rows->dk_no_rangka,
				$rows->dk_no_mesin,
				
				(($rows->dk_covernote != "")?date('d-m-Y', strtotime($rows->dk_covernote)):''),
				date('d-m-Y', strtotime($rows->ed_tanggal_jual)),
				$this->_button($rows->ed_id, $rows->ed_status, $rows->ed_flag, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

	public function _button($id, $status, $flag, $show = false)
	{
		$prev = $id;
		$id = strEncrypt($id);
		$button = '';

		if($show) {
			$preview = '<a data-uk-tooltip title="Preview" class="md-btn md-btn-primary md-btn-mini md-btn-wave-light md-btn-icon waves-effect waves-button waves-light ajaxify" href="'.base_url('search/track').'/'.$id.'">
								<i class="uk-icon-map"></i>
							</a>';

			$button = $preview;
		} else {
			$button = '-';
		}

		return $button;
	}

	public function change_status_by( $id, $status, $stat = FALSE )
	{
		$this->db->trans_begin();
		if ( $stat ) {
			$arrlamp = $this->m_global->get_data_all('lampiran', null, [strEncrypt('lamp_ed_id', TRUE) => $id]);
			$locations = str_replace(base_url(), '', str_replace('/'.$arrlamp[0]->lamp_nama, '', $arrlamp[0]->lamp_file));
			foreach (glob($locations."/*") as $filename) {
				unlink($filename);
			}
			if(rmdir($locations)){
				$result = $this->m_global->delete( $this->table_db, [strEncrypt($this->table_prefix.'id', true) => $id] );
				$result = $this->m_global->delete( 'lampiran', [strEncrypt('lamp_ed_id', true) => $id] );
			}else{
				echo json_encode( 0 );
				die();
			}
		} else {
			$result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [strEncrypt($this->table_prefix.'id', true) => $id]);
		}

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$data['status'] = 0;
		}else{
			$this->db->trans_commit();
			$data['status'] = 1;
		}
		echo json_encode( $data );
	}

	public function track($id)
	{
		$data['name'] 		 = "Tracking Data";
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'Tracking Fidusia' => base_url('search'), 'Tracking Data' => base_url('search/track').'/'.$id];

		$record = $this->m_global->get_data_all('v_'.$this->table_db, null, [strEncrypt('ed_id', TRUE) => $id])[0];

		$data['record'] = [
			'ed_id' 		=> $record->ed_id,
			'nomor_kontrak' => $record->ed_nomor_kontrak,
			'log'			=> $this->m_global->get_data_all('log_entry_data', [['users', 'user_id = log_ed_user_id']], ['log_ed_id' => $record->ed_id, 'log_ed_nomor_kontrak' => $record->ed_nomor_kontrak], 'log_entry_data.*, user_nik', null, ['log_ed_date', 'asc']),
		];

		$data['id'] = $id;

		$this->template->display('track', $data);
	}
	function export_excel()  {

		$query = "SELECT ks.kon_nama as nama_konsumen,cb.cabang_name nama_cabang,log1.date_upload,log1.ed_nomor_kontrak,log2.date_kirim,log3.date_terkirim,log4.date_diterima,log5.date_ahu,log6.date_sertifikat,log7.date_terdaftar,log8.note_reject,log8.date_reject
		 FROM (SELECT ed.ed_id AS upload_ed_id,ed_cabang,ed_konsumen_id,MAX(led.log_ed_date) AS date_upload,ed.ed_nomor_kontrak,log_ed_flag FROM entry_data ed INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id WHERE log_ed_flag = '0' GROUP BY ed.ed_nomor_kontrak, led.log_ed_flag ) AS log1 
		 INNER JOIN konsumen ks ON log1.ed_konsumen_id = kon_id 
		 INNER JOIN cabang cb ON log1.ed_cabang = cb.cabang_id	
		LEFT JOIN (SELECT ed.ed_id AS kirim_ed_id,MAX(led.log_ed_date) AS date_kirim FROM entry_data ed INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id WHERE log_ed_flag = '8' GROUP BY ed.ed_nomor_kontrak, led.log_ed_flag ORDER BY ed_id ASC ) AS log2 ON log1.upload_ed_id = log2.kirim_ed_id
		LEFT JOIN (SELECT ed.ed_id AS terkirim_ed_id, MAX(led.log_ed_date) AS date_terkirim FROM entry_data ed INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id WHERE log_ed_flag = '1' GROUP BY ed.ed_nomor_kontrak, led.log_ed_flag ORDER BY ed_id ASC) AS log3 ON log1.upload_ed_id = log3.terkirim_ed_id
		LEFT JOIN (SELECT ed.ed_id AS diterima_ed_id, MAX(led.log_ed_date) AS date_diterima FROM entry_data ed INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id WHERE log_ed_flag = '2' GROUP BY ed.ed_nomor_kontrak, led.log_ed_flag ORDER BY ed_id ASC) AS log4 ON log1.upload_ed_id = log4.diterima_ed_id
		LEFT JOIN (SELECT ed.ed_id AS ahu_ed_id, MAX(led.log_ed_date) AS date_ahu FROM entry_data ed INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id WHERE log_ed_flag = '3' GROUP BY ed.ed_nomor_kontrak, led.log_ed_flag ORDER BY ed_id ASC) AS log5 ON log1.upload_ed_id = log5.ahu_ed_id
		LEFT JOIN (SELECT ed.ed_id AS sertifikat_ed_id, MAX(led.log_ed_date) AS date_sertifikat FROM entry_data ed INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id WHERE log_ed_flag = '4' GROUP BY ed.ed_nomor_kontrak, led.log_ed_flag ORDER BY ed_id ASC) AS log6 ON log1.upload_ed_id = log6.sertifikat_ed_id
		LEFT JOIN (SELECT ed.ed_id AS terdaftar_ed_id, MAX(led.log_ed_date) AS date_terdaftar FROM entry_data ed INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id WHERE log_ed_flag = '4' GROUP BY ed.ed_nomor_kontrak, led.log_ed_flag ORDER BY ed_id ASC) AS log7 ON log1.upload_ed_id = log7.terdaftar_ed_id 
		LEFT JOIN (SELECT ed.ed_id AS reject_ed_id, MAX(led.log_ed_date) AS date_reject, led.log_ed_note AS note_reject FROM entry_data ed INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id WHERE log_ed_flag = '9' GROUP BY ed.ed_nomor_kontrak, led.log_ed_flag ORDER BY ed_id ASC) AS log8 ON log1.upload_ed_id = log8.reject_ed_id
		ORDER BY upload_ed_id ASC";
		

		$getData = $this->db->query($query);
		echo $this->db->last_query(); exit();
		// foreach($getData->result() as $key => $value){
		// 	$num++;
		// 	$row++;
		// 	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $num);
		// 	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $value->nama_cabang);
		// 	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $value->ed_nomor_kontrak);
		// 	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $value->nama_konsumen);
		// 	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $value->date_upload);
		// 	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $value->date_kirim);
		// 	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $value->date_terkirim);
		// 	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $value->date_reject);
		// 	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, $value->note_reject);
		// 	$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, $value->date_diterima);
		// 	$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, $value->date_ahu);
		// 	$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, $value->date_sertifikat);
		// 	$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, $value->date_terdaftar);
		// 	$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row, '');
		// }
		// $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// // Write file to the browser
		// $writer->save('php://output');
	}
	
	public function expo(){
		$where = "WHERE 1 = 1";
		$param = $this->input->get();
		if($param['region']) {
			$where .= " AND cabang_region_id = ".$param['region'];
		}
		$query = "SELECT
		@rownum := @rownum + 1 AS nomor,
		cb.cabang_name AS cabang,
		log1.ed_nomor_kontrak AS nomor_kontrak,
		ks.kon_nama AS nama_konsumen,
		log1.date_upload AS tgl_upload,
		log2.date_kirim AS tgl_kirim,
		log3.date_terkirim AS tgl_terkirim,
		log8.date_reject AS tgl_reject,
		log8.note_reject AS catatan_reject,
		log4.date_diterima AS tgl_diterima,
		log5.date_ahu AS tgl_ahu,
		log6.date_sertifikat tgl_sertifikat,
		log7.date_terdaftar AS tgl_terdaftar 
	FROM
		( SELECT @rownum := 0 ) AS rownum,
		(
	SELECT
		ed.ed_id AS upload_ed_id,
		ed_cabang,
		ed_konsumen_id,
		MAX( led.log_ed_date ) AS date_upload,
		ed.ed_nomor_kontrak,
		log_ed_flag 
	FROM
		entry_data ed
		INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id 
	WHERE
		log_ed_flag = '0' 
	GROUP BY
		ed.ed_nomor_kontrak,
		led.log_ed_flag 
		) AS log1
		INNER JOIN konsumen ks ON log1.ed_konsumen_id = kon_id
		INNER JOIN cabang cb ON log1.ed_cabang = cb.cabang_id
		LEFT JOIN (
	SELECT
		ed.ed_id AS kirim_ed_id,
		MAX( led.log_ed_date ) AS date_kirim 
	FROM
		entry_data ed
		INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id 
	WHERE
		log_ed_flag = '8' 
	GROUP BY
		ed.ed_nomor_kontrak,
		led.log_ed_flag 
	ORDER BY
		ed_id ASC 
		) AS log2 ON log1.upload_ed_id = log2.kirim_ed_id
		LEFT JOIN (
	SELECT
		ed.ed_id AS terkirim_ed_id,
		MAX( led.log_ed_date ) AS date_terkirim 
	FROM
		entry_data ed
		INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id 
	WHERE
		log_ed_flag = '1' 
	GROUP BY
		ed.ed_nomor_kontrak,
		led.log_ed_flag 
	ORDER BY
		ed_id ASC 
		) AS log3 ON log1.upload_ed_id = log3.terkirim_ed_id
		LEFT JOIN (
	SELECT
		ed.ed_id AS diterima_ed_id,
		MAX( led.log_ed_date ) AS date_diterima 
	FROM
		entry_data ed
		INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id 
	WHERE
		log_ed_flag = '2' 
	GROUP BY
		ed.ed_nomor_kontrak,
		led.log_ed_flag 
	ORDER BY
		ed_id ASC 
		) AS log4 ON log1.upload_ed_id = log4.diterima_ed_id
		LEFT JOIN (
	SELECT
		ed.ed_id AS ahu_ed_id,
		MAX( led.log_ed_date ) AS date_ahu 
	FROM
		entry_data ed
		INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id 
	WHERE
		log_ed_flag = '3' 
	GROUP BY
		ed.ed_nomor_kontrak,
		led.log_ed_flag 
	ORDER BY
		ed_id ASC 
		) AS log5 ON log1.upload_ed_id = log5.ahu_ed_id
		LEFT JOIN (
	SELECT
		ed.ed_id AS sertifikat_ed_id,
		MAX( led.log_ed_date ) AS date_sertifikat 
	FROM
		entry_data ed
		INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id 
	WHERE
		log_ed_flag = '4' 
	GROUP BY
		ed.ed_nomor_kontrak,
		led.log_ed_flag 
	ORDER BY
		ed_id ASC 
		) AS log6 ON log1.upload_ed_id = log6.sertifikat_ed_id
		LEFT JOIN (
	SELECT
		ed.ed_id AS terdaftar_ed_id,
		MAX( led.log_ed_date ) AS date_terdaftar 
	FROM
		entry_data ed
		INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id 
	WHERE
		log_ed_flag = '5' 
	GROUP BY
		ed.ed_nomor_kontrak,
		led.log_ed_flag 
	ORDER BY
		ed_id ASC 
		) AS log7 ON log1.upload_ed_id = log7.terdaftar_ed_id
		LEFT JOIN (
	SELECT
		ed.ed_id AS reject_ed_id,
		MAX( led.log_ed_date ) AS date_reject,
		led.log_ed_note AS note_reject 
	FROM
		entry_data ed
		INNER JOIN log_entry_data led ON led.log_ed_id = ed.ed_id 
	WHERE
		log_ed_flag = '9' 
	GROUP BY
		ed.ed_nomor_kontrak,
		led.log_ed_flag 
	ORDER BY
		ed_id ASC 
		) AS log8 ON log1.upload_ed_id = log8.reject_ed_id 
	 ".$where."
	ORDER BY
		nomor ASC" ;
		$data = $this->db->query($query);

		$border = (new BorderBuilder())
			       ->setBorderBottom( Border::WIDTH_THICK)
			       ->setBorderTOP( Border::WIDTH_THICK)
			       ->setBorderRIGHT( Border::WIDTH_THICK)
			       ->setBorderLEFT( Border::WIDTH_THICK)
			       ->build();

		$style = (new StyleBuilder())
       ->setFontBold()
           ->setFontSize(8)
           ->setShouldWrapText()
           ->setBorder($border)
           ->setBackgroundColor(Color::YELLOW)
           ->build();

           // style adding border
        $stylecoloumn = (new StyleBuilder())
           ->setFontSize(8)
           ->setShouldWrapText()
           ->setBorder($border)
           ->build();

 		$write = WriterFactory::create(Type::XLSX);
 		$title = ['No', 'Cabang', 'Nomor Kontrak', 'Nama Konsumen', 'Tgl Upload', 'Tgl Siap Kirim',
                  'Tgl Terkirim', 'Tgl Reject', 'Catatan Reject', 'Tgl Diterima', 'Tgl Proses AHU', 'Tgl Proses Sertifikat Fidusia',
                  'Tgl Terdaftar', 'Tgl Terbayar'];
       $write->openToBrowser('export.xlsx');
	   $write->addRowWithStyle($title,$style);
       foreach ($data->result_array() as $key => $value) {

	   $write->addRowWithStyle($value,$stylecoloumn);
		}
	   $write->close();
	}

	public function bypassTerdaftar($id)
	{
		if($id != "") {
			$update = array('ed_flag' => '5');
			$entry['bypass'] = $this->m_global->update('entry_data', $update, [strEncrypt('ed_id', TRUE) => $id]);
			redirect(base_url()."search/");
		} else {
			redirect(base_url()."search/");
		}
	}

	public function bypassTerkirim($id)
	{
		if($id != "") {
			$update = array('ed_flag' => '1');
			$entry['bypass'] = $this->m_global->update('entry_data', $update, [strEncrypt('ed_id', TRUE) => $id]);
			redirect(base_url()."search/");
		} else {
			redirect(base_url()."search/");
		}
	}
}
/* End of file Search.php */
/* Location: ./application/modules/entry_data/controllers/Entry_data.php */
