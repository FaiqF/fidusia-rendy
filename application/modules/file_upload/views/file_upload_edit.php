<form method="post" action="<?php echo $url.'/action_edit/'.$id ?>" id="form_edit">
	<div class="uk-grid">
		<div class="uk-width-medium-1-2">
			<div class="md-card md-card-primary">
				<div class="md-card-toolbar">
					<h3 class="md-card-toolbar-heading-text">Upload File Excel</h3>
				</div>
				<div class="md-card-content">
					<div id="alert_error"></div>
					<div class="uk-grid">
						<div class="uk-width-medium-1-1 uk-row-first">
							<div class="uk-grid">
								<div class="uk-width-medium-1-1 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>File Code <span class="required">*</span></label>
											<input name="upload_code" type="text" class="md-input uk-form-width-medium" value="<?php echo $records->upload_code ?>" readonly>
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>File Excel <span class="required">*</span></label>
										</div>
									</div>
								</div>
								<div class="uk-margin-medium-top">
									<div class="uk-grid">
										<div class="uk-width-medium-1-1" style="width:200%">
											<input accept=".xls,.xlsx,.xlsm,.xltm" data-max-file-size="2500K" name="upload_path" type="file" id="uploadExcel" class="uploadFileExcel"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
		</div>
	</div>
	<div class="uk-grid">
		<div class="uk-width-medium-1-1">
			<div class="md-card">
				<div class="md-card-content">
					<div id="alert_error"></div>
					<div class="uk-text-center uk-margin-small-top">
						<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Save</button>
						<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<a href="<?=$url; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){

        $('#form_edit').on('submit', function(e){
			e.preventDefault();

			var $this 	= $(this),
			url 		= $this.attr('action'),
			data 		= $this.serialize();

			var form = $('form')[0]	;
			var formData = new FormData(form);
			var err_res = 0,
			err_msg = [],
			suc_msg = [];

			$.ajax({
				url: url,
				type: 'post',
				dataType:'json',
				data: formData,
				processData: false,
				contentType: false,
				// async: false,
				success : function(res) {
					if (res.sts == 5) {
						$('#alert').html(res.alert)
						App.scrollTop();
					}
					if (res.sts == 0) {
						App.notif('error', res.msg, 'error');
					} else if (res.sts == 1) {
						App.notif('Success', res.msg, 'success');
						$('.reload').trigger('click');
					}
				},
				error 	: function(res) {
					App.notif('Error', res.msg , 'error');
				}
			});
		});

        $('.uploadFileExcel').dropify({
			messages: {
				'default': 'File Upload',
				'replace': '',
				'remove':  'Batal',
				'error': 'File hanya format PDF dan maksimal 2.5MB !'
			}
		});

		fileUpload()

	});

	function fileUpload () {
		$('.uploadFile').dropify({
			messages: {
				'default': 'File Upload',
				'replace': '',
				'remove':  'Batal',
				'error': 'File hanya format PDF dan maksimal 2.5MB !'
			}
		});
	}

	function add_form() {
		var count	= $('.upload-form').length;
		var param	= $('.upload-form').length + 1;
		var content = '<div class="uk-grid upload-form repeat-'+count+'">' +
							'<div class="uk-width-3-5">' +
								'<div class="uk-grid">' +
									'<div class="uk-width-medium-1-1 uk-row-first">' +
										'<div class="uk-form-row">' +
											'<div class="md-input-wrapper md-input-filled">' +
												'<label>File Upload <span class="required">*</span></label>' +
											'</div>' +
										'</div>' +
									'</div>' +
									'<div class="uk-margin-medium-top">' +
										'<div class="uk-grid">' +
											'<div class="uk-width-medium-1-1" style="width:200%">' +
												'<input accept=".pdf,.tif" data-max-file-size="2500K" name="upload_file[]" type="file" class="uploadFile"/>' +
											'</div>' +
										'</div>' +
									'</div>' +
								'</div>' +
							'</div>' +
							'<div class="uk-width-medium-1-5">' +
								'<div class="uk-vertical-align uk-height-1-1">' +
									'<div class="uk-vertical-align-middle">' +
										'<a class="btnSection"><i class="material-icons icon-repeat md-36">add_box</i></a>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>';
		$('.generate').append(content);

		//change icon
		$('.icon-repeat').text('delete').parent().removeAttr('class').attr('onclick', 'del_form('+count+')');
		$('.icon-repeat:last').text('add_box').parent().attr('onclick', 'add_form('+count+')');

		fileUpload();

	}

	function del_form(a) {
		$('.repeat-'+a).remove();
		console.log('hapus');
	}


</script>