<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-content">
		<div class="md-card">
			<div class="md-card-toolbar">
				<div class="md-card-toolbar-actions">
					<i class="md-icon material-icons md-card-toggle">&#xE316;</i>
				</div>
				<h3 class="md-card-toolbar-heading-text">
					Filter
				</h3>
			</div>
			<div class="md-card-content filter">
				<div class="uk-grid">
					<div class="uk-width-medium-1-3 uk-row-first">
						<div class="md-input-wrapper">
							<label>Kode</label>
							<input name="upload_code" type="text" class="md-input form-filter">
							<span class="md-input-bar"></span>
						</div>
					</div>
				</div>
				<div class="uk-grid uk-margin-medium-top uk-text-center">
					<div class="uk-width-1-1">
						<button type="button" class="filter-submit md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Cari</button>
						<button type="button" class="filter-reset md-btn md-btn-small md-btn-danger md-btn-wave-light waves-effect waves-button waves-light">Reset</button>
					</div>
				</div>
			</div>
		</div>
		<table id="datatable_ajax" class="uk-table" cellspacing="0" width="100%" fixedColumns="true" leftColumns="1">
			<thead>
				<tr>
					<th width="40">No.</th>
					<th>Kode</th>
					<th>Status</th>
					<th>Upload At</th>
					<th>File</th>
					<th width="152">Aksi</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</div>

<div <?php echo (!in_array(user_data()->user_level, ['1', '2', '5','4','3','8', '10'])) ? 'style="display : none;"' : ''; ?> class="md-fab-wrapper">
	<a class="md-fab md-fab-accent ajaxify" href="<?=$url; ?>/show_add">
		<i class="material-icons">&#xE145;</i>
	</a>
</div>

<script>
	$(document).ready(function(){
		var url 	= "<?=$url; ?>/select/",
			header  = [
						{ "className": "text-center" },
						{ "className": "text-left" },
						{ "className": "text-left" },
						{ "className": "text-left" },
						{ "className": "text-left" },
                        { "className": "text-center" },
					  ],
			order 	= [
						['1', 'asc'],
						['2', 'asc'],
						['3', 'asc'],
						['4', 'asc']
					  ],
			sort 	= [-1, 0];

		App.setDatatable(url, header, order, sort); //paling akhir tambahkan sendiri jika tabel lebih dari satu

		App.datepicker();
	});

	function some(element, event) {
		var url = "<?php echo base_url('file_upload/action_approve')?>";
		var id = $(element).attr('data-id');
		swal({
		title: "Approve data?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Yes',
	}).then(
		function(result){
			$.ajax({
				method:"POST",
				url:url,
				data:{id:id},
				success:function(response) {
					var response = JSON.parse(response);
					var status = response.status;
					var message = response.message;
					if(status == "0") {
						swal("Error", message, "error");
						location.reload();
						return;
					}

					if(status == "1") {
						swal("Success", message, "success");
						location.reload();
						return;
					}
				}
			})

		}, function(dismiss) {
			return false;
		}
	);
		event.preventDefault();
	}
</script>
