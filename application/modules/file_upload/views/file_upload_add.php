<form method="post" action="<?=$url; ?>/action_add" id="form_add">
	<div class="uk-grid">
		<div class="uk-width-medium-1-2">
			<div class="md-card md-card-primary">
				<div class="md-card-toolbar">
					<h3 class="md-card-toolbar-heading-text">Upload File Excel</h3>
				</div>
				<div class="md-card-content">
					<div id="alert_error"></div>
					<div class="uk-grid">
						<div class="uk-width-medium-1-1 uk-row-first">
							<div class="uk-grid">
								<div class="uk-width-medium-1-1 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>File Code <span class="required">*</span></label>
											<input name="upload_code" type="text" class="md-input uk-form-width-medium" value="<?php echo 'FU'.date('Ymdhis') ?>" readonly>
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>File Excel <span class="required">*</span></label>
										</div>
									</div>
								</div>
								<div class="uk-margin-medium-top">
									<div class="uk-grid">
										<div class="uk-width-medium-1-1" style="width:200%">
											<input accept=".xls,.xlsx,.xlsm,.xltm" name="upload_path" type="file" id="uploadExcel" class="uploadFile"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
		</div>
		
		<!-- <div class="uk-width-medium-1-2">
			<div class="md-card md-card-success">
				<div class="md-card-toolbar">
					<h3 class="md-card-toolbar-heading-text">Upload File PDF / Gambar</h3>
					<a class="btnSection uk-align-right" onclick="add_form()" style="margin-top:5px;"><i class="material-icons icon-repeat md-36">add_box</i></a>
				</div>
				<div class="md-card-content">
					<div id="alert_error"></div>
						<div class="uk-grid">
							<div class="uk-width-medium-1-1 generate">

							</div>
						</div>
				</div>
			</div>
		</div> -->
	</div>
	<div class="uk-grid">
		<div class="uk-width-medium-1-1">
			<div class="md-card">
				<div class="md-card-content">
					<div id="alert_error"></div>
					<div class="uk-text-center uk-margin-small-top">
						<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Save</button>
						<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<a href="<?php echo $url; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){

		$('#form_add').on('submit', function(e){
			e.preventDefault();

			var $this 	= $(this),
			url 		= $this.attr('action'),
			data 		= $this.serialize();

			var form = $('form')[0]	;
			var formData = new FormData(form);
			var err_res = 0,
			err_msg = [],
			suc_msg = [];

			$.ajax({
				url: url,
				type: 'post',
				dataType:'json',
				data: formData,
				processData: false,
				contentType: false,
				async: false,
				success : function(res) {
					if (res.sts == 5) {
						$('#alert').html(res.alert)
						App.scrollTop();
					}
					if (res.sts == 0) {
						App.notif('error', res.msg, 'error');
					}
					else if (res.inp == 2) {
						App.notif('Success', res.msg, 'success');
						$('.reload').trigger('click');
					}
					if (res.sts == 3 && res.inp == 2) {
						suc_res = '';
						var chk = 0;
						$.each(res, function( key, val ) {
							if(val['sts'] == 1) {
								suc_res = 1;
								suc_msg.push(val['msg']);
							} else {
								if(typeof val['msg'] !== "undefined") chk = 1;
								err_msg.push(val['msg']);
							}
						});
						if(suc_res == 1){
							App.notif('Success', suc_msg.join('<br>') , 'success');
							if(chk === 1){
								App.notif('Error', err_msg.join('<br>'), 'error');
							}
							$('.reload').trigger('click');
						}else{
							App.notif('Success', suc_msg.join('<br>') , 'success');
							if(chk === 1){
								App.notif('Error', err_msg.join('<br>'), 'error');
							}
							$('.reload').trigger('click');
						}
					}
				},
				error 	: function(res) {
					App.notif('Error', res.msg , 'error');
				}
			});
		});

		fileUpload();

	});

	function fileUpload () {
		$('.uploadFile').dropify({
			messages: {
				'default': 'File Upload',
				'replace': '',
				'remove':  'Batal',
				'error': 'File hanya format PDF dan maksimal 2.5MB !'
			}
		});
	}


	function add_form() {
		var count	= $('.upload-form').length;
		var param	= $('.upload-form').length + 1;
		var content = '<div class="uk-grid upload-form repeat">' +
							'<div class="uk-width-3-5">' +
								'<div class="uk-grid">' +
									'<div class="uk-width-medium-1-1 uk-row-first">' +
										'<div class="uk-form-row">' +
											'<div class="md-input-wrapper md-input-filled">' +
												'<label>File Upload <span class="required">*</span></label>' +
											'</div>' +
										'</div>' +
									'</div>' +
									'<div class="uk-margin-medium-top">' +
										'<div class="uk-grid">' +
											'<div class="uk-width-medium-1-1" style="width:200%">' +
												'<input accept=".pdf, .tiff, .jpeg, .jpg, .png" data-max-file-size="2500K" name="upload_file[]" type="file" class="uploadFile"/>' +
											'</div>' +
										'</div>' +
									'</div>' +
								'</div>' +
							'</div>' +
							'<div class="uk-width-medium-1-5">' +
								'<div class="uk-vertical-align uk-height-1-1">' +
									'<div class="uk-vertical-align-middle">' +
										'<a class="btnSection"><i class="material-icons icon-delete md-36" onclick="delete_form()">delete</i></a>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>';
		$('.generate').append(content);

		fileUpload();

	}

	function delete_form() {
		$(this).closest('.repeat').remove();
		console.log($(this));

	}

</script>