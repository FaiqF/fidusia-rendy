<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File_upload extends MX_Controller 
{

    private $table_db   = 'file_upload';
    private $table_db2  = 'data_upload';
    private $url        = 'file_upload';
    private $prefix     = 'upload_';

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->helper('download');

    }


	public function index()
	{
        $data['name'] 		= "File Upload";
		$data['url'] 		= base_url() . $this->url;
		$data['plugin']		= ['datatables', 'datatables_fixcolumns','kendo_ui'];
        $this->template->display($this->url, $data);
    }

    public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 				= [
            ['users', 'users.user_id = file_upload.created_by', 'left']
        ];
		$where  			= null;
		$where_e 			= null;
        $where = [];
        $roleNotaris = '1';
        $levelCabangId = '10';
        if( user_data()->user_level == $levelCabangId ) {
            $where['upload_cabang_id'] = user_data()->cabang_id;
        }

        if( user_data()->user_role == $roleNotaris ) {
            $where['upload_flag'] = '2';
        }
		$search = [
			'upload_code'   => $this->prefix.'code',
			// 'path'   => $this->prefix.'path'
		];

		if (@$post['action'] == 'filter')
		{
			//$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = $where_e." AND DATE(updated_at) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
					}else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
        }
        
		$keys 		= array_keys($search);
		@$order 	= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

        $select		= '*';
        $count 		= $this->m_global->count_data_all($this->table_db, $join, $where, $where_e );

		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();

        $multipleOrder = [
            ['upload_flag', 'ASC'],
            ['created_at', 'DESC']

        ];
        $data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, $where_e, null, $start, $length, null, null, $multipleOrder );

		$i = 1 + $start;

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
                $rows->upload_code,
                data_flag_status($rows->upload_flag),
                date('d-m-Y h:i', strtotime($rows->created_at)),
                $this->_fileButton($rows->upload_id),
                $this->_button($rows->upload_id, $rows->upload_path, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
        $records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

    public function show_add()
    {
        // pre(user_data()); exit;  
        $data['name']   = 'Entri Data';
        $data['url']    = base_url().$this->url;
        $data['plugin'] = ['kendo_ui','dropify'];
        $this->template->display($this->url.'_add', $data);
    }

    public function action_add()
    {
        $post = $this->input->post();

        // start file upload excel
        if( $_FILES['upload_path']['size'] == 0 ) {
            $result['msg'] = 'File tidak boleh kosong';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $filePathExcel      = $_FILES['upload_path']['name'];
        $extExcel           = pathinfo($filePathExcel, PATHINFO_EXTENSION);
        $allowedFileExcel   = [
            'xls', 'xlsx', 'xlsm', 'xltm'
        ];

            // validation if file not xls or xlsx
        if ( !in_array(@strtolower($extExcel), $allowedFileExcel) ) {
            $result['msg'] = 'File harus bertipe xls / xlsx';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $tempNameExcel  = $_FILES['upload_path']['tmp_name'];
        $dirUploadExcel = FCPATH."/assets/uploads/file_upload/";
        $fileNameExcel  = date('Y-m-d').'_'.user_data()->user_nik.'_'.rand().'.'.$extExcel;

        // end file upload excel

        $doUploadExcel  = move_uploaded_file($tempNameExcel, $dirUploadExcel.$fileNameExcel);

        if( user_data()->user_level == '10' ) {
            $cabangId = user_data()->cabang_id;
        } else {
            $cabangId = '1';
        }

        $data_ex['upload_cabang_id']  = $cabangId;
        $data_ex['upload_flag']       = '1';
        $data_ex['upload_code']       = $post['upload_code'];
        $data_ex['upload_path']       = $fileNameExcel;
        $data_ex['created_at']        = date('Y-m-d H:i:s');
        $data_ex['created_by']        = user_data()->user_id;
        $query_ex                     = $this->m_global->insert($this->table_db, $data_ex);

        if ( !$doUploadExcel){
            $result['msg'] = 'Gagal mengupload file';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        if(!$query_ex && !$query_din) {
            $result['msg'] = 'Data gagal ditambahkan !';
            $result['sts'] = 0;
            echo json_encode($result); exit;
        }

        $result['msg'] = 'Data berhasil ditambahkan !';
        $result['inp'] = 2;

        echo json_encode($result); exit();
    }

    public function show_edit($id)
    {
        $data['name']     = 'Edit data';
        $data['url']      = base_url().$this->url;
        $data['plugin']   = ['kendo_ui','dropify'];
        $data['id']       = $id;
        $data['records']  = $this->m_global->get_data_all($this->table_db, null, [strEncrypt($this->prefix.'id', true) => $id])[0];

        $this->template->display($this->url.'_edit', $data);
    }

    public function action_edit($id) 
    {

        $post = $this->input->post();

        // pre($post, 1);

        if( $_FILES['upload_path']['size'] == 0 ) {
            $result['msg'] = 'File tidak boleh kosong';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

		$filePath    = $_FILES['upload_path']['name'];
        $ext         = pathinfo($filePath, PATHINFO_EXTENSION);
        $allowedFile = [
            'xls', 'xlsx', 'xlsm', 'xltm'
        ];

        // validation if file not xls or xlsx
        if ( !in_array(@strtolower($ext), $allowedFile) ) {
            $result['msg'] = 'File harus bertipe xls / xlsx';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $tempName = $_FILES['upload_path']['tmp_name'];
        $dirUpload = FCPATH."/assets/uploads/file_upload/";
        $fileName = date('Y-m-d').'_'.user_data()->user_nik.'_'.rand().'.'.$ext;

        // upload the file
        $doUpload = move_uploaded_file($tempName, $dirUpload.$fileName);

        if ( !$doUpload ){
            $result['msg'] = 'Gagal mengupload file';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $getDataFile = $this->m_global->get_data_all('file_upload', null , [strEncrypt($this->prefix.'id', true) => $id])[0];
        // pre($getDataFile); exit();
        $deleteFile  = unlink(FCPATH . 'assets/uploads/file_upload/' . $getDataFile->upload_path);
        $updateFile  = [
            'upload_path' => $fileName
        ];
        $updateFilePath = $this->m_global->update('file_upload', $updateFile, [strEncrypt($this->prefix.'id', true) => $id]);

        $result['msg'] = 'Data berhasil diubah !';
        $result['sts'] = 1;

        echo json_encode($result); exit;
    }

    public function show_approve($id)
    {
        $data['name']     = 'Edit data';
        $data['url']      = base_url().$this->url;
        $data['plugin']   = ['kendo_ui','dropify'];
        $data['id']       = $id;
        $data['records']  = $this->m_global->get_data_all($this->table_db, null, [strEncrypt($this->prefix.'id', true) => $id])[0];

        $this->template->display($this->url.'_approve', $data);
    }

    public function action_approve()
    {
        $id = $this->input->post('id');
        $doUpdate = $this->m_global->update('file_upload', ['upload_flag' => '2'], [strEncrypt($this->prefix.'id', true) => $id]);

        if( !$doUpdate ) {
            $response['status'] = 0;
            $response['message'] = 'Failed approve data';

            echo json_encode( $response ); exit;
        }

        $response['status'] = 1;
        $response['message'] = 'Success approve data';

        echo json_encode( $response ); exit;
    }

    public function action_delete($id)
    {
        $get_data = $this->m_global->get_data_all($this->table_db, null, [strEncrypt($this->prefix.'id', true) => $id])[0];
        $cekData  = $this->m_global->get_data_all($this->table_db2, null, [strEncrypt($this->prefix.'file_id', true) => $id]);

        $path_ex  = FCPATH . 'assets/uploads/file_upload/'.$get_data->upload_path;
        $path_din = FCPATH . 'assets/uploads/data_konsumen/'.$get_data->upload_code.'/';
        unlink($path_ex);

        if (count($cekData) > 0) {
            foreach ($cekData as $key => $value) {
                unlink($path_din.$value->upload_path);
            }
            chmod($path_din, 0777);
            rmdir($path_din);
        }

        $query_ex   = $this->m_global->delete($this->table_db, [strEncrypt($this->prefix.'id', true) => $id]);
        $query_din  = $this->m_global->delete($this->table_db2, [strEncrypt($this->prefix.'file_id', true) => $id]);

        if($query_ex && $query_din) {
            $result['msg'] = 'Data berhasil dihapus !';
            $result['status'] = 1;
        } else {
            $result['msg'] = 'Data gagal dihapus !';
            $result['status'] = 0;
        }

        echo json_encode($result);
    }

    public function action_download($name)
    {
        $filepath = FCPATH . 'assets/uploads/file_upload/' . $name;

        ob_clean();
        // Process download
        if(file_exists($filepath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            flush(); // Flush system output buffer
            readfile($filepath);
            exit;
        }
    }

    public function _button($id, $path, $show = false)
	{
		$prev = $id;
        $id = strEncrypt($id);
        $preview    = '<div id="approve_button" data-uk-tooltip title="Preview" class="md-btn md-btn-primary md-btn-mini md-btn-wave-light md-btn-icon waves-effect waves-button waves-light" onclick="return some(this, event);" data-id="'.$id.'"><i class="uk-icon-map"></i></div>';
        $edit 	    = '<a data-uk-tooltip title="Edit" class="ajaxify md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="'.base_url('file_upload/show_edit').'/'.$id.'"><i class="uk-icon-pencil uk-icon-small"></i></a>';
		$delete     = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(2, this, event);" href="'.base_url($this->url.'/action_delete/'.$id).'"><i class="uk-icon-trash uk-icon-small"></i></a>';

        $getData = $this->m_global->get_data_all('file_upload', null, [strEncrypt($this->prefix.'id', true) => $id]);
        $flag    = $getData[0]->upload_flag;
        $prosesFinance = [
            '1'
        ];

        // pre(user_data()); exit;
        $userRole = user_data()->user_role;
        $authorizedRoleApprove = [
            '40',
            
        ];

        if( in_array($flag,$prosesFinance) ) {

            if(in_array($userRole, $authorizedRoleApprove)) {
                return $preview;
            }

            $button     = $edit . $delete;
            return $button;
        }

    }
    
    public function _fileButton( $id )
    {
        $id = strEncrypt($id);

        $data = $this->m_global->get_data_all('file_upload', null, [strEncrypt($this->prefix.'id', true) => $id]);
        $fileKonsumen 	= '<a href="'.base_url('file_upload/action_download/'.$data[0]->upload_path).'" target="_blank"><span class="uk-badge uk-badge-success">Excel</span></a> ';
        
        $button = $fileKonsumen;
        return $button;
    }
}

/* End of file File_upload.php */
/* Location: ./application/modules/file_upload/controllers/File_upload.php */