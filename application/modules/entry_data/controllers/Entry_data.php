<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entry_data extends MX_Controller {

	private $table_db       = 'entry_data'; //nama table
	private $table_prefix   = 'ed_'; //nama awal pada field
	private $prefix 		= 'entry_data';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index($sstatus = false)
	{
		$data['name'] 		= "Entry Data";
		$data['url'] 		= base_url().$this->prefix;
		$data['plugin']		= ['datatables', 'datatables_fixcolumns','kendo_ui'];

		$data['show_data'] 	= (!in_array(user_data()->user_level, ['0','1'])) ? 'style="display: none;"' : '';
		//$arrlamp = $this->m_global->get_data_all('referensi', null, [strEncrypt('lamp_ed_id', TRUE) => $id]);
		/*$data['export']		= [
								['name' => 'Export Excel', 'link' => base_url('entry_data/export_excel')],
							  ];*/
		$data['status'] = $sstatus;
		$this->template->display('index', $data);
	}

	public function select($sstatus = false)
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 				= null;
		$where  			= null;
		$where_e 			= null;
		$where = [];
		$where_e 			= "ed_flag IN('0', '9', '8')";
		if($sstatus !== false){
			$where['ed_flag'] = $sstatus;
		}
		
		if(in_array(user_data()->role_name, ['CUSTODY STAFF', 'BACK OFFICE 2 SPV', 'BACK OFFICE SPV', 'BOH'])){
			$where['ed_cabang'] = user_data()->cabang_id;
		}
		// if ((!in_array(user_data()->user_level, ['1', '5']))) {
		// 	$where['ed_cabang'] = user_data()->cabang_id;
		// } else {
		// 	$where 	= null;
		// }
		
		$search 	= [
			"kontrak" 					=> $this->table_prefix.'nomor_kontrak',
			"cabang_name" 				=> 'cabang_name',
			"flag" 						=> $this->table_prefix.'flag',
			"pem_nama" 					=> 'pem_nama',
			"nilai_penjaminan" 	=> 'pemb_nilai_penjaminan',
			"nilai_benda_fidusia" 	=> 'pemb_nilai_benda_fidusia',
			"merk" 					=> 'dk_merk',
			"tipe" 					=> 'dk_tipe',
			"tahun_pembuatan" 		=> 'dk_tahun_pembuatan',
			"no_rangka" 				=> 'dk_no_rangka',
			"no_mesin" 				=> 'dk_no_mesin',
			"covernote" 				=> 'dk_covernote',
			"tanggal_jual" 			=> $this->table_prefix.'tanggal_jual'
		];

		if (@$post['action'] == 'filter')
		{
			//$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = $where_e." AND DATE(ed_lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
					} else if($key == 'flag') {
						$where_e = $where_e." AND ".$this->table_prefix.$key." IN($post[$key])";
					} else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			//$where[$this->table_prefix.'status <>'] = '99';
		}
		$keys 		= array_keys($search);
		$order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

		$select		= '*, '.implode(',', $search);
		$count 		= $this->m_global->count_data_all( 'v_'.$this->table_db, $join, $where, $where_e );
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


		$data = $this->m_global->get_data_all('v_'.$this->table_db, $join, $where, $select, $where_e, $order, $start, $length);
		$i = 1 + $start;

		$status = [
			'0' => '<span class="uk-badge uk-badge-warning">InActive</span>',
			'1' => '<span class="uk-badge uk-badge-primary">Active</span>',
			'99' => '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
		];

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
				$rows->ed_nomor_kontrak,
				$rows->cabang_name,
				flag_status($rows->ed_flag),
				
				$rows->pem_nama,
				
				uang($rows->pemb_nilai_penjaminan,null),
				uang($rows->pemb_nilai_benda_fidusia,null),
				$rows->dk_merk,
				$rows->dk_tipe,
				$rows->dk_tahun_pembuatan,
				$rows->dk_no_rangka,
				$rows->dk_no_mesin,
				
				(($rows->dk_covernote != "")?date('d-m-Y', strtotime($rows->dk_covernote)):''),
				date('d-m-Y', strtotime($rows->ed_tanggal_jual)),
				$this->_button($rows->ed_id, $rows->ed_status, $rows->ed_flag, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

	public function _button($id, $status, $flag, $show = false)
	{
		$prev = $id;
		$id = strEncrypt($id);
		$button = '';

		if($show) {
			$c_status = '<a data-uk-tooltip title="Change Status" class="md-btn ' . (($status == '1') ? 'md-btn-success' : (($status == '99') ? 'md-btn-danger' : '')) . ' md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(1, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/'.($status == 1 ? '0' : '1' ) ).'">
						<i class="' . (($status == '1') ? 'uk-icon-check' : (($status == '99') ? 'uk-icon-warning' : 'uk-icon-ban')) . ' uk-icon-small"></i>
					  </a>';
			$edit 	= '<a data-uk-tooltip title="Edit" class="ajaxify md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="'.base_url('entry_data/show_edit').'/'.$id.'"><i class="uk-icon-pencil uk-icon-small"></i></a>';
			$delete = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(2, this, event);" href="'.base_url($this->prefix.'/change_status_by/'.$id.'/99'.($status == 99 ? '/true' : '' )).'"><i class="uk-icon-trash uk-icon-small"></i></a>';
			$preview = '<a data-uk-tooltip title="Preview" class="md-btn md-btn-primary md-btn-mini md-btn-wave-light md-btn-icon waves-effect waves-button waves-light ajaxify" href="'.base_url('entry_data/show_document').'/'.$id.'">
								<i class="uk-icon-map"></i>
							</a>';

			if ($flag > '0' && $flag != '8' && $flag != '9') {
				if (in_array(user_data()->user_level, ['1', '5'])) {
					$button = $preview . $edit . $delete;
				} else {
					if (user_data()->user_level == '2' && $flag == '9') {
						$button = $preview . $edit . $delete;
					} else {
						$button = $preview;
					}
				}
			} else {
				if($flag != '8'){
					if (in_array(user_data()->user_level, ['1'])) {
						$button = $preview . $edit . $delete;
					}else if(in_array(user_data()->user_level, ['4',  '5'])){
						$button = $preview . $edit . $delete;
					}else if (in_array(user_data()->user_level, ['3'])) {
						$button = $preview;
					}else if (in_array(user_data()->user_level, ['2', '8'])) {
						$button = $preview . $edit;
					}
				}else{
					if (in_array(user_data()->user_level, ['1'])) {
						$button = $preview;
					}else if(in_array(user_data()->user_level, ['2',  '5'])){
						$button = $preview . $delete;
					}else if (in_array(user_data()->user_level, ['3'])) {
						$button = $preview;
					}else if (in_array(user_data()->user_level, ['4', '8'])) {
						$button = $preview;
					}
				}
			}
		} else {
			$button = '-';
		}

		return $button;
	}

	public function export_excel(){
		ob_clean();

		$get = $this->input->get();
		
		if(in_array(user_data()->role_name, ['Staff', 'Supervisor'])){
			$where[] = "ed_cabang = '".user_data()->cabang_id."'";
		}
		if(@$get['kontrak'] != ""){
			$where[] = "ed_nomor_kontrak LIKE '%$get[kontrak]%'";
		}
		if(@$get['flag'] != ""){
			$where[] = "ed_flag = $get[flag]";
		}
		if(@$get['status'] != ""){
			$where[] = "ed_status = '$get[status]'";
		}

		if(@$where != "") @$where = " AND ".join(" AND ",  $where);
		$data = $this->db->query("SELECT * FROM v_entry_data WHERE ed_status = '1' ".@$where);

		include('application/libraries/PHPExcel.php');
		// We'll be outputting an excel file
		header('Content-type: application/vnd.ms-excel');

		// It will be called file.xls
		header('Content-Disposition: attachment; filename="entry_data.xls"');

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
		$styleArray = array(
                    'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                  );
		$styleArray1 = array(
                     'borders' => array(
                    'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ),
                     ),
                    );
                    //background
                    $styleArray12 = array(
                    'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array(
                    'rgb' => 'FFEC8B',
                    ),
                    ),
									);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
    	$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->mergeCells('A1:P1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'EXPORT DATA');
		$row = 3;
		$rowawal = $row;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':P'.$row)->applyFromArray($styleArray);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':P'.$row)->applyFromArray($styleArray1);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':P'.$row)->applyFromArray($styleArray12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'No');
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, 'No Order');
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, 'Nama Konsumen');
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, 'No Mesin');
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, 'No Rangka');
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, 'Tipe Kendaraan');
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, 'OTR');
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, 'Pokok');
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, 'Bunga');
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, 'Biaya Notaris');
		$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, 'Status Reason');
		$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, 'Nama Cabang');
		$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, 'No AKTA');
		$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row, 'No Resi');
		$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, 'No Sertifikat');
		$objPHPExcel->getActiveSheet()->SetCellValue('P'.$row, 'Tgl Terima');
		$row++;
		$objPHPExcel->getActiveSheet()->freezePane('A'.$row);
		foreach ($data->result() as $key => $dt) {
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':P'.$row)->applyFromArray($styleArray);
      		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':P'.$row)->applyFromArray($styleArray1);
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, ($row - $rowawal));
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $dt->kon_nama);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $dt->dk_no_mesin);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $dt->dk_no_rangka);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $dt->dk_tipe);
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row, $dt->pemb_no);
			$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, '');
			$objPHPExcel->getActiveSheet()->SetCellValue('P'.$row, '');
			$row++;
		}

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// Write file to the browser
		$writer->save('php://output');
	}

	public function export_pdf($perbank = false){
      $limit = 12;
      $this->load->library('fpdf');
      $panjang = 200;
      $lebar = 200;
      $margin = 10;
			$post = $this->input->post();
			if(@$post['nama_konsumen'] != ""){
				$where[] = "kon_nama LIKE '%$post[nama_konsumen]%'";
			}
			if(@$post['alamat_konsumen'] != ""){
				$where[] = "kon_alamat LIKE '%$post[alamat_konsumen]%'";
			}
			if(@$post['ktp_konsumen'] != ""){
				$where[] = "kon_ktp LIKE '%$post[ktp_konsumen]%'";
			}
			if(@$post['status'] != ""){
				$where[] = "ed_status = '$post[status]'";
			}
			if(@$where != "") @$where = " AND ".join(" AND ",  $where);
			$data = $this->db->query("SELECT * FROM v_entry_data WHERE ed_status = '1' ".@$where);
			$option['jenis_kelamin'] = array('1' => 'Laki-Laki', '2' => 'Perempuan');
			$option['status_pernikahan'] = array('2' => 'Belum Menikah', '0' => 'Suami', '1' => 'Istri');
			$pdf = new FPDF('P', 'mm', 'A4');
      $pdf->AddPage();
      $pdf->SetAutoPageBreak(false, 0);
      $t = $panjang;
      $pdf->SetFont('Arial', 'B', 12);
      $pdf->Cell($t, 7, 'EXPORT DATA ', 0, 0, 'C');
      $pdf->Ln();
      //$pdf->Cell($t - $margin, 7, 'TANGGAL ', 0, 0, 'C');
      $pdf->Ln();
      $pdf->Ln();
      $pdf->SetFont('Arial', '', 7);
      $x = $pdf->GetX();
      $y = $pdf->GetY();
      $mleft = $x;
      $w = 0;
      $l1 = 12;
      $l2 = 35;
      $l3 = 46;
      $l4 = 70;
      $l5 = 15;
			$l6 = 55;
      $h1 = 7;
      $h2 = 7;
      $h3 = 7;



			foreach ($data->result() as $key => $dt) {
				if ($pdf->GetY() > ($lebar - $margin)) {
          $pdf->AddPage();
					$pdf->SetY($pdf->GetY());

												$pdf->SetFont('Arial', 'B', 12);
									      $pdf->Cell($t, 7, 'EXPORT DATA ( ENTRY DATA ) ', 0, 0, 'C');
									      $pdf->Ln();
									      //$pdf->Cell($t - $margin, 7, 'TANGGAL ', 0, 0, 'C');
									      $pdf->Ln();
									      $pdf->Ln();
									      $pdf->SetFont('Arial', '', 7);
												  $y = $pdf->GetY();
				}
				$x = $mleft;
				$yy = $y;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l2, $h1 * 2, 'DATA KONSUMEN', 0, 'L');
				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nama', 0, 'L');
				$pdf->SetXY($x= $w, $y);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->kon_nama, 0, 'L');
				$xx = $l3 + $l4;

				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nomor KTP', 0, 'L');
				$pdf->SetXY($x= $w, $y);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->kon_ktp, 0, 'L');
				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Jenis Kelamin', 0, 'L');
				$pdf->SetXY($x= $w, $y);
	      $pdf->MultiCell($w = $l4, $h1 * 2,	@$option['jenis_kelamin'][$dt->kon_jenis_kelamin], 0, 'L');
				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Status Pernikahan', 0, 'L');
				$pdf->SetXY($x= $w, $y);
	      $pdf->MultiCell($w = $l4, $h1 * 2, @$option['status_pernikahan'][$dt->kon_status_pernikahan], 0, 'L');
				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Alamat Konsumen', 0, 'L');
				$pdf->SetXY($x= $w, $y);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->kon_alamat, 0, 'L');
				$y += $h1;
				$y += $h1;
				$pdf->MultiCell($w = $l2, $h1 * 2, 'DATA PEMBIAYAAN', 0, 'L');
				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nomor', 0, 'L');
				$pdf->SetXY($x= $w, $y);
		      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->pemb_no, 0, 'L');
				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Tanggal', 0, 'L');
				$pdf->SetXY($x= $w, $y);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->pemb_tanggal, 0, 'L');
				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nilai Penjaminan', 0, 'L');
				$pdf->SetXY($x= $w, $y);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->pemb_nilai_penjaminan, 0, 'L');
				$y += $h1;
				$x = $mleft;
				$pdf->SetXY($x, $y);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nilai Benda Fidusia', 0, 'L');
				$pdf->SetXY($x= $w, $y);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->pemb_nilai_benda_fidusia, 0, 'L');


				$chky = $pdf->GetY();


				$xx = $l3 + $l4;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'DATA PEMILIK KENDARAAN', 0, 'L');

				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nama', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->pem_nama, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nomor KTP', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->pem_ktp, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Jenis Kelamin', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, @$option['jenis_kelamin'][$dt->pem_jenis_kelamin], 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Alamat Pemilik', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->pem_alamat, 0, 'L');
				$yy += $h1;
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'DATA KENDARAAN', 0, 'L');

				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Merk', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->dk_merk, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Tipe', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->dk_tipe, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Tahun Pembuatan', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->dk_tahun_pembuatan, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nomor Rangka', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->dk_no_rangka, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nomor Mesin', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->dk_no_mesin, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Cover Note Dealer', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->dk_covernote, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nomor Polisi', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->dk_no_polisi, 0, 'L');
				$yy += $h1;
				$pdf->SetXY($xx, $yy);
	      $pdf->MultiCell($w = $l3, $h1 * 2, 'Nomor BPKB', 0, 'L');
				$pdf->SetXY(($xx + $w), $yy);
	      $pdf->MultiCell($w = $l4, $h1 * 2, $dt->dk_no_bpkb, 0, 'L');

				$chkyy = $pdf->GetY();
				if($chkyy > $chky){
					$y = $chkyy;
				}else{
					$y = $chky;
				}
				$y = $y - 10;
				$pdf->SetY($y);
				$pdf->MultiCell(($l4 * 4), $h1 * 2, '----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------', 0, 'L');
				$y += $h1;

			}




			$y += $h1;
      $x = $mleft;
      $x += $l1 + $l2 + $l3;
			$pdf->Output('Export Entry Data ( ' .date('d-m-Y'). ' )', 'I');
    }

	public function change_status_by( $id, $status, $stat = FALSE )
	{
		$this->db->trans_begin();
		if ( $stat ) {
			$arrlamp = $this->m_global->get_data_all('lampiran', null, [strEncrypt('lamp_ed_id', TRUE) => $id]);
			$arrdata = $this->m_global->get_data_all('entry_data', null, [strEncrypt('ed_id', TRUE) => $id]);
			$result = $this->m_global->delete( 'pembiayaan', ['pemb_id' => $arrdata[0]->ed_pembiayaan_id] );
			$result = $this->m_global->delete( 'data_kendaraan', ['dk_id' => $arrdata[0]->ed_data_kendaraan_id] );
			$result = $this->m_global->delete( 'pemilik', ['pem_id' => $arrdata[0]->ed_pemilik_id] );
			$result = $this->m_global->delete( 'konsumen', ['kon_id' => $arrdata[0]->ed_konsumen_id] );
			$result = $this->m_global->delete( 'entry_data', [strEncrypt('ed_id', true) => $id] );
			$result = $this->m_global->delete( 'log_entry_data', [strEncrypt('log_ed_id', true) => $id] );
			if(@$arrlamp[0]->lamp_file != ""){
				$locations = str_replace(base_url(), '', str_replace('/'.$arrlamp[0]->lamp_nama, '', $arrlamp[0]->lamp_file));
				foreach (glob($locations."/*") as $filename) {
					unlink($filename);
				}
				if(rmdir($locations)){
					$result = $this->m_global->delete( $this->table_db, [strEncrypt($this->table_prefix.'id', true) => $id] );
					$result = $this->m_global->delete( 'lampiran', [strEncrypt('lamp_ed_id', true) => $id] );
				}else{
					echo json_encode( 0 );
					die();
				}
			}
		} else if($status == "99"){
			$arrlamp = $this->m_global->get_data_all('lampiran', null, [strEncrypt('lamp_ed_id', TRUE) => $id]);
			$arrdata = $this->m_global->get_data_all('entry_data', null, [strEncrypt('ed_id', TRUE) => $id]);
			$result = $this->m_global->delete( 'pembiayaan', ['pemb_id' => $arrdata[0]->ed_pembiayaan_id] );
			$result = $this->m_global->delete( 'data_kendaraan', ['dk_id' => $arrdata[0]->ed_data_kendaraan_id] );
			$result = $this->m_global->delete( 'pemilik', ['pem_id' => $arrdata[0]->ed_pemilik_id] );
			$result = $this->m_global->delete( 'konsumen', ['kon_id' => $arrdata[0]->ed_konsumen_id] );
			$result = $this->m_global->delete( 'entry_data', [strEncrypt('ed_id', true) => $id] );
			$result = $this->m_global->delete( 'log_entry_data', [strEncrypt('log_ed_id', true) => $id] );
			if(@$arrlamp[0]->lamp_file != ""){
				$locations = str_replace(base_url(), '', str_replace('/'.$arrlamp[0]->lamp_nama, '', $arrlamp[0]->lamp_file));
				foreach (glob($locations."/*") as $filename) {
					unlink($filename);
				}
				if(rmdir($locations)){
					$result = $this->m_global->delete( $this->table_db, [strEncrypt($this->table_prefix.'id', true) => $id] );
					$result = $this->m_global->delete( 'lampiran', [strEncrypt('lamp_ed_id', true) => $id] );
				}else{
					echo json_encode( 0 );
					die();
				}
			}
		} else {
			$result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [strEncrypt($this->table_prefix.'id', true) => $id]);
		}

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$data['status'] = 0;
		}else{
			$this->db->trans_commit();
			$data['status'] = 1;
		}
		echo json_encode( $data );
	}

	public function show_add()
	{
		$data['name'] 		 = "Entri Data";
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'Entry Data' => base_url('entry_data'), 'Add' => base_url('entry_data/show_add')];
		$data['plugin'] 	 = ['kendo_ui','dropify', 'inputmask'];
		$data['upload']		 = $this->m_global->get_data_all('referensi', null, ['referensi_tipe' => 'UPD1']);
		if(!in_array(user_data()->user_level, ['1', '5','4','3','8'])){
			$data['cabang'] 	 = $this->m_global->get_data_all('cabang', null, ['cabang_status' => '1', 'cabang_id' => user_data()->cabang_id], 'cabang_id, cabang_name');
		}else{
			$data['cabang'] 	 = $this->m_global->get_data_all('cabang', null, ['cabang_status' => '1'], 'cabang_id, cabang_name');
		}
		$data['negara'] 	 = $this->m_global->get_data_all('negara');

		$this->template->display('entry_data_add', $data);
	}
	
	public function show_edit($id)
	{
		$data['name'] 		 = "Entry Data";
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'Entry Data' => base_url('entry_data'), 'Edit' => base_url('entry_data/show_edit').'/'.$id];
		$data['record']		 = $this->m_global->get_data_all('v_entry_data', null, [strEncrypt('ed_id', TRUE) => $id]);
		$data['note']		 = $this->m_global->get_data_all('entry_data a', [['pembiayaan b', 'a.ed_pembiayaan_id = b.pemb_id','inner']], [strEncrypt('a.ed_id',TRUE) =>$id],'b.pemb_note');
		$data['lampiran'] 	 = $this->m_global->get_data_all('lampiran', null, [strEncrypt('lamp_ed_id', TRUE) => $id, 'lamp_tipe' => 'UPD1']);
		$data['file']		 = $this->m_global->get_data_all('lampiran_referensi', null, [strEncrypt('lamp_ed_id', TRUE) => $id, 'lamp_tipe' => 'UPD1']);
		$data['upload']		 = $this->m_global->get_data_all('referensi', null, ['referensi_tipe' => 'UPD1']);
		
		foreach($data['file'] as $a => $b){
			$data['arrfile'][$b->referensi_id] = true;
		}
		
		$data['jml'] 		 = count($data['record']);
		$data['plugin'] 	 = ['kendo_ui','dropify', 'pdfobject' => 'js', 'inputmask'];
		$data['id'] 		 = $id;
		if(!in_array(user_data()->user_level, ['1', '5','4','3','8'])){
			$data['cabang'] 	 = $this->m_global->get_data_all('cabang', null, ['cabang_status' => '1', 'cabang_id' => user_data()->cabang_id], 'cabang_id, cabang_name');
		}else{
			$data['cabang'] 	 = $this->m_global->get_data_all('cabang', null, ['cabang_status' => '1'], 'cabang_id, cabang_name');
		}
		$data['negara'] 	 = $this->m_global->get_data_all('negara');

		if ($data['lampiran']) {
			$pdf = $data['lampiran'][0]->lamp_lokasi_file.'/'.$data['lampiran'][0]->lamp_nama;
			$expArr = explode('/', $data['lampiran'][0]->lamp_lokasi_file);
			$pdf = "assets/uploads/uploads/".$expArr[count($expArr) - 1]."/".$data['lampiran'][0]->lamp_nama;
			$filePDF = file_exists($pdf) ? base_url($pdf) : '';
		} else {
			$filePDF = '';
		}
		
		$data['filePDF'] = $filePDF;
		@$data['noteReject'] = $this->getReject($data['record'][0]->ed_id, $data['record'][0]->ed_nomor_kontrak);
		$this->template->display('entry_data_edit', $data);
	}
	
	public function show_document($id)
	{
		$data['lampiran'] 	 = $this->m_global->get_data_all('lampiran', null, [strEncrypt('lamp_ed_id', TRUE) => $id, 'lamp_tipe' => 'UPD1']);
		$data['name'] 		 = "Config User";
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'Entry Data' => base_url('entry_data'), 'Preview' => base_url('entry_data/show_document').'/'.$id];
		$data['plugin'] 	 = ['kendo_ui','dropify', 'datatables', 'datatables_fixcolumns', 'pdfobject' => 'js', 'inputmask'];
		$data['option']		 = array();
		$data['report']		 = $this->m_global->get_data_all('v_entry_data', [['negara a', 'a.negara_id = kon_negara', 'left'], ['negara b', 'b.negara_id = pem_negara', 'left'], ['cabang c', 'c.cabang_id = ed_cabang', 'left']], [strEncrypt('ed_id', TRUE) => $id], '*, a.negara_name kon_nama_negara, b.negara_name pem_nama_negara');
		$data['file']		 = $this->m_global->get_data_all('lampiran_referensi', [['referensi', 'lamp_tipe = referensi_tipe AND lampiran_referensi.referensi_id = referensi.referensi_id', 'left']], [strEncrypt('lamp_ed_id', TRUE) => $id, 'lamp_tipe' => 'UPD1']);
		$data['id'] 		 = $id;
		
		$data['report'][0]->pemb_nilai_penjaminan 		= uang($data['report'][0]->pemb_nilai_penjaminan,null);
		$data['report'][0]->pemb_nilai_benda_fidusia 	= uang($data['report'][0]->pemb_nilai_benda_fidusia,null);
		$data['option']['jenis_kelamin'] 				= array('1' => 'Laki-Laki', '2' => 'Perempuan');
		$data['option']['status_pernikahan'] 			= array('2' => 'Belum Menikah', '0' => 'Suami', '1' => 'Istri');
		
		foreach($data['file'] as $a => $b){
			$data['arrfile'][$b->referensi_id] = true;
		}

		if ($data['lampiran']) {
			$pdf = $data['lampiran'][0]->lamp_lokasi_file.'/'.$data['lampiran'][0]->lamp_nama;
			$expArr = explode('/', $data['lampiran'][0]->lamp_lokasi_file);
			$pdf = "assets/uploads/".$expArr[count($expArr) - 1]."/".$data['lampiran'][0]->lamp_nama;
			$filePDF = file_exists($pdf) ? base_url($pdf) : '';
		} else {
			$filePDF = '';
		}
		
		$data['filePDF'] = $filePDF;

		$data['noteReject'] = $this->getReject($data['report'][0]->ed_id, $data['report'][0]->ed_nomor_kontrak);

		$this->template->display('entry_data_document', $data);
	}

	public function log($id)
	{

	  $post 		= $this->input->post();
	  $length 	= intval($post['length']);
	  $start  	= intval($post['start']);
	  $sEcho		= intval($post['draw']);
	  $join 				= [['roles', 'log_ed_user_role = role_id', 'left'], ['referensi', "'FLAG' = referensi_tipe AND log_ed_flag = referensi.referensi_id", 'left']];
	  $where['log_ed_id'] 	= $id;
	  $where_e 			= null;

	  $search 	= [
	          "log_ed_user_role" 		=> 'log_ed_user_role',
	          "log_ed_action" 		=> 'log_ed_action',
	          "log_ed_flag" 			=> 'log_ed_flag',
	          "log_ed_date" 				=> 'log_ed_date'
	          ];

	  if (@$post['action'] == 'filter')
	  {
	    $where = [];
	    foreach ( $search as $key => $value )
	    {
	      if ( $post[$key] != '' )
	      {
	        if ( $key == 'lastupdate' )
	        {
	          $tmp = explode(' ', $post[$key]);
	          $where_e = "DATE(ed_lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

	        } else {
	          $where[$value.' LIKE '] = '%'.$post[$key].'%';
	        }
	      }
	    }
	  } else {
	    $where['log_ed_id ='] = $id;
	  }

	  $keys 		= array_keys($search);
	  $order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

	  $select		= 'log_ed_id, log_ed_action, role_name, referensi_nama, log_ed_date, '.implode(',', $search);
	  $count 		= $this->m_global->count_data_all('log_entry_data', $join, $where, $where_e );
	  $length 	= $length < 0 ? $count : $length;
	  $end 		= $start + $length;
	  $end 		= $end > $count ? $count : $end;

	  $result['iTotalRecords'] 			= $count;
	  $result['iTotalDisplayRecords'] 	= $length;

	  $records 			= array();
	  $records["data"]	= array();


	  $data = $this->m_global->get_data_all('log_entry_data', $join, $where, $select, $where_e, $order, $start, $length);

	  $i = 1 + $start;

	  $status = [
	        '0' => '<span class="uk-badge uk-badge-warning">InActive</span>',
	        '1' => '<span class="uk-badge uk-badge-primary">Active</span>',
	        '99' => '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
	        ];

	  foreach ( $data as $rows ) {
	    $records["data"][] = array(
	      $i,
	      $rows->log_ed_action,
	      $rows->role_name,
	      $rows->referensi_nama,
	      $rows->log_ed_date
	    );
	    $i++;
	  }

	  $records["draw"]            = $sEcho;
	  $records["recordsTotal"]    = $count;
	  $records["recordsFiltered"] = $count;

	  echo json_encode($records);
	}

	public function view_lampiran($ed_id){
		$file    	= $this->m_global->get_data_all('lampiran', null, [strEncrypt('lamp_ed_id', TRUE) => $ed_id], 'lamp_lokasi_file')[0]->lamp_lokasi_file;
		$nama_file  = $this->m_global->get_data_all('lampiran', null, [strEncrypt('lamp_ed_id', TRUE) => $ed_id], 'lamp_nama')[0]->lamp_nama;
		$file = $file."/".$nama_file;

		$file = str_replace('system/', '', str_replace('\\', '/',BASEPATH)).str_replace('./', '',str_replace(base_url(), '', $file));
		$imginfo = getimagesize($file);
		header("Content-type: $imginfo[mime]");
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
		ob_clean();
        flush();
        readfile($file);
        exit;
	}

	public function action_add()
	{	
		$result = [];
		$post 	= $this->input->post();

		// ================================================ Konsumen ================================================ //
		$this->form_validation->set_rules('nama_konsumen', 			'Nama Konsumen', 			'trim|required');

		// ================================================ Pemilik ================================================ //
		$this->form_validation->set_rules('nama_pemilik', 		'Nama Pemilik', 		'trim|required');
		
		// ================================================ Pembiayaan ================================================ //
		//if (in_array(user_data()->user_level, ['1', '5'])) {
			$this->form_validation->set_rules('cabang', 			'Cabang', 				'trim|required');
		//}

		$this->form_validation->set_rules('nilai_penjaminan', 	'Nilai Penjaminan', 	'trim|required');
		$this->form_validation->set_rules('nilai_benda_fidusia','Nilai Benda Fidusia', 	'trim|required');
		$this->form_validation->set_rules('nomor_kontrak', 		'Nomor Kontrak',		'trim|required|is_unique[entry_data.ed_nomor_kontrak]');
		
		// ================================================ Kendaraan ================================================ //
		$this->form_validation->set_rules('merk_kendaraan', 	'Merk Kendaraan', 		'trim|required');
		$this->form_validation->set_rules('tipe_kendaraan', 	'Tipe Kendaraan', 		'trim|required');
		$this->form_validation->set_rules('nomor_rangka', 		'Nomor Rangka', 		'trim|required');
		$this->form_validation->set_rules('nomor_mesin', 		'Nomor Mesin', 			'trim|required');
		$this->form_validation->set_rules('tahun_pembuatan', 	'Tahun Pembuatan', 		'trim|required');
		$this->form_validation->set_rules('tgl_jual', 			'Tanggal Jual', 		'trim|required');
		//$this->form_validation->set_rules('cover_note_dealer', 	'Covernote', 			'trim');

		if ($this->form_validation->run() == TRUE){
		// ================================================ Konsumen ================================================ //
			$konsumen 	= [
				'kon_nama' 				=> $post['nama_konsumen'],
			];
		
		// ================================================ Pemilik ================================================ //
			$pemilik 	= [
				'pem_nama'			=> $post['nama_pemilik'],
			];
		
		// ================================================ Pembiayaan ================================================ //
			$pembiayaan 	= [
				'pemb_nilai_penjaminan' 	=> str_replace(array('Rp.', ',', ' '), '', $post['nilai_penjaminan']),
				'pemb_nilai_benda_fidusia'	=> str_replace(array('Rp.', ',', ' '), '', $post['nilai_benda_fidusia']),
			];
		
		// ================================================ Kendaraan ================================================ //
			$kendaraan 	= [
				'dk_merk' 				=> $post['merk_kendaraan'],
				'dk_tipe' 				=> $post['tipe_kendaraan'],
				'dk_no_rangka' 			=> $post['nomor_rangka'],
				'dk_no_mesin' 			=> $post['nomor_mesin'],
				'dk_tahun_pembuatan' 	=> $post['tahun_pembuatan'],
				'dk_covernote' 			=> (($post['cover_note_dealer'] != "")?date('Y-m-d', strtotime($post['cover_note_dealer'])):''),
				'dk_no_bpkb' 			=> $post['nomor_bpkb'],
			];

		// ================================================ Entry Data ================================================ //
			$entry_data['ed_cabang'] 		= (!in_array(user_data()->user_level, ['1', '5','4','3','8'])) ? user_data()->cabang_id : $post['cabang'];
			$entry_data['ed_nomor_kontrak'] = $post['nomor_kontrak'];
			$entry_data['ed_tanggal_jual'] 	= date('Y-m-d', strtotime($post['tgl_jual']));
			$entry_data['ed_created_by'] 	= user_data()->user_id;
			$entry_data['ed_created_date'] 	= date("Y-m-d H:i:s");
			$entry_data['ed_lastupdate'] 	= date("Y-m-d H:i:s");

			$this->db->trans_begin();
			
			$this->m_global->insert('konsumen', $konsumen);
			$entry_data['ed_konsumen_id'] = $this->db->insert_id();
			
			$this->m_global->insert('pemilik', $pemilik);
			$entry_data['ed_pemilik_id'] = $this->db->insert_id();
			
			$this->m_global->insert('pembiayaan', $pembiayaan);
			$entry_data['ed_pembiayaan_id'] = $this->db->insert_id();
			
			$this->m_global->insert('data_kendaraan', $kendaraan);
			$entry_data['ed_data_kendaraan_id'] = $this->db->insert_id();

			$entry['entry'] = $this->m_global->insert('entry_data', $entry_data);
			$entry['ed_id'] = $this->db->insert_id();

			if ($entry['entry']['status'] == 1) {
				$result['msg'] = 'Data Berhasil Dimasukkan';
				$result['inp'] = '2';
			}
			if ($entry['entry']) {
				$entry['nomor_kontrak'] = $post['nomor_kontrak'];

				$entry['jenis'] = 'UPD1';
				$result['uploadmsg'] = $this->do_upload($entry);

				$result['sts'] = '3';
			}

			$entry_data = array('log_ed_id' => $entry['ed_id'], 'log_ed_nomor_kontrak' => $post['nomor_kontrak'], 'log_ed_note' => '', 'log_ed_action' => 'Simpan', 'log_ed_user_id' => user_data()->user_id, 'log_ed_user_role' => user_data()->user_role, 'log_ed_flag' => '0');
			$entry['entry'] = $this->m_global->insert('log_entry_data', $entry_data);
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
			}
			else{
				$this->db->trans_commit();
			}
		}else {
			$valid = validation_errors();
			$result['alert'] = '<div class="uk-alert uk-alert-danger" data-uk-alert="">
				<a href="#" class="uk-alert-close uk-close"></a>
				'.$valid.'
			</div>';
			$result['sts'] = '5';
		}
		echo json_encode($result);
	}

	public function action_edit($id){
		$result = [];
		$post 	= $this->input->post();

		// ================================================ Konsumen ================================================ //
		$this->form_validation->set_rules('nama_konsumen', 			'Nama Konsumen', 			'trim|required');

		// ================================================ Pemilik ================================================ //
		$this->form_validation->set_rules('nama_pemilik', 		'Nama Pemilik', 		'trim|required');
		
		// ================================================ Pembiayaan ================================================ //
		//if (in_array(user_data()->user_level, ['1', '5'])) {
			$this->form_validation->set_rules('cabang', 			'Cabang', 				'trim|required');
		//}

		$this->form_validation->set_rules('nilai_penjaminan', 	'Nilai Penjaminan', 	'trim|required');
		$this->form_validation->set_rules('nilai_benda_fidusia','Nilai Benda Fidusia', 	'trim|required');
		// $this->form_validation->set_rules('nomor_kontrak', 		'Nomor Kontrak',		'trim|required|is_unique[entry_data.ed_nomor_kontrak]');
		
		// ================================================ Kendaraan ================================================ //
		$this->form_validation->set_rules('merk_kendaraan', 	'Merk Kendaraan', 		'trim|required');
		$this->form_validation->set_rules('tipe_kendaraan', 	'Tipe Kendaraan', 		'trim|required');
		$this->form_validation->set_rules('nomor_rangka', 		'Nomor Rangka', 		'trim|required');
		$this->form_validation->set_rules('nomor_mesin', 		'Nomor Mesin', 			'trim|required');
		$this->form_validation->set_rules('tahun_pembuatan', 	'Tahun Pembuatan', 		'trim|required');
		$this->form_validation->set_rules('tgl_jual', 			'Tanggal Jual', 		'trim|required');
		//$this->form_validation->set_rules('cover_note_dealer', 	'Covernote', 			'trim');

		if ($this->form_validation->run() == TRUE){

			// ================================================ Konsumen ================================================ //
			$konsumen 	= [
				'kon_nama' 				=> $post['nama_konsumen'],
			];
		
		// ================================================ Pemilik ================================================ //
			$pemilik 	= [
				'pem_nama'			=> $post['nama_pemilik'],
			];
		
		// ================================================ Pembiayaan ================================================ //
			$pembiayaan 	= [
				'pemb_nilai_penjaminan' 	=> str_replace(array('Rp.', ',', ' '), '', $post['nilai_penjaminan']),
				'pemb_nilai_benda_fidusia'	=> str_replace(array('Rp.', ',', ' '), '', $post['nilai_benda_fidusia']),
				'pemb_note' => $post['note']
			];
		
		// ================================================ Kendaraan ================================================ //
			$kendaraan 	= [
				'dk_merk' 				=> $post['merk_kendaraan'],
				'dk_tipe' 				=> $post['tipe_kendaraan'],
				'dk_no_rangka' 			=> $post['nomor_rangka'],
				'dk_no_mesin' 			=> $post['nomor_mesin'],
				'dk_tahun_pembuatan' 	=> $post['tahun_pembuatan'],
				'dk_covernote' 			=> $post['cover_note_dealer'],
				'dk_no_bpkb' 			=> $post['nomor_bpkb'],
			];

		// ================================================ Entry Data ================================================ //
			$entry_data['ed_cabang'] 		= (!in_array(user_data()->user_level, ['1', '5','4','3','8'])) ? user_data()->cabang_id : $post['cabang'];
			$entry_data['ed_tanggal_jual'] 	= date('Y-m-d', strtotime($post['tgl_jual']));
			$entry_data['ed_flag'] 			= '0';
			$entry_data['ed_created_by'] 	= user_data()->user_id;
			$entry_data['ed_created_date'] 	= date("Y-m-d");

			$this->db->trans_begin();

			$entry['entry'] = $this->m_global->update('entry_data', $entry_data, [strEncrypt('ed_id', TRUE) => $id]);
			$param_id = $this->m_global->get_data_all('entry_data', null, [strEncrypt('ed_id', TRUE) => $id]);
			
			$this->m_global->update('konsumen', $konsumen, ['kon_id' => $param_id[0]->ed_data_kendaraan_id]);
			
			$this->m_global->update('pemilik', $pemilik, ['pem_id' => $param_id[0]->ed_pemilik_id]);
			
			$this->m_global->update('pembiayaan', $pembiayaan, ['pemb_id' => $param_id[0]->ed_pembiayaan_id]);
			
			$this->m_global->update('data_kendaraan', $kendaraan, ['dk_id' => $param_id[0]->ed_data_kendaraan_id]);
			$entry['ed_id'] = $param_id[0]->ed_id;
			
			if ($param_id[0]->ed_id != '') {
				$result['msg'] = 'Data Berhasil Diedit';
				$result['inp'] = '2';
			}

			if ($param_id[0]->ed_id != '' && $_FILES != '') {
				$entry['ed_id'] = $param_id[0]->ed_id;
				$entry['nomor_kontrak'] = $post['nomor_kontrak'];



					$entry['jenis'] = 'UPD1';
					if($_FILES['uploadFile']['name']){
						$result['uploadmsg'] = $this->do_upload($entry, 'update');
						
						$result['sts'] = '3';
					}
					
			}
			$key = false;
			if (array_key_exists('UPD105', $post['chkbox']) || array_key_exists('UPD110', $post['chkbox'])) {
				$key = true;
			} else {
				$key = false;
			}
			if(array_key_exists('UPD101', $post['chkbox']) && array_key_exists('UPD102', $post['chkbox']) && $key == true && array_key_exists('UPD106', $post['chkbox'])){			
				$lampiran = $this->m_global->get_data_all('lampiran', null, [strEncrypt('lamp_ed_id', TRUE) => $id, 'lamp_tipe' => 'UPD1']);
				if($lampiran[0]->lamp_ed_id != ""){
					$this->m_global->update('entry_data', array('ed_flag' => '8'), ['ed_id' => $entry['ed_id']]);
				}
			}  
			$entry_data = array('log_ed_id' => $param_id[0]->ed_id, 'log_ed_nomor_kontrak' => $post['nomor_kontrak'], 'log_ed_note' => '', 'log_ed_action' => 'Edit', 'log_ed_user_id' => user_data()->user_id, 'log_ed_user_role' => user_data()->user_role, 'log_ed_flag' => '0');
			$entry['entry'] = $this->m_global->insert('log_entry_data', $entry_data);
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
			}
		} else {
			$valid = validation_errors();
			$result['alert'] = '<div class="uk-alert uk-alert-danger" data-uk-alert="">
				<a href="#" class="uk-alert-close uk-close"></a>
				'.$valid.'
			</div>';
			$result['sts'] = '5';
		}
		echo json_encode($result);
	}

	// fungsi ini untuk aksi upload
	public function do_upload($entry, $datatype = false){
		$result = [];
		$post 	= $this->input->post();
	
		if ($entry['entry']) {
				if($_FILES['uploadFile']['name']){
					if(!$_FILES['uploadFile']['error']){

					$extension 	   = explode(".", $_FILES['uploadFile']['name']);
					$extension 	   = end($extension);
					if ($extension === 'PDF') {
						$extension = 'pdf';
					}
					$new_file_name = strtolower($_FILES['uploadFile']['name']); //rename file
					$size 		   = $_FILES['uploadFile']['size'];
					$rename 	   = explode('.', $new_file_name);
					$replace_name  = str_replace($rename[0], $entry['ed_id'].'_'.$entry['jenis'], $new_file_name);

					if ($size > 2760000) {
						$result['msg'] = $replace_name.' melebihi 2.5 MB';
						$result['sts'] = '0';
					}else if($extension != 'pdf'){
						$result['msg'] = 'Hanya File berformat PDF yang dapat di upload';
						$result['sts'] = '0';
					}else{

						$pathcore = './assets/uploads/';
						$kontrak  = $entry['nomor_kontrak'];
						// $id 	  = $entry['ed_id'].'_'.date("d-m-Y");

						if (!file_exists($pathcore)) {
							$oldmask = umask(0);
							mkdir($pathcore, 0777);
							umask($oldmask);
						}
						if (!file_exists($pathcore.'/'.$kontrak)) {
							$oldmask = umask(0);
							mkdir($pathcore.'/'.$kontrak, 0777);
							umask($oldmask);
						}
						// if (!file_exists($pathcore.'/'.$kontrak.'/'.$id)) {
						// 	$oldmask = umask(0);
						// 	mkdir($pathcore.'/'.$kontrak.'/'.$id, 0777);
						// 	umask($oldmask);
						// }

						// $file_path = $pathcore.'/'.$kontrak.'/'.$id.'/'.$replace_name;
						$file_path = $pathcore.'/'.$kontrak.'/'.$replace_name;
						move_uploaded_file($_FILES['uploadFile']['tmp_name'], FCPATH . $file_path);


						// upload file untuk win
						// set_include_path('phpseclib');
						// include('phpseclib/Net/SFTP.php');
						// $sftp = new Net_SFTP('139.59.106.78');
						
						// if (!$sftp->login('womadmin', 'WOMcend4n42ooo')) {
						// 	exit('Login Failed');
						// }

						$path = "/var/www/html/wom_finance/assets/file/ext-wom/uploads/".$kontrak;
						$file = "/var/www/html/wom_finance/assets/file/ext-wom/uploads/".$kontrak."/".$replace_name;

						// if(!$sftp->chdir($path)){
						// 	$sftp->mkdir($path);
						// }

						// $sftp->chmod(16895,$path);		
						// $sftp->put($file,FCPATH . $file_path,NET_SFTP_LOCAL_FILE);

						$save_path = base_url().$file_path;

						// $url_file 	= base_url().'assets/uploads/'.$kontrak.'/'.$id.'/'.$replace_name;
						$url_file 	= base_url().'assets/uploads/'.$kontrak.'/'.$replace_name;
						// $lokasi 	= 'assets/uploads/'.$kontrak.'/'.$id;
						$lokasi 	= 'assets/uploads/'.$kontrak;
						
						$lampiran_data = [
							'lamp_ed_id'		=> $entry['ed_id'],
							'lamp_nama'			=> $replace_name,
							'lamp_lokasi_file' 	=> $lokasi,
							'lamp_tipe' 		=> $entry['jenis']
						];

						$idd = [
							'lamp_ed_id' 		=> $entry['ed_id'],
							'lamp_tipe' 		=> $entry['jenis']
						];
						
						if ($datatype == 'update') {
							$this->m_global->delete('lampiran', $idd);
						}

						$lampiran = $this->m_global->insert('lampiran', $lampiran_data);
						$this->m_global->delete('lampiran_referensi', $idd);
						
						if ($post['chkbox']) {
							foreach($post['chkbox'] as $a => $b){
								$idd['referensi_id'] = $a;
								$lampiran = $this->m_global->insert('lampiran_referensi', $idd);
							}
						}
						
						if($lampiran) {
							$result['msg'] = 'Dokumen Berhasil ditambahkan!';
							$result['sts'] = '1';
						} else {
							$result['msg'] = 'Data gagal ditambahakan !';
							$result['sts'] = '0';
						}
					}
				}else{
					$result['msg'] = 'Data gagal ditambahakan !';
					$result['sts'] = '0';
				}
			}
		}else{
			$result['msg'] = 'Data gagal ditambahakan !';
			$result['sts'] = '0';
		}
		return $result;
	}

	public function verifikasi()
	{
		$id 		= $_POST['id'];
		$result		= [];

		$upd = ['ed_flag' => '1'];
		$res = $this->m_global->update('entry_data', $upd, ['ed_id' => $id]);
		$param_id = $this->m_global->get_data_all('entry_data', null, ['ed_id' => $id]);
		$entry_data = array('log_ed_id' => $id, 'log_ed_nomor_kontrak' => $param_id[0]->ed_nomor_kontrak, 'log_ed_note' => '', 'log_ed_action' => 'Proses', 'log_ed_user_id' => user_data()->user_id, 'log_ed_user_role' => user_data()->user_role, 'log_ed_flag' => '1');
		$entry['entry'] = $this->m_global->insert('log_entry_data', $entry_data);
		if ($res) {
			$result['msg'] = 'Berhasil diverifikasi !';
			$result['sts'] = '1';
		} else {
			$result['msg'] = 'Gagal diverifikasi !';
			$result['sts'] = '0';
		}

		echo json_encode($result);
	}

	public function action_reject()
	{
		$result = [];
		$post 	= $this->input->post();

		$this->form_validation->set_rules('catatan', 	'Catatan', 	'trim|required');
		
		if ($this->form_validation->run() == TRUE){
			$reject = [
				'log_ed_id' 			=> $post['id'],
				'log_ed_nomor_kontrak'	=> $post['kontrak'],
				'log_ed_note'			=> '(Supervisor) '.$post['catatan'],
				'log_ed_user_id'		=> user_data()->user_id,
				'log_ed_user_role'		=> user_data()->user_role,
				'log_ed_flag'			=> '9',
				'log_ed_action'			=> 'Tolak',
				'log_ed_date' 			=> date('Y-m-d H:i:s'),
			];

			$ed = [
				'ed_flag' => '9'
			];

			$this->db->trans_begin();

			$this->m_global->insert('log_entry_data', $reject);
			$this->m_global->update('entry_data', $ed, ['ed_id' => $post['id']]);

			if ($this->db->trans_status() === FALSE){
				$result['msg'] = 'Data gagal ditolak !';
				$result['sts'] = '0';

				$this->db->trans_rollback();
			}else{
				$result['msg'] = 'Data berhasil ditolak !';
				$result['sts'] = '1';
				
				$this->db->trans_commit();
			}
		} else {
			$result['msg'] = validation_errors();
			$result['sts'] = '99';
		}

		echo json_encode($result);
	}

	public function getReject($id, $kontrak)
	{
		$result = $this->m_global->get_data_all('log_entry_data', [['users', 'user_id = log_ed_user_id'], ['roles', 'role_id = log_ed_user_role']], ['log_ed_id' => $id, 'log_ed_nomor_kontrak' => $kontrak], 'log_entry_data.*, role_name, user_nik', null, ['log_ed_date', 'desc'], 0, 1);

		if (@$result[0]->log_ed_flag == '9') {
			$data 	= '<p><b>Catatan : </b></p><p>'.$result[0]->log_ed_note.'</p>';
			$return = '<div class="uk-alert uk-alert-danger" data-uk-alert=""><a href="#" class="uk-alert-close uk-close"></a>'.$data.'</div>';
		} else {
			$return = null;
		}

		return $return;
	}
}
/* End of file Entry_data.php */
/* Location: ./application/modules/entry_data/controllers/Entry_data.php */
