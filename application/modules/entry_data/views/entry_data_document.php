<style>
	.dropify-wrapper{
		height:100px;
		font-size:10px;
	}
</style>
<div class="uk-grid">
	<div class="uk-width-medium-1">
		<div class="uk-row-first">
			<div class="md-card">
				<div class="md-card-toolbar">
					<h3 class="md-card-toolbar-heading-text">Preview Entry Data</h3>
				</div>
				<div class="md-card-content">
					<div id="alert_error"></div>
					<div class="uk-width-medium-1-1 uk-row-first">
						<div class="uk-form-row text-center">
							<h3>Data Kelengkapan Pembiayaan</h3>
						</div>
						<?php echo $noteReject; ?>
					</div>
				</div>
			</div>

			<div class="uk-grid uk-grid-medium uk-grid-width-medium-1-2">
				<div class="uk-row-first">
					<div class="md-card md-card-primary">
						<div class="md-card-toolbar">
							<h3 class="md-card-toolbar-heading-text">Data Konsumen</h3>
						</div>
						<div class="md-card-content">
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
										<label>Nama </label>
									</div>
								</div>
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
										<label>: <?= @$report[0]->kon_nama; ?> </label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div>
					<div class="md-card md-card-success" style="padding-bottom: 15px;">
						<div class="md-card-toolbar">
							<h3 class="md-card-toolbar-heading-text">Data Pemilik Kendaraan</h3>
						</div>
						<div class="md-card-content">
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
										<label>Nama </label>
									</div>
								</div>
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
										<label>: <?= @$report[0]->pem_nama; ?> </label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="uk-grid-margin uk-row-first">
					<div class="md-card md-card-warning">
						<div class="md-card-toolbar">
							<h3 class="md-card-toolbar-heading-text">Data Pembiayaan</h3>
						</div>
						<div class="md-card-content">
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
										<label>Cabang </label>
									</div>
								</div>
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
										<label>: <?= @$report[0]->cabang_name; ?> </label>
									</div>
								</div>
							</div>
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
										<label>Nomor </label>
									</div>
								</div>
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
										<label>: <?= @$report[0]->ed_nomor_kontrak; ?> </label>
									</div>
								</div>
							</div>
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
										<label>Nilai Penjaminan </label>
									</div>
								</div>
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
										<label>: <?= @$report[0]->pemb_nilai_penjaminan; ?> </label>
									</div>
								</div>
							</div>
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
										<label>Nilai Benda Fidusia </label>
									</div>
								</div>
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
										<label>: <?= @$report[0]->pemb_nilai_benda_fidusia; ?> </label>
									</div>
								</div>
							</div>
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
										<label>Daftar Data Scan </label>
									</div>
								</div>
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
										<?php foreach($file as $key => $value){ ?>
											<label>- <?= @$value->referensi_nama; ?> </label><br>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="uk-grid-margin">
					<div class="md-card md-card-danger">
						<div class="md-card-toolbar">
							<h3 class="md-card-toolbar-heading-text">Data Kendaraan</h3>
						</div>
						<div class="md-card-content">
							<div class="uk-grid">
								<div class="uk-width-medium-1-1 uk-row-first">
									<div class="uk-grid">
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
												<label>Merk </label>
											</div>
										</div>
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
												<label>: <?= @$report[0]->dk_merk; ?> </label>
											</div>
										</div>
									</div>
									<div class="uk-grid">
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
												<label>Tipe </label>
											</div>
										</div>
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
												<label>: <?= @$report[0]->dk_tipe; ?> </label>
											</div>
										</div>
									</div>
									<div class="uk-grid">
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
												<label>Nomor BPKB </label>
											</div>
										</div>
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
												<label>: <?= @$report[0]->dk_no_bpkb; ?> </label>
											</div>
										</div>
									</div>
									<div class="uk-grid">
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
												<label>Tahun Pembuatan </label>
											</div>
										</div>
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
												<label>: <?= @$report[0]->dk_tahun_pembuatan; ?> </label>
											</div>
										</div>
									</div>
									<div class="uk-grid">
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
												<label>Nomor Rangka </label>
											</div>
										</div>
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
												<label>: <?= @$report[0]->dk_no_rangka; ?> </label>
											</div>
										</div>
									</div>
									<div class="uk-grid">
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
												<label>Nomor Mesin </label>
											</div>
										</div>
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
												<label>: <?= @$report[0]->dk_no_mesin; ?> </label>
											</div>
										</div>
									</div>
									<div class="uk-grid">
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
												<label>Cover Note Dealer Motor Baru </label>
											</div>
										</div>
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
												<label>: <?= ((@$report[0]->dk_covernote == "")?"":date('d-m-Y', strtotime(@$report[0]->dk_covernote))); ?> </label>
											</div>
										</div>
									</div>
									<div class="uk-grid">
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
												<label>Tanggal Jual </label>
											</div>
										</div>
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first uk-form-row">
												<label>: <?= ((@$report[0]->ed_tanggal_jual == "")?"":date('d-m-Y', strtotime(@$report[0]->ed_tanggal_jual))); ?> </label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="md-card">
				<div class="md-card-content">
					<div style="height: 500px;" id="my-container"></div>
					<div style="display: none;" id="form_tolak" class="uk-margin-medium-top">
						<form method="post" action="<?=$url; ?>/action_reject" id="form_reject">
							<div class="uk-grid">
								<div class="uk-width-medium-1-3">
									<div class="uk-form-row uk-row-first">
										<input type="hidden" name="id" value="<?=$report[0]->ed_id; ?>">
										<input type="hidden" name="kontrak" value="<?=$report[0]->ed_nomor_kontrak; ?>">
										<div class="md-input-wrapper">
											<label>Catatan <span class="required">*</span></label>
											<textarea name="catatan" class="md-input uk-form-width-large"></textarea>
											<span class="md-input-bar uk-form-width-large"></span>
										</div>
									</div>
									<div class="uk-text-center uk-margin-small-top">
										<button type="submit" class="md-btn md-btn-small md-btn-danger md-btn-wave-light waves-effect waves-button waves-light">Tolak</button>
										<button onclick="rejectData('1');" type="button" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light">Batal</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="uk-text-center uk-margin-medium-top">
						<span id="actionGroup" <?php echo ($report[0]->ed_flag !== '0' && $report[0]->ed_flag !== '8') ? 'style="display : none;"' : (!in_array(user_data()->user_level, ['1', '3', '5', '8', '10']) ? 'style="display : none;"' : '' ); ?>>
							<?php if($report[0]->ed_flag == '8'){ ?>
							<button onclick="verifikasi('<?=$report[0]->ed_id; ?>');" type="button" class="md-btn md-btn-small md-btn-success md-btn-wave-light waves-effect waves-button waves-light">Kirim</button>
							<?php } ?>
							<button onclick="rejectData('99');" type="button" class="md-btn md-btn-small md-btn-danger md-btn-wave-light waves-effect waves-button waves-light">Tolak</button>
						</span>
						<!-- <a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Kembali</a> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<a href="<?=$url; ?>/show_document/<?=$id; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		var urlPDF = "<?php echo $filePDF ?>";

		if (urlPDF !== '') {
			PDFObject.embed(urlPDF, "#my-container");
		} else {
			$('#my-container').hide();
		}

		App.form_submit($('#form_reject'));
	});

	function verifikasi(ed_id) {
		var url = base_url+'entry_data/verifikasi',
			id  = ed_id;

		swal({
			title: "Apakah anda yakin ?",
			text: "Data akan diverifikasi !",
			// html: html,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Yes',
		}).then(
			function(result){
				$.ajax({
					url: url,
					data: {id, id},
					type: 'post',
					dataType: 'json',
					success: function(data) {
						if(data.sts == '1') {
							App.notif('Success', data.msg, 'success');
							$('.reload').trigger('click');
						} else{
							App.notif('Error', data.msg, 'error');
						}
					}
				});
			}, function(dismiss) {
				return false;
			}
		);
	}

	function rejectData(code) {
		// code 99 = reject
		var form 	= $('#form_tolak'),
			action 	= $('#actionGroup');

		if (code == '99') {
			form.slideDown('400', function() {
				action.fadeOut();
			});
		} else {
			form.fadeOut('400', function() {
				action.fadeIn();
			});
		}
	}
</script>
