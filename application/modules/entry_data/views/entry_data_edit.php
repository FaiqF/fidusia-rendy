<style>
	.dropify-wrapper{
		width: initial;
	}
</style>
<?php $data = $record[0]; ?>
<div class="uk-grid">
	<div class="uk-width-medium-1">
		<div class="uk-row-first">
			<form method="post" action="<?=$url; ?>/action_edit/<?=$id?>" id="form_edit" enctype="multipart/form-data">
				<div class="md-card">
					<div class="md-card-toolbar">
						<h3 class="md-card-toolbar-heading-text">Edit Entry Data</h3>
					</div>
					<div class="md-card-content">
						<div id="alert"></div>
						<div class="uk-width-medium-1-1 uk-row-first">
							<div class="uk-form-row text-center">
								<h3>Data Kelengkapan Pembiayaan</h3>
							</div>
							<?php echo $noteReject; ?>
						</div>
					</div>
				</div>

				<div class="uk-grid uk-grid-medium uk-grid-width-medium-1-2">
					<div class="uk-row-first">
						<div class="md-card md-card-primary">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Konsumen</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Nama <span class="required">*</span></label>
												<input value="<?=$data->kon_nama; ?>" name="nama_konsumen" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div>
						<div class="md-card md-card-success" style="padding-bottom: 15px;">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Pemilik Kendaraan</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Nama <span class="required">*</span></label>
												<input value="<?=$data->pem_nama; ?>" name="nama_pemilik" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="uk-grid-margin uk-row-first">
						<div class="md-card md-card-warning">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Pembiayaan</h3>
							</div>
							<div class="md-card-content">
								<div <?=(!in_array(user_data()->user_level, ['1', '5'])) ? '' : ''; ?> class="uk-grid">
									<div class="uk-width-medium-1 uk-margin-small-bottom">
										<label>Cabang <span class="required">*</span></label>
										<select name="cabang" class="form-filter select-filter uk-form-width-large" select-style select-style-bottom>
											<option value="">Cabang</option>
											<?php foreach ($cabang as $key => $val): ?>
												<option <?=$val->cabang_id == $data->ed_cabang ? 'selected' : ''; ?> value="<?=$val->cabang_id; ?>"><?=$val->cabang_name; ?></option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Nilai Penjaminan <span class="required">*</span></label>
												<input value="<?=$data->pemb_nilai_penjaminan; ?>" name="nilai_penjaminan" type="text" class="md-input uk-form-width-medium masked_input" data-inputmask="'alias': 'currency', 'groupSeparator': '.', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': 'Rp. ', 'placeholder': '0'" data-inputmask-showmaskonhover="false">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Nilai Benda Fidusia <span class="required">*</span></label>
												<input value="<?=$data->pemb_nilai_benda_fidusia; ?>" name="nilai_benda_fidusia" type="text" class="md-input uk-form-width-medium masked_input" data-inputmask="'alias': 'currency', 'groupSeparator': '.', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': 'Rp. ', 'placeholder': '0'" data-inputmask-showmaskonhover="false">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>

									<div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>No. Kontrak Pembiayaan</label>
												<input disabled value="<?=$data->ed_nomor_kontrak; ?>" type="text" class="md-input uk-form-width-medium">
												<input type="hidden" name="nomor_kontrak" value="<?=$data->ed_nomor_kontrak; ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
								</div>
								<div class="uk-margin-medium-top">
									<div class="uk-grid">
										<div class="uk-width-medium-1-1 uk-row-first">
											<div class="uk-form-row text-center">
												<h3>Dokumen Kelengkapan Pembiayaan</h3>
											</div>
										</div>
										<div class="uk-width-medium-1-1">
											<input data-allowed-file-extensions="pdf" data-max-file-size="2500K" name="uploadFile" type="file" id="uploadFile" class="uploadFile"/>
										</div>
										<div class="uk-width-medium-1-1" style="margin-top: 7px;">
											<label>Catatan</label>
											<input type="text" name="note" type="text" id="uploadFile" class="md-input uk-form-width-medium"/ value="<?=$note[0]->pemb_note?>">
										</div>

										<div class="uk-width-medium-1-1 uk-margin-medium-top">
											<h4>Daftar data scan  <span class="required">*</span> :</h4>
											<div class="uk-grid uk-grid-width-medium-1-2">
												<?php
													foreach($upload as $a => $b){
												?>
													<p>
														<input type="checkbox" name="chkbox[<?= $b->referensi_id ?>]" id="<?= $b->referensi_id ?>" input-style-checkbox <?= (@$arrfile[$b->referensi_id])?'checked':''; ?>/>
														<label for="<?= $b->referensi_id ?>" class="inline-label"><?= $b->referensi_nama ?></label>
													</p>
												<?php
													}
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="uk-grid-margin">
						<div class="md-card md-card-danger">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Kendaraan</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Merk Kendaraan <span class="required">*</span></label>
												<input value="<?=$data->dk_merk; ?>" name="merk_kendaraan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Tipe Kendaraan <span class="required">*</span></label>
												<input value="<?=$data->dk_tipe; ?>" name="tipe_kendaraan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Nomor Rangka <span class="required">*</span></label>
												<input value="<?=$data->dk_no_rangka; ?>" name="nomor_rangka" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Nomor Mesin <span class="required">*</span></label>
												<input value="<?=$data->dk_no_mesin; ?>" name="nomor_mesin" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
									</div>

									<div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Nomor BPKB <span class="required"></span></label>
												<input value="<?=$data->dk_no_bpkb; ?>" name="nomor_bpkb" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tahun Pembuatan <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input value="<?=$data->dk_tahun_pembuatan; ?>" name="tahun_pembuatan" type="text" class="years">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Jual <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input value="<?=date('d-m-Y', strtotime($data->ed_tanggal_jual)); ?>" name="tgl_jual" type="text" class="datepicker">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="uk-margin-medium-top">
									<div class="uk-grid">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
											<label>Cover Note Dealer </label>
										</div>
										<div class="uk-width-medium-1 uk-margin-small-bottom">
											<input value="<?= ($data->dk_covernote != "")?date('d-m-Y', strtotime($data->dk_covernote)):''; ?>" name="cover_note_dealer" type="text" class="datepicker">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="md-card">
					<div class="md-card-content">
						<div style="height: 500px;" id="my-container"></div>
						<div class="uk-text-center uk-margin-medium-top">
							<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Save</button>
							<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Batal</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<a href="<?=$url; ?>/show_edit/<?=$id; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		var urlPDF = "<?php echo $filePDF ?>";

		if (urlPDF !== '') {
			PDFObject.embed(urlPDF, "#my-container");
		} else {
			$('#my-container').hide();
		}

		$('#form_edit').on('submit', function(e){
			e.preventDefault();

			var $this 	= $(this),
			url 		= $this.attr('action'),
			data 		= $this.serialize();

			var form = $('form')[0]	;
			var formData = new FormData(form);
			var err_res = 0,
			err_msg = [],
			suc_msg = [];

			$.ajax({
				url: url,
				type: 'post',
				dataType:'json',
				data: formData,
				processData: false,
				contentType: false,
				async: false,
				success : function(res) {
					if (res.sts == 5) {
						$('#alert').html(res.alert)
						App.scrollTop();
					}
					if (res.sts == 0) {
						App.notif('error', res.msg, 'error');
					}
					else if (res.inp == 2) {
						App.notif('Success', res.msg, 'success');
						$('.reload').trigger('click');
					}
					if (res.sts == 3 && res.inp == 2) {
						suc_res = '';
						var chk = 0;
						$.each(res, function( key, val ) {
							if(val['sts'] == 1) {
								suc_res = 1;
								suc_msg.push(val['msg']);
							} else {
								if(typeof val['msg'] !== "undefined") chk = 1;
								err_msg.push(val['msg']);
							}
						});
						if(suc_res == 1){
							App.notif('Success', suc_msg.join('<br>') , 'success');
							if(chk === 1){
								App.notif('Error', err_msg.join('<br>'), 'error');
							}
							$('.reload').trigger('click');
						}else{
							App.notif('Success', suc_msg.join('<br>') , 'success');
							if(chk === 1){
								App.notif('Error', err_msg.join('<br>'), 'error');
							}
							$('.reload').trigger('click');
						}
					}
				},
				error 	: function(res) {
					App.notif('Error', res.msg , 'error');
				}
			});
		});

		App.datepicker();
		App.datepicker('.years', null, true);
		App.masked_input();
		$('.masked_input').inputmask();
	
		$('.uploadFile').dropify({
			messages: {
				'default': 'File Upload',
				'replace': '',
				'remove':  'Batal',
				'error': 'File hanya format PDF dan maksimal 2.5MB !'
			}
		});
	});


function forceLower(ele) {
	var lc = $(ele).val().toLowerCase();

	$(ele).val(lc);
}
</script>
