<style>
	.dropify-wrapper {
	    width: initial;
	}
</style>
<div class="uk-grid">
	<div class="uk-width-medium-1">
		<div class="uk-row-first">
			<form method="post" action="<?=$url; ?>/action_add" id="form_add" enctype="multipart/form-data">
				<div class="md-card">
					<div class="md-card-toolbar">
						<h3 class="md-card-toolbar-heading-text">Add Entry Data</h3>
					</div>
					<div class="md-card-content">
						<div id="alert"></div>
						<div class="uk-width-medium-1-1 uk-row-first">
							<div class="uk-form-row text-center">
								<h3>Data Kelengkapan Pembiayaan</h3>
							</div>
						</div>
					</div>
				</div>

				<div class="uk-grid uk-grid-medium uk-grid-width-medium-1-2">
					<div class="uk-row-first">
						<div class="md-card md-card-primary">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Konsumen</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nama <span class="required">*</span></label>
												<input name="nama_konsumen" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div>
						<div class="md-card md-card-success" style="padding-bottom: 15px;">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Pemilik Kendaraan</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nama <span class="required">*</span></label>
												<input name="nama_pemilik" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="uk-grid-margin uk-row-first">
						<div class="md-card md-card-warning">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Pembiayaan</h3>
							</div>
							<div class="md-card-content">
								<div <?=(!in_array(user_data()->user_level, ['1', '5'])) ? '' : ''; ?> class="uk-grid">
									<div class="uk-width-medium-1 uk-margin-small-bottom">
										<label>Cabang <span class="required">*</span></label>
										<select name="cabang" class="form-filter select-filter uk-form-width-large" select-style select-style-bottom>
											<option value="">Cabang</option>
											<?php foreach ($cabang as $key => $val): ?>
												<option value="<?=$val->cabang_id; ?>"><?=$val->cabang_name; ?></option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Nilai Penjaminan <span class="required">*</span></label>
												<input name="nilai_penjaminan" type="text" class="md-input uk-form-width-medium masked_input" data-inputmask="'alias': 'currency', 'groupSeparator': '.', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': 'Rp. ', 'placeholder': '0'" data-inputmask-showmaskonhover="false">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Nilai Benda Fidusia <span class="required">*</span></label>
												<input name="nilai_benda_fidusia" type="text" class="md-input uk-form-width-medium masked_input" data-inputmask="'alias': 'currency', 'groupSeparator': '.', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': 'Rp. ', 'placeholder': '0'" data-inputmask-showmaskonhover="false">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>

									<div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>No. Kontrak Pembiayaan <span class="required">*</span></label>
												<input name="nomor_kontrak" type="number" class="md-input uk-form-width-medium" onkeydown="return FilterInput(event)" onpaste="handlePaste(event)">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
								</div>
								<div class="uk-margin-medium-top">
									<div class="uk-grid">
										<div class="uk-width-medium-1-1 uk-row-first">
											<div class="uk-form-row text-center">
												<h3>Dokumen Kelengkapan Pembiayaan</h3>
											</div>
										</div>
										<div class="uk-width-medium-1-1">
											<input accept="application/pdf" data-max-file-size="2500K" name="uploadFile" type="file" id="uploadFile" class="uploadFile"/>
										</div>
										<div class="uk-width-medium-1-1 uk-margin-medium-top">
											<h4>Daftar data scan :</h4>
											<div class="uk-grid uk-grid-width-medium-1-2">
												<?php
													foreach($upload as $a => $b){
												?>
													<p>
														<input type="checkbox" name="chkbox[<?= $b->referensi_id ?>]" id="<?= $b->referensi_id ?>" input-style-checkbox />
														<label for="<?= $b->referensi_id ?>" class="inline-label"><?= $b->referensi_nama ?></label>
													</p>
												<?php
													}
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="uk-grid-margin">
						<div class="md-card md-card-danger">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Kendaraan</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Merk Kendaraan <span class="required">*</span></label>
												<input name="merk_kendaraan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Tipe Kendaraan <span class="required">*</span></label>
												<input name="tipe_kendaraan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nomor Rangka <span class="required">*</span></label>
												<input name="nomor_rangka" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nomor Mesin <span class="required">*</span></label>
												<input name="nomor_mesin" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
									</div>

									<div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nomor BPKB <span class="required"></span></label>
												<input name="nomor_bpkb" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tahun Pembuatan <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="tahun_pembuatan" type="text" class="years">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Jual <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="tgl_jual" type="text" class="datepicker">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="uk-margin-medium-top">
									<div class="uk-grid">
										<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
											<label>Cover Note Dealer </label>
										</div>
										<div class="uk-width-medium-1 uk-margin-small-bottom">
											<input name="cover_note_dealer" type="text" class="datepicker">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="md-card">
					<div class="md-card-content">
						<div class="uk-text-center">
							<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Save</button>
							<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Batal</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<a href="<?=$url; ?>" class="ajaxify reload"></a>
<script>
	function FilterInput(event) {
		var keyCode = ('which' in event) ? event.which : event.keyCode;
		
		isNotWanted = (keyCode == 69 || keyCode == 101);
		return !isNotWanted;
	};
	function handlePaste (e) {
		var clipboardData, pastedData;
	
		// Get pasted data via clipboard API
		clipboardData = e.clipboardData || window.clipboardData;
		pastedData = clipboardData.getData('Text').toUpperCase();
	
		if(pastedData.indexOf('E')>-1) {
			//alert('found an E');
			e.stopPropagation();
			e.preventDefault();
		}
	};
	$(document).ready(function(){
		

		$('#form_add').on('submit', function(e){
			e.preventDefault();

			var $this 	= $(this),
			url 		= $this.attr('action'),
			data 		= $this.serialize();

			var form = $('form')[0]	;
			var formData = new FormData(form);
			var err_res = 0,
			err_msg = [],
			suc_msg = [];

			$.ajax({
				url: url,
				type: 'post',
				dataType:'json',
				data: formData,
				processData: false,
				contentType: false,
				async: false,
				success : function(res) {
					if (res.sts == 5) {
						$('#alert').html(res.alert)
						App.scrollTop();
					}
					if (res.sts == 0) {
						App.notif('error', res.msg, 'error');
					}
					else if (res.inp == 2) {
						App.notif('Success', res.msg, 'success');
						$('.reload').trigger('click');
					}
					if (res.sts == 3 && res.inp == 2) {
						suc_res = '';
						var chk = 0;
						$.each(res, function( key, val ) {
							if(val['sts'] == 1) {
								suc_res = 1;
								suc_msg.push(val['msg']);
							} else {
								if(typeof val['msg'] !== "undefined") chk = 1;
								err_msg.push(val['msg']);
							}
						});
						if(suc_res == 1){
							App.notif('Success', suc_msg.join('<br>') , 'success');
							if(chk === 1){
								App.notif('Error', err_msg.join('<br>'), 'error');
							}
							$('.reload').trigger('click');
						}else{
							App.notif('Success', suc_msg.join('<br>') , 'success');
							if(chk === 1){
								App.notif('Error', err_msg.join('<br>'), 'error');
							}
							$('.reload').trigger('click');
						}
					}
				},
				error 	: function(res) {
					App.notif('Error', res.msg , 'error');
				}
			});
		});

		App.datepicker();
		App.datepicker('.years', null, true);
		App.masked_input();
		$('.masked_input').inputmask();
		
		var fileUpload = $('.uploadFile').dropify({
			messages: {
				'default': 'File Upload',
				'replace': '',
				'remove':  'Batal',
				'error': 'File hanya format PDF dan maksimal 2.5MB !'
			}
		});
	});
</script>