<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail extends CI_Controller {

	private $table_db       = 'entry_data'; //nama table
	private $table_prefix   = 'ed_'; //nama awal pada field
	private $prefix 		= 'invoice/detail';
	private $name 			= 'Detail';

	public function __construct(){
		parent::__construct();
		//Do your magic here
	}

	public function index(){
		$data['name'] 	= "Invoice";
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables', 'datatables_fixcolumns'];

		$data['export']		= [
								['name' => 'Export Excel', 'link' => base_url('invoice/export_excel')],
							  ];

		$this->template->display('detail/index', $data);
	}

	public function select(){
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 				= null;
		$where_e 			= null;

		$search 	= [
			"no_order" => "ed_nomor_kontrak",
			"nama" => "kon_nama",
			"kon_alamat" => "kon_alamat",
			"kon_ktp" => "kon_ktp",
			"pem_nama" => "pem_nama",
			"pem_alamat" => "pem_alamat",
			"pem_ktp" => "pem_ktp",
			"pemb_nilai_penjaminan" => "pemb_nilai_penjaminan",
			"pemb_nilai_benda_fidusia" => "pemb_nilai_benda_fidusia",
			"dk_merk" => "dk_merk",
			"dk_tipe" => "dk_tipe",
			"dk_tahun_pembuatan" => "dk_tahun_pembuatan",
			"dk_no_rangka" => "dk_no_rangka",
			"dk_no_mesin" => "dk_no_mesin",
			"dk_no_polisi" => "dk_no_polisi",
			"dk_no_bpkb" => "dk_no_bpkb",
			"dk_covernote" => "dk_covernote",
			"ed_tanggal_jual"=> "ed_tanggal_jual"
		];

		if (@$post['action'] == 'filter')
		{
			$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = "DATE(ed_lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

					} else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			$where[$this->table_prefix.'status <>'] = '99';
		}
		$where[$this->table_prefix.'flag'] = '5';

		$keys 		= array_keys($search);
		@$order 		= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

		$select		= '*, '.implode(',', $search);

		$count 		= $this->m_global->count_data_all( 'v_'.$this->table_db, $join, $where, $where_e );
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


		$data = $this->m_global->get_data_all('v_'.$this->table_db, $join, $where, $select, $where_e, $order, $start, $length);
		$i = 1 + $start;
		foreach ( $data as $rows ) {
			$records["data"][] = array(
				'<input type="checkbox" class="checklist_data" value="'.$rows->ed_id.'" name="chk['.$rows->ed_id.']">',
				$i,
				$rows->ed_nomor_kontrak,
				$rows->kon_nama,
				$rows->kon_alamat,
				$rows->kon_ktp,
				$rows->pem_nama,
				$rows->pem_alamat,
				$rows->pem_ktp,
				uang($rows->pemb_nilai_penjaminan,null),
				uang($rows->pemb_nilai_benda_fidusia,null),
				$rows->dk_merk,
				$rows->dk_tipe,
				$rows->dk_tahun_pembuatan,
				$rows->dk_no_rangka,
				$rows->dk_no_mesin,
				$rows->dk_no_polisi,
				$rows->dk_no_bpkb,
				$rows->dk_covernote,
				date('d-m-Y', strtotime($rows->ed_tanggal_jual)),
				$this->_button($rows->ed_id, $rows->ed_status, $rows->ed_flag, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

	public function _button($id, $flag, $show = false){
		$prev = $id;
		$id = strEncrypt($id);
		$button = '';

		if($show) {
			// $preview = '<a target = "blank" data-uk-tooltip title="Show Invoice" class="md-btn md-btn-primary md-btn-mini md-btn-wave-light md-btn-icon waves-effect waves-button waves-light ajaxify" href="'.base_url('invoice/show_invoice').'/'.$id.'"><i class="uk-icon-map"></i></a>';
			$konfirmasi = '<a data-uk-tooltip title="Konfirmasi Pembayaran" class="md-btn md-btn-success md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(4, this, event);" href="'.base_url().'invoice/konfirmasi_pembayaran/'.$id.'"><i class="uk-icon-check uk-icon-small"></i></a>';
			$button = $konfirmasi;
		}else{
			$button = '-';
		}
		return $button;
	}

	public function konfirmasi_pembayaran($id){
		$flag = ['ed_flag' => '4'];
		$data['konfirmasi'] = $this->m_global->update('entry_data', $flag, [strEncrypt('ed_id', TRUE) => $id]);
		$data['status'] = 1;
		echo json_encode($data);
	}

	public function add_invoice(){
		$get = $_GET;
		$data = str_replace(array('}]', '[{'), '',explode('},{', $get['as']));
		foreach($data as $a=>$b){
			$arrb = explode(',', $b);
			if($arrb[0] != '"name":"datatable_ajax_length"' && $arrb[0] != '"name":"name"'){
				foreach($arrb as $exa=>$exb){
					if($exa == "1") $hexb[] = str_replace(array('"value":"', '"'), "", $exb);
				}
			}
		}	
		$id = join("', '", $hexb);
		$data['id']		 = $id;
		$data['name'] 		 = "Show Invoice";
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'Invoice' => base_url('invoice'), 'Preview' => base_url('invoice/show_invoice').'/'.$id];		
		$data['plugin'] 	 = ['kendo_ui','dropify', 'inputmask'];
		$this->template->display('detail/add', $data);
	}
	
	public function action_add()
	{	
		$result = [];
		$post 	= $this->input->post();
		$this->form_validation->set_rules('nomor', 		'Nomor',		'trim|required');
		$this->form_validation->set_rules('tanggal', 	'Tanggal', 	'trim|required');

		if ($this->form_validation->run() == TRUE){
			$data 	= [
				'invoice_nomor' 				=> $post['nomor'],
				'invoice_tanggal' 				=> $post['tanggal'],
			];
			$this->db->trans_begin();
			$ed_invoice = array();
			$res = $this->m_global->insert('invoice', $data);
			$ed_invoice['invoice_id'] = $this->db->insert_id();
			$id = $post['id'];
			$arrid = explode("', '", $id);
			foreach($arrid as $a=>$b){
				$ed_invoice['ed_id'] = $b;
				$res = $this->m_global->insert('ed_invoice', $ed_invoice);
			}
			$upd['flag'] = "6";
			$this->db->query("UPDATE entry_data SET ed_flag = '6' WHERE ed_id IN('$id')");
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$result['msg'] = 'Data gagal ditambahkan !';
				$result['sts'] = '0';
			}else{
				$this->db->trans_commit();
				$result['msg'] = 'Data berhasil ditambahkan !';
				$result['sts'] = '1';
			}
		} else {
			$result['msg'] = validation_errors();
			$result['sts'] = '99';
		}
		echo json_encode($result);
	}
	
	public function show_invoice(){
		$get = $_GET;
		$data = str_replace(array('}]', '[{'), '',explode('},{', $get['as']));
		foreach($data as $a=>$b){
			$arrb = explode(',', $b);
			if($arrb[0] != '"name":"datatable_ajax_length"' && $arrb[0] != '"name":"name"'){
				foreach($arrb as $exa=>$exb){
					if($exa == "1") $hexb[] = str_replace(array('"value":"', '"'), "", $exb);
				}
			}
		}	
		$id = join("', '", $hexb);
		$data['name'] 		 = "Show Invoice";
		$data['url'] 		 = base_url().$this->prefix;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), 'Invoice' => base_url('invoice'), 'Preview' => base_url('invoice/show_invoice').'/'.$id];		

		$this->template->display('show_invoice', $data);
	}

	public function update_invoice(){
		$post = $this->input->post();
		$joinid = join("', '", $post['chk']);
		print_r($post);
		echo $joinid;
		die();
		$this->db->trans_begin();
		$this->db->query("UPDATE entry_data SET ed_flag = '7' WHERE ed_id IN('$joinid')");
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		}
		else{
			$this->db->trans_commit();
			$result['sukses'] = 'Data Berhasil Diubah';
		}
		echo json_encode($result);
	}

	public function export_excel(){
		ob_clean();
		$where[$this->table_prefix.'flag'] = '3';
		$select	= 'ed_id, ed_nama_konsumen, ed_alamat_konsumen, ed_ktp_konsumen, ed_nama_pemilik, ed_alamat_pemilik, ed_ktp_pemilik, ed_no_pembiayaan, ed_nilai_penjaminan, ed_nilai_benda_fidusia, ed_merk_kendaraan, ed_tipe_kendaraan, ed_thn_pembuatan, ed_no_rangka_kendaraan, ed_no_mesin_kendaraan, ed_no_polisi, ed_no_bpkb, ed_cover_note, ed_no_order';
		$data['master'] = $this->m_global->get_data_all($this->table_db, null, $where, $select);

		/*---START EXCEL---*/
		include('application/libraries/PHPExcel.php');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="Invoice.xls"');

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);

		$styleArray = array(
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				),
			);
		$styleArray1 = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
				),
			);
                    //background
		$styleArray12 = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'rgb' => 'FFEC8B',
					),
				),
			);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A1:R1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->mergeCells('A1:R1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'DATA INVOICE');
		$row = 3;
		$rowawal = $row;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':R'.$row)->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':R'.$row)->applyFromArray($styleArray1);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':R'.$row)->applyFromArray($styleArray12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(40);
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'No');
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, 'No Order');
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, 'Alamat Konsumen');
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, 'KTP Konsumen');
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, 'Nama Pemilik');
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, 'Alamat Pemilik');
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, 'KTP Pemilik');
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, 'No Pembiayaan');
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, 'Nilai Penjaminan');
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, 'Nilai Benda Fidusia');
		$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, 'Merk Kendaraan');
		$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, 'Tipe Kendaraan');
		$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, 'Thn Pembuatan');
		$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row, 'No Rangka Kendaraan');
		$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, 'No Mesin kendaraan');
		$objPHPExcel->getActiveSheet()->SetCellValue('P'.$row, 'No Polisi');
		$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$row, 'No BPKB');
		$objPHPExcel->getActiveSheet()->SetCellValue('R'.$row, 'Cover Note');
		$row++;
		$objPHPExcel->getActiveSheet()->freezePane('A'.$row);
		foreach ($data['master'] as $key => $c) {
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':R'.$row)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':R'.$row)->applyFromArray($styleArray1);
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, ($row - $rowawal));
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, 'No : '.$c->ed_no_order);
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $c->ed_alamat_konsumen);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $c->ed_ktp_konsumen);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $c->ed_nama_pemilik);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $c->ed_alamat_pemilik);
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $c->ed_ktp_pemilik);
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $c->ed_no_pembiayaan);
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, $c->ed_nilai_penjaminan);
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, $c->ed_nilai_benda_fidusia);
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, $c->ed_merk_kendaraan);
			$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, $c->ed_tipe_kendaraan);
			$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, $c->ed_thn_pembuatan);
			$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row, $c->ed_no_rangka_kendaraan);
			$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, $c->ed_no_mesin_kendaraan);
			$objPHPExcel->getActiveSheet()->SetCellValue('P'.$row, $c->ed_no_polisi);
			$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$row, $c->ed_no_bpkb);
			$objPHPExcel->getActiveSheet()->SetCellValue('R'.$row, $c->ed_cover_note);
			$row++;
		}

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			// Write file to the browser
		$writer->save('php://output');
	}
}

/* End of file Invoice.php */
/* Location: ./application/modules/invoice/controllers/Invoice.php */