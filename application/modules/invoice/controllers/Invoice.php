<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends MX_Controller {

    private $url        = 'invoice';
    private $table_db   = 'invoice_upload';
    private $prefix     = 'invoice_';

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }


	public function index()
	{
        // pre($this->session->all_userdata()); exit();
        $data['name']       = "Invoice";
        $data['url']        = base_url().$this->url;
        $data['plugin']     = ['datatables', 'datatables_fixcolumns','kendo_ui'];

        // pre($this->session->all_userdata()); exit();
		$this->template->display($this->url, $data);
    }

    public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 				= [
                                ['users', 'users.user_id = invoice_upload.invoice_createdby', 'left']
                            ];
		$where  			= null;
		$where_e 			= null;
		$where = [];
		$search = [
			'invoice_code'   => $this->prefix.'code',
			// 'path'   => $this->prefix.'path'
		];

		if (@$post['action'] == 'filter')
		{
			//$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = $where_e." AND DATE(updated_at) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
					}else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			//$where[$this->table_prefix.'status <>'] = '99';
		}
		$keys 		= array_keys($search);
		@$order 	= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

        $select		= '*';
        $count 		= $this->m_global->count_data_all($this->table_db, $join, $where, $where_e );

		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


        $data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, $where_e, $order, $start, $length);
        $i = 1 + $start;

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
				$rows->invoice_code,
                $rows->invoice_nokontrak,
                $rows->invoice_path,
				date('d-m-Y', strtotime($rows->invoice_createddate)),
				$rows->user_name,
                $this->_button($rows->invoice_id, $rows->invoice_nokontrak, $rows->invoice_path, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
        $records["recordsFiltered"] = $count;

		echo json_encode($records);
    }

    public function show_add()
    {
        $data['name']   = 'Entri Data';
        $data['url']    = base_url().$this->url;
        $data['plugin'] = ['kendo_ui','dropify'];
        $this->template->display($this->url.'_add', $data);
    }

    public function action_add()
    {
        $post   = $this->input->post();
        $this->form_validation->set_rules('invoice_nokontrak', 'No kontrak', 'trim|required');
        if($this->form_validation->run() == false) {
           $result['sts'] = 0;
           $result['msg'] = validation_errors();
           
           echo json_encode($result); exit();
        }

        $cek = count($this->m_global->get_data_all($this->table_db, null, [$this->prefix.'nokontrak' => $post['invoice_nokontrak']], 'invoice_nokontrak'));

        if ($cek > 0) {
            $result['sts']  = 0;
            $result['msg']  = 'No Kontrak ' . $post['invoice_nokontrak'] . ' sudah dipakai';

            echo json_encode($result);

        } else {
            if( $_FILES['invoice_path']['size'] == 0 ) {
                $result['msg'] = 'File tidak boleh kosong';
                $result['sts'] = 0;

                echo json_encode($result); exit;
            }

            $filePath    = $_FILES['invoice_path']['name'];
            $ext         = pathinfo($filePath, PATHINFO_EXTENSION);
            $allowedFile = [
                'pdf'
            ];

            // validation if file not xls or xlsx
            if ( !in_array(@strtolower($ext), $allowedFile) ) {
                $result['msg'] = 'File harus bertipe pdf';
                $result['sts'] = 0;

                echo json_encode($result); exit;
            }

            $dir = FCPATH."/assets/uploads/invoice/".$post['invoice_nokontrak'];

            mkdir($dir);
            chmod($dir, 0777);
            $tempName = $_FILES['invoice_path']['tmp_name'];
            $dirUpload = $dir.'/';
            $fileName = date('Y-m-d').'_'.user_data()->user_nik.'_'.rand().'.pdf';

            // upload the file
            $doUpload = move_uploaded_file($tempName, $dirUpload.$fileName);

            if ( !$doUpload ){
                $result['msg'] = 'Gagal mengupload file';
                $result['sts'] = 0;

                echo json_encode($result); exit;
            }

            $data['invoice_code']            = $post['invoice_code'];
            $data['invoice_nokontrak']       = $post['invoice_nokontrak'];
            $data['invoice_path']            = $fileName;
            $data['invoice_createddate']     = date('Y-m-d H:i:s');
            $data['invoice_createdby']       = user_data()->user_id;
            $query                              = $this->m_global->insert($this->table_db, $data);

            // pre($this->db->last_query(), 1);

            if( !$query ) {
                $result['msg'] = 'Data gagal ditambahkan !';
                $result['sts'] = 0;
                echo json_encode($result); exit;
            }

            $result['msg'] = 'Data berhasil ditambahkan !';
            $result['inp'] = 2;

            echo json_encode($result); exit();
        }

    }

    public function show_edit($id)
    {
        $data['name']    = 'Edit data';
        $data['url']     = base_url().$this->url;
        $data['plugin']  = ['kendo_ui','dropify'];
        $data['id']      = $id;
        $data['records'] = $this->m_global->get_data_all($this->table_db, null, [strEncrypt($this->prefix.'id', true) => $id])[0];

        $this->template->display($this->url.'_edit', $data);
    }

    public function action_edit($id)
    {
        $post = $this->input->post();
        $this->form_validation->set_rules('invoice_nokontrak', 'No kontrak', 'trim|required');

        if($this->form_validation->run() == false) {
            $result['sts'] = 0;
            $result['msg'] = validation_errors();

            echo json_encode($result); exit();
        }

        if( $_FILES['invoice_path']['size'] == 0 ) {
            $result['msg'] = 'File tidak boleh kosong';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

		$filePath    = $_FILES['invoice_path']['name'];
        $ext         = pathinfo($filePath, PATHINFO_EXTENSION);
        $allowedFile = [
            'pdf',
        ];

        // validation if file not xls or xlsx
        if ( !in_array(@strtolower($ext), $allowedFile) ) {
            $result['msg'] = 'File harus bertipe pdf';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $cek = $this->m_global->get_data_all($this->table_db, null, [$this->prefix.'nokontrak' => $post['invoice_nokontrak_old']], '*')[0];
        $dir = FCPATH.'/assets/uploads/invoice/';
        $tempName = $_FILES['invoice_path']['tmp_name'];
        $fileName = date('Y-m-d').'_'.user_data()->user_nik.'_'.rand().'.pdf';

        if ($post['invoice_nokontrak'] != $post['invoice_nokontrak_old']) {
            unlink($dir.$post['invoice_nokontrak_old'].'/'.$cek->invoice_path);
            rmdir($dir.$post['invoice_nokontrak_old']);

            mkdir($dir.$post['invoice_nokontrak']);
            chmod($dir.$post['invoice_nokontrak'], 0777);

            $dirUpload = FCPATH."/assets/uploads/invoice/".$post['invoice_nokontrak'].'/';
        } else {
            unlink($dir.$post['invoice_nokontrak_old'].'/'.$cek->invoice_path);
            chmod($dir.$post['invoice_nokontrak_old'], 0777);
            $dirUpload = FCPATH."/assets/uploads/invoice/".$post['invoice_nokontrak_old'].'/';
        }

        // upload the file
        $doUpload = move_uploaded_file($tempName, $dirUpload.$fileName);

        if ( !$doUpload ){
            $result['msg'] = 'Gagal mengupload file';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $getDataFile = $this->m_global->get_data_all($this->table_db, null , [strEncrypt($this->prefix.'id', true) => $id], '*')[0];

        $updateFile  = [
            'invoice_path' => $fileName
        ];
        $updateFilePath = $this->m_global->update($this->table_db, $updateFile, [strEncrypt($this->prefix.'id', true) => $id]);

        $result['msg'] = 'Data berhasil diubah !';
        $result['sts'] = 1;

        echo json_encode($result); exit;
    }

    public function action_delete($id)
    {
        $get_data   = $this->m_global->get_data_all($this->table_db, null, [strEncrypt($this->prefix.'id', true) => $id])[0];
        $path       = FCPATH . 'assets/uploads/invoice/'.$get_data->invoice_nokontrak;
        // pre($path, 1);
        unlink($path.'/'.$get_data->invoice_path);
        rmdir($path);
        $query      = $this->m_global->delete($this->table_db, [strEncrypt($this->prefix.'id', true) => $id]);

        if($query) {
            $result['msg'] = 'Data berhasil dihapus !';
            $result['status'] = 1;
        } else {
            $result['msg'] = 'Data gagal dihapus !';
            $result['status'] = 0;
        }

        echo json_encode($result);
    }

    public function action_download($nokontrak, $name)
    {
        $filepath = FCPATH . 'assets/uploads/invoice/'.$nokontrak.'/'.$name;

        // Process download
        if(file_exists($filepath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($filepath));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            ob_clean();
            flush();
            readfile($filepath);
            exit;
        }
    }


    public function _button($id, $nokontrak, $path, $show = false)
	{
		$prev       = $id;
        $id         = strEncrypt($id);

        // btn action
        $download 	= '<a data-uk-tooltip target="_blank" title="Download" class="md-btn md-btn-success md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="'.base_url('invoice/action_download').'/'.$nokontrak.'/'.$path.'"><i class="uk-icon-download uk-icon-small"></i></a>';
        $edit 	    = '<a data-uk-tooltip title="Edit" class="ajaxify md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="'.base_url('invoice/show_edit').'/'.$id.'"><i class="uk-icon-pencil uk-icon-small"></i></a>';
		$delete     = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(2, this, event);" href="'.base_url($this->url.'/action_delete/'.$id).'"><i class="uk-icon-trash uk-icon-small"></i></a>';

        $button = $download . $edit . $delete;

		return $button;
    }

}

/* End of file invoice.php */
/* Location: ./application/modules/invoice/controllers/invoice.php */