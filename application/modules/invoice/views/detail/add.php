<style>
	.dropify-wrapper {
	    width: initial;
	}
</style>
<div class="uk-grid">
	<div class="uk-width-medium-1-1">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Invoice</h3>
			</div>
			<div class="md-card-content">
				<div id="alert_error"></div>
				<form method="post" action="<?=$url; ?>/action_add" id="form_add">
				<input type="hidden" name="id" value="<?= $id?>">
					<div class="uk-grid">
						<div class="uk-width-medium-1-5 uk-row-first"></div>
						<div class="uk-width-medium-4-5 uk-row-first">
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<label>Nomor <span class="required">*</span></label>
									<input name="nomor" type="text" class="md-input uk-form-width-large">
									<span class="md-input-bar uk-form-width-large"></span>
								</div>
							</div>
							<div class="uk-form-row">
								<div class="md-input-wrapper">
									<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
										<label>Tanggal <span class="required">*</span></label>
									</div>
									<div class="uk-width-medium-1 uk-margin-small-bottom">
										<input name="tanggal" type="text" class="datepicker">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-text-center uk-margin-large-top">
						<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Save</button>
						<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<a href="<?=$url; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		App.datepicker();
		App.datepicker('.years', null, true);
		App.form_submit($('#form_add'));
	});
</script>