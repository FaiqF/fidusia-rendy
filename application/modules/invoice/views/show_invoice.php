<style>
	#logo{
		height: 80%;
		width: 80%;
	}
	.font_red{
		color: red;
	}
	.right{
		text-align: right;
	}
</style>
<div class="uk-grid">
	<div class="uk-width-medium-1-1">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Show Invoice</h3>
			</div>
			<div class="md-card-content">
				<form method="post" action="<?=$url; ?>/act_invoice" id="form_konfirmasi" enctype="multipart/form-data">
					<div class="uk-grid">
						<div class="uk-width-medium-2-4 uk-row-first">
							<div class="uk-grid">
								<div class="uk-width-medium-2-1 uk-row-first">
									<div class="uk-grid">
										<div class="uk-width-medium-2-2 uk-row-first">
											<div class="uk-form-row">
												<img id="logo" src="<?=base_url().'assets/img/wom.png'?>" alt="Wom Finance">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="uk-width-medium-2-4">
							<div class="uk-grid">
								<div class="uk-width-medium-1-1 uk-row-first">
									<div class="uk-grid">
										<div class="uk-width-medium-2-2 uk-row-first">
											<div class="uk-form-row">
												<div class="uk-grid uk-margin-medium-top uk-text-center">
													<h4><b><u class="font_red">UNIPAID</u></b></h4>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="uk-width-medium-1-1 uk-row-first">
									<div class="uk-grid">
										<div class="uk-width-medium-1-2 uk-row-first">
											<div class="uk-form-row">
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<select id="user_role" name="user_role" class="uk-form-width-large" select-style select-style-bottom>
														<option value="">Pilih Bank</option>
														<option value="1">Bank BNI</option>
														<option value="2">Bank Mandiri</option>
														<option value="3">Bank BRI</option>
														<option value="4">Bank Mandiri Syariah</option>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="uk-width-medium-2-2 uk-row-first">
									<div class="uk-form-row">
										Bank BNI 019-191-6455 a/n. Ari Rundagi Sigit
									</div>
								</div>
								<div class="uk-width-medium-2-2 uk-row-first">
									<div class="uk-form-row">
										Cantumkan Berita Transfer
									</div>
								</div>
							</div>
							<div class="uk-width-medium-1-1 uk-row-first">
								<div class="uk-grid">
									<div class="uk-width-medium-2-2 uk-row-first">
										<div class="uk-form-row">
											<div class="uk-grid uk-margin-medium-top uk-text-center">
												<h4><b><u class="font_red">INVOICE-18730</u></b></h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="uk-width-medium-2-2 uk-row-first">
								<b>Total Transfer : IDR 130000.00</b>
							</div>
							<div class="uk-grid">
								<div class="uk-grid uk-margin-medium-top uk-text-center">
									<div class="uk-width-1-1">
										<button type="submit" class="filter-submit md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Konfirmasi Pembayaran</button>
										<button type="button" class="md-btn md-btn-small md-btn-success md-btn-wave-light waves-effect waves-button waves-light" name="cetak">Export PDF</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="uk-grid">
					<div class="uk-width-medium-5-5 uk-row-first">
						<div class="uk-grid">
							<div class="uk-width-medium-1-1 uk-row-first">
								<div class="uk-form-row text-center">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-grid">
					<div class="uk-width-medium-2-4 uk-row-first">
						<div class="uk-grid">
							<div class="uk-width-medium-2-1 uk-row-first">
								<div class="uk-grid">
									<div class="uk-width-medium-2-2 uk-row-first">
										<div class="uk-form-row">
											<div class="md-card md-card-primary">
												<div class="md-card-content">
													<b>Invoice To</b><br>
													Rahmat Agung Aryanata <br>
													jln.Semeru XIX / Y - 21, jln.Semeru XIX / Y - 21 <br>
													Jember, Jawa Timur, 68121 <br>
													Indonesia.
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-width-medium-2-4 uk-row-first">
						<div class="uk-grid">
							<div class="uk-width-medium-2-1 uk-row-first">
								<div class="uk-grid">
									<div class="uk-width-medium-2-2 uk-row-first">
										<div class="uk-form-row">
											<div class="md-card md-card-success">
												<div class="md-card-content">
													<b>Pay To</b><br>
													Bian Arganeta Suganda <br>
													jln.Soekarnohatta XIX / Y - 21, jln.Soekarnohatta XIX / Y - 21 <br>
													Malang, Jawa Timur, 68121 <br>
													Indonesia.
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-grid">
					<div class="uk-width-medium-2-4 uk-row-first">
						<div class="uk-grid">
							<div class="uk-width-medium-1-1 uk-row-first">
								<div class="uk-grid">
									<div class="uk-width-medium-2-2 uk-row-first">
										<div class="uk-form-row">
											<div class="uk-grid uk-margin-medium-top uk-text-center">
												<h4><b>INVOICE #18730</b></h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="uk-width-medium-2-2 uk-row-first">
								Invoice Date : 08/01/2016
							</div>
							<div class="uk-width-medium-2-2 uk-row-first">
								Due Date : 19/01/2016
							</div>
						</div>
					</div>
				</div>
				<div class="uk-grid">
					<div class="uk-width-medium-5-5 uk-row-first">
						<div class="uk-grid">
							<div class="uk-width-medium-1-1 uk-row-first">
								<div class="md-card-content">
									<div class="uk-overflow-container">
										<table class="uk-table">
											<thead>
												<tr>
													<th>Description</th>
													<th>Amount</th>
												</tr>
											</thead>
											<tfoot>
												<tr>
												<td class="right"><b>Sub Total :</b></td>
													<td><b>Rp.130,000</b></td>
												</tr>
												<tr>
													<td class="right"><b>Credit :</b></td>
													<td><b>Rp.10,000</b></td>
												</tr>
												<tr>
													<td class="right"><b>Total :</b></td>
													<td><b>Rp.120,000</b></td>
												</tr>
											</tfoot>
											<tbody>
												<tr>
													<td>Blog Hosting Basic IIX - pasarmagic.com (08/01/2013 - 07/01/2014)</td>
													<td>Rp.130,000</td>
												</tr>
												<tr>
													<td>Domain Registration - pasarmagic.com - 1 Years/s (08/01/2013 - 07/01/2014)</td>
													<td>Rp.0</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>