<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-content">
		<div class="md-card">
			<div class="md-card-toolbar">
				<div class="md-card-toolbar-actions">
					<i class="md-icon material-icons md-card-toggle">&#xE316;</i>
				</div>
				<h3 class="md-card-toolbar-heading-text">
					Filter
				</h3>
			</div>
			<div class="md-card-content">
				<div class="uk-grid uk-margin-medium-top uk-text-center">
					<div class="uk-width-1-1">
						<label>No Order</label>
						<input name="no_order" type="text" class="md-input form-filter">
						<span class="md-input-bar"></span>
					</div>
					<input name="kon_alamat" type="hidden" class="form-filter select-filter">
					<input name="nama" type="hidden" class="form-filter select-filter">
					<input name="kon_ktp" type="hidden" class="form-filter select-filter">
					<input name="pem_nama" type="hidden" class="form-filter select-filter">
					<input name="pem_alamat" type="hidden" class="form-filter select-filter">
					<input name="pem_ktp" type="hidden" class="form-filter select-filter">
					<input name="pemb_nilai_penjaminan" type="hidden" class="form-filter select-filter">
					<input name="pemb_nilai_benda_fidusia" type="hidden" class="form-filter select-filter">
					<input name="dk_merk" type="hidden" class="form-filter select-filter">
					<input name="dk_tipe" type="hidden" class="form-filter select-filter">
					<input name="dk_tahun_pembuatan" type="hidden" class="form-filter select-filter">
					<input name="dk_no_rangka" type="hidden" class="form-filter select-filter">
					<input name="dk_no_mesin" type="hidden" class="form-filter select-filter">
					<input name="dk_no_polisi" type="hidden" class="form-filter select-filter">
					<input name="dk_no_bpkb" type="hidden" class="form-filter select-filter">
					<input name="dk_covernote" type="hidden" class="form-filter select-filter">
					<input name="ed_tanggal_jual" type="hidden" class="form-filter select-filter">
				</div>
				<div class="uk-grid uk-margin-medium-top uk-text-center">
					<div class="uk-width-1-1">
						<button type="button" class="filter-submit md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Cari</button>
						<button type="button" class="filter-reset md-btn md-btn-small md-btn-danger md-btn-wave-light waves-effect waves-button waves-light">Reset</button>
					</div>
				</div>
			</div>
		</div>
		<form id="upload" method="post" action="<?=$url; ?>/update_invoice" enctype="multipart/form-data">
			<table id="datatable_ajax" class="uk-table" cellspacing="0" width="100%" fixedColumns="true" leftColumns="1" rightColumns="1">
				<thead>
					<tr>
						<th width="20">
							<center>
								<input id="checkAll" type="checkbox" name="name">
							</center>
						</th>
						<th width="40">No.</th>
						<th>No Order</th>
						<th>Nama Konsumen</th>
						<th>Alamat Konsumen</th>
						<th>Ktp Konsumen</th>
						<th>Nama Pemilik</th>
						<th>Alamat Pemilik</th>
						<th>Ktp Pemilik</th>
						<th>No Pembiayaan</th>
						<th>Nilai Penjaminan</th>
						<th>Nilai Benda Fidusia</th>
						<th>Merk Kendaraan</th>
						<th>Tipe Kendaraan</th>
						<th>Thn Pembuatan</th>
						<th>No Rangka Kendaraan</th>
						<th>No Mesin Kendaraan</th>
						<th>No Polisi</th>
						<th>No Bpkb</th>
						<th>Cover Note</th>
						<th width="152">Aksi</th>
					</tr>
				</thead>
				<tbody>
	
				</tbody>
			</table>
			<div class="uk-grid">
				<div class="uk-width-medium-4-4 uk-row-first">
					<div class="uk-grid">
						<div class="uk-width-medium-1-1 uk-row-first">
							<div class="uk-form-row text-center">
								<div class="uk-width-1-1">
									<a id="post" class="md-btn md-btn-small md-btn-success md-btn-wave-light waves-effect waves-button waves-light">Invoice</a>
									<input type="hidden" id="edit">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	$(document).ready(function(){
		
		var urlk 	= "<?=$url; ?>/select",
		header  = [
		{ "className": "text-center" },
		{ "className": "text-center" },
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		{ "className": "text-center" },
		{ "className": "text-center" },
		],
		order 	= [
		['1', 'asc']
		],
		sort 	= [-1, 0];

		App.setDatatable(urlk, header, order, sort); //paling akhir tambahkan sendiri jika tabel lebih dari satu
		
		var table = $('#datatable_ajax').DataTable();
		$('#post').click(function() {
			if ( ! table.data().count() ) {
				App.notif('Error', 'Tidak Ada Data' , 'error');
			}else{
				altair_helpers.content_preloader_show('md');
				$.ajax({
					url: 'invoice/update_invoice',
					type: 'POST',
					dataType: 'json',
					data: $( "#upload" ).serialize(),
					success : function(res) {
						if (typeof res.sukses !== 'undefined') {
							App.notif('Success', res.sukses, 'success');
							$('.reload').trigger('click');
							table.clear().draw();
							$('#post').hide();
						}
						/*---valiasi cek data jika sudah ada---*/
						// if (typeof res.gagal !== 'undefined') {
						// 	var gagal = res.jml_gagal.length;
						// 	App.notif('Error', 'Data sudah ada = ' + gagal + ' data', 'error');
						// 	var error = $('#error');
						// 	App.scrollTop();
						// 	error.html('<div class="uk-alert uk-alert-danger" data-uk-alert><a href="#" class="uk-alert-close uk-close"></a>'+res.gagal+'</div>');
						// }
						/*---end valiasi cek data jika sudah ada---*/
					},
					error 	: function(res) {
						App.notif('Error', 'Terjadi Kesalahan' , 'error');
					}
				});
				altair_helpers.content_preloader_hide();  
			}    	
		});
	});
</script>
