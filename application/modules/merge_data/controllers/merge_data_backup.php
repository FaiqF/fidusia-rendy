<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merge_data extends CI_Controller {

	private $table_db       = 'entry_data'; //Table name
	private $table_prefix   = 'ed_'; //The initial name of the field
	private $prefix 		= 'merge_data';

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$data['name'] 	= "Import Data";
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables','dropify'];

		$this->template->display('index', $data);
	}

	public function select($insert_data){
		$post 		= $this->input->post();
		$length 	= intval(@$post['length']);
		$start  	= intval(@$post['start']);
		$sEcho		= intval(@$post['draw']);

		$count 		= count($insert_data);
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();
		$i = 1 + $start;
		if(count($insert_data) > 0){
			foreach ($insert_data as $key => $value) {
			$records["data"][]  = array(
				$i,
				$value['transaksi_no_kontrak'],
				$value['akta_notaris'],
				$value['akta_domisili'],
				$value['akta_nomor']
			);
				$i++;
			}
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		return $records;
	}

	public function _button($show = false)
	{
		$button = '';

		if($show) {
			$delete = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light button"><i class="uk-icon-trash uk-icon-small"></i></a>';
			$button = $delete;
		} else {
			$button = '-';
		}

		return $button;
	}
	
	public function select2($insert_data){
		$post 		= $this->input->post();
		$length 	= intval(@$post['length']);
		$start  	= intval(@$post['start']);
		$sEcho		= intval(@$post['draw']);

		$count 		= count($insert_data);
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();
		$i = 1 + $start;
		if(count($insert_data) > 0){
			foreach ($insert_data as $key => $value) {
			$records["data"][]  = array(
				$i,
				$value['transaksi_no_kontrak'],
				$value['akta_notaris'],
				$value['akta_domisili'],
				$value['akta_nomor']
			);
				$i++;
			}
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		return $records;
	}

	public function _button2($show = false)
	{
		$button = '';

		if($show) {
			$delete = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light button2"><i class="uk-icon-trash uk-icon-small"></i></a>';
			$button = $delete;
		} else {
			$button = '-';
		}

		return $button;
	}
	
	public function upload_excel(){
		$path = $_FILES['file_xls']['name'];
		$ext  = pathinfo($path, PATHINFO_EXTENSION);
		if($_FILES['file_xls']['size'] == "0"){
			$err['feedback'] = 2;
			echo json_encode($err);exit();
		}else{
			if (@strtolower($ext)=='xls' || @strtolower($ext)=='xlsx') {
				$this->load->library("PHPExcel");
				$phpExcel = new PHPExcel();
				/*---Read your Excel workbook---*/
				$inputFileName = $_FILES['file_xls']['tmp_name'];
				try {
					$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$objPHPExcel = $objReader->load($inputFileName);
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
	
				/*---Get worksheet dimensions---*/
				$sheet = $objPHPExcel->getSheet(0);
				$highestRow = $sheet->getHighestRow();
				$highestColumn = $sheet->getHighestColumn();
	
				/*---Loop through each row of the worksheet in turn---*/
				$start = 0;
				$last = date("Y-m-d h:i:sa");
				$last_insert = $last;
	
				$insert_data = array();
				$insert_data_false = array();
				$no_kontrak = array();
				for ($row = 4; $row <= $highestRow; $row++){
					/*---Read	 a row of data into an array---*/
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
					$key = ($start -1);
					$key++;

					$where = ['trans_no_kontrak' => str_replace(' ', '', $rowData[$key][1])];
					$select = 'trans_no_kontrak';
					$data_cek = $this->m_global->get_data_all('transaksi', null, $where, $select);
					$hitung_no = count($data_cek);
					if (@$rowData[$key][2] != "" && $hitung_no < 1 ) {
						$insert_data_false[] = [
							'transaksi_no_kontrak'		 => @$rowData[$key][1],
							'akta_notaris' 				 => @$rowData[$key][6],
							'akta_domisili'    			 => @$rowData[$key][8],
							'akta_nomor' 				 => @$rowData[$key][9]
						];
					}else if(@$rowData[$key][2] != ""){
						$no_kontrak[] = @$rowData[$key][2];
						$insert_data[] = [
							'transaksi_no_kontrak'		 => @$rowData[$key][1],
							'akta_notaris' 				 => @$rowData[$key][6],
							'akta_domisili'    			 => @$rowData[$key][8],
							'akta_nomor' 				 => @$rowData[$key][9]
						];
					}
				}
				
				$select22  = $this->select2(@$insert_data_false);
				$select2['data2'] = $select22['data'];
				$select2['recordsTotal2'] = $select22['recordsTotal'];			
				$select  = $this->select(@$insert_data);
				$select = array_merge($select, $select2);
				$result['feedback'] = 1;
			}else{
				$err['feedback'] = 0;
				echo json_encode($err);exit();
			}
		}
		$return = array_merge($select, $result);
		echo json_encode($return);
	}
	
	public function save(){
		$path = $_FILES['file_xls']['name'];
		$ext  = pathinfo($path, PATHINFO_EXTENSION);
		
		if($_FILES['file_xls']['size'] == "0"){
			$result['feedback'] = 2;
			echo json_encode($err);exit();
		}

		// when condition file size not null
		$allowedFileType = [
			'xls',
			'xlsx'
		];

		if ( !in_array(@strtolower($ext), $allowedFileType) ) {
			$result['feedback'] = '2';
			$result['msg']		= 'File not allowed';

			echo json_encode($result); exit;
		}


		// when condition file type is xls or xlsx
		$this->load->library("PHPExcel");
		$phpExcel = new PHPExcel();
		/*---Read your Excel workbook---*/
		$inputFileName = $_FILES['file_xls']['tmp_name'];
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		/*---Get worksheet dimensions---*/
		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();

		/*---Loop through each row of the worksheet in turn---*/
		$start = 0;
		$last = date("Y-m-d h:i:sa");
		$last_insert = $last;

		$insert_data = array();
		$insert_data_false = array();
		$no_kontrak = array();

		$result['feedback'] = 0;
		for ($row = 4; $row <= $highestRow; $row++){
			$this->db->trans_begin();
			/*---Read	 a row of data into an array---*/
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
			$key = ($start -1);
			$key++;
			
			// pre($rowData[$key]); exit();
			$where = ['trans_no_kontrak' => $rowData[$key][1]];
			$select = '*';
			$data_cek = $this->m_global->get_data_all('transaksi', null, $where, $select);
			$hitung_no = count($data_cek);
			if($hitung_no > 0) {
				$akta_id    = $data_cek[0]->trans_akta_id;
				$perjanjian_id = $data_cek[0]->trans_perjanjian_id;
				$dk_id		= $data_cek[0]->trans_dk_id;

				$dataAkta = [
					'akta_tanggal' => date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][2],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
					'akta_jam_awal' => PHPExcel_Style_NumberFormat::toFormattedString($rowData[$key][3], 'hh:mm'),
					'akta_jam_akhir' => PHPExcel_Style_NumberFormat::toFormattedString($rowData[$key][4], 'hh:mm'),
					'akta_zona_waktu' => $rowData[$key][5],
					'akta_nama_notaris' => $rowData[$key][6],
					'akta_gelar_notaris' => $rowData[$key][7],
					'akta_domisili_notaris' => $rowData[$key][8],
					'akta_nomor'	=> $rowData[$key][9],
					'akta_saksi1_panggilan' => $rowData[$key][15],
					'akta_saksi1_nama' => $rowData[$key][16],
					'akta_saksi1_tempat_lahir' =>  $rowData[$key][17],
					'akta_saksi1_tanggal_lahir' => date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][18],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
					'akta_saksi1_kewarganegaraan' => $rowData[$key][19],
					'akta_saksi1_pekerjaan' => $rowData[$key][20],
					'akta_saksi1_alamat' => $rowData[$key][21],
					'akta_saksi1_rt' => $rowData[$key][22],
					'akta_saksi1_rw' => $rowData[$key][23],
					'akta_saksi1_kelurahan' => $rowData[$key][24],
					'akta_saksi1_kecamatan' => $rowData[$key][25],
					'akta_saksi1_kabupaten' => $rowData[$key][26],
					'akta_saksi1_jenis_identitas' => $rowData[$key][27],
					'akta_saksi1_nik' => $rowData[$key][28],
					'akta_saksi2_panggilan' => $rowData[$key][29],
					'akta_saksi2_nama' => $rowData[$key][30],
					'akta_saksi2_tempat_lahir' => $rowData[$key][31],
					'akta_saksi2_tanggal_lahir' => date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][32],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
					'akta_saksi2_kewarganegaraan' => @$rowData[$key][33],
					'akta_saksi2_pekerjaan' => @$rowData[$key][34],
					'akta_saksi2_alamat' => @$rowData[$key][35],
					'akta_saksi2_rt' => @$rowData[$key][36],
					'akta_saksi2_rw' => @$rowData[$key][37],
					'akta_saksi2_kelurahan' => @$rowData[$key][38],
					'akta_saksi2_kecamatan' => @$rowData[$key][39],
					'akta_saksi2_kabupaten' => @$rowData[$key][40],
					'akta_saksi2_jenis_identitas' => @$rowData[$key][41],
					'akta_saksi2_nik' => @$rowData[$key][42]
				];
				$this->m_global->update('akta', $dataAkta, ['akta_id' => $akta_id]);
				$dataPerjanjian = [
					'perjanjian_jenis_penggunaan' => @$rowData[$key][10],
					'perjanjian_berdasarkan' => @$rowData[$key][11]
				];
				$this->m_global->update('perjanjian_pembiayaan', $dataPerjanjian, ['perjanjian_id' => $perjanjian_id]);
				
				$dataObyek = [
					'dk_jenis_bukti' => @$rowData[$key][12],
					'dk_no_bukti_bpkb'	=> @$rowData[$key][13],
					'dk_kalimat_bukti' => @$rowData[$key][14]
				];
				$this->m_global->update('data_kendaraan', $dataObyek, ['dk_id' => $dk_id]);
				echo $this->db->last_query(); exit();
				if($this->db->trans_status() == false) {
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
			}

		}

		$result['feedback'] = 1;
		$result['msg'] 		= 'Success update data';

		echo json_encode($result); exit;
	}
	
	public function save2(){ 
		$path = $_FILES['file_xls']['name'];
		$ext  = pathinfo($path, PATHINFO_EXTENSION);
		if($_FILES['file_xls']['size'] == "0"){
			$err['feedback'] = 2;
			echo json_encode($err);exit();
		}else{
			if (@strtolower($ext)=='xls' || @strtolower($ext)=='xlsx') {
				$this->load->library("PHPExcel");
				$phpExcel = new PHPExcel();
				/*---Read your Excel workbook---*/
				$inputFileName = $_FILES['file_xls']['tmp_name'];
				try {
					$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$objPHPExcel = $objReader->load($inputFileName);
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
	
				/*---Get worksheet dimensions---*/
				$sheet = $objPHPExcel->getSheet(0);
				$highestRow = $sheet->getHighestRow();
				$highestColumn = $sheet->getHighestColumn();
	
				/*---Loop through each row of the worksheet in turn---*/
				$start = 0;
				$last = date("Y-m-d h:i:sa");
				$last_insert = $last;
	
				$insert_data = array();
				$insert_data_false = array();
				$no_kontrak = array();
				$this->db->trans_begin();
				$result['feedback'] = 0;
				for ($row = 2; $row <= $highestRow; $row++){
					/*---Read	 a row of data into an array---*/
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
					$key = ($start -1);
					$key++;
					if(@$rowData[$key][0] != ""){
						$daerah 	= [
							'daerah_kode_pos' 					=> @$rowData[$key][0],
							'daerah_kelurahan' 					=> @$rowData[$key][1],
							'daerah_kecamatan' 					=> @$rowData[$key][2],
							'daerah_jenis_kota' 				=> @$rowData[$key][3],
							'daerah_kota' 						=> @$rowData[$key][4],
							'daerah_provinsi' 					=> @$rowData[$key][5]
							];
						$this->m_global->insert('daerah', $daerah);
					}
				}
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$result['feedback'] = 1;
				}
				
			}else{
				echo json_encode($result);
			}
		}
		echo json_encode($result);
	}
	
	public function save3(){ 
		$path = $_FILES['file_xls']['name'];
		$ext  = pathinfo($path, PATHINFO_EXTENSION);
		if($_FILES['file_xls']['size'] == "0"){
			$err['feedback'] = 2;
			echo json_encode($err);exit();
		}else{
			if (@strtolower($ext)=='xls' || @strtolower($ext)=='xlsx') {
				$this->load->library("PHPExcel");
				$phpExcel = new PHPExcel();
				/*---Read your Excel workbook---*/
				$inputFileName = $_FILES['file_xls']['tmp_name'];
				try {
					$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$objPHPExcel = $objReader->load($inputFileName);
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
	
				/*---Get worksheet dimensions---*/
				$sheet = $objPHPExcel->getSheet(0);
				$highestRow = $sheet->getHighestRow();
				$highestColumn = $sheet->getHighestColumn();
	
				/*---Loop through each row of the worksheet in turn---*/
				$start = 0;
				$last = date("Y-m-d h:i:sa");
				$last_insert = $last;
	
				$insert_data = array();
				$insert_data_false = array();
				$no_kontrak = array();
				$this->db->trans_begin();
				$result['feedback'] = 0;
				for ($row = 3; $row <= $highestRow; $row++){
					/*---Read	 a row of data into an array---*/
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
					$key = ($start -1);
					$key++;
					if(@$rowData[$key][2] != ""){
						$this->m_global->update('daerah', array('daerah_pengadilan' => @$rowData[$key][2]), ['daerah_jenis_kota' => @$rowData[$key][4], 'daerah_kota' => @$rowData[$key][5]]);
					}
				}
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$result['feedback'] = 1;
				}
				
			}else{
				echo json_encode($result);
			}
		}
		echo json_encode($result);
	}

	/*---save data from table value---*/
	//public function save(){
	//	die('asdadasd');
	//	$post = $this->input->post();
	//	foreach ($post as $key => $value) {
	//		$accept = $value;
	//	}
	//	$loop = count($accept);
	//	$parse = $loop -1;
	//	$result['gagal'] = '';
	//	for ($i = 1; $i <= $parse; $i++){
	//		$where = ['cabang_name' => $accept[$i][1]];
	//		$select = 'cabang_id';
	//		$cabang_id = $this->m_global->get_data_all('cabang', null, $where, $select);
	//		$konsumen 	= [
	//			'kon_nama' 					=> $accept[$i][3]
	//			];
	//		$pemilik 	= [
	//			'pem_nama' 					=> $accept[$i][4]
	//			];
	//		$entry_data 	= [
	//			'ed_nomor_kontrak'		=> $accept[$i][2],
	//			'ed_cabang' 			=> $cabang_id[0]->cabang_id,
	//			'ed_tanggal_jual' 		=> date('Y-m-d', strtotime($accept[$i][5])),
	//			'ed_created_by'			=> user_data()->user_id,
	//			'ed_created_date'		=> date("Y-m-d H:i:s"),
	//			'ed_lastupdate'		=> date("Y-m-d H:i:s")
	//			];
	//		$pembiayaan 	= [
	//			'pemb_nilai_penjaminan' 			=> str_replace(array('Rp.', ',', ' ', '.'), '', $accept[$i][6]),
	//			'pemb_nilai_benda_fidusia' 		=> str_replace(array('Rp.', ',', ' ', '.'), '', $accept[$i][7])
	//			];
	//		$kendaraan 	= [
	//			'dk_merk' 				=> @$accept[$i][8],
	//			'dk_tipe' 				=> @$accept[$i][9],
	//			'dk_tahun_pembuatan' 					=> @$accept[$i][10],
	//			'dk_no_rangka' 		=> @$accept[$i][11],
	//			'dk_no_mesin' 		=> @$accept[$i][12],
	//			'dk_covernote' 						=> ((@$accept[$i][13] != "")?date('Y-m-d', strtotime($accept[$i][13])):''),
	//			'dk_no_bpkb' 						=> @$accept[$i][14]
	//			];
	//		//cek data berdasarkan no order
	//		$where = ['ed_nomor_kontrak' => '$accept[$i][1] Pada File excel belum ada field no order'];
	//		$select = 'ed_nomor_kontrak';
	//		$data_cek = $this->m_global->get_data_all('entry_data', null, $where, $select);
	//		$hitung_no = count($data_cek);
	//		if ($hitung_no > 0) {
	//			$result['gagal'] .= 'No Order : '.$data_cek[0]->ed_nomor_kontrak.' Sudah Ada. </br>';
	//			$result['jml_gagal'][] = $data_cek[0]->ed_nomor_kontrak;
	//		}else{
	//			$this->db->trans_begin();
	//			$this->m_global->insert('konsumen', $konsumen);
	//			$entry_data['ed_konsumen_id'] = $this->db->insert_id();
	//			$this->m_global->insert('pemilik', $pemilik);
	//			$entry_data['ed_pemilik_id'] = $this->db->insert_id();
	//			$this->m_global->insert('pembiayaan', $pembiayaan);
	//			$entry_data['ed_pembiayaan_id'] = $this->db->insert_id();
	//			$this->m_global->insert('data_kendaraan', $kendaraan);
	//			$entry_data['ed_data_kendaraan_id'] = $this->db->insert_id();
	//			$insert = $this->m_global->insert('entry_data', $entry_data);
	//			// $entry_data = array('log_ed_id' => $this->db->insert_id(), 'log_ed_no_order' => $accept[$i][1], 'log_ed_note' => '', 'log_ed_action' => 'Simpan', 'log_ed_user_id' => user_data()->user_id, 'log_ed_user_role' => user_data()->user_role, 'log_ed_flag' => '0');
	//			// $entry['entry'] = $this->m_global->insert('log_entry_data', $entry_data);
	//			if ($this->db->trans_status() === FALSE){
	//				$this->db->trans_rollback();
	//			}
	//			else{
	//				$this->db->trans_commit();
	//				$result['sukses'] = 'Data Berhasil Ditambahkan';
	//			}
	//		}
	//	}
	//	echo json_encode($result);
	//}
	
	public function export(){
		$path = $_FILES['file_xls']['name'];
		$ext  = pathinfo($path, PATHINFO_EXTENSION);
		if($_FILES['file_xls']['size'] == "0"){
			$err['feedback'] = 2;
			echo json_encode($err);exit();
		}else{
			if (@strtolower($ext)=='xls' || @strtolower($ext)=='xlsx') {
				$this->load->library("PHPExcel");
				$phpExcel = new PHPExcel();
				/*---Read your Excel workbook---*/
				$inputFileName = $_FILES['file_xls']['tmp_name'];
				try {
					$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$objPHPExcel = $objReader->load($inputFileName);
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
	
				/*---Get worksheet dimensions---*/
				$sheet = $objPHPExcel->getSheet(0);
				$highestRow = $sheet->getHighestRow();
				$highestColumn = $sheet->getHighestColumn();
	
				/*---Loop through each row of the worksheet in turn---*/
				$start = 0;
				$last = date("Y-m-d h:i:sa");
				$last_insert = $last;
	
				$insert_data = array();
				$insert_data_false = array();
				$no_kontrak = array();
				$result['feedback'] = 0;
				for ($row = 2; $row <= $highestRow; $row++){
					/*---Read	 a row of data into an array---*/
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
					$key = ($start -1);
					$key++;
					$where = ['ed_nomor_kontrak' => $rowData[$key][2]];
					$select = 'ed_nomor_kontrak';
					$data_cek = $this->m_global->get_data_all('entry_data', null, $where, $select);
					$where = ['cabang_name' => $rowData[$key][1]];  
					$select = 'cabang_id';
					$cabang_id = $this->m_global->get_data_all('cabang', null, $where, $select);
					$hitung_no = count($data_cek);
					//31/05/15
					//print_r(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][5],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2));
					//die();  
						if (@$rowData[$key][2] != "" && ($hitung_no > 0 || in_array(@$rowData[$key][2], $no_kontrak, true) || @$cabang_id[0]->cabang_id == "")) {
								$insert_data_false[] 	= [
								'ed_cabang'					=> @$rowData[$key][1],
								'ed_no_kontrak_konsumen'	=> @$rowData[$key][2],
								'ed_nama_konsumen'			=> @$rowData[$key][3],
								'ed_nama_pemilik' 			=> @$rowData[$key][4],
								'ed_tgl_jual' 				=> date('d-m-Y', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][5],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
								'ed_nilai_penjaminan' 		=> uang(@$rowData[$key][6],null),
								'ed_nilai_benda_fidusia' 	=> uang(@$rowData[$key][7],null),
								'ed_merk_kendaraan' 		=> @$rowData[$key][8],
								'ed_tipe_kendaraan' 		=> @$rowData[$key][9],
								'ed_thn_pembuatan' 			=> @$rowData[$key][10],
								'ed_no_rangka_kendaraan'	=> @$rowData[$key][11],
								'ed_no_mesin_kendaraan' 	=> @$rowData[$key][12],
								'ed_cover_note' 			=> ((@$rowData[$key][13]!="")?date('d-m-Y', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][13],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))):''),
								'ed_bpkb'	 				=> @$rowData[$key][14],
								'ed_last_insert' 			=> $last_insert,
								'ed_created_by'				=> user_data()->user_id,
								'ed_created_date'			=> date("Y-m-d")
								];
						}else if(@$rowData[$key][2] != ""){	
						}else{
						}	
				}
			}else{
			}
		}
		$this->export_real($insert_data_false);
		
	}	
	
	function export_real($insert_data_false){
		ob_clean(); 
		//include('application/libraries/PHPExcel.php');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="data_gagal.xls"');
		$e = 0;
				$z = 0;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
		$styleArray = array(
                    'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                  );
		$styleArray1 = array(
                     'borders' => array(
                    'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ),
                     ),
                    );
                    //background
                    $styleArray12 = array(
                    'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array(
                    'rgb' => 'FFEC8B',
                    ),
                    ),
									);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
    	$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->mergeCells('A1:O1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'DATA GAGAL');
		$row = 3;
		$rowawal = $row;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':O'.$row)->applyFromArray($styleArray);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':O'.$row)->applyFromArray($styleArray1);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':O'.$row)->applyFromArray($styleArray12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'No.');
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, 'Cabang');
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, 'No Kontrak');
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, 'Nama Konsumen');
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, 'Nama Pemilik');
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, 'Tgl Jual');
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, 'Nama Penjaminan');
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, 'Nilai Benda Fidusia');
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, 'Merk');
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, 'Type');
		$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, 'Tahun');
		$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, 'No Rangka');
		$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, 'No Mesin');
		$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row, 'Covernote Dealer');
		$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, 'No BPKB');
		$row++;
		$objPHPExcel->getActiveSheet()->freezePane('A'.$row);
		
		
	
								
								  
								
								
							foreach($insert_data_false as $id => $data){
								
								//$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':O'.$row)->applyFromArray($styleArray);
								//$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':O'.$row)->applyFromArray($styleArray1);
								$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, ($row - 3));
								$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, @$data['ed_cabang']);
								$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, @$data['ed_no_kontrak_konsumen']);
								$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, @$data['ed_nama_konsumen']);
								$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, @$data['ed_nama_pemilik']);
								$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, @$data['ed_tgl_jual']);
								$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, @$data['ed_nilai_penjaminan']);
								$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, @$data['ed_nilai_benda_fidusia']);
								$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, @$data['ed_merk_kendaraan']);
								$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, @$data['ed_tipe_kendaraan']);
								$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, @$data['ed_thn_pembuatan']);
								$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, @$data['ed_no_rangka_kendaraan']);
								$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, @$data['ed_no_mesin_kendaraan']);
								$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row, @$data['ed_cover_note']);
								$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, @$data['ed_bpkb']);
								
								
								$row++;
							} 
		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// Write file to the browser
		$writer->save('php://output');
		exit();
	}
}

/* End of file import_data.php */
/* Location: ./application/modules/import_data/controllers/import_data.php */
