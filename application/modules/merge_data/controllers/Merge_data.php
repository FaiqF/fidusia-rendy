<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merge_data extends CI_Controller {

	private $table_db       = 'entry_data'; //Table name
	private $table_prefix   = 'ed_'; //The initial name of the field
	private $prefix 		= 'merge_data';

	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$data['name'] 	= "Import Data";
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables','dropify'];

		$this->template->display('index', $data);
	}

	public function migrate()
	{
		$select = "*";
		$join = [
            ['sertifikat_upload', 'pdf_nokontrak = sertifikat_nokontrak', 'left'],
            ['invoice_upload', 'pdf_nokontrak = invoice_nokontrak', 'left']
		];

		$dataTerkirim = [];
		$sertifikatData = [];
		$getAllData = $this->m_global->get_data_all('pdf_upload', $join, null, $select);
		echo count($getAllData, 1);
		// pre($getAllData, 1);
		foreach($getAllData as $key => $value) {
			if($value->sertifikat_id == null) {
				$insertData = [
					'data_no_kontrak' => $value->pdf_nokontrak,
					'data_flag' => '1',
					'data_cabang_id'=> '1',
					'data_pdf_id' => $value->pdf_id,
					'data_status' => '1',
					'createdat' => $value->pdf_createddate

				];
				$this->m_global->insert('data_transaksi', $insertData);
				$dataTerkirim[] = $value->pdf_nokontrak;
			}

			if($value->sertifikat_id != null) {
				$insertData = [
					'data_no_kontrak' => $value->pdf_nokontrak,
					'data_flag' => '4',
					'data_cabang_id'=> '1',
					'data_pdf_id' => $value->pdf_id,
					'data_sertifikat_id' => $value->sertifikat_id,
					'data_invoice_id' => $value->invoice_id,
					'data_status' => '1',
					'createdat' => $value->pdf_createddate

				];
				$this->m_global->insert('data_transaksi', $insertData);
				$sertifikatData[] = $value->sertifikat_nokontrak; 
			}
			
		}

		$pre = [
			"dataTerkirim" => $dataTerkirim,
			"dataSertifikat" => $sertifikatData
		];
		pre($pre);
	  
		
	}

	public function cek_sertifikat_invoice()
	{
		$select = "sertifikat_id,invoice_id,sertifikat_nokontrak,invoice_nokontrak";
		$join = [
			['invoice_upload', 'sertifikat_nokontrak = invoice_nokontrak', 'left']
		];

		$getData = $this->m_global->get_data_all('sertifikat_upload', $join, null, $select);

		$notMatch = [];

		foreach($getData as $key => $value) {
			if($value->invoice_id == null) {
				$notMatch[] = $value->sertifikat_nokontrak;
			}
		}
		pre($notMatch);
	}

	public function cek_sertifikat_upload()
	{
		$select = "sertifikat_id,pdf_id,sertifikat_nokontrak,pdf_nokontrak";
		$join = [
			['pdf_upload', 'sertifikat_nokontrak = pdf_nokontrak', 'left']
		];

		$getData = $this->m_global->get_data_all('sertifikat_upload', $join, null, $select);

		$notMatch = [];

		foreach($getData as $key => $value) {
			if($value->pdf_id == null) {
				$notMatch[] = $value->sertifikat_nokontrak;
			}
		}
		pre($notMatch);
	}
}

/* End of file import_data.php */
/* Location: ./application/modules/import_data/controllers/import_data.php */
