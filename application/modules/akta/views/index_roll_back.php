<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-content">
		<div class="md-card">
			<div class="md-card-toolbar">
				<div class="md-card-toolbar-actions">
					<i class="md-icon material-icons md-card-toggle">&#xE316;</i>
				</div>
				<h3 class="md-card-toolbar-heading-text">
					Filter
				</h3>
			</div>
			<div class="md-card-content filter">
				<form id="form">
				<div class="uk-grid">
					<div class="uk-width-medium-1-2 uk-row-first"> 
						<div class="uk-grid">
							<div class="uk-width-medium-1-3 uk-margin-small-bottom uk-row-first">
								<label>No Kontrak</label> <input name="no_kontrak" type="text" class="md-input form-filter select-filter" id="text3">
							</div>
						</div>
					</div>
				</div>
				<div class="uk-grid uk-margin-medium-top uk-text-center">
					<div class="uk-width-1-1">
						<a onclick="send()" class="export_excel"><button type="button" class="filter-submit md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Cari</button></a>
						<button type="button" class="filter-reset md-btn md-btn-small md-btn-danger md-btn-wave-light waves-effect waves-button waves-light">Reset</button>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	function send(){
        console.log('some');
		// var no_kontrak = $('input[name=no_kontrak]').val();
		// window.location = '<?php echo base_url('Akta/rollBack_action')?>/'+no_kontrak;
	}
</script>