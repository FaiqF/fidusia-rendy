<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akta extends MX_Controller {

    private $url        = 'akta';
    private $table_db   = 'akta_upload';
    private $prefix     = 'akta_';

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }


	public function index()
	{
        // pre($this->session->all_userdata()); exit();
        $data['name']       = "Akta";
        $data['url']        = base_url().$this->url;
        $data['plugin']     = ['datatables', 'datatables_fixcolumns','kendo_ui'];

		$this->template->display($this->url, $data);
    }

    public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 	 = [];
		$where   = null;
		$where_e = null;
        $where   = ['data_flag' => '4'];
		$search  = [
			'no_kontrak'   => 'data_no_kontrak',
			// 'path'   => $this->prefix.'path'
		];

		if (@$post['action'] == 'filter')
		{
			//$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = $where_e." AND DATE(updated_at) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
					}else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			//$where[$this->table_prefix.'status <>'] = '99';
		}
		$keys 		= array_keys($search);
		@$order 	= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

        $select		= '*';
        $count 		= $this->m_global->count_data_all('data_transaksi', $join, $where, $where_e );

		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


        $data = $this->m_global->get_data_all('data_transaksi', $join, $where, $select, $where_e, $order, $start, $length);
        $i = 1 + $start;

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
                $rows->data_no_kontrak,
                data_flag_status($rows->data_flag),
                $this->_button($rows->data_id)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
        $records["recordsFiltered"] = $count;

		echo json_encode($records);
    }

    public function show_add()
    {
        $data['name']   = 'Entri Data';
        $data['url']    = base_url().$this->url;
        $data['plugin'] = ['kendo_ui','dropify'];
        $this->template->display($this->url.'_add', $data);
    }

    public function show_data( $id )
    {
            $data['pdf'] = "";
            $data['name']    = 'Upload Akta';
            $data['url']     = base_url().$this->url;
            $data['plugin']  = ['kendo_ui','dropify','pdfobject' => 'js'];
            $data['id']      = $id;
    
            $joinQuery = [
                ['pdf_upload', 'data_pdf_id = pdf_id', 'left']
            ];
            $data['records'] = $this->m_global->get_data_all('data_transaksi', $joinQuery, [strEncrypt('data_id', true) => $id])[0];
    
            $noKontrak = $data['records']->data_no_kontrak;
            $name      = $data['records']->pdf_path;
            $filePath  = '/assets/uploads/data_konsumen/'.$noKontrak.'/'.$name;
            $fullPath  = FCPATH.$filePath;
            if( file_exists($fullPath) ){
                $data['pdf'] = base_url($filePath);
            }
    
            // pre($data['records']); exit;
            $this->template->display('akta_show', $data);
    }

    public function action_akta($id)
    {
        $post   = $this->input->post();

        if( $_FILES['akta_path']['size'] == 0 ){
            $result['msg'] = 'File tidak boleh kosong';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $filePath    = $_FILES['akta_path']['name'];
        $ext         = pathinfo($filePath, PATHINFO_EXTENSION);
        $allowedFile = [
            'docx',
            'doc'
        ];

        // validation if file not xls or xlsx
        if ( !in_array(@strtolower($ext), $allowedFile) ) {
            $result['msg'] = 'File harus bertipe docx atau doc';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $getData    = $this->m_global->get_data_all('data_transaksi', null, [strEncrypt('data_id', true) => $id])[0];
        $noKontrak  = $getData->data_no_kontrak;
        $dir        = FCPATH."/assets/uploads/akta/".$noKontrak;

        if( !is_dir($dir) ){
            mkdir($dir);
            chmod($dir, 0777);
        }

        $tempName  = $_FILES['akta_path']['tmp_name'];
        $dirUpload = $dir.'/';
        $fileName  = date('Y-m-d').'_'.user_data()->user_nik.'_'.rand().'.doc';

        // upload the file
        $doUpload = move_uploaded_file($tempName, $dirUpload.$fileName);

        if ( !$doUpload ){
            $result['msg'] = 'Gagal mengupload file';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $data['akta_nokontrak']    = $noKontrak;
        $data['akta_path']         = $fileName;
        $data['akta_createddate']  = date('Y-m-d H:i:s');
        $data['akta_createdby']    = user_data()->user_id;
        $query                     = $this->m_global->insert($this->table_db, $data);
        $idAkta                    = $this->db->insert_id();
        $paramDataTransaksi = [
            'data_akta_id' => $idAkta,
            'data_flag'    => '5',
            'updatedat'    => date('Y-m-d H:i:s'),
        ];

        $updateDataTransaksi = $this->m_global->update('data_transaksi', $paramDataTransaksi, [strEncrypt('data_id', true) => $id]);

        if( !$query ) {
            $result['msg'] = 'Data gagal diupdate !';
            $result['sts'] = 0;
            echo json_encode( $result ); exit;
        }

        $result['msg'] = 'Data berhasil diupdate !';
        $result['sts'] = 1;

        echo json_encode( $result ); exit();
    }

    public function action_download($nokontrak, $name)
    {
        $filepath = FCPATH . 'assets/uploads/akta/'.$nokontrak.'/'.$name;

        // Process download
        if(file_exists($filepath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($filepath));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            ob_clean();
            flush();
            readfile($filepath);
            exit;
        }
    }


    public function _button($id)
	{
        $button = null;
		$prev       = $id;
        $id         = strEncrypt($id);
        $getData = $this->m_global->get_data_all('data_transaksi', null, [strEncrypt('data_id', true) => $id]);

        $flag = $getData[0]->data_flag;
        $preview    = '<a data-uk-tooltip title="Preview" class="md-btn md-btn-primary md-btn-mini md-btn-wave-light md-btn-icon waves-effect waves-button waves-light ajaxify" href="'.base_url($this->url.'/show_data').'/'.$id.'"><i class="uk-icon-map"></i></a>';
        $prosesFinance = [
            '2','3','4'
        ];

        if( in_array($flag,$prosesFinance) ) {

            $button     = $preview;
            return $button;
        }

		return $button;
    }

}

/* End of file Sertifikat.php */
/* Location: ./application/modules/sertifikat/controllers/Sertifikat.php */