<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends MX_Controller 
{

	private $table_db       = 'akta'; //nama table
	private $table_prefix   = 'akta_'; //nama awal pada field
	private $url 			= 'transaksi';
	private $pagetitle		= 'Transaksi';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$data['name'] 		= $this->pagetitle;
		$data['url'] 		= base_url().$this->url;
		$data['plugin']		= ['datatables', 'datatables_fixcolumns','kendo_ui'];
		$this->template->display($this->url, $data);
	}

	public function select($sstatus = false)
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join 				= null;
		$where  			= null;
		$where_e 			= null;
		$where = [];

		$search 	= [
			"no_kontrak"	=> $this->table_prefix.'nomor'
		];

		if (@$post['action'] == 'filter')
		{
			//$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'akta_tanggal' )
					{
						$tmp = explode(' ', $post[$key]);
						$where_e = $where_e." AND DATE(akta_nomor) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
					} else if($key == 'akta_nomor') {
						$where_e = $where_e." AND ".$this->table_prefix.$key." IN($post[$key])";
					} else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			//$where[$this->table_prefix.'status <>'] = '99';
		}
		$keys 		= array_keys($search);
		@$order 	= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

		$select		= '*, '.implode(',', $search);
		$count 		= $this->m_global->count_data_all($this->table_db, $join, $where, $where_e );
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


		$data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, $where_e, $order, $start, $length);
		$i = 1 + $start;

		$status = [
					'0' => '<span class="uk-badge uk-badge-warning">InActive</span>',
					'1' => '<span class="uk-badge uk-badge-primary">Active</span>',
					'99' => '<span class="uk-badge uk-badge-danger">Soft Delete</span>'
				  ];

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
				date('d M Y', strtotime($rows->akta_tanggal)),
				$rows->akta_nomor,
				$rows->akta_jam_awal,
				$rows->akta_jam_akhir,
				$rows->akta_zona_waktu,
				$rows->akta_nama_notaris,
				$rows->akta_gelar_notaris,
				$rows->akta_domisili_notaris,
				$this->_button($rows->akta_id, true)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		echo json_encode($records);
	}

	public function _button($id, $show = false)
	{
		$prev = $id;
		$id = strEncrypt($id);

		$edit 	= '<a data-uk-tooltip title="Edit" class="ajaxify md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="'.base_url($this->url.'/show_edit').'/'.$id.'"><i class="uk-icon-pencil uk-icon-small"></i></a>';
		$delete = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(2, this, event);" href="'.base_url($this->url.'/action_delete/'.$id).'"><i class="uk-icon-trash uk-icon-small"></i></a>';
		return $edit.$delete;
	}

	public function show_add()
	{
		$data['name'] 		 = $this->pagetitle;
		$data['url'] 		 = base_url().$this->url;
		$data['breadcrumbs'] = ['Dashboard' => base_url('dashboard'), $this->pagetitle => base_url($this->url), 'Add' => base_url($this->url.'/show_add')];
		$data['plugin'] 	 = ['kendo_ui','dropify', 'inputmask'];

		$this->template->display($this->url.'_add', $data);
	}

	public function action_add()
	{
		$result = [];
		$post 	= $this->input->post();

		// validasi akta
		$this->form_validation->set_rules('akta_tanggal', 'Tanggal Akta', 'trim|required');
		$this->form_validation->set_rules('akta_jamawal', 'Jam Awal', 'trim|required');
		$this->form_validation->set_rules('akta_jamakhir', 'Jam Akhir', 'trim|required');
		$this->form_validation->set_rules('akta_zonawkt', 'Zona Waktu', 'trim|required');
		$this->form_validation->set_rules('akta_namantr', 'Nama Notaris', 'trim|required');
		$this->form_validation->set_rules('akta_gelarntr', 'Gelar Notaris', 'trim|required');
		$this->form_validation->set_rules('akta_kotkabntr', 'Domisili Notaris', 'trim|required');
		$this->form_validation->set_rules('akta_nomor', 'Nomor Akta Akta', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$result['sts'] = 0;
			$result['msg'] = validation_errors();

			echo json_encode($result);
			exit;
		}

		// tabel akta
		$data_tabel_akta 	= [
			// data akta
			'akta_tanggal'					=>	(!isset($post['akta_tanggal'])) ? null : date("Y-m-d", strtotime($post['akta_tanggal'])),
			'akta_jam_awal'					=>	$post['akta_jamawal'],
			'akta_jam_akhir'				=>	$post['akta_jamakhir'],
			'akta_zona_waktu'				=>	$post['akta_zonawkt'],
			'akta_nama_notaris'				=>	$post['akta_namantr'],
			'akta_gelar_notaris'			=>	$post['akta_gelarntr'],
			'akta_domisili_notaris'			=>	$post['akta_kotkabntr'],
			'akta_nomor'					=>	$post['akta_nomor'],
			//	data leasing
			'akta_pejabat_panggilan'		=>	$post['leasing_panggilan'],
			'akta_pejabat_nama'				=>	$post['leasing_namapjbt'],
			'akta_pejabat_tempat_lahir'		=>	$post['leasing_tmptlahir'],
			'akta_pejabat_tanggal_lahir'	=>	(!isset($post['leasing_tgllahir'])) ? null : date("Y-m-d", strtotime($post['leasing_tgllahir'])),
			'akta_pejabat_kewarganegaraan'	=>	$post['leasing_kewarganegaraan'],
			'akta_pejabat_pekerjaan'		=>	$post['leasing_pekerjaan'],
			'akta_pejabat_alamat'			=>	$post['leasing_alamat'],
			'akta_pejabat_kelurahan'		=>	$post['leasing_kelurahan'],
			'akta_pejabat_kecamatan'		=>	$post['leasing_kecamatan'],
			'akta_pejabat_kabupaten'		=>	$post['leasing_kotakab'],
			'akta_pejabat_provinsi'			=>	$post['leasing_propinsi'],
			'akta_pejabat_jenis_identitas'	=>	$post['leasing_jnsidtts'],
			'akta_pejabat_nik'				=>	$post['leasing_nik'],
			'akta_pejabat_tanggal_kuasa'	=>	$post['leasing_tglkuasa'],
			'akta_pejabat_no_kuasa'			=>	$post['leasing_nokuasa'],
			// data notaris satu
			'akta_saksi1_panggilan'			=>	$post['notsatu_panggilan'],
			'akta_saksi1_nama'				=>	$post['notsatu_nama'],
			'akta_saksi1_tempat_lahir'		=>	$post['notsatu_tmpt_lahir'],
			'akta_saksi1_tanggal_lahir'		=>	(!isset($post['notsatu_tgl_lahir'])) ? null : date("Y-m-d", strtotime($post['notsatu_tgl_lahir'])),
			'akta_saksi1_kewarganegaraan'	=>	$post['notsatu_kewarganegaraan'],
			'akta_saksi1_pekerjaan'			=>	$post['notsatu_pekerjaan'],
			'akta_saksi1_alamat'			=>	$post['notsatu_alamat'],
			'akta_saksi1_rt'				=>	$post['notsatu_rt'],
			'akta_saksi1_rw'				=>	$post['notsatu_rw'],
			'akta_saksi1_kelurahan'			=>	$post['notsatu_kelurahan'],
			'akta_saksi1_kecamatan'			=>	$post['notsatu_kecamatan'],
			'akta_saksi1_kabupaten'			=>	$post['notsatu_kota_kab'],
			'akta_saksi1_jenis_identitas'	=>	$post['notsatu_jnsidentitas'],
			'akta_saksi1_nik'				=>	$post['notsatu_nik'],
			// data notaris dua
			'akta_saksi2_panggilan'			=>	$post['notdua_panggilan'],
			'akta_saksi2_nama'				=>	$post['notdua_nama'],
			'akta_saksi2_tempat_lahir'		=>	$post['notdua_tmpt_lahir'],
			'akta_saksi2_tanggal_lahir'		=>	(!isset($post['notdua_tgl_lahir'])) ? null : date("Y-m-d", strtotime($post['notdua_tgl_lahir'])),
			'akta_saksi2_kewarganegaraan'	=>	$post['notdua_kewarganegaraan'],
			'akta_saksi2_pekerjaan'			=>	$post['notdua_pekerjaan'],
			'akta_saksi2_alamat'			=>	$post['notdua_alamat'],
			'akta_saksi2_rt'				=>	$post['notdua_rt'],
			'akta_saksi2_rw'				=>	$post['notdua_rw'],
			'akta_saksi2_kelurahan'			=>	$post['notdua_kelurahan'],
			'akta_saksi2_kecamatan'			=>	$post['notdua_kecamatan'],
			'akta_saksi2_kabupaten'			=>	$post['notdua_kota_kab'],
			'akta_saksi2_jenis_identitas'	=>	$post['notdua_jnsidentitas'],
			'akta_saksi2_nik'				=>	$post['notdua_nik'],
		];
		// tabel perjanjian pembiayaan
		$data_tabel_perjanjian 	= [
			'perjanjian_tanggal_skpjf' 		=> (!isset($post['perjanjian_tgl_skpjf'])) ? null : date("Y-m-d", strtotime($post['perjanjian_tgl_skpjf'])),
			'perjanjian_no_kontrak' 		=> $post['perjanjian_nokontrak'],
			'perjanjian_tanggal_pk' 		=> (!isset($post['perjanjian_tgljual_pk'])) ? null : date("Y-m-d", strtotime($post['perjanjian_tgljual_pk'])),
			'perjanjian_tanggal_akhir' 		=> (!isset($post['perjanjian_tglakhir_jual'])) ? null : date("Y-m-d", strtotime($post['perjanjian_tglakhir_jual'])),
			'perjanjian_tenor_pk' 			=> $post['perjanjian_tenorpk'],
			'perjanjian_total_hutang' 		=> $post['perjanjian_tothtgpokok'],
			'perjanjian_nilai_penjaminan' 	=> $post['perjanjian_nilaipenjaminan'],
			'perjanjian_tanggal_penagihan' 	=> (!isset($post['perjanjian_tglpnghn_leasing'])) ? null : date("Y-m-d", strtotime($post['perjanjian_tglpnghn_leasing'])),
			'perjanjian_is_asuransi' 		=> $post['perjanjian_asuransi'],
			'perjanjian_jenis_penggunaan'	=> $post['perjanjian_jnspenggunaan'],
			'perjanjian_jenis_pembiayaan'	=> $post['perjanjian_jnspembiayaan'],
			'perjanjian_berdasarkan' 		=> $post['perjanjian_bdsperjanjian'],
		];
		//	tabel data kendaraan
		$data_tabel_kendaraan 	= [
			// objek jaminan
			'dk_obyek_jaminan' 				=> $post['objek_jaminan'],
			'dk_merk' 						=> $post['objek_merk'],
			'dk_type' 						=> $post['objek_tipe'],
			'dk_jenis' 						=> $post['objek_jenis'],
			'dk_model' 						=> $post['objek_model'],
			'dk_tahun_pembuatan' 			=> $post['objek_thnpembuatan'],
			'dk_warna' 						=> $post['objek_warna'],
			'dk_no_rangka' 					=> $post['objek_norangka'],
			'dk_no_mesin' 					=> $post['objek_nomesin'],
			'dk_jenis_bukti' 				=> $post['objek_jnbukti'],
			'dk_no_bukti_bpkb' 				=> $post['objek_nobpkb'],
			'dk_kalimat_bukti' 				=> $post['objek_kalimatbukti'],
			'dk_nilai_obyek' 				=> $post['objek_nilai'],
			'dk_tanggal_bukti' 				=> (!isset($post['objek_tglbukti'])) ? null : date("Y-m-d", strtotime($post['objek_tglbukti'])),
			'dk_no_bukti_faktur' 			=> $post['objek_nobukti'],
			'dk_pemasok' 					=> $post['objek_pemasok'],
			'dk_no_polisi' 					=> $post['objek_nopolisi'],
			'dk_no_bpkb' 					=> $post['objek_nobpkb'],
			'dk_tanggal_surat_penyerahan' 	=> (!isset($post['objek_tglsrtserah'])) ? null : date("Y-m-d", strtotime($post['objek_tglsrtserah'])),
			'dk_sp_bpkb' 					=> $post['objek_sp_bpkb'],
			'dk_nama_pemilik' 				=> $post['objek_nama_pemilik'],
			'dk_alamat_pemilik' 			=> $post['objek_alamat_pemilik'],
		];
		//	tabel pemberi fidusia
		$data_tabel_fidusia 	= [
			//	data pemberi fidusia
			'pemberi_panggilan'						=>	$post['fidusia_panggilan'],
			'pemberi_nama'							=>	$post['fidusia_nama'],
			'pemberi_jenis_kelamin'					=>	(empty($post['fidusia_jenkel'])) ? NULL : $post['fidusia_jenkel'],
			'pemberi_tempat_lahir'					=>	$post['fidusia_tmpt_lahir'],
			'pemberi_tanggal_lahir'					=>	(empty($post['fidusia_tgl_lahir'])) ? NULL : date("Y-m-d", strtotime($post['fidusia_tgl_lahir'])),
			'pemberi_agama'							=>	$post['fidusia_agama'],
			'pemberi_kewarganegaraan'				=>	$post['fidusia_kewarganegaraan'],
			'pemberi_pekerjaan'						=>	$post['fidusia_pekerjaan'],
			'pemberi_alamat'						=>	$post['fidusia_alamat'],
			'pemberi_rt'							=>	$post['fidusia_rt'],
			'pemberi_rw'							=>	$post['fidusia_rw'],
			'pemberi_desa'							=>	$post['fidusia_desa'],
			'pemberi_kelurahan'						=>	$post['fidusia_kelurahan'],
			'pemberi_kecamatan'						=>	$post['fidusia_kecamatan'],
			'pemberi_kabupaten'						=>	$post['fidusia_kota_kab'],
			'pemberi_kabupaten_kode'				=>	$post['fidusia_kode_kotakab'],
			'pemberi_provinsi'						=>	$post['fidusia_propinsi'],
			'pemberi_provinsi_kode'					=>	$post['fidusia_kodepropinsi'],
			'pemberi_kode_pos'						=>	$post['fidusia_kodepos'],
			'pemberi_telp'							=>	$post['fidusia_notelp'],
			'pemberi_jenis_identitas'				=>	$post['fidusia_jnsidentitas'],
			'pemberi_nik'							=>	$post['fidusia_nik'],
			'pemberi_ktp_berlaku'					=>	$post['fidusia_kibh'],
			//	data pemberi status pernikahan
			'pemberi_status_pernikahan'				=>	$post['psp_status'],
			'pemberi_surat_pengadilan'				=>	$post['psp_surat_pengadilan'],
			'pemberi_surat_cerai'					=>	$post['psp_surat_cerai'],
			'pemberi_tanggal_dokumen'				=>	(empty($post['psp_thnpembuatan'])) ? NULL : date("Y-m-d", strtotime($post['psp_thnpembuatan'])),
			'pemberi_lokasi_penerbit'				=>	$post['psp_lokasi_penerbit'],
			'pemberi_uraian_status'					=>	$post['psp_urstt_cerai'],
			//	data pasangan
			'pemberi_status_pasangan'				=>	$post['pasangan_sttsutri'],
			'pemberi_tanggal_persetujuan_pasangan'	=>	(empty($post['pasangan_tglsetuju'])) ? NULL : date("Y-m-d", strtotime($post['pasangan_tglsetuju'])),
			'pemberi_panggilan_pasangan'			=>	$post['pasangan_panggilan'],
			'pemberi_nama_pasangan'					=>	$post['pasangan_nama'],
			'pemberi_tempat_lahir_pasangan'			=>	$post['pasangan_tmpt_lahir'],
			'pemberi_tanggal_lahir_pasangan'		=>	(empty($post['pasangan_tgl_lahir'])) ? NULL : date("Y-m-d", strtotime($post['pasangan_tgl_lahir'])),
			'pemberi_agama_pasangan'				=>	$post['pasangan_agama'],
			'pemberi_kewarganegaraan_pasangan'		=>	$post['pasangan_kewarganegaraan'],
			'pemberi_pekerjaan_pasangan'			=>	$post['pasangan_pekerjaan'],
			'pemberi_alamat_pasangan'				=>	$post['pasangan_alamat'],
			'pemberi_rt_pasangan'					=>	$post['pasangan_rt'],
			'pemberi_rw_pasangan'					=>	$post['pasangan_rw'],
			'pemberi_desa_pasangan'					=>	$post['pasangan_desa'],
			'pemberi_kelurahan_pasangan'			=>	$post['pasangan_kelurahan'],
			'pemberi_kecamatan_pasangan'			=>	$post['pasangan_kecamatan'],
			'pemberi_kabupaten_pasangan'			=>	$post['pasangan_kota_kab'],
			'pemberi_jenis_identitas_pasangan'		=>	$post['pasangan_jnsidentitas'],
			'pemberi_nik_pasangan'					=>	$post['pasangan_nik'],
			'pemberi_ktp_berlaku_pasangan'			=>	$post['pasangan_kibh'],
			// data debitur
			'pemberi_status_hubungan_debitur'		=>	$post['debitur_sttsutri'],
			'pemberi_panggilan_debitur'				=>	$post['debitur_panggilan'],
			'pemberi_nama_debitur'					=>	$post['debitur_nama'],
			'pemberi_tempat_lahir_debitur'			=>	$post['debitur_tmpt_lahir'],
			'pemberi_tanggal_lahir_debitur'			=>	(empty($post['debitur_tgl_lahir'])) ? NULL : date("Y-m-d", strtotime($post['debitur_tgl_lahir'])),
			'pemberi_agama_debitur'					=>	$post['debitur_agama'],
			'pemberi_kewarganegaraan_debitur'		=>	$post['debitur_kewarganegaraan'],
			'pemberi_pekerjaan_debitur'				=>	$post['debitur_pekerjaan'],
			'pemberi_alamat_debitur'				=>	$post['debitur_alamat'],
			'pemberi_rt_debitur'					=>	$post['debitur_rt'],
			'pemberi_rw_debitur'					=>	$post['debitur_rw'],
			'pemberi_desa_debitur'					=>	$post['debitur_desa'],
			'pemberi_kelurahan_debitur'				=>	$post['debitur_kelurahan'],
			'pemberi_kecamatan_debitur'				=>	$post['debitur_kecamatan'],
			'pemberi_kabupaten_debitur'				=>	$post['debitur_kota_kab'],
			'pemberi_jenis_identitas_debitur'		=>	$post['debitur_jnsidentitas'],
			'pemberi_nik_debitur'					=>	$post['debitur_nik'],
			'pemberi_ktp_berlaku_debitur'			=>	$post['debitur_kibh'],
		];

		// insert akta
		$insert_akta		= $this->m_global->insert('akta', $data_tabel_akta);
		// insert perjanjian pembayaran
		$insert_perjanjian	= $this->m_global->insert('perjanjian_pembiayaan', $data_tabel_perjanjian);
		// insert data kendaraan
		$insert_kendaraan 	= $this->m_global->insert('data_kendaraan', $data_tabel_kendaraan);
		// insert pemberi fidusia
		$insert_fidusia		= $this->m_global->insert('pemberi_fidusia', $data_tabel_fidusia);

		if ($insert_akta && $insert_perjanjian && $insert_kendaraan && $insert_fidusia) {
			$result['msg'] = 'Data Berhasil Dimasukkan';
			$result['inp'] = '2';
		} else {
			$result['msg'] = 'Data Gagal Dimasukkan';
			$result['inp'] = '0';
		}

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		}
		else{
			$this->db->trans_commit();
		}

		echo json_encode($result);
	}

	public function show_edit($id)
	{
		$data['name'] 		 	= $this->pagetitle;
		$data['url'] 		 	= base_url().$this->url;
		$data['breadcrumbs'] 	= ['Dashboard' => base_url('dashboard'), $this->pagetitle => base_url($this->url), 'Add' => base_url($this->url.'/show_add')];
		$data['plugin'] 	 	= ['kendo_ui','dropify', 'inputmask'];
		$data['id']				= $id;
		// get data
		$data['get_akta']		= $this->m_global->get_data_all('akta', null, [strEncrypt('akta_id', 1) => $id])[0];
		$data['get_perjanjian']	= $this->m_global->get_data_all('perjanjian_pembiayaan', null, [strEncrypt('perjanjian_id', 1) => $id])[0];
		$data['get_kendaraan']	= $this->m_global->get_data_all('data_kendaraan', null, [strEncrypt('dk_id', 1) => $id])[0];
		$data['get_fidusia']	= $this->m_global->get_data_all('pemberi_fidusia', null, [strEncrypt('pemberi_id', 1) => $id])[0];

		// pre($data['get_kendaraan'], 1);

		$this->template->display($this->url.'_edit', $data);
	}

	public function action_edit($id){
		$result = [];
		$post 	= $this->input->post();

		// validasi akta
		$this->form_validation->set_rules('akta_tanggal', 'Tanggal Akta', 'trim|required');
		$this->form_validation->set_rules('akta_jamawal', 'Jam Awal', 'trim|required');
		$this->form_validation->set_rules('akta_jamakhir', 'Jam Akhir', 'trim|required');
		$this->form_validation->set_rules('akta_zonawkt', 'Zona Waktu', 'trim|required');
		$this->form_validation->set_rules('akta_namantr', 'Nama Notaris', 'trim|required');
		$this->form_validation->set_rules('akta_gelarntr', 'Gelar Notaris', 'trim|required');
		$this->form_validation->set_rules('akta_kotkabntr', 'Domisili Notaris', 'trim|required');
		$this->form_validation->set_rules('akta_nomor', 'Nomor Akta Akta', 'trim|required');


		if ($this->form_validation->run() == FALSE) {
			$result['sts'] = 0;
			$result['msg'] = validation_errors();

			echo json_encode($result);
			exit;
		}

		// tabel akta
		$data_tabel_akta 	= [
			// data akta
			'akta_tanggal'					=>	(!isset($post['akta_tanggal'])) ? null : date("Y-m-d", strtotime($post['akta_tanggal'])),
			'akta_jam_awal'					=>	$post['akta_jamawal'],
			'akta_jam_akhir'				=>	$post['akta_jamakhir'],
			'akta_zona_waktu'				=>	$post['akta_zonawkt'],
			'akta_nama_notaris'				=>	$post['akta_namantr'],
			'akta_gelar_notaris'			=>	$post['akta_gelarntr'],
			'akta_domisili_notaris'			=>	$post['akta_kotkabntr'],
			'akta_nomor'					=>	$post['akta_nomor'],
			//	data leasing
			'akta_pejabat_panggilan'		=>	$post['leasing_panggilan'],
			'akta_pejabat_nama'				=>	$post['leasing_namapjbt'],
			'akta_pejabat_tempat_lahir'		=>	$post['leasing_tmptlahir'],
			'akta_pejabat_tanggal_lahir'	=>	(!isset($post['leasing_tgllahir'])) ? null : date("Y-m-d", strtotime($post['leasing_tgllahir'])),
			'akta_pejabat_kewarganegaraan'	=>	$post['leasing_kewarganegaraan'],
			'akta_pejabat_pekerjaan'		=>	$post['leasing_pekerjaan'],
			'akta_pejabat_alamat'			=>	$post['leasing_alamat'],
			'akta_pejabat_kelurahan'		=>	$post['leasing_kelurahan'],
			'akta_pejabat_kecamatan'		=>	$post['leasing_kecamatan'],
			'akta_pejabat_kabupaten'		=>	$post['leasing_kotakab'],
			'akta_pejabat_provinsi'			=>	$post['leasing_propinsi'],
			'akta_pejabat_jenis_identitas'	=>	$post['leasing_jnsidtts'],
			'akta_pejabat_nik'				=>	$post['leasing_nik'],
			'akta_pejabat_tanggal_kuasa'	=>	$post['leasing_tglkuasa'],
			'akta_pejabat_no_kuasa'			=>	$post['leasing_nokuasa'],
			// data notaris satu
			'akta_saksi1_panggilan'			=>	$post['notsatu_panggilan'],
			'akta_saksi1_nama'				=>	$post['notsatu_nama'],
			'akta_saksi1_tempat_lahir'		=>	$post['notsatu_tmpt_lahir'],
			'akta_saksi1_tanggal_lahir'		=>	(!isset($post['notsatu_tgl_lahir'])) ? null : date("Y-m-d", strtotime($post['notsatu_tgl_lahir'])),
			'akta_saksi1_kewarganegaraan'	=>	$post['notsatu_kewarganegaraan'],
			'akta_saksi1_pekerjaan'			=>	$post['notsatu_pekerjaan'],
			'akta_saksi1_alamat'			=>	$post['notsatu_alamat'],
			'akta_saksi1_rt'				=>	$post['notsatu_rt'],
			'akta_saksi1_rw'				=>	$post['notsatu_rw'],
			'akta_saksi1_kelurahan'			=>	$post['notsatu_kelurahan'],
			'akta_saksi1_kecamatan'			=>	$post['notsatu_kecamatan'],
			'akta_saksi1_kabupaten'			=>	$post['notsatu_kota_kab'],
			'akta_saksi1_jenis_identitas'	=>	$post['notsatu_jnsidentitas'],
			'akta_saksi1_nik'				=>	$post['notsatu_nik'],
			// data notaris dua
			'akta_saksi2_panggilan'			=>	$post['notdua_panggilan'],
			'akta_saksi2_nama'				=>	$post['notdua_nama'],
			'akta_saksi2_tempat_lahir'		=>	$post['notdua_tmpt_lahir'],
			'akta_saksi2_tanggal_lahir'		=>	(!isset($post['notdua_tgl_lahir'])) ? null : date("Y-m-d", strtotime($post['notdua_tgl_lahir'])),
			'akta_saksi2_kewarganegaraan'	=>	$post['notdua_kewarganegaraan'],
			'akta_saksi2_pekerjaan'			=>	$post['notdua_pekerjaan'],
			'akta_saksi2_alamat'			=>	$post['notdua_alamat'],
			'akta_saksi2_rt'				=>	$post['notdua_rt'],
			'akta_saksi2_rw'				=>	$post['notdua_rw'],
			'akta_saksi2_kelurahan'			=>	$post['notdua_kelurahan'],
			'akta_saksi2_kecamatan'			=>	$post['notdua_kecamatan'],
			'akta_saksi2_kabupaten'			=>	$post['notdua_kota_kab'],
			'akta_saksi2_jenis_identitas'	=>	$post['notdua_jnsidentitas'],
			'akta_saksi2_nik'				=>	$post['notdua_nik'],
		];
		// tabel perjanjian pembiayaan
		$data_tabel_perjanjian 	= [
			'perjanjian_tanggal_skpjf' 		=> (!isset($post['perjanjian_tgl_skpjf'])) ? null : date("Y-m-d", strtotime($post['perjanjian_tgl_skpjf'])),
			'perjanjian_no_kontrak' 		=> $post['perjanjian_nokontrak'],
			'perjanjian_tanggal_pk' 		=> (!isset($post['perjanjian_tgljual_pk'])) ? null : date("Y-m-d", strtotime($post['perjanjian_tgljual_pk'])),
			'perjanjian_tanggal_akhir' 		=> (!isset($post['perjanjian_tglakhir_jual'])) ? null : date("Y-m-d", strtotime($post['perjanjian_tglakhir_jual'])),
			'perjanjian_tenor_pk' 			=> $post['perjanjian_tenorpk'],
			'perjanjian_total_hutang' 		=> $post['perjanjian_tothtgpokok'],
			'perjanjian_nilai_penjaminan' 	=> $post['perjanjian_nilaipenjaminan'],
			'perjanjian_tanggal_penagihan' 	=> (!isset($post['perjanjian_tglpnghn_leasing'])) ? null : date("Y-m-d", strtotime($post['perjanjian_tglpnghn_leasing'])),
			'perjanjian_is_asuransi' 		=> $post['perjanjian_asuransi'],
			'perjanjian_jenis_penggunaan'	=> $post['perjanjian_jnspenggunaan'],
			'perjanjian_jenis_pembiayaan'	=> $post['perjanjian_jnspembiayaan'],
			'perjanjian_berdasarkan' 		=> $post['perjanjian_bdsperjanjian'],
		];
		//	tabel data kendaraan
		$data_tabel_kendaraan 	= [
			// objek jaminan
			'dk_obyek_jaminan' 				=> $post['objek_jaminan'],
			'dk_merk' 						=> $post['objek_merk'],
			'dk_type' 						=> $post['objek_tipe'],
			'dk_jenis' 						=> $post['objek_jenis'],
			'dk_model' 						=> $post['objek_model'],
			'dk_tahun_pembuatan' 			=> $post['objek_thnpembuatan'],
			'dk_warna' 						=> $post['objek_warna'],
			'dk_no_rangka' 					=> $post['objek_norangka'],
			'dk_no_mesin' 					=> $post['objek_nomesin'],
			'dk_jenis_bukti' 				=> $post['objek_jnbukti'],
			'dk_no_bukti_bpkb' 				=> $post['objek_nobpkb'],
			'dk_kalimat_bukti' 				=> $post['objek_kalimatbukti'],
			'dk_nilai_obyek' 				=> $post['objek_nilai'],
			'dk_tanggal_bukti' 				=> (!isset($post['objek_tglbukti'])) ? null : date("Y-m-d", strtotime($post['objek_tglbukti'])),
			'dk_no_bukti_faktur' 			=> $post['objek_nobukti'],
			'dk_pemasok' 					=> $post['objek_pemasok'],
			'dk_no_polisi' 					=> $post['objek_nopolisi'],
			'dk_no_bpkb' 					=> $post['objek_nobpkb'],
			'dk_tanggal_surat_penyerahan' 	=> (!isset($post['objek_tglsrtserah'])) ? null : date("Y-m-d", strtotime($post['objek_tglsrtserah'])),
			'dk_sp_bpkb' 					=> $post['objek_sp_bpkb'],
			'dk_nama_pemilik' 				=> $post['objek_nama_pemilik'],
			'dk_alamat_pemilik' 			=> $post['objek_alamat_pemilik'],
		];
		//	tabel pemberi fidusia
		$data_tabel_fidusia 	= [
			//	data pemberi fidusia
			'pemberi_panggilan'						=>	$post['fidusia_panggilan'],
			'pemberi_nama'							=>	$post['fidusia_nama'],
			'pemberi_jenis_kelamin'					=>	(empty($post['fidusia_jenkel'])) ? NULL : $post['fidusia_jenkel'],
			'pemberi_tempat_lahir'					=>	$post['fidusia_tmpt_lahir'],
			'pemberi_tanggal_lahir'					=>	(empty($post['fidusia_tgl_lahir'])) ? NULL : date("Y-m-d", strtotime($post['fidusia_tgl_lahir'])),
			'pemberi_agama'							=>	$post['fidusia_agama'],
			'pemberi_kewarganegaraan'				=>	$post['fidusia_kewarganegaraan'],
			'pemberi_pekerjaan'						=>	$post['fidusia_pekerjaan'],
			'pemberi_alamat'						=>	$post['fidusia_alamat'],
			'pemberi_rt'							=>	$post['fidusia_rt'],
			'pemberi_rw'							=>	$post['fidusia_rw'],
			'pemberi_desa'							=>	$post['fidusia_desa'],
			'pemberi_kelurahan'						=>	$post['fidusia_kelurahan'],
			'pemberi_kecamatan'						=>	$post['fidusia_kecamatan'],
			'pemberi_kabupaten'						=>	$post['fidusia_kota_kab'],
			'pemberi_kabupaten_kode'				=>	$post['fidusia_kode_kotakab'],
			'pemberi_provinsi'						=>	$post['fidusia_propinsi'],
			'pemberi_provinsi_kode'					=>	$post['fidusia_kodepropinsi'],
			'pemberi_kode_pos'						=>	$post['fidusia_kodepos'],
			'pemberi_telp'							=>	$post['fidusia_notelp'],
			'pemberi_jenis_identitas'				=>	$post['fidusia_jnsidentitas'],
			'pemberi_nik'							=>	$post['fidusia_nik'],
			'pemberi_ktp_berlaku'					=>	$post['fidusia_kibh'],
			//	data pemberi status pernikahan
			'pemberi_status_pernikahan'				=>	$post['psp_status'],
			'pemberi_surat_pengadilan'				=>	$post['psp_surat_pengadilan'],
			'pemberi_surat_cerai'					=>	$post['psp_surat_cerai'],
			'pemberi_tanggal_dokumen'				=>	(empty($post['psp_thnpembuatan'])) ? NULL : date("Y-m-d", strtotime($post['psp_thnpembuatan'])),
			'pemberi_lokasi_penerbit'				=>	$post['psp_lokasi_penerbit'],
			'pemberi_uraian_status'					=>	$post['psp_urstt_cerai'],
			//	data pasangan
			'pemberi_status_pasangan'				=>	$post['pasangan_sttsutri'],
			'pemberi_tanggal_persetujuan_pasangan'	=>	(empty($post['pasangan_tglsetuju'])) ? NULL : date("Y-m-d", strtotime($post['pasangan_tglsetuju'])),
			'pemberi_panggilan_pasangan'			=>	$post['pasangan_panggilan'],
			'pemberi_nama_pasangan'					=>	$post['pasangan_nama'],
			'pemberi_tempat_lahir_pasangan'			=>	$post['pasangan_tmpt_lahir'],
			'pemberi_tanggal_lahir_pasangan'		=>	(empty($post['pasangan_tgl_lahir'])) ? NULL : date("Y-m-d", strtotime($post['pasangan_tgl_lahir'])),
			'pemberi_agama_pasangan'				=>	$post['pasangan_agama'],
			'pemberi_kewarganegaraan_pasangan'		=>	$post['pasangan_kewarganegaraan'],
			'pemberi_pekerjaan_pasangan'			=>	$post['pasangan_pekerjaan'],
			'pemberi_alamat_pasangan'				=>	$post['pasangan_alamat'],
			'pemberi_rt_pasangan'					=>	$post['pasangan_rt'],
			'pemberi_rw_pasangan'					=>	$post['pasangan_rw'],
			'pemberi_desa_pasangan'					=>	$post['pasangan_desa'],
			'pemberi_kelurahan_pasangan'			=>	$post['pasangan_kelurahan'],
			'pemberi_kecamatan_pasangan'			=>	$post['pasangan_kecamatan'],
			'pemberi_kabupaten_pasangan'			=>	$post['pasangan_kota_kab'],
			'pemberi_jenis_identitas_pasangan'		=>	$post['pasangan_jnsidentitas'],
			'pemberi_nik_pasangan'					=>	$post['pasangan_nik'],
			'pemberi_ktp_berlaku_pasangan'			=>	$post['pasangan_kibh'],
			// data debitur
			'pemberi_status_hubungan_debitur'		=>	$post['debitur_sttsutri'],
			'pemberi_panggilan_debitur'				=>	$post['debitur_panggilan'],
			'pemberi_nama_debitur'					=>	$post['debitur_nama'],
			'pemberi_tempat_lahir_debitur'			=>	$post['debitur_tmpt_lahir'],
			'pemberi_tanggal_lahir_debitur'			=>	(empty($post['debitur_tgl_lahir'])) ? NULL : date("Y-m-d", strtotime($post['debitur_tgl_lahir'])),
			'pemberi_agama_debitur'					=>	$post['debitur_agama'],
			'pemberi_kewarganegaraan_debitur'		=>	$post['debitur_kewarganegaraan'],
			'pemberi_pekerjaan_debitur'				=>	$post['debitur_pekerjaan'],
			'pemberi_alamat_debitur'				=>	$post['debitur_alamat'],
			'pemberi_rt_debitur'					=>	$post['debitur_rt'],
			'pemberi_rw_debitur'					=>	$post['debitur_rw'],
			'pemberi_desa_debitur'					=>	$post['debitur_desa'],
			'pemberi_kelurahan_debitur'				=>	$post['debitur_kelurahan'],
			'pemberi_kecamatan_debitur'				=>	$post['debitur_kecamatan'],
			'pemberi_kabupaten_debitur'				=>	$post['debitur_kota_kab'],
			'pemberi_jenis_identitas_debitur'		=>	$post['debitur_jnsidentitas'],
			'pemberi_nik_debitur'					=>	$post['debitur_nik'],
			'pemberi_ktp_berlaku_debitur'			=>	$post['debitur_kibh'],
		];

		$this->db->trans_begin();

		// update akta
		$update_akta		= $this->m_global->update('akta', $data_tabel_akta, [ StrEncrypt('akta_id', 1) => $id]);
		// update perjanjian pembiayaan
		$update_perjanjian	= $this->m_global->update('perjanjian_pembiayaan', $data_tabel_perjanjian, [ StrEncrypt('perjanjian_id', 1) => $id]);
		// update data kendaraan
		$update_kendaraan	= $this->m_global->update('data_kendaraan', $data_tabel_kendaraan, [ StrEncrypt('dk_id', 1) => $id]);
		// update pemberi fidusia
		$update_fidusia		= $this->m_global->update('pemberi_fidusia', $data_tabel_fidusia, [ StrEncrypt('pemberi_id', 1) => $id]);

		if ($update_akta && $update_perjanjian && $update_kendaraan && $update_fidusia) {
			$result['msg'] = 'Data Berhasil Diubah';
			$result['inp'] = '2';
		} else {
			$result['msg'] = 'Data Gagal Diubah';
			$result['inp'] = '0';
		}

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}

		echo json_encode($result);
	}

	public function action_delete($id)
	{
		$akta		= $this->m_global->delete('akta', [strEncrypt('akta_id', 1) => $id]);
		$perjanjian	= $this->m_global->delete('perjanjian_pembiayaan', [strEncrypt('perjanjian_id', 1) => $id]);
		$kendaraan	= $this->m_global->delete('data_kendaraan', [strEncrypt('dk_id', 1) => $id]);
		$fidusia	= $this->m_global->delete('pemberi_fidusia', [strEncrypt('pemberi_id', 1) => $id]);

		if($akta && $perjanjian && $kendaraan && $fidusia) {
            $result['msg'] = 'Data berhasil dihapus !';
            $result['status'] = 1;
        } else {
            $result['msg'] = 'Data gagal dihapus !';
            $result['status'] = 0;
        }

        echo json_encode($result);

	}

}
/* End of file Transaksi.php */
/* Location: ./application/modules/transaksi/controllers/Transaksi.php */
