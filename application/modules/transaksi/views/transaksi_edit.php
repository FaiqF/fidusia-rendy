<div class="uk-grid">
	<div class="uk-width-medium-1">
		<div class="uk-row-first">
			<form method="post" action="<?=$url; ?>/action_edit/<?=$id?>" id="form_edit" enctype="multipart/form-data">
				<div class="md-card">
					<div class="md-card-toolbar">
						<h3 class="md-card-toolbar-heading-text">Edit <?php echo $name ?></h3>
					</div>
				</div>
				<div class="uk-grid uk-grid-medium uk-grid-width-medium-1-2">
					<div class="uk-grid-margin uk-row-first">
						<div class="md-card md-card-primary">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Akta</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Akta <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="akta_tanggal" type="text" class="datepicker" value="<?php echo $get_akta->akta_tanggal ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Jam Akta (Awal) <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="akta_jamawal" type="text" class="uk-form-width-medium timepicker" value="<?php echo $get_akta->akta_jam_awal ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Jam Akta (Akhir) <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="akta_jamakhir" type="text" class="uk-form-width-medium timepicker" value="<?php echo $get_akta->akta_jam_akhir ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_zona_waktu))?'filled':'wrapper'?>">
												<label>Zona Waktu <span class="required">*</span></label>
												<input name="akta_zonawkt" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_zona_waktu ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_nama_notaris))?'filled':'wrapper'?>">
												<label>Nama Notaris <span class="required">*</span></label>
												<input name="akta_namantr" type="text" class="md-input uk-form-width-large" value="<?php echo $get_akta->akta_nama_notaris ?>">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_gelar_notaris))?'filled':'wrapper'?>">
												<label>Gelar <span class="required">*</span></label>
												<input name="akta_gelarntr" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_gelar_notaris ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_domisili_notaris))?'filled':'wrapper'?>">
												<label>Kota/Kab Notaris <span class="required">*</span></label>
												<input name="akta_kotkabntr" type="text" class="md-input uk-form-width-large" value="<?php echo $get_akta->akta_domisili_notaris ?>">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_nomor))?'filled':'wrapper'?>">
												<label>Nomor Akta <span class="required">*</span></label>
												<input name="akta_nomor" type="text" class="md-input uk-form-width-large" value="<?php echo $get_akta->akta_nomor ?>">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin">
						<div class="md-card md-card-success" style="padding-bottom: 15px;">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Leasing</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_pejabat_panggilan))?'filled':'wrapper'?>">
												<label>Panggilan <span class="required">*</span></label>
												<input name="leasing_panggilan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_pejabat_panggilan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_pejabat_nama))?'filled':'wrapper'?>">
												<label>Nama Pejabat <span class="required">*</span></label>
												<input name="leasing_namapjbt" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_pejabat_nama ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_pejabat_tempat_lahir))?'filled':'wrapper'?>">
												<label>Tempat Lahir <span class="required">*</span></label>
												<input name="leasing_tmptlahir" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_pejabat_tempat_lahir ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Lahir <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="leasing_tgllahir" type="text" class="datepicker" value="<?php echo $get_akta->akta_pejabat_tanggal_lahir ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_pejabat_kewarganegaraan))?'filled':'wrapper'?>">
												<label>Kewarganegaraan <span class="required">*</span></label>
												<input name="leasing_kewarganegaraan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_pejabat_kewarganegaraan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_pejabat_pekerjaan))?'filled':'wrapper'?>">
												<label>Pekerjaan <span class="required">*</span></label>
												<input name="leasing_pekerjaan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_pejabat_pekerjaan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_pejabat_alamat))?'filled':'wrapper'?>">
												<label>Alamat <span class="required">*</span></label>
												<textarea name="leasing_alamat" class="md-input uk-form-width-medium"><?php echo $get_akta->akta_pejabat_alamat ?></textarea>
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_pejabat_kelurahan))?'filled':'wrapper'?>">
												<label>Kelurahan <span class="required">*</span></label>
												<input name="leasing_kelurahan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_pejabat_kelurahan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_pejabat_kecamatan))?'filled':'wrapper'?>">
												<label>Kecamatan <span class="required">*</span></label>
												<input name="leasing_kecamatan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_pejabat_kecamatan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_pejabat_kabupaten))?'filled':'wrapper'?>">
												<label>Kota/Kabupaten <span class="required">*</span></label>
												<input name="leasing_kotakab" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_pejabat_kabupaten ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_pejabat_provinsi))?'filled':'wrapper'?>">
												<label>Provinsi <span class="required">*</span></label>
												<input name="leasing_propinsi" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_pejabat_provinsi ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_pejabat_jenis_identitas))?'filled':'wrapper'?>">
												<label>Jenis Identitas <span class="required">*</span></label>
												<input name="leasing_jnsidtts" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_pejabat_jenis_identitas ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_pejabat_nik))?'filled':'wrapper'?>">
												<label>NIK <span class="required">*</span></label>
												<input name="leasing_nik" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_pejabat_nik ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Kuasa <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="leasing_tglkuasa" type="text" class="datepicker" value="<?php echo $get_akta->akta_pejabat_tanggal_kuasa ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_pejabat_no_kuasa))?'filled':'wrapper'?>">
												<label>Nomor Kuasa <span class="required">*</span></label>
												<input name="leasing_nokuasa" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_pejabat_no_kuasa ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin uk-row-first">
						<div class="md-card md-card-warning">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Perjanjian Pembiayaan</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tgl SKPJF <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="perjanjian_tgl_skpjf" type="text" class="datepicker" value="<?php echo $get_perjanjian->perjanjian_tanggal_skpjf ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_perjanjian->perjanjian_no_kontrak))?'filled':'wrapper'?>">
												<label>Nomor Kontrak<span class="required">*</span></label>
												<input name="perjanjian_nokontrak" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_perjanjian->perjanjian_no_kontrak ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Jual/Tanggal PK <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="perjanjian_tgljual_pk" type="text" class="datepicker" value="<?php echo $get_perjanjian->perjanjian_tanggal_pk ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Akhir Jual <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="perjanjian_tglakhir_jual" type="text" class="datepicker" value="<?php echo $get_perjanjian->perjanjian_tanggal_akhir ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_perjanjian->perjanjian_tenor_pk))?'filled':'wrapper'?>">
												<label>Tenor PK<span class="required">*</span></label>
												<input name="perjanjian_tenorpk" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_perjanjian->perjanjian_tenor_pk ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_perjanjian->perjanjian_total_hutang))?'filled':'wrapper'?>">
												<label>Total Hutang Pokok<span class="required">*</span></label>
												<input name="perjanjian_tothtgpokok" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_perjanjian->perjanjian_total_hutang ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_perjanjian->perjanjian_nilai_penjaminan))?'filled':'wrapper'?>">
												<label>Nilai Penjaminan <span class="required">*</span></label>
												<input name="perjanjian_nilaipenjaminan" type="number" class="md-input uk-form-width-medium" value="<?php echo $get_perjanjian->perjanjian_nilai_penjaminan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Penagihan Leasing <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="perjanjian_tglpnghn_leasing" type="text" class="datepicker" value="<?php echo $get_perjanjian->perjanjian_tanggal_penagihan ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_perjanjian->perjanjian_is_asuransi))?'filled':'wrapper'?>">
												<label>Pakai Asuransi <span class="required">*</span></label>
												<input name="perjanjian_asuransi" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_perjanjian->perjanjian_is_asuransi ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_perjanjian->perjanjian_jenis_penggunaan))?'filled':'wrapper'?>">
												<label>Jenis Penggunaan <span class="required">*</span></label>
												<input name="perjanjian_jnspenggunaan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_perjanjian->perjanjian_jenis_penggunaan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_perjanjian->perjanjian_jenis_pembiayaan))?'filled':'wrapper'?>">
												<label>Jenis Pembiayaan <span class="required">*</span></label>
												<input name="perjanjian_jnspembiayaan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_perjanjian->perjanjian_jenis_pembiayaan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_perjanjian->perjanjian_berdasarkan))?'filled':'wrapper'?>">
												<label>Berdasarkan Perjanjian <span class="required">*</span></label>
												<textarea name="perjanjian_bdsperjanjian" class="md-input uk-form-width-medium"><?php echo $get_perjanjian->perjanjian_berdasarkan ?></textarea>
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin">
						<div class="md-card md-card-danger">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Objek Jaminan</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_obyek_jaminan))?'filled' : 'wrapper' ?>">
												<label>Objek Jaminan <span class="required">*</span></label>
												<input name="objek_jaminan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_kendaraan->dk_obyek_jaminan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_merk))?'filled' : 'wrapper' ?>">
												<label>Merk <span class="required">*</span></label>
												<input name="objek_merk" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_kendaraan->dk_merk ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_type))?'filled' : 'wrapper' ?>">
												<label>Type <span class="required">*</span></label>
												<input name="objek_tipe" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_kendaraan->dk_type ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_jenis))?'filled' : 'wrapper' ?>">
												<label>Jenis <span class="required">*</span></label>
												<input name="objek_jenis" type="text" class="md-input uk-form-width-large" value="<?php echo $get_kendaraan->dk_jenis ?>">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_model))?'filled' : 'wrapper' ?>">
												<label>Model <span class="required">*</span></label>
												<input name="objek_model" type="text" class="md-input uk-form-width-large" value="<?php echo $get_kendaraan->dk_model ?>">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tahun Pembuatan <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="objek_thnpembuatan" type="text" class="years" value="<?php echo $get_kendaraan->dk_tahun_pembuatan ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_warna))?'filled' : 'wrapper' ?>">
												<label>Warna <span class="required">*</span></label>
												<input name="objek_warna" type="text" class="md-input uk-form-width-large" value="<?php echo $get_kendaraan->dk_warna ?>">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_no_rangka))?'filled' : 'wrapper' ?>">
												<label>Nomor Rangka <span class="required">*</span></label>
												<input name="objek_norangka" type="text" class="md-input uk-form-width-large" value="<?php echo $get_kendaraan->dk_no_rangka ?>">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_no_mesin))?'filled' : 'wrapper' ?>">
												<label>Nomor Mesin <span class="required">*</span></label>
												<input name="objek_nomesin" type="text" class="md-input uk-form-width-large" value="<?php echo $get_kendaraan->dk_no_mesin ?>">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_jenis_bukti))?'filled' : 'wrapper' ?>">
												<label>Jenis Bukti <span class="required">*</span></label>
												<input name="objek_jnbukti" type="text" class="md-input uk-form-width-large" value="<?php echo $get_kendaraan->dk_jenis_bukti ?>">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_no_bukti_bpkb))?'filled' : 'wrapper' ?>">
												<label>Nomor Bukti BPKB <span class="required"></span></label>
												<input name="objek_nobpkb" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_kendaraan->dk_no_bukti_bpkb ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_kalimat_bukti))?'filled' : 'wrapper' ?>">
												<label>Kalimat Bukti <span class="required"></span></label>
												<input name="objek_kalimatbukti" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_kendaraan->dk_kalimat_bukti ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_nilai_obyek))?'filled' : 'wrapper' ?>">
												<label>Nilai Objek <span class="required"></span></label>
												<input name="objek_nilai" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_kendaraan->dk_nilai_obyek ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Bukti Objek/Kwitansi <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="objek_tglbukti" type="text" class="datepicker" value="<?php echo $get_kendaraan->dk_tanggal_bukti ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_no_bukti_faktur))?'filled' : 'wrapper' ?>">
												<label>No. Bukti Objek/Faktur <span class="required">*</span></label>
												<input name="objek_nobukti" type="text" class="md-input uk-form-width-large" value="<?php echo $get_kendaraan->dk_no_bukti_faktur ?>">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_pemasok))?'filled' : 'wrapper' ?>">
												<label>Pemasok Objek <span class="required">*</span></label>
												<input name="objek_pemasok" type="text" class="md-input uk-form-width-large" value="<?php echo $get_kendaraan->dk_pemasok ?>">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_no_polisi))?'filled' : 'wrapper' ?>">
												<label>No. Polisi <span class="reqired">(*harus diisi jika bekas)</span></label>
												<input name="objek_nopolisi" type="text" class="md-input uk-form-width-large" value="<?php echo $get_kendaraan->dk_no_polisi ?>">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_no_bpkb))?'filled' : 'wrapper' ?>">
												<label>No. BPKB <span class="reqired">(*harus diisi jika bekas)</span></label>
												<input name="objek_nobpkb" type="text" class="md-input uk-form-width-large" value="<?php echo $get_kendaraan->dk_no_bpkb ?>">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Surat Penyerahan/Tgl BPKB <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="objek_tglsrtserah" type="text" class="datepicker" value="<?php echo $get_kendaraan->dk_tanggal_surat_penyerahan ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_sp_bpkb))?'filled' : 'wrapper' ?>">
												<label>SP BPKB Dikeluarkan oleh <span class="required">*</span></label>
												<input name="objek_sp_bpkb" type="text" class="md-input uk-form-width-large" value="<?php echo $get_kendaraan->dk_sp_bpkb ?>">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_nama_pemilik))?'filled' : 'wrapper' ?>">
												<label>Nama Pemilik BPKB <span class="required">*</span></label>
												<input name="objek_nama_pemilik" type="text" class="md-input uk-form-width-large" value="<?php echo $get_kendaraan->dk_nama_pemilik ?>">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_kendaraan->dk_alamat_pemilik))?'filled' : 'wrapper' ?>">
												<label>Alamat Pemilik BPKB <span class="required">*</span></label>
												<textarea name="objek_alamat_pemilik" class="md-input uk-form-width-large"><?php echo $get_kendaraan->dk_alamat_pemilik ?></textarea>
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin uk-row-first">
						<div class="md-card md-card-primary">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Pemberi Fidusia</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_panggilan))?'filled' : 'wrapper' ?>">
												<label>Panggilan <span class="required">*</span></label>
												<input name="fidusia_panggilan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_panggilan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_nama))?'filled' : 'wrapper' ?>">
												<label>Nama <span class="required">*</span></label>
												<input name="fidusia_nama" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_nama ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<label>Jenis Kelamin <span class="required">*</span></label><br><br>
											<span class="icheck-inline">
												<input type="radio" name="fidusia_jenkel"  value="0" data-md-icheck <?=($get_fidusia->pemberi_jenis_kelamin == '0')?'checked' : '' ?>/>
												<label for="radio_demo_inline_1" class="inline-label">Perempuan</label>
											</span>
											<span class="icheck-inline">
												<input type="radio" name="fidusia_jenkel"  value="1" data-md-icheck <?=($get_fidusia->pemberi_jenis_kelamin == '1')?'checked' : '' ?> />
												<label for="radio_demo_inline_2" class="inline-label">Laki-laki</label>
											</span>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_tempat_lahir))?'filled' : 'wrapper' ?>">
												<label>Tempat Lahir <span class="required">*</span></label>
												<input name="fidusia_tmpt_lahir" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_tempat_lahir ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Lahir <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="fidusia_tgl_lahir" type="text" class="datepicker" value="<?php echo $get_fidusia->pemberi_tanggal_lahir ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_agama))?'filled' : 'wrapper' ?>">
												<label>Agama <span class="required">*</span></label>
												<input name="fidusia_agama" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_agama ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_kewarganegaraan))?'filled' : 'wrapper' ?>">
												<label>Kewarganegaraan <span class="required">*</span></label>
												<input name="fidusia_kewarganegaraan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_kewarganegaraan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_pekerjaan))?'filled' : 'wrapper' ?>">
												<label>Pekerjaan <span class="required">*</span></label>
												<input name="fidusia_pekerjaan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_pekerjaan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_alamat))?'filled' : 'wrapper' ?>">
												<label>Alamat <span class="required">*</span></label>
												<textarea name="fidusia_alamat" class="md-input uk-form-width-medium"><?php echo $get_fidusia->pemberi_alamat ?></textarea>
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row uk-grid">
											<div class="uk-width-medium-2-4">
												<div class="md-input-<?=(!empty($get_fidusia->pemberi_rt))?'filled' : 'wrapper' ?>">
													<label>RT <span class="required">*</span></label>
													<input name="fidusia_rt" type="number" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_rt ?>">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
											<div class="uk-width-medium-2-4">
												<div class="md-input-<?=(!empty($get_fidusia->pemberi_rw))?'filled' : 'wrapper' ?>">
													<label>RW <span class="required">*</span></label>
													<input name="fidusia_rw" type="number" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_rw ?>">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_desa))?'filled' : 'wrapper' ?>">
												<label>Desa/Dukuh </label>
												<input name="fidusia_desa" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_desa ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_kelurahan))?'filled' : 'wrapper' ?>">
												<label>Kelurahan </label>
												<input name="fidusia_kelurahan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_kelurahan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_kecamatan))?'filled' : 'wrapper' ?>">
												<label>Kecamatan <span class="required">*</span></label>
												<input name="fidusia_kecamatan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_kecamatan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_kabupaten))?'filled' : 'wrapper' ?>">
												<label>Kota/Kabupaten <span class="required">*</span></label>
												<input name="fidusia_kota_kab" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_kabupaten ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_kabupaten_kode))?'filled' : 'wrapper' ?>">
												<label>Kode Kota/Kabupaten <span class="required">*</span></label>
												<input name="fidusia_kode_kotakab" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_kabupaten_kode ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_provinsi))?'filled' : 'wrapper' ?>">
												<label>Propinsi <span class="required">*</span></label>
												<input name="fidusia_propinsi" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_provinsi ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_provinsi_kode))?'filled' : 'wrapper' ?>">
												<label>Kode Propinsi <span class="required">*</span></label>
												<input name="fidusia_kodepropinsi" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_provinsi_kode ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_kode_pos))?'filled' : 'wrapper' ?>">
												<label>Kode Pos <span class="required">*</span></label>
												<input name="fidusia_kodepos" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_kode_pos ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_telp))?'filled' : 'wrapper' ?>">
												<label>No. Telepon <span class="required">*</span></label>
												<input name="fidusia_notelp" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_telp ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_jenis_identitas))?'filled' : 'wrapper' ?>">
												<label>Jenis Identitas <span class="required">*</span></label>
												<input name="fidusia_jnsidentitas" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_jenis_identitas ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_nik))?'filled' : 'wrapper' ?>">
												<label>NIK <span class="required">*</span></label>
												<input name="fidusia_nik" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_nik ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Kartu Identitas Berlaku Hingga</label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="fidusia_kibh" type="text" class="datepicker" value="<?php echo $get_fidusia->pemberi_ktp_berlaku ?>">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin">
						<div class="md-card md-card-success">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Pemberi Status Pernikahan</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_status_pernikahan))?'filled' : 'wrapper' ?>">
												<label>Status Pernikahan </label>
												<input name="psp_status" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_status_pernikahan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_surat_pengadilan))?'filled' : 'wrapper' ?>">
												<label>Surat Pengadilan </label>
												<input name="psp_surat_pengadilan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_surat_pengadilan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_surat_cerai))?'filled' : 'wrapper' ?>">
												<label>Surat Cerai </label>
												<input name="psp_surat_cerai" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_surat_cerai ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Dokumen <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="psp_thnpembuatan" type="text" class="datepicker" value="<?php echo $get_fidusia->pemberi_tanggal_dokumen ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_lokasi_penerbit))?'filled' : 'wrapper' ?>">
												<label>Lokasi Penerbit </label>
												<input name="psp_lokasi_penerbit" type="text" class="md-input uk-form-width-large" value="<?php echo $get_fidusia->pemberi_lokasi_penerbit ?>">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_uraian_status))?'filled' : 'wrapper' ?>">
												<label>Uraian Status Cerai </label>
												<textarea name="psp_urstt_cerai" class="md-input uk-form-width-large"><?php echo $get_fidusia->pemberi_uraian_status ?></textarea>
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin uk-row-first">
						<div class="md-card md-card-warning">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Pasangan (*harus diisi jika ada persetujuan pasangan)</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_status_pasangan))?'filled' : 'wrapper' ?>">
												<label>Status dengan Pemberi Suami/Istri</label>
												<input name="pasangan_sttsutri" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_status_pasangan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Persetujuan <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="pasangan_tglsetuju" type="text" class="datepicker" value="<?php echo $get_fidusia->pemberi_tanggal_persetujuan_pasangan ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_panggilan_pasangan))?'filled' : 'wrapper' ?>">
												<label>Panggilan <span class="required">*</span></label>
												<input name="pasangan_panggilan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_panggilan_pasangan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_nama_pasangan))?'filled' : 'wrapper' ?>">
												<label>Nama <span class="required">*</span></label>
												<input name="pasangan_nama" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_nama_pasangan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_tempat_lahir_pasangan))?'filled' : 'wrapper' ?>">
												<label>Tempat Lahir <span class="required">*</span></label>
												<input name="pasangan_tmpt_lahir" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_tempat_lahir_pasangan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Lahir <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="pasangan_tgl_lahir" type="text" class="datepicker" value="<?php echo $get_fidusia->pemberi_tanggal_lahir_pasangan ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_agama_pasangan))?'filled' : 'wrapper' ?>">
												<label>Agama <span class="required">*</span></label>
												<input name="pasangan_agama" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_agama_pasangan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_kewarganegaraan_pasangan))?'filled' : 'wrapper' ?>">
												<label>Kewarganegaraan <span class="required">*</span></label>
												<input name="pasangan_kewarganegaraan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_kewarganegaraan_pasangan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_pekerjaan_pasangan))?'filled' : 'wrapper' ?>">
												<label>Pekerjaan <span class="required">*</span></label>
												<input name="pasangan_pekerjaan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_pekerjaan_pasangan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_alamat_pasangan))?'filled' : 'wrapper' ?>">
												<label>Alamat <span class="required">*</span></label>
												<textarea name="pasangan_alamat" class="md-input uk-form-width-medium"><?php echo $get_fidusia->pemberi_alamat_pasangan ?></textarea>
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row uk-grid">
											<div class="uk-width-medium-2-4">
												<div class="md-input-<?=(!empty($get_fidusia->pemberi_rt_pasangan))?'filled' : 'wrapper' ?>">
													<label>RT <span class="required">*</span></label>
													<input name="pasangan_rt" type="number" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_rt_pasangan ?>">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
											<div class="uk-width-medium-2-4">
												<div class="md-input-<?=(!empty($get_fidusia->pemberi_rw_pasangan))?'filled' : 'wrapper' ?>">
													<label>RW <span class="required">*</span></label>
													<input name="pasangan_rw" type="number" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_rw_pasangan ?>">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_desa_pasangan))?'filled' : 'wrapper' ?>">
												<label>Desa/Dukuh </label>
												<input name="pasangan_desa" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_desa_pasangan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_kelurahan_pasangan))?'filled' : 'wrapper' ?>">
												<label>Kelurahan </label>
												<input name="pasangan_kelurahan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_kelurahan_pasangan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_kecamatan_pasangan))?'filled' : 'wrapper' ?>">
												<label>Kecamatan <span class="required">*</span></label>
												<input name="pasangan_kecamatan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_kecamatan_pasangan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_kabupaten_pasangan))?'filled' : 'wrapper' ?>">
												<label>Kota/Kabupaten <span class="required">*</span></label>
												<input name="pasangan_kota_kab" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_kabupaten_pasangan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_jenis_identitas_pasangan))?'filled' : 'wrapper' ?>">
												<label>Jenis Identitas <span class="required">*</span></label>
												<input name="pasangan_jnsidentitas" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_jenis_identitas_pasangan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_nik_pasangan))?'filled' : 'wrapper' ?>">
												<label>NIK <span class="required">*</span></label>
												<input name="pasangan_nik" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_nik_pasangan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Kartu Identitas Berlaku Hingga <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="pasangan_kibh" type="text" class="datepicker" value="<?php echo $get_fidusia->pemberi_ktp_berlaku_pasangan ?>">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin">
						<div class="md-card md-card-danger">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">DEBITUR (diisi jika penerima fasilitas berbeda dengan pemberi)</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_status_hubungan_debitur))?'filled' : 'wrapper' ?>">
												<label>Status dengan Pemberi Suami/Istri</label>
												<input name="debitur_sttsutri" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_status_hubungan_debitur ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_panggilan_debitur))?'filled' : 'wrapper' ?>">
												<label>Panggilan <span class="required">*</span></label>
												<input name="debitur_panggilan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_panggilan_debitur ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_nama_debitur))?'filled' : 'wrapper' ?>">
												<label>Nama <span class="required">*</span></label>
												<input name="debitur_nama" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_nama_debitur ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_tempat_lahir_debitur))?'filled' : 'wrapper' ?>">
												<label>Tempat Lahir <span class="required">*</span></label>
												<input name="debitur_tmpt_lahir" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_tempat_lahir_debitur ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Lahir <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="debitur_tgl_lahir" type="text" class="datepicker" value="<?php echo $get_fidusia->pemberi_tanggal_lahir_debitur ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_agama_debitur))?'filled' : 'wrapper' ?>">
												<label>Agama <span class="required">*</span></label>
												<input name="debitur_agama" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_agama_debitur ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_kewarganegaraan_debitur))?'filled' : 'wrapper' ?>">
												<label>Kewarganegaraan <span class="required">*</span></label>
												<input name="debitur_kewarganegaraan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_kewarganegaraan_debitur ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_pekerjaan_debitur))?'filled' : 'wrapper' ?>">
												<label>Pekerjaan <span class="required">*</span></label>
												<input name="debitur_pekerjaan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_pekerjaan_debitur ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_alamat_debitur))?'filled' : 'wrapper' ?>">
												<label>Alamat <span class="required">*</span></label>
												<textarea name="debitur_alamat" class="md-input uk-form-width-medium"><?php echo $get_fidusia->pemberi_alamat_debitur ?></textarea>
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row uk-grid">
											<div class="uk-width-medium-2-4">
												<div class="md-input-<?=(!empty($get_fidusia->pemberi_rt_debitur))?'filled' : 'wrapper' ?>">
													<label>RT <span class="required">*</span></label>
													<input name="debitur_rt" type="number" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_rt_debitur ?>">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
											<div class="uk-width-medium-2-4">
												<div class="md-input-<?=(!empty($get_fidusia->pemberi_rw_debitur))?'filled' : 'wrapper' ?>">
													<label>RW <span class="required">*</span></label>
													<input name="debitur_rw" type="number" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_rw_debitur ?>">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_desa_debitur))?'filled' : 'wrapper' ?>">
												<label>Desa/Dukuh </label>
												<input name="debitur_desa" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_desa_debitur ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_kelurahan_debitur))?'filled' : 'wrapper' ?>">
												<label>Kelurahan </label>
												<input name="debitur_kelurahan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_kelurahan_debitur ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_kecamatan_debitur))?'filled' : 'wrapper' ?>">
												<label>Kecamatan <span class="required">*</span></label>
												<input name="debitur_kecamatan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_kecamatan_debitur ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_kabupaten_debitur))?'filled' : 'wrapper' ?>">
												<label>Kota/Kabupaten <span class="required">*</span></label>
												<input name="debitur_kota_kab" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_kabupaten_debitur ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_jenis_identitas_debitur))?'filled' : 'wrapper' ?>">
												<label>Jenis Identitas <span class="required">*</span></label>
												<input name="debitur_jnsidentitas" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_jenis_identitas_debitur ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_fidusia->pemberi_nik_debitur))?'filled' : 'wrapper' ?>">
												<label>NIK <span class="required">*</span></label>
												<input name="debitur_nik" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_fidusia->pemberi_nik_debitur ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Kartu Identitas Berlaku Hingga <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="debitur_kibh" type="text" class="datepicker" value="<?php echo $get_fidusia->pemberi_ktp_berlaku_debitur ?>">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin uk-row-first">
						<div class="md-card md-card-primary">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">SAKSI NOTARIS 1</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi1_panggilan))?'filled':'wrapper'?>">
												<label>Panggilan <span class="required">*</span></label>
												<input name="notsatu_panggilan" type="text" class="md-input uk-form-width-medium"value="<?php echo $get_akta->akta_saksi1_panggilan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi1_nama))?'filled':'wrapper'?>">
												<label>Nama <span class="required">*</span></label>
												<input name="notsatu_nama" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi1_nama ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi1_tempat_lahir))?'filled':'wrapper'?>">
												<label>Tempat Lahir <span class="required">*</span></label>
												<input name="notsatu_tmpt_lahir" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi1_tempat_lahir ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Lahir <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="notsatu_tgl_lahir" type="text" class="datepicker" value="<?php echo $get_akta->akta_saksi1_tanggal_lahir ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi1_kewarganegaraan))?'filled':'wrapper'?>">
												<label>Kewarganegaraan <span class="required">*</span></label>
												<input name="notsatu_kewarganegaraan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi1_kewarganegaraan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi1_pekerjaan))?'filled':'wrapper'?>">
												<label>Pekerjaan <span class="required">*</span></label>
												<input name="notsatu_pekerjaan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi1_pekerjaan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi1_alamat))?'filled':'wrapper'?>">
												<label>Alamat <span class="required">*</span></label>
												<textarea name="notsatu_alamat" class="md-input uk-form-width-medium"><?php echo $get_akta->akta_saksi1_alamat ?></textarea>
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row uk-grid">
											<div class="uk-width-medium-2-4">
												<div class="md-input-<?=(!empty($get_akta->akta_saksi1_rt))?'filled':'wrapper'?>">
													<label>RT <span class="required">*</span></label>
													<input name="notsatu_rt" type="number" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi1_rt ?>">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
											<div class="uk-width-medium-2-4">
												<div class="md-input-<?=(!empty($get_akta->akta_saksi1_rw))?'filled':'wrapper'?>">
													<label>RW <span class="required">*</span></label>
													<input name="notsatu_rw" type="number" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi1_rw ?>">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi1_kelurahan))?'filled':'wrapper'?>">
												<label>Kelurahan </label>
												<input name="notsatu_kelurahan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi1_kelurahan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi1_kecamatan))?'filled':'wrapper'?>">
												<label>Kecamatan <span class="required">*</span></label>
												<input name="notsatu_kecamatan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi1_kecamatan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi1_kabupaten))?'filled':'wrapper'?>">
												<label>Kota/Kabupaten <span class="required">*</span></label>
												<input name="notsatu_kota_kab" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi1_kabupaten ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi1_jenis_identitas))?'filled':'wrapper'?>">
												<label>Jenis Identitas <span class="required">*</span></label>
												<input name="notsatu_jnsidentitas" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi1_jenis_identitas ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi1_nik))?'filled':'wrapper'?>">
												<label>NIK <span class="required">*</span></label>
												<input name="notsatu_nik" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi1_nik ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin">
						<div class="md-card md-card-success">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">SAKSI NOTARIS 2</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi2_panggilan))?'filled':'wrapper'?>">
												<label>Panggilan <span class="required">*</span></label>
												<input name="notdua_panggilan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi2_panggilan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi2_nama))?'filled':'wrapper'?>">
												<label>Nama <span class="required">*</span></label>
												<input name="notdua_nama" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi2_nama ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi2_tempat_lahir))?'filled':'wrapper'?>">
												<label>Tempat Lahir <span class="required">*</span></label>
												<input name="notdua_tmpt_lahir" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi2_tempat_lahir ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Lahir <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="notdua_tgl_lahir" type="text" class="datepicker" value="<?php echo $get_akta->akta_saksi2_tanggal_lahir ?>">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi2_kewarganegaraan))?'filled':'wrapper'?>">
												<label>Kewarganegaraan <span class="required">*</span></label>
												<input name="notdua_kewarganegaraan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi2_kewarganegaraan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi2_pekerjaan))?'filled':'wrapper'?>">
												<label>Pekerjaan <span class="required">*</span></label>
												<input name="notdua_pekerjaan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi2_pekerjaan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi2_alamat))?'filled':'wrapper'?>">
												<label>Alamat <span class="required">*</span></label>
												<textarea name="notdua_alamat" class="md-input uk-form-width-medium"><?php echo $get_akta->akta_saksi2_alamat ?></textarea>
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row uk-grid">
											<div class="uk-width-medium-2-4">
												<div class="md-input-<?=(!empty($get_akta->akta_saksi2_rt))?'filled':'wrapper'?>">
													<label>RT <span class="required">*</span></label>
													<input name="notdua_rt" type="number" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi2_rt ?>">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
											<div class="uk-width-medium-2-4">
												<div class="md-input-<?=(!empty($get_akta->akta_saksi2_rw))?'filled':'wrapper'?>">
													<label>RW <span class="required">*</span></label>
													<input name="notdua_rw" type="number" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi2_rw ?>">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi2_kelurahan))?'filled':'wrapper'?>">
												<label>Kelurahan </label>
												<input name="notdua_kelurahan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi2_kelurahan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi2_kecamatan))?'filled':'wrapper'?>">
												<label>Kecamatan <span class="required">*</span></label>
												<input name="notdua_kecamatan" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi2_kecamatan ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi2_kabupaten))?'filled':'wrapper'?>">
												<label>Kota/Kabupaten <span class="required">*</span></label>
												<input name="notdua_kota_kab" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi2_kabupaten ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi2_jenis_identitas))?'filled':'wrapper'?>">
												<label>Jenis Identitas <span class="required">*</span></label>
												<input name="notdua_jnsidentitas" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi2_jenis_identitas ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-<?=(!empty($get_akta->akta_saksi2_nik))?'filled':'wrapper'?>">
												<label>NIK <span class="required">*</span></label>
												<input name="notdua_nik" type="text" class="md-input uk-form-width-medium" value="<?php echo $get_akta->akta_saksi2_nik ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="md-card">
					<div class="md-card-content">
						<div class="uk-text-center">
							<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Save</button>
							<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Batal</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<a href="<?=$url; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		$('.timepicker').kendoTimePicker();

		App.datepicker();
		App.datepicker('.years', null, true);
		App.datepicker('.hours', null, true);


		$('#form_edit').on('submit', function(e){
			e.preventDefault();

			var $this 	= $(this),
			url 		= $this.attr('action'),
			data 		= $this.serialize();

			var form = $('form')[0]	;
			var formData = new FormData(form);
			var err_res = 0,
			err_msg = [],
			suc_msg = [];

			$.ajax({
				url: url,
				type: 'post',
				dataType:'json',
				data: formData,
				processData: false,
				contentType: false,
				async: false,
				success : function(res) {
					if (res.sts == 5) {
						$('#alert').html(res.alert)
						App.scrollTop();
					}
					if (res.sts == 0) {
						App.notif('error', res.msg, 'error');
					}
					else if (res.inp == 2) {
						App.notif('Success', res.msg, 'success');
						$('.reload').trigger('click');
					}
					if (res.sts == 3 && res.inp == 2) {
						suc_res = '';
						var chk = 0;
						$.each(res, function( key, val ) {
							if(val['sts'] == 1) {
								suc_res = 1;
								suc_msg.push(val['msg']);
							} else {
								if(typeof val['msg'] !== "undefined") chk = 1;
								err_msg.push(val['msg']);
							}
						});
						if(suc_res == 1){
							App.notif('Success', suc_msg.join('<br>') , 'success');
							if(chk === 1){
								App.notif('Error', err_msg.join('<br>'), 'error');
							}
							$('.reload').trigger('click');
						}else{
							App.notif('Success', suc_msg.join('<br>') , 'success');
							if(chk === 1){
								App.notif('Error', err_msg.join('<br>'), 'error');
							}
							$('.reload').trigger('click');
						}
					}
				},
				error 	: function(res) {
					App.notif('Error', res.msg , 'error');
				}
			});
		});

	});
</script>