<div class="uk-grid">
	<div class="uk-width-medium-1">
		<div class="uk-row-first">
			<form method="post" action="<?=$url; ?>/action_add" id="form_add" enctype="multipart/form-data">
				<div class="md-card">
					<div class="md-card-toolbar">
						<h3 class="md-card-toolbar-heading-text">Add <?php echo $name ?></h3>
					</div>
				</div>
				<div class="uk-grid uk-grid-medium uk-grid-width-medium-1-2">
					<div class="uk-grid-margin uk-row-first">
						<div class="md-card md-card-primary">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Akta</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Akta <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="akta_tanggal" type="text" class="datepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Jam Akta (Awal) <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="akta_jamawal" type="number" class="uk-form-width-medium timepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Jam Akta (Akhir) <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="akta_jamakhir" type="number" class="uk-form-width-medium timepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Zona Waktu <span class="required">*</span></label>
												<input name="akta_zonawkt" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nama Notaris <span class="required">*</span></label>
												<input name="akta_namantr" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Gelar <span class="required">*</span></label>
												<input name="akta_gelarntr" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kota/Kab Notaris <span class="required">*</span></label>
												<input name="akta_kotkabntr" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nomor Akta <span class="required">*</span></label>
												<input name="akta_nomor" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin">
						<div class="md-card md-card-success" style="padding-bottom: 15px;">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Data Leasing</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Panggilan <span class="required">*</span></label>
												<input name="leasing_panggilan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nama Pejabat <span class="required">*</span></label>
												<input name="leasing_namapjbt" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Tempat Lahir <span class="required">*</span></label>
												<input name="leasing_tmptlahir" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Lahir <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="leasing_tgllahir" type="text" class="datepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kewarganegaraan <span class="required">*</span></label>
												<input name="leasing_kewarganegaraan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Pekerjaan <span class="required">*</span></label>
												<input name="leasing_pekerjaan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Alamat <span class="required">*</span></label>
												<textarea name="leasing_alamat" class="md-input uk-form-width-medium"></textarea>
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kelurahan <span class="required">*</span></label>
												<input name="leasing_kelurahan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kecamatan <span class="required">*</span></label>
												<input name="leasing_kecamatan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kota/Kabupaten <span class="required">*</span></label>
												<input name="leasing_kotakab" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Provinsi <span class="required">*</span></label>
												<input name="leasing_propinsi" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Jenis Identitas <span class="required">*</span></label>
												<input name="leasing_jnsidtts" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>NIK <span class="required">*</span></label>
												<input name="leasing_nik" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Kuasa <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="leasing_tglkuasa" type="text" class="datepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nomor Kuasa <span class="required">*</span></label>
												<input name="leasing_nokuasa" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin uk-row-first">
						<div class="md-card md-card-warning">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Perjanjian Pembiayaan</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tgl SKPJF <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="perjanjian_tgl_skpjf" type="text" class="datepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Nomor Kontrak<span class="required">*</span></label>
												<input name="perjanjian_nokontrak" type="text" class="md-input uk-form-width-medium masked_input" >
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Jual/Tanggal PK <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="perjanjian_tgljual_pk" type="text" class="datepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Akhir Jual <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="perjanjian_tglakhir_jual" type="text" class="datepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Tenor PK<span class="required">*</span></label>
												<input name="perjanjian_tenorpk" type="text" class="md-input uk-form-width-medium masked_input" >
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Total Hutang Pokok<span class="required">*</span></label>
												<input name="perjanjian_tothtgpokok" type="text" class="md-input uk-form-width-medium masked_input" >
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nilai Penjaminan <span class="required">*</span></label>
												<input name="perjanjian_nilaipenjaminan" type="number" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Penagihan Leasing <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="perjanjian_tglpnghn_leasing" type="text" class="datepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Pakai Asuransi <span class="required">*</span></label>
												<input name="perjanjian_asuransi" type="text" class="md-input uk-form-width-medium" >
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Jenis Penggunaan <span class="required">*</span></label>
												<input name="perjanjian_jnspenggunaan" type="text" class="md-input uk-form-width-medium" >
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Jenis Pembiayaan <span class="required">*</span></label>
												<input name="perjanjian_jnspembiayaan" type="text" class="md-input uk-form-width-medium" >
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Berdasarkan Perjanjian <span class="required">*</span></label>
												<textarea name="perjanjian_bdsperjanjian" class="md-input uk-form-width-medium"></textarea>
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin">
						<div class="md-card md-card-danger">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Objek Jaminan</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Objek Jaminan <span class="required">*</span></label>
												<input name="objek_jaminan" type="text" class="md-input uk-form-width-medium" ">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Merk <span class="required">*</span></label>
												<input name="objek_merk" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Type <span class="required">*</span></label>
												<input name="objek_tipe" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Jenis <span class="required">*</span></label>
												<input name="objek_jenis" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Model <span class="required">*</span></label>
												<input name="objek_model" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tahun Pembuatan <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="objek_thnpembuatan" type="text" class="years">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Warna <span class="required">*</span></label>
												<input name="objek_warna" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nomor Rangka <span class="required">*</span></label>
												<input name="objek_norangka" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nomor Mesin <span class="required">*</span></label>
												<input name="objek_nomesin" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Jenis Bukti <span class="required">*</span></label>
												<input name="objek_jnbukti" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nomor Bukti BPKB <span class="required"></span></label>
												<input name="objek_nobpkb" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kalimat Bukti <span class="required"></span></label>
												<input name="objek_kalimatbukti" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nilai Objek <span class="required"></span></label>
												<input name="objek_nilai" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Bukti Objek/Kwitansi <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="objek_tglbukti" type="text" class="datepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>No. Bukti Objek/Faktur <span class="required">*</span></label>
												<input name="objek_nobukti" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Pemasok Objek <span class="required">*</span></label>
												<input name="objek_pemasok" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>No. Polisi <span class="reqired">(*harus diisi jika bekas)</span></label>
												<input name="objek_nopolisi" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>No. BPKB <span class="reqired">(*harus diisi jika bekas)</span></label>
												<input name="objek_nobpkb" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Surat Penyerahan/Tgl BPKB <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="objek_tglsrtserah" type="text" class="datepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>SP BPKB Dikeluarkan oleh <span class="required">*</span></label>
												<input name="objek_sp_bpkb" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nama Pemilik BPKB <span class="required">*</span></label>
												<input name="objek_nama_pemilik" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Alamat Pemilik BPKB <span class="required">*</span></label>
												<textarea name="objek_alamat_pemilik" class="md-input uk-form-width-large"></textarea>
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin uk-row-first">
						<div class="md-card md-card-primary">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Pemberi Fidusia</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Panggilan <span class="required">*</span></label>
												<input name="fidusia_panggilan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nama <span class="required">*</span></label>
												<input name="fidusia_nama" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<label>Jenis Kelamin <span class="required">*</span></label><br><br>
											<span class="icheck-inline">
												<input type="radio" name="fidusia_jenkel"  value="0" data-md-icheck />
												<label for="radio_demo_inline_1" class="inline-label">Perempuan</label>
											</span>
											<span class="icheck-inline">
												<input type="radio" name="fidusia_jenkel"  value="1" data-md-icheck />
												<label for="radio_demo_inline_2" class="inline-label">Laki-laki</label>
											</span>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Tempat Lahir <span class="required">*</span></label>
												<input name="fidusia_tmpt_lahir" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Lahir <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="fidusia_tgl_lahir" type="text" class="datepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Agama <span class="required">*</span></label>
												<input name="fidusia_agama" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kewarganegaraan <span class="required">*</span></label>
												<input name="fidusia_kewarganegaraan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Pekerjaan <span class="required">*</span></label>
												<input name="fidusia_pekerjaan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Alamat <span class="required">*</span></label>
												<textarea name="fidusia_alamat" class="md-input uk-form-width-medium"></textarea>
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row uk-grid">
											<div class="uk-width-medium-2-4">
												<div class="md-input-wrapper">
													<label>RT <span class="required">*</span></label>
													<input name="fidusia_rt" type="number" class="md-input uk-form-width-medium">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
											<div class="uk-width-medium-2-4">
												<div class="md-input-wrapper">
													<label>RW <span class="required">*</span></label>
													<input name="fidusia_rw" type="number" class="md-input uk-form-width-medium">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Desa/Dukuh </label>
												<input name="fidusia_desa" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kelurahan </label>
												<input name="fidusia_kelurahan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kecamatan <span class="required">*</span></label>
												<input name="fidusia_kecamatan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kota/Kabupaten <span class="required">*</span></label>
												<input name="fidusia_kota_kab" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kode Kota/Kabupaten <span class="required">*</span></label>
												<input name="fidusia_kode_kotakab" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Propinsi <span class="required">*</span></label>
												<input name="fidusia_propinsi" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kode Propinsi <span class="required">*</span></label>
												<input name="fidusia_kodepropinsi" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kode Pos <span class="required">*</span></label>
												<input name="fidusia_kodepos" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>No. Telepon <span class="required">*</span></label>
												<input name="fidusia_notelp" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Jenis Identitas <span class="required">*</span></label>
												<input name="fidusia_jnsidentitas" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>NIK <span class="required">*</span></label>
												<input name="fidusia_nik" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Kartu Identitas Berlaku Hingga <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="fidusia_kibh" type="text" class="datepicker">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin">
						<div class="md-card md-card-success">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Pemberi Status Pernikahan</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Status Pernikahan </label>
												<input name="psp_status" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Surat Pengadilan </label>
												<input name="psp_surat_pengadilan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Surat Cerai </label>
												<input name="psp_surat_cerai" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Dokumen <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="psp_thnpembuatan" type="text" class="datepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Lokasi Penerbit </label>
												<input name="psp_lokasi_penerbit" type="text" class="md-input uk-form-width-large">
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Uraian Status Cerai </label>
												<textarea name="psp_urstt_cerai" class="md-input uk-form-width-large"></textarea>
												<span class="md-input-bar uk-form-width-large"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin uk-row-first">
						<div class="md-card md-card-warning">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">Pasangan (*harus diisi jika ada persetujuan pasangan)</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Status dengan Pemberi Suami/Istri</label>
												<input name="pasangan_sttsutri" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Persetujuan <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="pasangan_tglsetuju" type="text" class="datepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Panggilan <span class="required">*</span></label>
												<input name="pasangan_panggilan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nama <span class="required">*</span></label>
												<input name="pasangan_nama" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Tempat Lahir <span class="required">*</span></label>
												<input name="pasangan_tmpt_lahir" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Lahir <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="pasangan_tgl_lahir" type="text" class="datepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Agama <span class="required">*</span></label>
												<input name="pasangan_agama" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kewarganegaraan <span class="required">*</span></label>
												<input name="pasangan_kewarganegaraan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Pekerjaan <span class="required">*</span></label>
												<input name="pasangan_pekerjaan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Alamat <span class="required">*</span></label>
												<textarea name="pasangan_alamat" class="md-input uk-form-width-medium"></textarea>
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row uk-grid">
											<div class="uk-width-medium-2-4">
												<div class="md-input-wrapper">
													<label>RT <span class="required">*</span></label>
													<input name="pasangan_rt" type="number" class="md-input uk-form-width-medium">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
											<div class="uk-width-medium-2-4">
												<div class="md-input-wrapper">
													<label>RW <span class="required">*</span></label>
													<input name="pasangan_rw" type="number" class="md-input uk-form-width-medium">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Desa/Dukuh </label>
												<input name="pasangan_desa" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kelurahan </label>
												<input name="pasangan_kelurahan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kecamatan <span class="required">*</span></label>
												<input name="pasangan_kecamatan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kota/Kabupaten <span class="required">*</span></label>
												<input name="pasangan_kota_kab" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Jenis Identitas <span class="required">*</span></label>
												<input name="pasangan_jnsidentitas" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>NIK <span class="required">*</span></label>
												<input name="pasangan_nik" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Kartu Identitas Berlaku Hingga <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="pasangan_kibh" type="text" class="datepicker">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin">
						<div class="md-card md-card-danger">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">DEBITUR (diisi jika penerima fasilitas berbeda dengan pemberi)</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Status dengan Pemberi Suami/Istri</label>
												<input name="debitur_sttsutri" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Panggilan <span class="required">*</span></label>
												<input name="debitur_panggilan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nama <span class="required">*</span></label>
												<input name="debitur_nama" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Tempat Lahir <span class="required">*</span></label>
												<input name="debitur_tmpt_lahir" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Lahir <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="debitur_tgl_lahir" type="text" class="datepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Agama <span class="required">*</span></label>
												<input name="debitur_agama" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kewarganegaraan <span class="required">*</span></label>
												<input name="debitur_kewarganegaraan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Pekerjaan <span class="required">*</span></label>
												<input name="debitur_pekerjaan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Alamat <span class="required">*</span></label>
												<textarea name="debitur_alamat" class="md-input uk-form-width-medium"></textarea>
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row uk-grid">
											<div class="uk-width-medium-2-4">
												<div class="md-input-wrapper">
													<label>RT <span class="required">*</span></label>
													<input name="debitur_rt" type="number" class="md-input uk-form-width-medium">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
											<div class="uk-width-medium-2-4">
												<div class="md-input-wrapper">
													<label>RW <span class="required">*</span></label>
													<input name="debitur_rw" type="number" class="md-input uk-form-width-medium">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Desa/Dukuh </label>
												<input name="debitur_desa" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kelurahan </label>
												<input name="debitur_kelurahan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kecamatan <span class="required">*</span></label>
												<input name="debitur_kecamatan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kota/Kabupaten <span class="required">*</span></label>
												<input name="debitur_kota_kab" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Jenis Identitas <span class="required">*</span></label>
												<input name="debitur_jnsidentitas" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>NIK <span class="required">*</span></label>
												<input name="debitur_nik" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Kartu Identitas Berlaku Hingga <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="debitur_kibh" type="text" class="datepicker">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin uk-row-first">
						<div class="md-card md-card-primary">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">SAKSI NOTARIS 1</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Panggilan <span class="required">*</span></label>
												<input name="notsatu_panggilan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nama <span class="required">*</span></label>
												<input name="notsatu_nama" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Tempat Lahir <span class="required">*</span></label>
												<input name="notsatu_tmpt_lahir" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Lahir <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="notsatu_tgl_lahir" type="text" class="datepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kewarganegaraan <span class="required">*</span></label>
												<input name="notsatu_kewarganegaraan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Pekerjaan <span class="required">*</span></label>
												<input name="notsatu_pekerjaan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Alamat <span class="required">*</span></label>
												<textarea name="notsatu_alamat" class="md-input uk-form-width-medium"></textarea>
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row uk-grid">
											<div class="uk-width-medium-2-4">
												<div class="md-input-wrapper">
													<label>RT <span class="required">*</span></label>
													<input name="notsatu_rt" type="number" class="md-input uk-form-width-medium">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
											<div class="uk-width-medium-2-4">
												<div class="md-input-wrapper">
													<label>RW <span class="required">*</span></label>
													<input name="notsatu_rw" type="number" class="md-input uk-form-width-medium">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kelurahan </label>
												<input name="notsatu_kelurahan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kecamatan <span class="required">*</span></label>
												<input name="notsatu_kecamatan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kota/Kabupaten <span class="required">*</span></label>
												<input name="notsatu_kota_kab" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Jenis Identitas <span class="required">*</span></label>
												<input name="notsatu_jnsidentitas" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>NIK <span class="required">*</span></label>
												<input name="notsatu_nik" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid-margin">
						<div class="md-card md-card-success">
							<div class="md-card-toolbar">
								<h3 class="md-card-toolbar-heading-text">SAKSI NOTARIS 2</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-medium-1-2">
									<div class="uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Panggilan <span class="required">*</span></label>
												<input name="notdua_panggilan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Nama <span class="required">*</span></label>
												<input name="notdua_nama" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Tempat Lahir <span class="required">*</span></label>
												<input name="notdua_tmpt_lahir" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="uk-grid">
												<div class="uk-width-medium-1 uk-margin-small-bottom uk-row-first">
													<label>Tanggal Lahir <span class="required">*</span></label>
												</div>
												<div class="uk-width-medium-1 uk-margin-small-bottom">
													<input name="notdua_tgl_lahir" type="text" class="datepicker">
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kewarganegaraan <span class="required">*</span></label>
												<input name="notdua_kewarganegaraan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Pekerjaan <span class="required">*</span></label>
												<input name="notdua_pekerjaan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Alamat <span class="required">*</span></label>
												<textarea name="notdua_alamat" class="md-input uk-form-width-medium"></textarea>
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
									<div>
										<div class="uk-form-row uk-grid">
											<div class="uk-width-medium-2-4">
												<div class="md-input-wrapper">
													<label>RT <span class="required">*</span></label>
													<input name="notdua_rt" type="number" class="md-input uk-form-width-medium">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
											<div class="uk-width-medium-2-4">
												<div class="md-input-wrapper">
													<label>RW <span class="required">*</span></label>
													<input name="notdua_rw" type="number" class="md-input uk-form-width-medium">
													<span class="md-input-bar uk-form-width-medium"></span>
												</div>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kelurahan </label>
												<input name="notdua_kelurahan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kecamatan <span class="required">*</span></label>
												<input name="notdua_kecamatan" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Kota/Kabupaten <span class="required">*</span></label>
												<input name="notdua_kota_kab" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>Jenis Identitas <span class="required">*</span></label>
												<input name="notdua_jnsidentitas" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper">
												<label>NIK <span class="required">*</span></label>
												<input name="notdua_nik" type="text" class="md-input uk-form-width-medium">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="md-card">
					<div class="md-card-content">
						<div class="uk-text-center">
							<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Save</button>
							<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Batal</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<a href="<?=$url; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		$('.timepicker').kendoTimePicker();

		App.datepicker();
		App.datepicker('.years', null, true);
		App.datepicker('.hours', null, true);


		$('#form_add').on('submit', function(e){
			e.preventDefault();

			var $this 	= $(this),
			url 		= $this.attr('action'),
			data 		= $this.serialize();

			var form = $('form')[0]	;
			var formData = new FormData(form);
			var err_res = 0,
			err_msg = [],
			suc_msg = [];

			$.ajax({
				url: url,
				type: 'post',
				dataType:'json',
				data: formData,
				processData: false,
				contentType: false,
				async: false,
				success : function(res) {
					if (res.sts == 5) {
						$('#alert').html(res.alert)
						App.scrollTop();
					}
					if (res.sts == 0) {
						App.notif('error', res.msg, 'error');
					}
					else if (res.inp == 2) {
						App.notif('Success', res.msg, 'success');
						$('.reload').trigger('click');
					}
					if (res.sts == 3 && res.inp == 2) {
						suc_res = '';
						var chk = 0;
						$.each(res, function( key, val ) {
							if(val['sts'] == 1) {
								suc_res = 1;
								suc_msg.push(val['msg']);
							} else {
								if(typeof val['msg'] !== "undefined") chk = 1;
								err_msg.push(val['msg']);
							}
						});
						if(suc_res == 1){
							App.notif('Success', suc_msg.join('<br>') , 'success');
							if(chk === 1){
								App.notif('Error', err_msg.join('<br>'), 'error');
							}
							$('.reload').trigger('click');
						}else{
							App.notif('Success', suc_msg.join('<br>') , 'success');
							if(chk === 1){
								App.notif('Error', err_msg.join('<br>'), 'error');
							}
							$('.reload').trigger('click');
						}
					}
				},
				error 	: function(res) {
					App.notif('Error', res.msg , 'error');
				}
			});
		});

	});
</script>