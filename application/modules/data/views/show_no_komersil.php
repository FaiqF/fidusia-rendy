<div class="uk-grid">
	<div class="uk-width-medium-1-1">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Upload Sertifikat</h3>
			</div>
			<div class="md-card-content">
				<div id="alert_error"></div>
				<form method="post" action="<?php echo $url.'/action_non_komersil/'.$id ?>" id="upload">
					<div class="uk-grid">
						<div class="uk-width-medium-2-4 uk-row-first">
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>Nomor Kontrak <span class="required">*</span></label>
											<input name="data_nokontrak" type="text" class="md-input uk-form-width-large" value="<?php echo $records->data_no_kontrak ?>" readonly>
											<input name="data_nokontrak_old" type="hidden" class="md-input uk-form-width-medium" value="<?php echo $records->data_no_kontrak ?>">
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="uk-grid">
								<div class="uk-width-medium-1-1 uk-row-first">
									<div class="uk-grid">
										<div class="uk-width-large-1-1">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-text-center uk-margin-large-top">
					</div>
				</form>
			</div>
		</div>

		<!-- Dynamic file download and delete -->
			<div class="md-card">
				<div class="md-card-content">
					<div class="uk-grid form-content">
						<div class="uk-width-medium-1-1 uk-row-first" style = "margin-top: 8px;">
							<!-- datatable true data -->
								<table id="datatable_ajax" class="uk-table" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th width="40">No</th>
											<th>Nama file</th>
											<th>Nomor akta</th>
											<th>Download</th>
										</tr>
									</thead>
									<tbody>
				
									</tbody>
								</table>
							<!-- end datatable true data -->
						</div>
					</div>
				</div>
			</div>
		<!-- end dynamic file download -->

		<!--  -->
			<div class="md-card">
				<div class="md-card-toolbar">
					<h3 class="md-card-toolbar-heading-text">Data kelengkapan</h3>
				</div>

				<div class="md-card-content">
					<div style="height: 500px;" id="my-container"></div>
				</div>
			</div>
		<!--  -->

		<!-- button reset table -->
			<div style="display:none;" class="reset-table">
				<button type="button" class="filter-reset md-btn md-btn-small md-btn-danger md-btn-wave-light waves-effect waves-button waves-light">Reset</button>
			</div>
		<!-- button reset -->
		
	</div>
</div>

<a href="<?=$url; ?>" class="ajaxify reload"></a>
<script>
	var global = {};
	$(document).ready(function(){
		var url 	= "<?=$url . '/select_non_komersil/' . $id;?>";
		var	header  = [
				null,
				null,
				null,
				null
			];
		var	order 	= [
				['1', 'asc']
			]
		var	sort 	= [-1, 0];

		App.setDatatable(url, header, order, sort); //paling akhir tambahkan sendiri jika tabel lebih dari satu
		

		global.table = $('#datatable_status').DataTable();
		var pdf = "<?php echo $pdf ?>";
		if (pdf !== '') {
			PDFObject.embed(pdf, "#my-container");
		} else {
			$('#my-container').hide();
		}

        var fileUpload = $('.uploadFile').dropify({
			messages: {
				'default': 'File Upload',
				'replace': '',
				'remove':  'Batal',
				'error': 'File hanya format PDF dan maksimal 2.5MB !'
			}
		});
	});
	
	function l_status(stat, ele, e, dtele) {
		e.preventDefault();

		var href = $(ele).attr('href');
		swal({
			title: "Are you sure?",
			text: 'Are you sure want to change Status ?',
			// html: html,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Yes',
		}).then(
			function(result){
				$.post(href, function(data, textStatus, xhr) {
					if(data.status == 1){
						location.reload();
						swal("Success", 'Successfully Delete data!', "success");
					}else{
						swal("Error", 'Error Delete data!', "error");
					}
				}, 'json');
			}, function(dismiss) {
				return false;
			}
		);
	};
	
</script>