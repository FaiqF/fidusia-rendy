<div class="uk-grid">
	<div class="uk-width-medium-1-1">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Upload Sertifikat</h3>
			</div>
			<div class="md-card-content">
				<div id="alert_error"></div>
				<form method="post" action="<?=$url; ?>/action_add" id="form_add">
					<div class="uk-grid">
						<div class="uk-width-medium-2-5 uk-row-first">
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>Kode Upload<span class="required">*</span></label>
											<input name="file_code" type="text" class="md-input uk-form-width-medium" value="<?php echo 'FU'.date('Ymdhis') ?>" readonly>
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
                                    <div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>File PDF </label>
										</div>
									</div>
								</div>
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>Nomor KTP<span class="required">*</span></label>
											<input name="nokontrak" type="text" class="md-input uk-form-width-medium" >
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
								</div>
                                <div class="uk-margin-medium-top">
									<div class="uk-grid">
										<div class="uk-width-large-1-1" style="width:200%">
											<input accept="application/pdf" data-max-file-size="10000K" name="pdf_path" type="file" id="uploadFile" class="uploadFile"/>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="uk-width-medium-1-5 uk-row first">
							<div class="uk-grid">
								<div class="uk-width-medium-1-1 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
									<div class="uk-form-row">
										<input name="tipe_kontrak" type="hidden" value="K">
										<span class="md-input-bar uk-form-width-medium"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-text-center uk-margin-large-top">
						<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Save</button>
						<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<a href="<?php echo $url; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){

		// handle form submit
			$('#form_add').on('submit', function(e){
				e.preventDefault();

				var $this 	= $(this),
				url 		= $this.attr('action'),
				data 		= $this.serialize();

				var form = $('form')[0]	;
				var formData = new FormData(form);
				var err_res = 0,
				err_msg = [],
				suc_msg = [];

				$.ajax({
					url: url,
					type: 'post',
					dataType:'json',
					data: formData,
					processData: false,
					contentType: false,
					async: false,
					success : function(res) {
						if (res.status == false) {
							App.notif('error', res.message, 'error');
						} else if (res.status == true) {
							App.notif('Success', res.message, 'success');
							$('.reload').trigger('click');
						}
					},
					error : function(res) {
						App.notif('Error', res.msg , 'error');
					}
				});
			});

		// end form submit

        var fileUpload = $('.uploadFile').dropify({
			messages: {
				'default': 'File Upload',
				'replace': '',
				'remove':  'Batal',
				'error': 'File hanya format PDF dan maksimal 2.5MB !'
			}
		});

	});

</script>