<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sertifikat extends MX_Controller {

    private $url        = 'jaminan';
    private $table_db   = 'data_transaksi';
    private $prefix     = 'data_';

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }


	public function index()
	{
        $data['name']       = "Jaminan";
        $data['url']        = base_url().$this->url;
        $data['upload']	    = [['name' => 'Upload', 'link' => base_url('jaminan/show_bulk_upload')]];
        $data['plugin']     = ['datatables', 'datatables_fixcolumns','kendo_ui'];

		$this->template->display($this->url, $data);
    }

    public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join = [
            ['users', 'users.user_id = data_transaksi.createdby', 'left']
        ];
		$where    = null;
		$where_e  = "data_flag in ('2','3')";
		$where    = [];
		$search   = [
            'kontrak'   => $this->prefix.'no_kontrak',
            'lastupdate' => 'createdat'
		];

		if (@$post['action'] == 'filter')
		{
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
                    // echo "some"; exit;
					if ( $key == 'lastupdate' )
					{
                        $tmp = $post['lastupdate'];
                        if($tmp[0] != "" && $tmp[1] != ""){
                            $where_e = $where_e."DATE(createdat) BETWEEN '".date('Y-m-d', strtotime($tmp[0]))."' AND '".date('Y-m-d', strtotime($tmp[1]))."'";
						}
					}else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			//$where[$this->table_prefix.'status <>'] = '99';
        }

		$keys 		= array_keys($search);
		@$order 	= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

        $select		= '*';
        $count 		= $this->m_global->count_data_all($this->table_db, $join, $where, $where_e );

		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


        $data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, $where_e, $order, $start, $length);
        $i = 1 + $start;

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
				$rows->data_no_kontrak,
                data_flag_status($rows->data_flag),
                date('d-m-Y h:i', strtotime($rows->createdat)),
                $this->_fileButton($rows->data_id),
                $this->_button($rows->data_id)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
        $records["recordsFiltered"] = $count;

		echo json_encode($records);
    }

    public function show_data($id)
    {
        $data['pdf'] = "";
        $data['name']    = 'Approve data';
        $data['url']     = base_url().$this->url;
        $data['plugin']  = ['kendo_ui','dropify','pdfobject' => 'js'];
        $data['id']      = $id;

        $joinQuery = [
            ['pdf_upload', 'data_pdf_id = pdf_id', 'left']
        ];
        $data['records'] = $this->m_global->get_data_all($this->table_db, $joinQuery, [strEncrypt($this->prefix.'id', true) => $id])[0];

        $noKontrak = $data['records']->data_no_kontrak;
        $name      = $data['records']->pdf_path;
        $filePath  = '/assets/uploads/data_konsumen/'.$noKontrak.'/'.$name;
        $fullPath  = FCPATH.$filePath;
        if( file_exists($fullPath) ){
            $data['pdf'] = base_url($filePath);
        }

        // pre($data['records']); exit;
        $this->template->display('jaminan_show', $data);
    }

    public function action_jaminan($id)
    {
        $post   = $this->input->post();

        if( $_FILES['jaminan_path']['size'] == 0 ){
            $result['msg'] = 'File tidak boleh kosong';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $filePath    = $_FILES['jaminan_path']['name'];
        $ext         = pathinfo($filePath, PATHINFO_EXTENSION);
        $allowedFile = [
            'pdf'
        ];

        // validation if file not pdf
        if ( !in_array(@strtolower($ext), $allowedFile) ) {
            $result['msg'] = 'File harus bertipe pdf';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $getData    = $this->m_global->get_data_all('data_transaksi', null, [strEncrypt($this->prefix.'id', true) => $id])[0];
        $noKontrak  = $getData->data_no_kontrak;
        $dir        = FCPATH."/assets/uploads/jaminan/".$noKontrak;

        if( !is_dir($dir) ){
            mkdir($dir);
            chmod($dir, 0777);
        }

        $tempName  = $_FILES['jaminan_path']['tmp_name'];
        $dirUpload = $dir.'/';
        $fileName  = date('Y-m-d').'_'.user_data()->user_nik.'_'.rand().'.pdf';

        // upload the file
        $doUpload = move_uploaded_file($tempName, $dirUpload.$fileName);

        if ( !$doUpload ){
            $result['msg'] = 'Gagal mengupload file';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $paramJaminan = [
            'jaminan_path'        => $fileName,
            'jaminan_createddate' => date('Y-m-d H:i:s'),
            'jaminan_createdby'   => user_data()->user_id
        ];

        $insertJaminan    = $this->m_global->insert('jaminan_upload', $paramJaminan);
        $idJaminan       = $this->db->insert_id();
        $paramDataTransaksi = [
            'data_jaminan_id' => $idjaminan,
            'data_flag' => '3',
            'updatedat' => date('Y-m-d H:i:s'),
        ];

        $updateDataTransaksi = $this->m_global->update('data_transaksi', $paramDataTransaksi, [strEncrypt($this->prefix.'id', true) => $id]);

        if( !$updateDataTransaksi ) {
            $result['sts'] = 0;
            $result['msg'] = 'Data gagal diupdate !';
            echo json_encode($result); exit;
        }

        $result['sts'] = 1;
        $result['msg'] = 'Data berhasil diupdate !';

        echo json_encode($result); exit();
    }

    public function action_invoice($id)
    {
        $post = $this->input->post();

        if( $_FILES['invoice_path']['size'] == 0 ){
            $result['msg'] = 'File tidak boleh kosong';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $filePath    = $_FILES['invoice_path']['name'];
        $ext         = pathinfo($filePath, PATHINFO_EXTENSION);
        $allowedFile = [
            'pdf'
        ];

        // validation if file not pdf
        if ( !in_array(@strtolower($ext), $allowedFile) ) {
            $result['msg'] = 'File harus bertipe pdf';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $getData   = $this->m_global->get_data_all('data_transaksi', null, [strEncrypt($this->prefix.'id', true) => $id])[0];
        $noKontrak = $getData->data_no_kontrak;
        $dir       = FCPATH."/assets/uploads/invoice/".$noKontrak;

        if( !is_dir($dir) ){
            mkdir($dir);
            chmod($dir, 0777);
        }

        $tempName  = $_FILES['invoice_path']['tmp_name'];
        $dirUpload = $dir.'/';
        $fileName  = date('Y-m-d').'_'.user_data()->user_nik.'_'.rand().'.pdf';

        // upload the file
        $doUpload = move_uploaded_file($tempName, $dirUpload.$fileName);

        if ( !$doUpload ){
            $result['msg'] = 'Gagal mengupload file';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $paramInvoice = [
            'invoice_path'        => $fileName,
            'invoice_createddate' => date('Y-m-d H:i:s'),
            'invoice_createdby'   => user_data()->user_id
        ];

        $insertInvoice      = $this->m_global->insert('invoice_upload', $paramInvoice);
        $idInvoice          = $this->db->insert_id();
        $paramDataTransaksi = [
            'data_invoice_id' => $idInvoice,
            'data_flag'       => '4',
            'updatedat'       => date('Y-m-d H:i:s')
        ];

        $updateDataTransaksi = $this->m_global->update('data_transaksi', $paramDataTransaksi, [strEncrypt($this->prefix.'id', true) => $id]);

        if( !$updateDataTransaksi ) {
            $result['sts'] = 0;
            $result['msg'] = 'Data gagal diupdate !';
            echo json_encode($result); exit;
        }

        $result['sts'] = 1;
        $result['msg'] = 'Data berhasil diupdate !';

        echo json_encode($result); exit();
    }

    public function download($id = '', $type = '')
    {
        $join = [
            ['pdf_upload', 'data_pdf_id = pdf_id', 'left'],
            ['jaminan_upload', 'data_jaminan_id = jaminan_id', 'left'],
            ['invoice_upload', 'data_invoice_id = invoice_id', 'left']
        ];
        $data      = $this->m_global->get_data_all('data_transaksi', $join, [strEncrypt($this->prefix.'id', true) => $id]);
        $noKontrak = $data[0]->data_no_kontrak;

        $file = [
            'UPF1' => ['path' => 'data_konsumen', 'file' => $data[0]->pdf_path, 'name' => 'data_konsumen_'],
            'UPF2' => ['path' => 'jaminan', 'file' => $data[0]->jaminan_path, 'name' => 'jaminan_'],
            'UPF3' => ['path' => 'invoice', 'file' => $data[0]->invoice_path, 'name' => 'invoice_'],
        ];

        $filePath = FCPATH . 'assets/uploads/'.$file[$type]['path'].'/'.$noKontrak.'/'.$file[$type]['file'];

        if(file_exists($filePath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.$file[$type]['name'].$noKontrak.'.pdf');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filePath));
            ob_clean();
            flush();
            readfile($filePath);
            exit;
        }
    }

    public function _button($id)
	{
        $button = null;
		$prev       = $id;
        $id         = strEncrypt($id);
        $getData = $this->m_global->get_data_all('data_transaksi', null, [strEncrypt($this->prefix.'id', true) => $id]);

        $flag = $getData[0]->data_flag;
        $preview    = '<a data-uk-tooltip title="Preview" class="md-btn md-btn-primary md-btn-mini md-btn-wave-light md-btn-icon waves-effect waves-button waves-light ajaxify" href="'.base_url($this->url.'/show_data').'/'.$id.'"><i class="uk-icon-map"></i></a>';
        $prosesFinance = [
            '2','3','4'
        ];

        if( in_array($flag,$prosesFinance) ) {

            $button     = $preview;
            return $button;
        }

		return $button;
    }

    public function _fileButton( $id )
    {
        $id = strEncrypt($id);
        $join = [['pdf_upload', 'pdf_id = data_pdf_id', 'left']];

        $data = $this->m_global->get_data_all('data_transaksi', $join, [strEncrypt($this->prefix.'id', true) => $id]);
        $fileKonsumen 	= '<a href="'.base_url('jaminan/download/'.$id.'/UPF1').'" target="_blank"><span class="uk-badge uk-badge-primary">Data konsumen</span></a> ';
        $fileJaminan = '<a href="'.base_url('jaminan/download/'.$id.'/UPF2').'" target="_blank" ><span class="uk-badge uk-badge-primary">Sertifikat</span></a> ';
        $fileInvoice    = '<a href="'.base_url('jaminan/download/'.$id.'/UPF3').'" target="_blank" ><span class="uk-badge uk-badge-primary">Invoice</span></a> ';
        
        $dataFlag           = $data[0]->data_flag;
        $prosesFinance      = ['1', '2', '9'];
        $prosesSertifikat   = ['3'];
        $prosesDone         = ['4', '5'];

        if( in_array($dataFlag, $prosesFinance) ) {
            $button = $fileKonsumen;
            return $button;
        }

        if( in_array($dataFlag, $prosesSertifikat) ) {
            $button = $fileKonsumen.$fileSertifikat;
            return $button;
        }

        if( in_array($dataFlag, $prosesDone) ) {
            $button = $fileKonsumen.$fileJaminan.$fileInvoice;
            return $button;
        }

    }

    public function show_bulk_upload()
    {
        $data['name']       = "Jaminan";
        $data['url']        = base_url().$this->url;
        $data['plugin']     = ['datatables', 'datatables_fixcolumns', 'dropify'];

		$this->template->display('jaminan_bulk_upload', $data);
    }

    public function action_bulk_upload()
    {
        // header('Content-type:application/json');
        $file         = $_FILES['fileinput'];
        $fileName     = $file['name'];
        $fileExplode  = explode('_', $file['name']);
        $fileType = str_replace(['.pdf', '.PDF'], '', $fileExplode[1]);

        // response if file size is empty
            $fileSize      = $_FILES['fileinput']['size'];
            $emptyFileSize = 0;
            if( $fileSize == $emptyFileSize ){
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $fileName,
                    'fileType'     => $this->_status_badge('info', $fileType),
                    'noKontrak'    => $this->_status_badge('error', 'Tidak ditemukan'),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'File tidak boleh kosong'
                ];

                echo json_encode( $response ); exit;
            }
        // end response

        // response if file extension not pdf
            $filePath             = $file['name'];
            $fileExtension        = pathinfo($filePath, PATHINFO_EXTENSION);
            $allowedFileExtension = [
                'pdf'
            ];
            if ( !in_array(@strtolower($fileExtension), $allowedFileExtension) ) {
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $fileName,
                    'fileType'     => $this->_status_badge('info', $fileType),
                    'noKontrak'    => $this->_status_badge('error', 'Tidak ditemukan'),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'File harus bertipe PDF'
                ];

                echo json_encode( $response ); exit;
            }
        //end response file extension

        // response if name format file is wrong
            $isTrueFormat = 2;
            if( count($fileExplode) != $isTrueFormat ) {
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $fileName,
                    'fileType'     => $this->_status_badge('info', $fileType),
                    'noKontrak'    => $this->_status_badge('error', 'Tidak ditemukan'),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'Penulisan format file salah'
                ];

                echo json_encode( $response ); exit;
            }
        // end response if format file wrong

        // response when type file not PNBP or jaminan
            $allowedTypeFile = [
                'PNBP',
                'JAMINAN'
            ];
            $fileType = str_replace(['.pdf', '.PDF'], '', $fileExplode[1]);
            if ( !in_array($fileType, $allowedTypeFile) ) {
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $fileName,
                    'fileType'     => $this->_status_badge('info', $fileType),
                    'noKontrak'    => $this->_status_badge('error', 'Tidak ditemukan'),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'Type file harus PNBP / JAMINAN'
                ];

                echo json_encode( $response ); exit;
            }
        // end response type file wrong
            
        // response if nomor kontrak not exist
            $noKontrak     = $fileExplode[0]; 
            $getData       = $this->m_global->get_data_all('data_transaksi', null, ['data_no_kontrak' => $noKontrak]);
            $getNoKontrak  = count($getData);
            $isNotExist    = 0;
            if( $getNoKontrak == $isNotExist ) {
                $insertDataTransaksi = [
                    'data_no_kontrak' => $noKontrak,
                    'data_flag'       => '1',
                    'data_cabang_id'  => '1',
                    'createdat'       => date('Y-m-d H:i:s'),
                    'createdby'       => user_data()->user_id,
                    'updatedat'       => date('Y-m-d H:i:s')
                ];
                $query = $this->m_global->insert('data_transaksi', $insertDataTransaksi);
            }
        // end response nomor kontrak not exist

        // do upload the process
            switch( $fileType ) {
                case "SERTIFIKAT" : 
                    $typeDir = 'jaminan';
                    break;
                case "PNBP" :
                    $typeDir = 'invoice';
                    break;
            }
            $dir = FCPATH."/assets/uploads/".$typeDir."/".$noKontrak;
            if( !is_dir($dir) ) {
                mkdir($dir);
                chmod($dir, 0777);
            }

            $tempName  = $file['tmp_name'];
            $dirUpload = $dir.'/';
            $paramfileName  = date('Y-m-d').'_'.user_data()->user_nik.'_'.rand().'.pdf';
            $doUpload = move_uploaded_file($tempName, $dirUpload.$paramfileName);
            if( !$doUpload ) {
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $paramfileName,
                    'fileType'     => $this->_status_badge('info', $fileType),
                    'noKontrak'    => $this->_status_badge('info', $noKontrak),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'Gagal mengupload file'
                ];

                echo json_encode( $response ); exit;
            }
        // end do upload process

        // upload process if type file is invoice
            if( $fileType == 'PNBP' ) {
                $paramInvoice = [
                    'invoice_path'        => $paramfileName,
                    'invoice_createddate' => date('Y-m-d H:i:s'),
                    'invoice_createdby'   => user_data()->user_id
                ];
        
                $insertInvoice      = $this->m_global->insert('invoice_upload', $paramInvoice);
                $idInvoice          = $this->db->insert_id();
                $paramDataTransaksi = [
                    'data_invoice_id' => $idInvoice,
                    'data_flag'       => '4',
                    'updatedat'       => date('Y-m-d H:i:s')
                ];
                $updateDataTransaksi = $this->m_global->update('data_transaksi', $paramDataTransaksi, ['data_no_kontrak' => $noKontrak]);

                // failed response when sertifikat not uploaded
                    if( !$updateDataTransaksi ) {
                        $response['status'] = false;
                        $response['data'] = [
                            'fileName'     => $fileName,
                            'fileType'     => $this->_status_badge('info', $fileType),
                            'noKontrak'    => $this->_status_badge('info', $noKontrak),
                            'statusUpload' => $this->_status_badge('error', 'Failed'),
                            'note'         => 'Gagal mengupload data'
                        ];

                        echo json_encode( $response ); exit;
                    }
                // end failed response

                // success response when jaminan uploaded
                    $response['status'] = true;
                    $response['data'] = [
                        'fileName'     => $fileName,
                        'noKontrak'    => $this->_status_badge('info', $noKontrak),
                        'fileType'     => $this->_status_badge('info', $fileType),
                        'statusUpload' => $this->_status_badge('success', 'Success'),
                        'note'         => 'Berhasil mengupload data'
                    ];

                    echo json_encode( $response ); exit;
                // end success response

            }
        // end invoice process
        
        // upload process if type file is sertifikat
            if( $fileType == 'JAMINAN' ) {
                $paramJaminan = [
                    'jaminan_path'        => $paramfileName,
                    'jaminan_createddate' => date('Y-m-d H:i:s'),
                    'jaminan_createdby'   => user_data()->user_id
                ];   
                $insertJaminan    = $this->m_global->insert('jaminan_upload', $paramJaminan);
                
                $idJaminan       = $this->db->insert_id();
                $paramDataTransaksi = [
                    'data_jaminan_id' => $idJaminan,
                    'data_flag'          => '4',
                    'updatedat'          => date('Y-m-d H:i:s'),
                ];
                $updateDataTransaksi = $this->m_global->update('data_transaksi', $paramDataTransaksi, ['data_no_kontrak' => $noKontrak]);

                // failed response when jaminan not uploaded
                    if( !$updateDataTransaksi ) {
                        $response['status'] = false;
                        $response['data'] = [
                            'fileName'     => $fileName,
                            'fileType'     => $this->_status_badge('info', $fileType),
                            'noKontrak'    => $this->_status_badge('info', $noKontrak),
                            'statusUpload' => $this->_status_badge('error', 'Gagal mengupload data'),
                            'note'         => ''
                        ];

                        echo json_encode( $response ); exit;
                    }
                // end failed response

                // success response when jaminan uploaded
                    $response['status'] = true;
                    $response['data'] = [
                        'fileName'     => $fileName,
                        'noKontrak'    => $this->_status_badge('info', $noKontrak),
                        'fileType'     => $this->_status_badge('info', 'Jaminan'),
                        'statusUpload' => $this->_status_badge('success', 'Success'),
                        'note'         => 'Berhasil mengupload data'
                    ];

                    echo json_encode( $response ); exit;
                // end success response

            }
        // end jaminan process

    }

    public function _status_badge( $status = 'error', $message = 'unknown' )
    {
        $arr = [
            'error' => '<span class="uk-badge uk-badge-danger">'.$message.'</span>',
            'warning' => '<span class="uk-badge uk-badge-warning">'.$message.'</span>',
            'info' => '<span class="uk-badge uk-badge-info">'.$message.'</span>',
            'success' => '<span class="uk-badge uk-badge-success">'.$message.'</span>'
        ];

        return $arr[$status];
    }

}

/* End of file Jaminan.php */
/* Location: ./application/modules/jaminan/controllers/Jaminan.php */