<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sertifikat extends MX_Controller {

    private $url        = 'sertifikat';
    private $table_db   = 'data_transaksi';
    private $prefix     = 'data_';

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }


	public function index()
	{
        $data['name']       = "Sertifikat";
        $data['url']        = base_url().$this->url;
        $data['upload']	    = [['name' => 'Upload', 'link' => base_url('sertifikat/show_bulk_upload')]];
        $data['plugin']     = ['datatables', 'datatables_fixcolumns','kendo_ui'];

		$this->template->display($this->url, $data);
    }

    public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join = [
            ['users', 'users.user_id = data_transaksi.createdby', 'left']
        ];
		$where    = null;
		$where_e  = "data_flag in ('2','3')";
		$where    = [];
		$search   = [
            'kontrak'   => $this->prefix.'no_kontrak',
            'lastupdate' => 'createdat'
		];

		if (@$post['action'] == 'filter')
		{
            $where_e = "data_flag in ('2', '3', '4', '5')";
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
                    // echo "some"; exit;
					if ( $key == 'lastupdate' )
					{
                        $tmp = $post['lastupdate'];
                        if($tmp[0] != "" && $tmp[1] != ""){
                            $where_e = $where_e."DATE(createdat) BETWEEN '".date('Y-m-d', strtotime($tmp[0]))."' AND '".date('Y-m-d', strtotime($tmp[1]))."'";
						}
					}else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			//$where[$this->table_prefix.'status <>'] = '99';
        }

		$keys 		= array_keys($search);
		@$order 	= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

        $select		= '*';
        $count 		= $this->m_global->count_data_all($this->table_db, $join, $where, $where_e );

		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


        $data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, $where_e, $order, $start, $length);
        $i = 1 + $start;

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
                $rows->data_no_kontrak,
                flag_badge_tipe_kontrak($rows->data_tipe_kontrak),
                data_flag_status($rows->data_flag),
                date('d-m-Y h:i', strtotime($rows->createdat)),
                $this->_fileButton($rows->data_id),
                $this->_button($rows->data_id)
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
        $records["recordsFiltered"] = $count;

		echo json_encode( $records );
    }

    public function select_non_komersil( $id = '' )
	{
        $post       = $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);        
        $records = [];
		$join = [
            ['data_transaksi_upload', 'data_transaksi_upload.data_id = data_transaksi.data_id', 'inner'],
            ['sertifikat_upload', 'data_transaksi_upload.sertifikat_id = sertifikat_upload.sertifikat_id', 'left'],
            ['invoice_upload', 'data_transaksi_upload.invoice_id = invoice_upload.invoice_id', 'left']
        ];
		$where    = [strEncrypt('data_transaksi.data_id', true) => $id];
        $select	  = 'data_transaksi.data_no_kontrak, data_transaksi.data_id, data_transaksi_upload.id, sertifikat_upload.sertifikat_path, sertifikat_upload.sertifikat_id, data_transaksi_upload.data_nomor_akta, data_transaksi_upload.invoice_id, invoice_upload.invoice_path';
        $order    = ['data_nomor_akta', 'asc']; 
        $countData = $this->m_global->count_data_all($this->table_db, $join, $where);
        $data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, null, $order);

        $i = 1;
		foreach ( $data as $rows ) {
			$records[] = [
                $i,
                $rows->data_no_kontrak.'_'.$rows->data_nomor_akta.'.pdf',
                $this->_status_badge('info', $rows->data_nomor_akta),
                $this->_fileButtonNonkomersil($rows->data_id, $rows->sertifikat_id, $rows->invoice_id),
                $this->_sertifikatButton($rows->data_id, $rows->sertifikat_id)
            ];
            $i++;
        }

        $response['draw']            = $sEcho;
        $response["recordsTotal"]    = $countData;
        $response["recordsFiltered"] = $countData;
        $response['data']            = $records;

        echo json_encode( $response ); exit;
    }

    public function show_data($id)
    {
        $data['pdf'] = "";
        $data['name']    = 'Approve data';
        $data['url']     = base_url().$this->url;
        $data['plugin']  = ['kendo_ui', 'datatables', 'datatables_fixcolumns', 'dropify','pdfobject' => 'js'];
        $data['id']      = $id;
        $joinQuery = [
            ['pdf_upload', 'data_pdf_id = pdf_id', 'left']
        ];
        $data['records'] = $this->m_global->get_data_all($this->table_db, $joinQuery, [strEncrypt($this->prefix.'id', true) => $id])[0];

        $noKontrak = $data['records']->data_no_kontrak;
        $name      = $data['records']->pdf_path;
        $filePath  = '/assets/uploads/data_konsumen/'.$noKontrak.'/'.$name;
        $fullPath  = FCPATH.$filePath;
        if( file_exists($fullPath) ){
            $data['pdf'] = base_url($filePath);
        }
        $tipeKontrak = $data['records']->data_tipe_kontrak;

        if( $tipeKontrak == 'K') {
            $views = 'sertifikat_show_komersil';
        } else {
            $views = 'sertifikat_show';
        }
        
        $this->template->display($views, $data);
    }

    public function action_sertifikat($id)
    {
        $post   = $this->input->post();

        if( $_FILES['sertifikat_path']['size'] == 0 ){
            $result['msg'] = 'File tidak boleh kosong';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $filePath    = $_FILES['sertifikat_path']['name'];
        $ext         = pathinfo($filePath, PATHINFO_EXTENSION);
        $allowedFile = [
            'pdf'
        ];

        // validation if file not pdf
        if ( !in_array(@strtolower($ext), $allowedFile) ) {
            $result['msg'] = 'File harus bertipe pdf';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $getData    = $this->m_global->get_data_all('data_transaksi', null, [strEncrypt($this->prefix.'id', true) => $id])[0];
        $noKontrak  = $getData->data_no_kontrak;
        $dir        = FCPATH."/assets/uploads/sertifikat/".$noKontrak;

        if( !is_dir($dir) ){
            mkdir($dir);
            chmod($dir, 0777);
        }

        $tempName  = $_FILES['sertifikat_path']['tmp_name'];
        $dirUpload = $dir.'/';
        $fileName  = date('Y-m-d').'_'.user_data()->user_nik.'_'.rand().'.pdf';

        // upload the file
        $doUpload = move_uploaded_file($tempName, $dirUpload.$fileName);

        if ( !$doUpload ){
            $result['msg'] = 'Gagal mengupload file';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $paramSertifikat = [
            'sertifikat_path'        => $fileName,
            'sertifikat_createddate' => date('Y-m-d H:i:s'),
            'sertifikat_createdby'   => user_data()->user_id
        ];

        $insertSerifikat    = $this->m_global->insert('sertifikat_upload', $paramSertifikat);
        $idSertifikat       = $this->db->insert_id();
        $paramDataTransaksi = [
            'data_sertifikat_id' => $idSertifikat,
            'data_flag' => '3',
            'updatedat' => date('Y-m-d H:i:s'),
        ];

        $updateDataTransaksi = $this->m_global->update('data_transaksi', $paramDataTransaksi, [strEncrypt($this->prefix.'id', true) => $id]);

        if( !$updateDataTransaksi ) {
            $result['sts'] = 0;
            $result['msg'] = 'Data gagal diupdate !';
            echo json_encode($result); exit;
        }

        $result['sts'] = 1;
        $result['msg'] = 'Data berhasil diupdate !';

        echo json_encode($result); exit();
    }

    public function action_non_komersil($id)
    {
        $post = $this->input->post();
        $type = $post['tipe_kontrak'];
        
        switch( $type ) {
            case "sertifikat" : 
                $this->action_sertifikat_non_komersil($id);
                break;
            case "invoice" :
                $this->action_invoice_non_komersil($id);
                break;
        }


    }

    public function action_sertifikat_non_komersil($id)
    {
        $file         = $_FILES['fileinput'];
        $fileName     = $file['name'];

        $getData      = $this->m_global->get_data_all('data_transaksi', null, [strEncrypt('data_id', true) => $id])[0];
        $noKontrak    = $getData->data_no_kontrak;

        // response if file size is empty
            $fileSize      = $_FILES['fileinput']['size'];
            $emptyFileSize = 0;
            if( $fileSize == $emptyFileSize ){
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $fileName,
                    'nomorAkta'    => $this->_status_badge('error', 'Tidak ditemukan'),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'File tidak boleh kosong',
                    'fileButton'   => ''
                ];

                echo json_encode( $response ); exit;
            }
        // end response

        // response if file extension not pdf
            $filePath             = $file['name'];
            $fileExtension        = pathinfo($filePath, PATHINFO_EXTENSION);
            $allowedFileExtension = [
                'pdf'
            ];
            if ( !in_array(@strtolower($fileExtension), $allowedFileExtension) ) {
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $fileName,
                    'nomorAkta'    => $this->_status_badge('error', 'Tidak ditemukan'),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'File harus bertipe PDF',
                    'fileButton'   => ''
                ];

                echo json_encode( $response ); exit;
            }
        //end response file extension

        // validation if file format was wrong
            $isTrueFormat = 3;
            $countFileExplode  = count(explode('_', $fileName));
            if( $countFileExplode != $isTrueFormat ) {
                $response = false;
                $response['data'] = [
                    'fileName'    => $fileName,
                    'nomorAkta'      => $this->_status_badge('error', 'Tidak ditemukan'),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'        => 'File Format salah',
                    'fileButton'  => ''
                ];

                echo json_encode( $response ); exit;
            }
        // end validation
            
        // begin process

            // delete old data transaksi upload
                $nomorAkta = str_replace(['.pdf', '.PDF'], '', explode('_', $fileName)[1]);
                $joinExistData = [
                    ['data_transaksi_upload', 'data_transaksi.data_id = data_transaksi_upload.data_id', 'inner'],
                    ['sertifikat_upload', 'sertifikat_upload.sertifikat_id = data_transaksi_upload.sertifikat_id', 'left'],
                    ['invoice_upload', 'invoice_upload.invoice_id = data_transaksi_upload.invoice_id', 'left']
                ];

                $whereExistData = [
                    strEncrypt('data_transaksi.data_id', true) => $id,
                    'data_nomor_akta' => $nomorAkta
                ];
                $isData = (int) $this->m_global->count_data_all('data_transaksi', $joinExistData, $whereExistData);
                $isNotExist = 0;
                if( $isData > $isNotExist ) {
                    $selectExistData = 'data_transaksi.data_no_kontrak, data_transaksi_upload.sertifikat_id, data_transaksi_upload.invoice_id, data_transaksi_upload.id, sertifikat_upload.sertifikat_path,invoice_upload.invoice_path' ;
                    $getExistData = $this->m_global->get_data_all('data_transaksi', $joinExistData, $whereExistData, $selectExistData)[0];
                    if( $getExistData->sertifikat_id != '' ) {
                        $fileSertifikat = FCPATH."/assets/uploads/sertifikat/".$noKontrak."/". $getExistData->sertifikat_path;
                        if( file_exists($fileSertifikat) ) {
                            unlink($fileSertifikat);
                            $this->m_global->delete('sertifikat_upload', ['sertifikat_id' => $getExistData->sertifikat_id]);
                        }
                    }

                    if ( $getExistData->invoice_id != '' ) {
                        $fileInvoice = FCPATH."/assets/uploads/invoice/".$noKontrak.$getExistData->invoice_path;
                        if ( file_exists($fileInvoice) ) {
                            $this->m_global->delete('invoice_upload', ['invoice_upload' => $getExistData->invoice_id]);
                            unlink( $fileInvoice );
                        }
                    }

                    $this->m_global->delete('data_transaksi_upload', ['id' => $getExistData->id]);
                }
            // end delete old data


            $dir = FCPATH."/assets/uploads/sertifikat/".$noKontrak;
            if( !is_dir($dir) ) {
                mkdir($dir);
                chmod($dir, 0777);
            }
            $tempName  = $file['tmp_name'];
            $dirUpload = $dir.'/';
            $paramfileName  = date('Y-m-d').'_'.user_data()->user_nik.'_'.rand().'.pdf';
            $doUpload = move_uploaded_file($tempName, $dirUpload.$paramfileName);

            if( !$doUpload ) {
                $response['status'] = false;
                $response['data'] = [
                    'fileName' => $fileName,
                    'nomorAkta' => $this->_status_badge('info', $noKontrak),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note' => 'Gagal mengupload file',
                    'fileButton'   => ''
                ];

                echo json_encode( $response ); exit;
            }

            $paramSertifikat = [
                'sertifikat_path' => $paramfileName,
                'sertifikat_createddate' => date('Y-m-d H:i:s'),
                'sertifikat_createdby'   => user_data()->user_id
            ];

            $insertSertifikat = $this->m_global->insert('sertifikat_upload', $paramSertifikat);
            $idSertifikat = $this->db->insert_id();

            if( !$insertSertifikat ) {
                $response['status'] = false;
                $response['data'] = [
                    'fileName' => $fileName,
                    'nomorAkta' => $this->_status_badge('info', $noKontrak),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note' => 'Gagal mengupload file'
                ];

                echo json_encode( $response ); exit;
            }

            $noAkta = str_replace('.pdf', '', explode('_', $fileName)[1]);
            // pre($noAkta); exit;
            $paramDataSertifikat = [
                'data_id' => $getData->data_id,
                'data_nomor_akta' => $nomorAkta,
                'sertifikat_id' => $idSertifikat
            ];
            $this->m_global->insert('data_transaksi_upload', $paramDataSertifikat);

            $response['status'] = true;
            $response['data'] = [
                'fileName'     => $fileName,
                'nomorAkta'    => $this->_status_badge('info', $noAkta),
                'statusUpload' => $this->_status_badge('success', 'Success'),
                'note'         => 'Data berhasil ditambahkan',
                'fileButton'   => ''
            ];

            echo json_encode( $response ); exit;
        // end process
    }

    public function action_invoice_non_komersil($id)
    {
        $file         = $_FILES['fileinput'];
        $fileName     = $file['name'];
        $select = "data_transaksi.data_id, data_transaksi.data_no_kontrak, sertifikat_upload.sertifikat_path, invoice_upload.invoice_path";
        $join = [
            ['data_transaksi_upload', 'data_transaksi.data_id = data_transaksi_upload.data_id', 'left'],
            ['sertifikat_upload', 'sertifikat_upload.sertifikat_id = data_transaksi_upload.sertifikat_id', 'left'],
            ['invoice_upload', 'invoice_upload.invoice_id = data_transaksi_upload.invoice_id', 'left']

        ];
        $getData      = $this->m_global->get_data_all('data_transaksi', $join, [strEncrypt('data_transaksi.'.$this->prefix.'id', true) => $id])[0];
        $noKontrak    = $getData->data_no_kontrak;

        // response if file size is empty

            $fileSize      = $_FILES['fileinput']['size'];
            $emptyFileSize = 0;
            if( $fileSize == $emptyFileSize ){
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $fileName,
                    'nomorAkta'    => $this->_status_badge('error', 'Tidak ditemukan'),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'File tidak boleh kosong',
                    'fileButton'   => '',
                    'actionButton' => ''
                ];

                echo json_encode( $response ); exit;
            }
        // end response

        // response if file extension not pdf
            $filePath             = $file['name'];
            $fileExtension        = pathinfo($filePath, PATHINFO_EXTENSION);
            $allowedFileExtension = [
                'pdf'
            ];
            if ( !in_array(@strtolower($fileExtension), $allowedFileExtension) ) {
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $fileName,
                    'nomorAkta'    => $this->_status_badge('error', 'Tidak ditemukan'),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'File harus bertipe PDF',
                    'fileButton'   => '',
                    'actionButton' => ''
                ];

                echo json_encode( $response ); exit;
            }
        // end response file extension
        
        // response if no akta not match
            $nomorAkta = str_replace(['.pdf', '.PDF'], '', explode('_', $fileName)[1]);
            $whereAkta = [  
              strEncrypt('data_transaksi.'.$this->prefix.'id', true) => $id,
              'data_transaksi_upload.data_nomor_akta' => $nomorAkta  
            ];
            $checkNomorAkta  = $this->m_global->count_data_all('data_transaksi', $join, $whereAkta);
            if( !$checkNomorAkta ) {
                $response['status'] = false;
                $response['data']   = [
                  'fileName'     => $fileName,
                  'nomorAkta'    => $this->_status_badge('info', $nomorAkta),
                  'statusUpload' => $this->_status_badge('error', 'Error'),
                  'note'         => 'Nomor akta tidak ditemukan',
                  'fileButton'   => '',
                  'actionButton' => ''
                ];

                echo json_encode( $response ); exit;
            }
        // end response

        // begin process
            $dir = FCPATH."/assets/uploads/invoice/".$noKontrak;
            if( !is_dir($dir) ) {
                mkdir($dir);
                chmod($dir, 0777);
            }
            $tempName       = $file['tmp_name'];
            $dirUpload      = $dir.'/';
            $paramfileName  = date('Y-m-d').'_'.user_data()->user_nik.'_'.rand().'.pdf';
            $doUpload       = move_uploaded_file($tempName, $dirUpload.$paramfileName);

            if( !$doUpload ) {
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $fileName,
                    'nomorAkta'    => $this->_status_badge('info', $noKontrak),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'Gagal mengupload file',
                    'fileButton'   => '',
                    'actionButton' => ''
                ];

                echo json_encode( $response ); exit;
            }

            $paramInvoice = [
                'invoice_path'        => $paramfileName,
                'invoice_createddate' => date('Y-m-d H:i:s'),
                'invoice_createdby'   => user_data()->user_id
            ];

            $insertInvoice = $this->m_global->insert('invoice_upload', $paramInvoice);
            $idInvoice     = $this->db->insert_id();

            if( !$insertInvoice ) {
                $response['status'] = false;
                $response['data'] = [
                    'fileName'      => $fileName,
                    'noKontrak'     => $this->_status_badge('info', $noKontrak),
                    'statusUpload'  => $this->_status_badge('error', 'Error'),
                    'note'          => 'Gagal mengupload file',
                    'fileButton'    => '',
                    'actionButton'  => ''
                ];

                echo json_encode( $response ); exit;
            }

            $getIdDataUpload = $this->m_global->get_data_all('data_transaksi', $join, $whereAkta, 'id')[0]->id;
            $paramDataInvoice = [
                'invoice_id' => $idInvoice
            ];
            $whereDataInvoice = [
                'id' =>  $getIdDataUpload
            ];

            $updateData = $this->m_global->update('data_transaksi_upload', $paramDataInvoice, $whereDataInvoice);
            $updateFlag = $this->m_global->update('data_transaksi', ['data_flag' => '4'], [strEncrypt('data_transaksi.'.$this->prefix.'id', true) => $id]);

            if( !$updateData ) {
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $fileName,
                    'nomorAkta'    => $this->_status_badge('info', $nomorAkta),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'PNBP gagal ditambahkan',
                    'fileButton'   => '',
                    'actionButton' => ''
                ];
    
                echo json_encode( $response ); exit;
            }

            $response['status'] = true;
            $response['data'] = [
                'fileName'     => $fileName,
                'nomorAkta'    => $this->_status_badge('info', $nomorAkta),
                'statusUpload' => $this->_status_badge('success', 'sucess'),
                'note'         => 'PNBP berhasil ditambahkan',
                'fileButton'   => ''
            ];

            echo json_encode( $response ); exit;
        // end process
    }

    public function action_invoice($id)
    {
        $post = $this->input->post();

        if( $_FILES['invoice_path']['size'] == 0 ){
            $result['msg'] = 'File tidak boleh kosong';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $filePath    = $_FILES['invoice_path']['name'];
        $ext         = pathinfo($filePath, PATHINFO_EXTENSION);
        $allowedFile = [
            'pdf'
        ];

        // validation if file not pdf
        if ( !in_array(@strtolower($ext), $allowedFile) ) {
            $result['msg'] = 'File harus bertipe pdf';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $getData   = $this->m_global->get_data_all('data_transaksi', null, [strEncrypt($this->prefix.'id', true) => $id])[0];
        $noKontrak = $getData->data_no_kontrak;
        $dir       = FCPATH."/assets/uploads/invoice/".$noKontrak;

        if( !is_dir($dir) ){
            mkdir($dir);
            chmod($dir, 0777);
        }

        $tempName  = $_FILES['invoice_path']['tmp_name'];
        $dirUpload = $dir.'/';
        $fileName  = date('Y-m-d').'_'.user_data()->user_nik.'_'.rand().'.pdf';

        // upload the file
        $doUpload = move_uploaded_file($tempName, $dirUpload.$fileName);

        if ( !$doUpload ){
            $result['msg'] = 'Gagal mengupload file';
            $result['sts'] = 0;

            echo json_encode($result); exit;
        }

        $paramInvoice = [
            'invoice_path'        => $fileName,
            'invoice_createddate' => date('Y-m-d H:i:s'),
            'invoice_createdby'   => user_data()->user_id
        ];

        $insertInvoice      = $this->m_global->insert('invoice_upload', $paramInvoice);
        $idInvoice          = $this->db->insert_id();
        $paramDataTransaksi = [
            'data_invoice_id' => $idInvoice,
            'data_flag'       => '4',
            'updatedat'       => date('Y-m-d H:i:s')
        ];

        $updateDataTransaksi = $this->m_global->update('data_transaksi', $paramDataTransaksi, [strEncrypt($this->prefix.'id', true) => $id]);

        if( !$updateDataTransaksi ) {
            $result['sts'] = 0;
            $result['msg'] = 'Data gagal diupdate !';
            echo json_encode($result); exit;
        }

        $result['sts'] = 1;
        $result['msg'] = 'Data berhasil diupdate !';

        echo json_encode($result); exit();
    }

    public function download($id = '', $type = '')
    {
        $join = [
            ['pdf_upload', 'data_pdf_id = pdf_id', 'left'],
            ['sertifikat_upload', 'data_sertifikat_id = sertifikat_id', 'left'],
            ['invoice_upload', 'data_invoice_id = invoice_id', 'left']
        ];
        $data      = $this->m_global->get_data_all('data_transaksi', $join, [strEncrypt($this->prefix.'id', true) => $id]);
        $noKontrak = $data[0]->data_no_kontrak;

        $file = [
            'UPF1' => ['path' => 'data_konsumen', 'file' => $data[0]->pdf_path, 'name' => 'data_konsumen_'],
            'UPF2' => ['path' => 'sertifikat', 'file' => $data[0]->sertifikat_path, 'name' => 'sertifikat_'],
            'UPF3' => ['path' => 'invoice', 'file' => $data[0]->invoice_path, 'name' => 'invoice_'],
        ];

        $filePath = FCPATH . 'assets/uploads/'.$file[$type]['path'].'/'.$noKontrak.'/'.$file[$type]['file'];

        if(file_exists($filePath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.$file[$type]['name'].$noKontrak.'.pdf');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filePath));
            ob_clean();
            flush();
            readfile($filePath);
            exit;
        }
    }

    public function download_non_komersil( $id = '', $type = '', $fileId = '' )
    {
        $filedKey = [
            'UPF2' => ['field' => 'data_transaksi_upload.sertifikat_id'],
            'UPF3' => ['field' => 'data_transaksi_upload.invoice_id']
        ];
        $select = 'data_transaksi.data_id,data_transaksi.data_no_kontrak, data_transaksi_upload.sertifikat_id, sertifikat_upload.sertifikat_path, invoice_upload.invoice_path';
        $join = [
            ['data_transaksi_upload', 'data_transaksi_upload.data_id = data_transaksi.data_id', 'left'],
            ['sertifikat_upload', 'sertifikat_upload.sertifikat_id = data_transaksi_upload.sertifikat_id', 'left'],
            ['invoice_upload', 'invoice_upload.invoice_id = data_transaksi_upload.invoice_id', 'left']
        ];
        $where = [
            strEncrypt('data_transaksi.'.$this->prefix.'id', true) => $id,
            $filedKey[$type]['field'] => $fileId
        ];

        $data = $this->m_global->get_data_all('data_transaksi', $join,  $where, $select);
        // echo $this->db->last_query(); exit;
        $noKontrak = $data[0]->data_no_kontrak;
        $file = [
            'UPF2' => ['path' => 'sertifikat', 'file' => $data[0]->sertifikat_path, 'name' => 'sertifikat_'],
            'UPF3' => ['path' => 'invoice', 'file' => $data[0]->invoice_path, 'name' => 'invoice_']
        ];

        $filePath = FCPATH . 'assets/uploads/'.$file[$type]['path'].'/'.$noKontrak.'/'.$file[$type]['file'];

        if(file_exists($filePath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.$file[$type]['name'].$noKontrak.'.pdf');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filePath));
            ob_clean();
            flush();
            readfile($filePath);
            exit;
        }

    }

    public function action_delete_non_komersil( $id = '', $idSertifikat = '' )
    {

        $select = 'data_transaksi_upload.id,data_transaksi.data_id,sertifikat_upload.sertifikat_path,invoice_upload.invoice_path';
        $join = [
            ['data_transaksi_upload', 'data_transaksi_upload.data_id = data_transaksi.data_id', 'left'],
            ['sertifikat_upload', 'data_transaksi_upload.sertifikat_id = sertifikat_upload.sertifikat_id', 'left'],
            ['invoice_upload', 'data_transaksi_upload.invoice_id = invoice_upload.invoice_id', 'left']
        ];

        $where = [
            'data_transaksi.data_id' => $id,
            'data_transaksi_upload.sertifikat_id' => $idSertifikat
        ];
        $getData = $this->m_global->get_data_all('data_transaksi', $join, $where, $select)[0];

        // pre($getData); 
        // exit;

        $sertifikatFile = FCPATH.'/assets/uploads/sertifikat/'.$getData->sertifikat_path;
        $invoiceFile = FCPATH.'/assets/uploads/invoice'.$getData->invoice_path;

        if( $getData->sertifikat_path != null ) {
            if ( file_exists($sertifikatFile )) {
                unlink($sertifikatFile);
            }
        }

        if( $getData->invoice_path != null ) {
            if(file_exists($invoiceFile)) {
                unlink($invoiceFile);
            }
        }

        $idTransaksi = $getData->data_id;
        $idFile = $getData->id;
        $whereDelete = [
            'data_id' => $idTransaksi,
            'id'      => $idFile
        ];
        $deleteFile = $this->m_global->delete('data_transaksi_upload', $whereDelete);

        if( !$deleteFile ) {
            $response['status'] = 0;
            $response['message'] = 'Gagal menghapus data';

            echo json_encode( $response ); exit;
        }

        $response['status'] = 1;
        $response['message'] = 'Berhasil menghapus data';

        echo json_encode( $response ); exit;
    }

    public function _button($id)
	{
        $button  = null;
		$prev    = $id;
        $id      = strEncrypt($id);
        $getData = $this->m_global->get_data_all('data_transaksi', null, [strEncrypt($this->prefix.'id', true) => $id]);

        $flag = $getData[0]->data_flag;
        $preview    = '<a data-uk-tooltip title="Preview" class="md-btn md-btn-primary md-btn-mini md-btn-wave-light md-btn-icon waves-effect waves-button waves-light ajaxify" href="'.base_url($this->url.'/show_data').'/'.$id.'"><i class="uk-icon-map"></i></a>';
        $prosesFinance = [
            '2','3','4'
        ];

        if( in_array($flag,$prosesFinance) ) {

            $button     = $preview;
            return $button;
        }

		return $button;
    }

    public function _fileButton( $id )
    {
        $id = strEncrypt($id);
        $join = [['pdf_upload', 'pdf_id = data_pdf_id', 'left']];

        $data = $this->m_global->get_data_all('data_transaksi', $join, [strEncrypt($this->prefix.'id', true) => $id]);
        $fileKonsumen 	= '<a href="'.base_url('sertifikat/download/'.$id.'/UPF1').'" target="_blank"><span class="uk-badge uk-badge-primary">Data konsumen</span></a> ';
        $fileSertifikat = '<a href="'.base_url('sertifikat/download/'.$id.'/UPF2').'" target="_blank" ><span class="uk-badge uk-badge-primary">Sertifikat</span></a> ';
        $fileInvoice    = '<a href="'.base_url('sertifikat/download/'.$id.'/UPF3').'" target="_blank" ><span class="uk-badge uk-badge-primary">Invoice</span></a> ';
        
        $dataFlag           = $data[0]->data_flag;
        $prosesFinance      = ['1', '2', '9'];
        $prosesSertifikat   = ['3'];
        $prosesDone         = ['4', '5'];
        $tipeKontrak        = $data[0]->data_tipe_kontrak;
        // pre($data); exit;

       if( $tipeKontrak == 'NK' ) {
           return $fileKonsumen;
       }
        if( in_array($dataFlag, $prosesFinance) ) {
            $button = $fileKonsumen;
            return $button;
        }

        if( in_array($dataFlag, $prosesSertifikat) ) {
            $button = $fileKonsumen.$fileSertifikat;
            return $button;
        }

        if( in_array($dataFlag, $prosesDone) ) {
            $button = $fileKonsumen.$fileSertifikat.$fileInvoice;
            return $button;
        }

    }

    public function _fileButtonNonkomersil( $id = '', $sertifikatId = '', $invoiceId = '' )
    {
        $id = strEncrypt($id);

        $fileSertifikat = '<a href="'.base_url('sertifikat/download_non_komersil/'.$id.'/UPF2/'.$sertifikatId).'" target="_blank" ><span class="uk-badge uk-badge-primary">Sertifikat</span></a> ';
        $fileInvoice    = '<a href="'.base_url('sertifikat/download_non_komersil/'.$id.'/UPF3/'.$invoiceId).'" target="_blank" ><span class="uk-badge uk-badge-primary">Invoice</span></a> ';
        
        $button = null;

        if( $sertifikatId != '' ) {
            $button .= $fileSertifikat;
        }

        if( $invoiceId != '' ) {
            $button .= $fileInvoice;
        }

        return $button;
    }

    public function _sertifikatButton( $id = '', $sertifikatId = '' )
    {
        $edit 	    = '<a data-uk-tooltip title="Edit" class="ajaxify md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href=""><i class="uk-icon-pencil uk-icon-small"></i></a>';
        $delete     = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return l_status(2, this, event);" href="'.base_url('sertifikat/action_delete_non_komersil/'.$id.'/'.$sertifikatId).'"><i class="uk-icon-trash uk-icon-small"></i></a>';
    
        return $delete;
    }

    public function show_bulk_upload()
    {
        $data['name']       = "Sertifikat";
        $data['url']        = base_url().$this->url;
        $data['plugin']     = ['datatables', 'datatables_fixcolumns', 'dropify'];

		$this->template->display('sertifikat_bulk_upload', $data);
    }

    public function action_bulk_upload()
    {
        // header('Content-type:application/json');
        $file         = $_FILES['fileinput'];
        $fileName     = $file['name'];
        $fileExplode  = explode('_', $file['name']);
        $fileType = str_replace(['.pdf', '.PDF'], '', $fileExplode[1]);

        // response if file size is empty
            $fileSize      = $_FILES['fileinput']['size'];
            $emptyFileSize = 0;
            if( $fileSize == $emptyFileSize ){
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $fileName,
                    'fileType'     => $this->_status_badge('info', $fileType),
                    'noKontrak'    => $this->_status_badge('error', 'Tidak ditemukan'),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'File tidak boleh kosong'
                ];

                echo json_encode( $response ); exit;
            }
        // end response

        // response if file extension not pdf
            $filePath             = $file['name'];
            $fileExtension        = pathinfo($filePath, PATHINFO_EXTENSION);
            $allowedFileExtension = [
                'pdf'
            ];
            if ( !in_array(@strtolower($fileExtension), $allowedFileExtension) ) {
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $fileName,
                    'fileType'     => $this->_status_badge('info', $fileType),
                    'noKontrak'    => $this->_status_badge('error', 'Tidak ditemukan'),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'File harus bertipe PDF'
                ];

                echo json_encode( $response ); exit;
            }
        //end response file extension

        // response if name format file is wrong
            $isTrueFormat = 3;
            if( count($fileExplode) != $isTrueFormat ) {
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $fileName,
                    'fileType'     => $this->_status_badge('info', $fileType),
                    'noKontrak'    => $this->_status_badge('error', 'Tidak ditemukan'),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'Penulisan format file salah'
                ];

                echo json_encode( $response ); exit;
            }
        // end response if format file wrong

        // response when type file not PNBP or sertifikat
            $allowedTypeFile = [
                'PNBP',
                'SERTIFIKAT'
            ];
            $fileType = str_replace(['.pdf', '.PDF'], '', $fileExplode[2]);
            // pre($fileType);
            // exit;
            if ( !in_array($fileType, $allowedTypeFile) ) {
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $fileName,
                    'fileType'     => $this->_status_badge('info', $fileType),
                    'noKontrak'    => $this->_status_badge('error', 'Tidak ditemukan'),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'Type file harus PNBP / SERTIFIKAT'
                ];

                echo json_encode( $response ); exit;
            }
        // end response type file wrong
            
        // response if nomor kontrak not exist
            $noKontrak     = $fileExplode[0]; 
            $getData       = $this->m_global->get_data_all('data_transaksi', null, ['data_no_kontrak' => $noKontrak]);
            $getNoKontrak  = count($getData);
            $isNotExist    = 0;
            if( $getNoKontrak == $isNotExist ) {
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $fileName,
                    'fileType'     => $this->_status_badge('info', $fileType),
                    'noKontrak'    => $this->_status_badge('info', $noKontrak),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'Nomor kontrak tidak ditemukan'
                ];

                echo json_encode( $response ); exit;
            }
        // end response nomor kontrak not exist

        // do upload the process
            switch( $fileType ) {
                case "SERTIFIKAT" : 
                    $typeDir = 'sertifikat';
                    break;
                case "PNBP" :
                    $typeDir = 'invoice';
                    break;
            }
            $dir = FCPATH."/assets/uploads/".$typeDir."/".$noKontrak;
            if( !is_dir($dir) ) {
                mkdir($dir);
                chmod($dir, 0777);
            }

            $tempName  = $file['tmp_name'];
            $dirUpload = $dir.'/';
            $paramfileName  = date('Y-m-d').'_'.user_data()->user_nik.'_'.rand().'.pdf';
            $doUpload = move_uploaded_file($tempName, $dirUpload.$paramfileName);
            if( !$doUpload ) {
                $response['status'] = false;
                $response['data'] = [
                    'fileName'     => $paramfileName,
                    'fileType'     => $this->_status_badge('info', $fileType),
                    'noKontrak'    => $this->_status_badge('info', $noKontrak),
                    'statusUpload' => $this->_status_badge('error', 'Error'),
                    'note'         => 'Gagal mengupload file'
                ];

                echo json_encode( $response ); exit;
            }
        // end do upload process

        // upload process if type file is invoice
            if( $fileType == 'PNBP' ) {
                $paramInvoice = [
                    'invoice_path'        => $paramfileName,
                    'invoice_createddate' => date('Y-m-d H:i:s'),
                    'invoice_createdby'   => user_data()->user_id
                ];
        
                $insertInvoice      = $this->m_global->insert('invoice_upload', $paramInvoice);
                $idInvoice          = $this->db->insert_id();
                $paramDataTransaksi = [
                    'data_invoice_id' => $idInvoice,
                    'data_flag'       => '4',
                    'updatedat'       => date('Y-m-d H:i:s')
                ];
                $updateDataTransaksi = $this->m_global->update('data_transaksi', $paramDataTransaksi, ['data_no_kontrak' => $noKontrak]);

                // failed response when sertifikat not uploaded
                    if( !$updateDataTransaksi ) {
                        $response['status'] = false;
                        $response['data'] = [
                            'fileName'     => $fileName,
                            'fileType'     => $this->_status_badge('info', $fileType),
                            'noKontrak'    => $this->_status_badge('info', $noKontrak),
                            'statusUpload' => $this->_status_badge('error', 'Failed'),
                            'note'         => 'Gagal mengupload data'
                        ];

                        echo json_encode( $response ); exit;
                    }
                // end failed response

                // success response when sertifikat uploaded
                    $response['status'] = true;
                    $response['data'] = [
                        'fileName'     => $fileName,
                        'noKontrak'    => $this->_status_badge('info', $noKontrak),
                        'fileType'     => $this->_status_badge('info', $fileType),
                        'statusUpload' => $this->_status_badge('success', 'Success'),
                        'note'         => 'Berhasil mengupload data'
                    ];

                    echo json_encode( $response ); exit;
                // end success response

            }
        // end invoice process
        
        // upload process if type file is sertifikat
            if( $fileType == 'SERTIFIKAT' ) {
                $paramSertifikat = [
                    'sertifikat_path'        => $paramfileName,
                    'sertifikat_createddate' => date('Y-m-d H:i:s'),
                    'sertifikat_createdby'   => user_data()->user_id
                ];   
                $insertSerifikat    = $this->m_global->insert('sertifikat_upload', $paramSertifikat);
                
                $idSertifikat       = $this->db->insert_id();
                $paramDataTransaksi = [
                    'data_sertifikat_id' => $idSertifikat,
                    'data_flag'          => '4',
                    'updatedat'          => date('Y-m-d H:i:s'),
                ];
                $updateDataTransaksi = $this->m_global->update('data_transaksi', $paramDataTransaksi, ['data_no_kontrak' => $noKontrak]);

                // failed response when sertifikat not uploaded
                    if( !$updateDataTransaksi ) {
                        $response['status'] = false;
                        $response['data'] = [
                            'fileName'     => $fileName,
                            'fileType'     => $this->_status_badge('info', $fileType),
                            'noKontrak'    => $this->_status_badge('info', $noKontrak),
                            'statusUpload' => $this->_status_badge('error', 'Gagal mengupload data'),
                            'note'         => ''
                        ];

                        echo json_encode( $response ); exit;
                    }
                // end failed response

                // success response when sertifikat uploaded
                    $response['status'] = true;
                    $response['data'] = [
                        'fileName'     => $fileName,
                        'noKontrak'    => $this->_status_badge('info', $noKontrak),
                        'fileType'   => $this->_status_badge('info', 'Sertifikat'),
                        'statusUpload' => $this->_status_badge('success', 'Success'),
                        'note'         => 'Berhasil mengupload data'
                    ];

                    echo json_encode( $response ); exit;
                // end success response

            }
        // end sertifikat process

    }

    public function _status_badge( $status = 'error', $message = 'unknown' )
    {
        $arr = [
            'error' => '<span class="uk-badge uk-badge-danger">'.$message.'</span>',
            'warning' => '<span class="uk-badge uk-badge-warning">'.$message.'</span>',
            'info' => '<span class="uk-badge uk-badge-info">'.$message.'</span>',
            'success' => '<span class="uk-badge uk-badge-success">'.$message.'</span>'
        ];

        return $arr[$status];
    }

}

/* End of file Sertifikat.php */
/* Location: ./application/modules/sertifikat/controllers/Sertifikat.php */