<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-content">
		<div class="md-card">
			<div class="md-card-toolbar">
				<div class="md-card-toolbar-actions">
					<i class="md-icon material-icons md-card-toggle">&#xE316;</i>
				</div>
				<h3 class="md-card-toolbar-heading-text">
					Filter
				</h3>
			</div>
			<div class="md-card-content filter">
				<div class="uk-width-medium-1-2 uk-row-first"> 
					<div class="uk-grid">
						<div class="uk-width-medium-1-3 uk-margin-small-bottom uk-row-first">
							<label>Tanggal Awal: </label> <input name="lastupdate[0]" type="text" class="md-input form-filter select-filter" id="text3">
						</div>
						<div class="uk-width-medium-1-3 uk-margin-small-bottom uk-row-first">
							<label>Tanggal Akhir: </label> <input name="lastupdate[1]" type="text" class="md-input form-filter select-filter" id="text4">
						</div>
					</div>
				</div>
				<div class="uk-grid">
					<div class="uk-width-medium-1-3 uk-row-first">
						<div class="md-input-wrapper">
							<label>Nomor KTP</label>
							<input name="kontrak" type="text" class="md-input form-filter">
							<span class="md-input-bar"></span>
						</div>
						<input name="ed_nomor_fidusia" type="hidden" class="md-input form-filter">
						<input name="akta_waktu" type="hidden" class="md-input form-filter">
						<input name="ed_tanggal_fidusia" type="hidden" class="md-input form-filter">
					</div>
				</div>
				<div class="uk-grid uk-margin-medium-top uk-text-center">
					<div class="uk-width-1-1">
						<button type="button" class="filter-reset md-btn md-btn-small md-btn-danger md-btn-wave-light waves-effect waves-button waves-light">Reset</button>
						<button type="button" class="filter-submit md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Cari</button>
					</div>
				</div>
			</div>
		</div>
		<table id="datatable_ajax" class="uk-table" cellspacing="0" width="100%" leftColumns="1" rightColumns="1">
			<thead>
				<tr>
					<th width="40">No.</th>
					<th>No KTP</th>
					<th>Status fidusia</th>
					<th>Tanggal Upload</th>
					<th>File</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</div>

<script>
	$(document).ready(function(){
		var url 	= "<?=$url; ?>/select/",
			header  = [
				null,
				null,
				null,
				null,
				null,
				null
			],
			order 	= [
				['1', 'asc']
			],
			sort 	= [-1, 0];

		App.setDatatable(url, header, order, sort); //paling akhir tambahkan sendiri jika tabel lebih dari satu

		App.datepicker();
	});

	function reject(element, event) {
		event.preventDefault();
		var url = "<?php echo base_url('/sertifikat/sertifikat_non_komersil/action_reject')?>";
		var id = $(element).attr('data-id');
		swal({
			title: "Tolak data?",
			type: "warning",
 			input: 'textarea',
			inputPlaceholder: 'Tambahkan catatan',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Yes',
		}).then(
			function(result){
				var note = result;
				$.ajax({
					method:"POST",
					url:url,
					data:{id:id,note:note},
					success:function(response) {
						var response = JSON.parse(response);
						var status = response.status;
						var message = response.message;
						if(status == "0") {
							swal("Error", message, "error");
							// location.reload();
							// return;
						}

						if(status == "1") {
							swal("Success", message, "success");
							location.reload();
							return;
						}
					}
				})

		}, function(dismiss) {
			return false;
		});
	}
</script>
