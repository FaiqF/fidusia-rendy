<style>
	.dropify-wrapper{
		width: initial;
	}
	.center{
		text-align: center;
	}
</style>
<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-content">
		<div id="error"></div>
		<form id="upload" method="post" action="<?=$url; ?>/upload_excel" enctype="multipart/form-data">
			<div class="md-card">
				<div class="md-card-toolbar">
					<div class="md-card-toolbar-actions">
						<i class="md-icon material-icons md-card-toggle">&#xE316;</i>
					</div>
					<h3 class="md-card-toolbar-heading-text">
						Upload File
					</h3>
				</div>
				<div class="md-card-content">
					<div class="uk-grid">
						<div class="uk-width-medium-4-4 uk-row-first">
							<div class="uk-grid">
								<div class="uk-width-medium-1-1 uk-row-first">
									<div class="uk-form-row text-center">
										<input data-allowed-file-extensions="pdf" name="pdf[]" type="file" id="file_upload" class="file_upload" multiple/>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid">
						<div class="uk-width-medium-4-4 uk-row-first">
							<div class="uk-grid">
								<div class="uk-width-medium-1-1 uk-row-first">
									<div class="uk-form-row text-center">
										<div class="uk-width-1-1">
											<button name="btn-upload" value="btn-upload" type="button" id="btn-upload" class="filter-submit md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Upload</button>
											<input type="hidden" id="edit">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!-- datatable true data -->
			<table id="datatable_ajax" class="uk-table" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th width="40">No.</th>
						<th>Nama File</th>
						<th>Nomor KTP</th>
						<th>Type Dokumen</th>
						<th>Status Upload</th>
						<th>Catatan</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		<!-- end datatable true data -->

	</div>
</div>

<script>
	var dataTableNumber = 1;
	// file upload dropify
		$('.file_upload').dropify({
			messages: {
				'default': 'Upload File Excel',
				'replace': '',
				'remove' :  'Batal'
			}
		});
	// end file upload dropify
	
	// action upload (data cek)
		$("#btn-upload").click(function() {
			altair_helpers.content_preloader_show('md');
			setTimeout(function () {
				$('#upload').submit();
			}, 2000);
			
		});
	// end action upload

	// handle form submit
		$('#upload').on('submit', function(e) {
			e.preventDefault();
			var table = $('#datatable_ajax').DataTable();
			var file = $('#file_upload')[0];
			$.each(file.files, function(index, files) {
				var formData   = new FormData;
				var ajaxUrl    = "<?php echo base_url('sertifikat/sertifikat_non_komersil/action_bulk_upload')?>";
				var ajaxMethod = "post"
				formData.append('fileinput', files);
				$.ajax({
					method		: ajaxMethod,
					dataType	: 'json',
					url 		: ajaxUrl,
					data 		: formData,
					processData : false,
					contentType : false,
					async		: false,
					success		: function( response )
					{
						table.row.add([
							dataTableNumber,
							response.data.fileName,
							response.data.noKontrak,
							response.data.fileType,
							response.data.statusUpload,
							response.data.note
						]).draw( false );
						dataTableNumber += 1;
					}

				})
			});
			altair_helpers.content_preloader_hide('md');
		});
	// end handle
</script>