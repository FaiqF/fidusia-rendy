<?php if($records->data_flag == '2'){?>
	<div class="uk-grid">
		<div class="uk-width-medium-1-1">
			<div class="md-card">
				<div class="md-card-toolbar">
					<h3 class="md-card-toolbar-heading-text">Upload Sertifikat</h3>
				</div>
				<div class="md-card-content">
					<div id="alert_error"></div>
					<form method="post" action="<?php echo $url.'/action_sertifikat/'.$id ?>" id="form_edit">
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<div class="uk-grid">
									<div class="uk-width-medium-1-1 uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Nomor KTP<span class="required">*</span></label>
												<input name="data_nokontrak" type="text" class="md-input uk-form-width-medium" value="<?php echo $records->data_no_kontrak ?>" readonly>
												<input name="data_nokontrak_old" type="hidden" class="md-input uk-form-width-medium" value="<?php echo $records->data_no_kontrak ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>File PDF <span class="required">*</span></label>
											</div>
										</div>
									</div>
									<div class="uk-margin-medium-top">
										<div class="uk-grid">
											<div class="uk-width-large-1-1" style="width: 200%">
												<input accept="application/pdf" data-max-file-size="2500K" name="sertifikat_path" type="file" id="uploadFile" class="uploadFile"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="uk-text-center uk-margin-large-top">
							<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Save</button>
							<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
						</div>
					</form>
				</div>
			</div>
			<div class="md-card">
				<div class="md-card-toolbar">
					<h3 class="md-card-toolbar-heading-text">Data kelengkapan</h3>
				</div>

				<div class="md-card-content">
					<div style="height: 500px;" id="my-container"></div>
				</div>
			</div>
		</div>
	</div>
<?php }?>

<?php if($records->data_flag == '3'){?>
	<div class="uk-grid">
		<div class="uk-width-medium-1-1">
			<div class="md-card">
				<div class="md-card-toolbar">
					<h3 class="md-card-toolbar-heading-text">Upload Invoice</h3>
				</div>
				<div class="md-card-content">
					<div id="alert_error"></div>
					<form method="post" action="<?php echo $url.'/action_invoice/'.$id ?>" id="form_edit">
						<div class="uk-grid">
							<div class="uk-width-medium-2-5 uk-row-first">
								<div class="uk-grid">
									<div class="uk-width-medium-1-1 uk-row-first">
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>Nomor Kontrak <span class="required">*</span></label>
												<input name="data_nokontrak" type="text" class="md-input uk-form-width-medium" value="<?php echo $records->data_no_kontrak ?>" readonly>
												<input name="data_nokontrak_old" type="hidden" class="md-input uk-form-width-medium" value="<?php echo $records->data_no_kontrak ?>">
												<span class="md-input-bar uk-form-width-medium"></span>
											</div>
										</div>
										<div class="uk-form-row">
											<div class="md-input-wrapper md-input-filled">
												<label>File PDF <span class="required">*</span></label>
											</div>
										</div>
									</div>
									<div class="uk-margin-medium-top">
										<div class="uk-grid">
											<div class="uk-width-large-1-1" style="width: 200%">
												<input accept="application/pdf" data-max-file-size="2500K" name="invoice_path" type="file" id="uploadFile" class="uploadFile"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="uk-text-center uk-margin-large-top">
							<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Save</button>
							<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
						</div>
					</form>
				</div>
			</div>
			<div class="md-card">
				<div class="md-card-toolbar">
					<h3 class="md-card-toolbar-heading-text">Data kelengkapan</h3>
				</div>

				<div class="md-card-content">
					<div style="height: 500px;" id="my-container"></div>
				</div>
			</div>
		</div>
	</div>
<?php }?>
<a href="<?=$url; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){
		var pdf = "<?php echo $pdf ?>";
		if (pdf !== '') {
			PDFObject.embed(pdf, "#my-container");
		} else {
			$('#my-container').hide();
		}

        $('#form_edit').on('submit', function(e){
			e.preventDefault();

			var $this 	= $(this),
			url 		= $this.attr('action'),
			data 		= $this.serialize();

			var form = $('form')[0]	;
			var formData = new FormData(form);
			var err_res = 0,
			err_msg = [],
			suc_msg = [];

			$.ajax({
				url: url,
				type: 'post',
				dataType:'json',
				data: formData,
				processData: false,
				contentType: false,
				// async: false,
				success : function(res) {
					if (res.sts == 5) {
						$('#alert').html(res.alert)
						App.scrollTop();
					}
					if (res.sts == 0) {
						App.notif('error', res.msg, 'error');
					} else if (res.sts == 1) {
						App.notif('Success', res.msg, 'success');
						$('.reload').trigger('click');
					}
				},
				error 	: function(res) {
					App.notif('Error', res.msg , 'error');
				}
			});
		});

        var fileUpload = $('.uploadFile').dropify({
			messages: {
				'default': 'File Upload',
				'replace': '',
				'remove':  'Batal',
				'error': 'File hanya format PDF dan maksimal 2.5MB !'
			}
		});

	});

</script>