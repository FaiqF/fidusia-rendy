<style>
	.dropify-wrapper{
		width: initial;
	}
	.center{
		text-align: center;
	}
</style>
<div class="md-card uk-margin-medium-bottom">
	<div class="md-card-content">
		<div id="error"></div>
		<form id="upload" method="post" action="<?=$url; ?>/upload_excel" enctype="multipart/form-data">
			<div class="md-card">
				<div class="md-card-toolbar">
					<div class="md-card-toolbar-actions">
						<i class="md-icon material-icons md-card-toggle">&#xE316;</i>
					</div>
					<h3 class="md-card-toolbar-heading-text">
						Upload File
					</h3>
				</div>
				<div class="md-card-content">
					<div class="uk-grid">
						<div class="uk-width-medium-4-4 uk-row-first">
							<div class="uk-grid">
								<div class="uk-width-medium-1-1 uk-row-first">
									<div class="uk-form-row text-center">
										<input data-allowed-file-extensions="xls xlsx" name="file_xls" type="file" id="file_xls" class="file_xls"/>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-grid">
						<div class="uk-width-medium-4-4 uk-row-first">
							<div class="uk-grid">
								<div class="uk-width-medium-1-1 uk-row-first">
									<div class="uk-form-row text-center">
										<div class="uk-width-1-1">
											<button name="test" value="test" type="button" id="smb" class="filter-submit md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Upload</button>
											<button name="save" value="save" type="button" id="save" class="md-btn md-btn-small md-btn-success md-btn-wave-light waves-effect waves-button waves-light">Save</button>
											<input type="hidden" id="edit">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		<table id="datatable_ajax" class="uk-table" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th width="40">No.</th>
					<th>No Kontrak</th>
					<th>Pemberi Fidusia</th>
					<th>Tanggal jual</th>
					<th>Merk</th>
					<th>Type</th>
					<th>Tahun</th>
					<th>No Polisi</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
</div>

<div class="md-card uk-margin-medium-bottom" id="contet_datatable2" style="display:none;">
	<div class="md-card-content">

		<table id="datatable_ajax2" class="uk-table" cellspacing="0" width="100%" style="display:none;">
			<thead>
				<tr>
					<th width="40">No.</th>
					<th>No Kontrak</th>
					<th>Pemberi Fidusia</th>
					<th>Tanggal jual</th>
					<th>Merk</th>
					<th>Type</th>
					<th>Tahun</th>
					<th>No Polisi</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>

		<div class="uk-grid">
			<div class="uk-width-medium-4-4 uk-row-first">
				<div class="uk-grid">
					<div class="uk-width-medium-1-1 uk-row-first">
						<div class="uk-form-row text-center">
							<div class="uk-width-1-1">
								<button name="export" value="export" type="button" id="export" class="md-btn md-btn-small md-btn-success md-btn-wave-light waves-effect waves-button waves-light">Export Excel</button>
								<input type="hidden" id="edit">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>

<script>
	$("#export").click(function() {
		$('#upload').unbind('submit');
		$("#upload").attr('target','_blank');
		$("#upload").attr('action','<?=$url; ?>/export');
		$("#upload").submit();
	});
	$("#smb").click(function() {
		$('#upload').on('submit', function(e){
			altair_helpers.content_preloader_show('md');
			e.preventDefault();

			var $this 	= $(this),
			url 		= $this.attr('action'),
			data 		= $this.serialize();

			var form = $('form')[0]	;
			var formData = new FormData(form);
			$.ajax({
				url: url,
				type: 'post',
				dataType:'json',
				data: formData,
				processData: false,
				contentType: false,
				async: false,
				success : function(res) {
					if (res.feedback == 1) {
						$('#save').show();
						$('#datatable_ajax tbody').show();
						getTable(res.data);
						if(res.recordsTotal2 >= 1){		
							$('#contet_datatable2').show();					
							$('#datatable_ajax2').show();
							getTable2(res.data2);
						}else{ 
							$('#datatable_ajax2').hide();
						}
					}else if(res.feedback == 0){
						App.notif('Error', res.message , 'error');
					}
				},
				error 	: function(res) {
					App.notif('Error', 'Terjadi Kesalahan' , 'error');
				}
			});
			altair_helpers.content_preloader_hide();  
		});
		$("#upload").attr('action','<?=$url; ?>/upload_excel');
		$("#upload").submit();
		
	});
	$("#save").click(function() {
		$('#upload').on('submit', function(e){
			var table = $('#datatable_ajax').DataTable();
			altair_helpers.content_preloader_show('md');
			e.preventDefault();

			var $this 	= $(this),
			url 		= $this.attr('action'),
			data 		= $this.serialize();

			var form = $('form')[0]	;
			var formData = new FormData(form);
			$.ajax({
				url: 'import_data/save',
				type: 'POST',
				dataType: 'json',
				data: formData,
				processData: false,
				contentType: false,
				async: false,
				success : function(res) {
					if (res.feedback == 1) {
						App.notif('Success', 'Data Berhasil Ditambahkan', 'success');
						$('.reload').trigger('click');
						table.clear().draw();
						$('#save').hide();
					}else{
						App.notif('Error', 'Terjadi Kesalahan' , 'error');
					}
				},
				error 	: function(res) {
					App.notif('Error', 'Terjadi Kesalahan' , 'error');
				}
			});
			altair_helpers.content_preloader_hide();  
		});
		$("#upload").attr('action','<?=$url; ?>/save');
		$("#upload").submit();
	});
	$('#save').hide();
	// $('#datatable_ajax tbody').hide();
	$('.file_xls').dropify({
		messages: {
			'default': 'Upload File Excel',
			'replace': '',
			'remove':  'Batal',}
		});

	function getTable(data){
		if($('#datatable_ajax').is(":visible")){
			var table = $('#datatable_ajax').DataTable();
			/*---clear value table---*/
			table.clear();
			/*---Add data in rows---*/
			table.rows.add(data);
			/*---Redraw table---*/
			table.draw();
			/*---Call of Button Delete---*/
			callback_del();	
		}
	}
	
	function getTable2(data){
		if($('#datatable_ajax2').is(":visible")){
			var table = $('#datatable_ajax2').DataTable();
			/*---clear value table---*/
			table.clear();
			/*---Add data in rows---*/
			table.rows.add(data);
			/*---Redraw table---*/
			table.draw();
			/*---Call of Button Delete---*/
			callback_del2();	
		}
	}

	/*---function for button delete---*/
	function callback_del(){
		var table = $('#datatable_ajax').DataTable();
		$('.button').click( function () {
			table.row( $(this).parents('tr')).remove().draw();
		});
		
	}
	
	function callback_del2(){
		var table2 = $('#datatable_ajax2').DataTable();
		$('.button2').click( function () {
			table2.row( $(this).parents('tr')).remove().draw();
		});
		
	}
	

	/*---DELETE ROW TABLE---*/
	$(document).ready(function(){
		

		/*---ACTION SAVE---*/
		var table2 = $('#datatable_ajax2').DataTable({bPaginate:false,});
		$('#post2').click(function() {
			if ( ! table2.data().count() ) {
				App.notif('Error', 'Tidak Ada Data' , 'error');
			}else{
				//altair_helpers.content_preloader_show('md');
				var oTable = document.getElementById('datatable_ajax2');
				var rowLength = oTable.rows.length;
				var sendValue = [];
				for (i = 0; i < rowLength; i++){
					var oCells = oTable.rows.item(i).cells;
					var cellLength = oCells.length;
					var sendRow = [];
					for(var j = 0; j < cellLength; j++){
						sendRow[j] = oCells.item(j).innerHTML;
					}
					sendValue[i] = sendRow;
				}
				/*alert(sendValue);
				return false;*/
				//window.location = 'import_data/export?test=123';

				/*var url = 'import_data/export',
					data = {param1: sendValue};

				window.open(url,'_blank' );
				return false;
				e.preventDefault()*/;
				window.open('import_data/export?'+'as='+encodeURIComponent(JSON.stringify(sendValue)), '_blank');	
				/*$.ajax({
					url: 'import_data/export',
					type: 'POST',
					dataType: 'json',
					data: {param1: sendValue},
					success : function(res) {
						if (typeof res.sukses !== 'undefined') {
							App.notif('Success', res.sukses, 'success');
							$('.reload').trigger('click');
							table.clear().draw();
							$('#post').hide();
						}
					},
					error 	: function(res) {
						App.notif('Error', 'Terjadi Kesalahan' , 'error');
					}
				});
				altair_helpers.content_preloader_hide();*/
			}    	
		});
		
		var table = $('#datatable_ajax').DataTable();
		$('#post').click(function() {
			if ( ! table.data().count() ) {
				App.notif('Error', 'Tidak Ada Data' , 'error');
			}else{
				altair_helpers.content_preloader_show('md');
				/*---gets table---*/
				var oTable = document.getElementById('datatable_ajax');
				/*---gets rows of table---*/
				var rowLength = oTable.rows.length;
				/*---loops through rows---*/
				var sendValue = [];
				//alert(rowLength);
				for (i = 0; i < rowLength; i++){
					/*---gets cells of current row---*/
					var oCells = oTable.rows.item(i).cells;
					/*---gets amount of cells of current row---*/
					var cellLength = oCells.length;
					
					/*---loops through each cell in current row---*/
					var sendRow = [];
					for(var j = 0; j < cellLength; j++){
						/*---get your cell info here---*/
						sendRow[j] = oCells.item(j).innerHTML;
					}
					sendValue[i] = sendRow;
				}
				//alert(sendValue.length);
				//return false();
				/*---SEND VALUE TO CONTROLLER---*/
				$.ajax({
					url: 'import_data/save',
					type: 'POST',
					dataType: 'json',
					data: {param1: sendValue},
					success : function(res) {
						if (typeof res.sukses !== 'undefined') {
							App.notif('Success', res.sukses, 'success');
							$('.reload').trigger('click');
							table.clear().draw();
							$('#post').hide();
						}
						/*---valiasi cek data jika sudah ada---*/
						// if (typeof res.gagal !== 'undefined') {
						// 	var gagal = res.jml_gagal.length;
						// 	App.notif('Error', 'Data sudah ada = ' + gagal + ' data', 'error');
						// 	var error = $('#error');
						// 	App.scrollTop();
						// 	error.html('<div class="uk-alert uk-alert-danger" data-uk-alert><a href="#" class="uk-alert-close uk-close"></a>'+res.gagal+'</div>');
						// }
						/*---end valiasi cek data jika sudah ada---*/
					},
					error 	: function(res) {
						App.notif('Error', 'Terjadi Kesalahan' , 'error');
					}
				});
				altair_helpers.content_preloader_hide();  
			}    	
		});
	});
</script>