<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import_data extends CI_Controller {

	private $table_db       = 'entry_data'; //Table name
	private $table_prefix   = 'ed_'; //The initial name of the field
	private $prefix 		= 'import_data';

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$data['name'] 	= "Import Data";
		$data['url'] 	= base_url().$this->prefix;
		$data['plugin']	= ['datatables','dropify'];

		$this->template->display('index', $data);
	}

	public function select($insert_data){
		$post 		= $this->input->post();
		$length 	= intval(@$post['length']);
		$start  	= intval(@$post['start']);
		$sEcho		= intval(@$post['draw']);

		$count 		= count($insert_data);
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();
		$i = 1 + $start;
		if(count($insert_data) > 0){
			foreach ($insert_data as $key => $value) {
			$records["data"][]  = array(
				$i,
				$value['perjanjian_no_kontrak'],
				$value['perjanjian_pemberi_fidusia'],
				$value['perjanjian_tanggal_jual'],
				$value['dk_merk'],
				$value['dk_type'],
				$value['dk_tahun'],
				$value['dk_no_polisi']
			);
				$i++;
			}
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		return $records;
	}

	public function _button($show = false)
	{
		$button = '';

		if($show) {
			$delete = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light button"><i class="uk-icon-trash uk-icon-small"></i></a>';
			$button = $delete;
		} else {
			$button = '-';
		}

		return $button;
	}
	
	public function select2($insert_data){
		$post 		= $this->input->post();
		$length 	= intval(@$post['length']);
		$start  	= intval(@$post['start']);
		$sEcho		= intval(@$post['draw']);

		$count 		= count($insert_data);
		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();
		$i = 1 + $start;
		if(count($insert_data) > 0){
			foreach ($insert_data as $key => $value) {
			$records["data"][]  = array(
				$i,
				$value['perjanjian_no_kontrak'],
				$value['perjanjian_pemberi_fidusia'],
				$value['perjanjian_tanggal_jual'],
				$value['dk_merk'],
				$value['dk_type'],
				$value['dk_tahun'],
				$value['dk_no_polisi']
			);
				$i++;
			}
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
		$records["recordsFiltered"] = $count;

		return $records;
	}

	public function _button2($show = false)
	{
		$button = '';

		if($show) {
			$delete = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light button2"><i class="uk-icon-trash uk-icon-small"></i></a>';
			$button = $delete;
		} else {
			$button = '-';
		}

		return $button;
	}
	
	public function upload_excel()
	{

		$path 			= $_FILES['file_xls']['name'];
		$fileSize 		= $_FILES['file_xls']['size'];
		$emptySize 		= "0";
		
		// Condition when file is empty or file size = 0
			if ( $fileSize == $emptySize ) {
				$result['feedback'] = 0;
				$result['message']  = 'File Harus Bertipe .xls / .xlsx';
				
				echo json_encode( $result );exit();
			}
		// end condition file size

		
		// Condition when file extension not xls or xlsx
			$fileExtension   = pathinfo($path, PATHINFO_EXTENSION);
			$allowedFileType = [
				'xls',
				'xlsx'
			];

			if ( !in_array($fileExtension, $allowedFileType) ) {
				$result['feedback'] = 0;
				$result['message']  = 'Mohon Pilih File Upload';

				echo json_encode( $result );exit();
			}
		// end condition file extension

		
		// begin process when all condition true
			$this->load->library("PHPExcel");
			$phpExcel = new PHPExcel();
			/*---Read your Excel workbook---*/
			$inputFileName = $_FILES['file_xls']['tmp_name'];
			try {
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch(Exception $e) {
				die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
			}

			/*---Get worksheet dimensions---*/
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();

			/*---Loop through each row of the worksheet in turn---*/

			$start = 0;
			$insert_data = array();
			$insert_data_false = array();
			$no_kontrak = array();

			for ($row = 4; $row <= $highestRow; $row++){
				/*---Read	 a row of data into an array---*/
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
				$key = ($start -1);
				$key++;

				$where     = [ 
					'trans_no_kontrak' => $rowData[$key][17] 
				];

				$select    = 'trans_no_kontrak';
				$data_cek  = $this->m_global->get_data_all( 'transaksi', null, $where, $select );
				$hitung_no = count($data_cek);

					if (@$rowData[$key][2] != "" && ($hitung_no > 0 || in_array(@$rowData[$key][17], $no_kontrak, true))) {
						$insert_data_false[] = [
							'perjanjian_no_kontrak'		 => @$rowData[$key][17],
							'perjanjian_pemberi_fidusia' => @$rowData[$key][46],
							'perjanjian_tanggal_jual'    => date('d-m-Y', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][18],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
							'dk_merk' 					 => @$rowData[$key][27],
							'dk_type' 					 => @$rowData[$key][28],
							'dk_tahun' 					 => @$rowData[$key][31],
							'dk_no_polisi' 			     => @$rowData[$key][39]
						];
						$no_kontrak[] = @$rowData[$key][17];

					}else if(@$rowData[$key][2] != ""){
						$no_kontrak[] = @$rowData[$key][2];
						$insert_data[] = [
							'perjanjian_no_kontrak'		 => @$rowData[$key][17],
							'perjanjian_pemberi_fidusia' => @$rowData[$key][46],
							'perjanjian_tanggal_jual'    => date('d-m-Y', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][18],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
							'dk_merk' 					 => @$rowData[$key][27],
							'dk_type' 					 => @$rowData[$key][28],
							'dk_tahun' 					 => @$rowData[$key][31],
							'dk_no_polisi' 			     => @$rowData[$key][39]
						];
					}
			}
			
			$select22  = $this->select2(@$insert_data_false);
			$select2['data2'] = $select22['data'];
			$select2['recordsTotal2'] = $select22['recordsTotal'];			
			$select  = $this->select(@$insert_data);
			$select = array_merge($select, $select2);
			$result['feedback'] = 1;
			$return = array_merge($select, $result);
			echo json_encode($return);
		// end process 
	}
	
	public function save()
	{

		$path = $_FILES['file_xls']['name'];
		$ext  = pathinfo($path, PATHINFO_EXTENSION);
		if( $_FILES['file_xls']['size'] == "0" ){

			$result['feedback'] = 2;
			echo json_encode($err);exit();
		}


		// when condition file size not null
		$allowedFileType = [
			'xls',
			'xlsx'
		];

		if ( !in_array(@strtolower($ext), $allowedFileType) ){

			$result['feedback'] = '2';
			$result['msg']		= 'File not allowed';

			echo json_encode($result); exit;
		}


		$this->load->library("PHPExcel");
		$phpExcel = new PHPExcel();
		/*---Read your Excel workbook---*/
		$inputFileName = $_FILES['file_xls']['tmp_name'];
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		/*---Get worksheet dimensions---*/
		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();

		/*---Loop through each row of the worksheet in turn---*/
		$start = 0;
		$last = date("Y-m-d h:i:sa");
		$last_insert = $last;

		$insert_data = array();
		$insert_data_false = array();
		$no_kontrak = array();

		$this->db->trans_begin();
		$result['feedback'] = 0;
		for ($row = 4; $row <= $highestRow; $row++){
			/*---Read	 a row of data into an array---*/
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
			$key = ($start -1);
			$key++;

			$where = ['perjanjian_no_kontrak' => $rowData[$key][17]];
			$select = 'perjanjian_no_kontrak';
			$data_cek = $this->m_global->get_data_all('perjanjian_pembiayaan', null, $where, $select);

			$hitung_no = count($data_cek);

				if (@$rowData[$key][2] != "" && ($hitung_no > 0 || in_array(@$rowData[$key][2], $no_kontrak, true))) {
						$insert_data_false[] = [
						];
						
				}else if(@$rowData[$key][2] != ""){
					$dataAkta = [
						'akta_pejabat_panggilan' 	   => @$rowData[$key][1],
						'akta_pejabat_nama' 		   => @$rowData[$key][2],
						'akta_pejabat_tempat_lahir'    => @$rowData[$key][3],
						'akta_pejabat_tanggal_lahir'   => date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][4],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
						'akta_pejabat_kewarganegaraan' => @$rowData[$key][5],
						'akta_pejabat_pekerjaan' 	   => @$rowData[$key][6],
						'akta_pejabat_alamat' 		   => @$rowData[$key][7],
						'akta_pejabat_kelurahan' 	   => @$rowData[$key][8],
						'akta_pejabat_kecamatan' 	   => @$rowData[$key][9],
						'akta_pejabat_kabupaten' 	   => @$rowData[$key][10],
						'akta_pejabat_provinsi' 	   => @$rowData[$key][11],
						'akta_pejabat_jenis_identitas' => @$rowData[$key][12],
						'akta_pejabat_nik' 			   => @$rowData[$key][13],
						'akta_pejabat_tanggal_kuasa'   => date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][14],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
						'akta_pejabat_no_kuasa' 	   => @$rowData[$key][15]

					];

					$insertAkta 				    = $this->m_global->insert('akta', $dataAkta);
					$akta_id = $this->db->insert_id();

					$dataPerjanjian = [
						'perjanjian_tanggal_skpjf' 		=> date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][16],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
						'perjanjian_no_kontrak'			=> @$rowData[$key][17],
						'perjanjian_tanggal_pk' 		=> date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][18],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
						'perjanjian_tanggal_akhir' 		=> date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][19],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
						'perjanjian_tenor_pk' 			=> (string)@$rowData[$key][20],
						'perjanjian_total_hutang' 		=> @$rowData[$key][21],
						'perjanjian_nilai_penjaminan' 	=> @$rowData[$key][22],
						'perjanjian_tanggal_penagihan' 	=> date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][22],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
						'perjanjian_is_asuransi' 		=> @$rowData[$key][24],
						'perjanjian_jenis_pembiayaan' 	=> @$rowData[$key][25],
					];

					$insertPerjanjian 	= $this->m_global->insert('perjanjian_pembiayaan', $dataPerjanjian);
					$perjanjian_id 		= $this->db->insert_id();

					$dataObyek = [
						'dk_obyek_jaminan' 	 		   => @$rowData[$key][26],
						'dk_merk' 			 		   => @$rowData[$key][27],
						'dk_type' 			 		   => @$rowData[$key][28],
						'dk_jenis' 			 		   => @$rowData[$key][29],
						'dk_model' 			 		   => @$rowData[$key][30],
						'dk_tahun_pembuatan' 		   => @$rowData[$key][31],
						'dk_warna' 			 		   => @$rowData[$key][32],
						'dk_no_rangka' 		 		   => @$rowData[$key][33],
						'dk_no_mesin' 		 		   => @$rowData[$key][34],
						'dk_nilai_obyek' 	 		   => @$rowData[$key][35],
						'dk_tanggal_bukti' 	 		   => @$rowData[$key][36] == "" ? null : date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][36],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
						'dk_no_bukti_faktur' 		   => @$rowData[$key][37],
						'dk_pemasok' 		 		   => @$rowData[$key][38],
						'dk_no_polisi' 		 		   => @$rowData[$key][39],
						'dk_no_bpkb' 		 		   => @$rowData[$key][40],
						'dk_tanggal_surat_penyerahan'  => date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][41],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
						'dk_sp_bpkb' 				   => @$rowData[$key][42],
						'dk_nama_pemilik' 			   => @$rowData[$key][43],
						'dk_alamat_pemilik' 		   => @$rowData[$key][44]

					];
					
					$insertObyek = $this->m_global->insert('data_kendaraan', $dataObyek);
					$dk_id 		 = $this->db->insert_id();

					$dataPemberi = [
						'pemberi_panggilan' 	   			   => @$rowData[$key][45],
						'pemberi_nama' 			   			   => @$rowData[$key][46],
						'pemberi_jenis_kelamin'    			   => @$rowData[$key][47],
						'pemberi_tempat_lahir' 	   			   => @$rowData[$key][48],
						'pemberi_tanggal_lahir'    			   => @$rowData[$key][49] == "" ? null : date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][49],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
						'pemberi_kewarganegaraan'  			   => @$rowData[$key][50],
						'pemberi_pekerjaan' 	   			   => @$rowData[$key][51],
						'pemberi_alamat' 		   			   => @$rowData[$key][52],
						'pemberi_rt' 			   			   => @$rowData[$key][53],
						'pemberi_rw' 			   			   => @$rowData[$key][54],
						'pemberi_desa' 			   			   => @$rowData[$key][55],
						'pemberi_kelurahan' 	   			   => @$rowData[$key][56],
						'pemberi_kecamatan' 	   			   => @$rowData[$key][57],
						'pemberi_kabupaten' 	   			   => @$rowData[$key][58],
						'pemberi_kabupaten_kode'   			   => @$rowData[$key][59],
						'pemberi_provinsi' 	   	   			   => @$rowData[$key][60],
						'pemberi_provinsi_kode'    			   => @$rowData[$key][61],
						'pemberi_kode_pos' 		   			   => @$rowData[$key][62],
						'pemberi_telp' 			   			   => @$rowData[$key][63],
						'pemberi_jenis_identitas'  			   => @$rowData[$key][64],
						'pemberi_nik' 			   			   => @$rowData[$key][65],
						'pemberi_ktp_berlaku' 	   			   => @$rowData[$key][66],
						'pemberi_agama' 		   			   => @$rowData[$key][67],
						'pemberi_status_pernikahan'			   => @$rowData[$key][68],
						'pemberi_surat_pengadilan' 			   => @$rowData[$key][69],
						'pemberi_surat_cerai' 	   			   => @$rowData[$key][70],
						'pemberi_tanggal_dokumen'  			   => @$rowData[$key][71] == "" ? null : date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][71],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
						'pemberi_lokasi_penerbit'  			   => @$rowData[$key][72],
						'pemberi_uraian_status'    			   => @$rowData[$key][73],
						'pemberi_status_pasangan' 		   	   => @$rowData[$key][74],
						'pemberi_tanggal_persetujuan_pasangan' => @$rowData[$key][75] == "" ? null : date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][75],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
						'pemberi_panggilan_pasangan' 		   => @$rowData[$key][76],
						'pemberi_nama_pasangan' 			   => @$rowData[$key][77],
						'pemberi_tempat_lahir_pasangan' 	   => @$rowData[$key][78],
						'pemberi_tanggal_lahir_pasangan' 	   => @$rowData[$key][79] == "" ? null : date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][79],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
						'pemberi_kewarganegaraan_pasangan' 	   => @$rowData[$key][80],
						'pemberi_pekerjaan_pasangan' 		   => @$rowData[$key][81],
						'pemberi_alamat_pasangan' 		   	   => @$rowData[$key][82],
						'pemberi_rt_pasangan' 		  		   => @$rowData[$key][83],
						'pemberi_rw_pasangan' 		   		   => @$rowData[$key][84],
						'pemberi_desa_pasangan' 		   	   => @$rowData[$key][85],
						'pemberi_kelurahan_pasangan' 		   => @$rowData[$key][86],
						'pemberi_kecamatan_pasangan' 		   => @$rowData[$key][87],
						'pemberi_kabupaten_pasangan' 		   => @$rowData[$key][88],
						'pemberi_jenis_identitas_pasangan' 	   => @$rowData[$key][89],
						'pemberi_nik_pasangan' 				   => @$rowData[$key][90],
						'pemberi_ktp_berlaku_pasangan' 		   => @$rowData[$key][91],
						'pemberi_agama_pasangan' 			   => @$rowData[$key][92],
						'pemberi_status_hubungan_debitur'	   => @$rowData[$key][93],
						'pemberi_panggilan_debitur'			   => @$rowData[$key][94],
						'pemberi_nama_debitur' 				   => @$rowData[$key][95],
						'pemberi_tempat_lahir_debitur' 		   => @$rowData[$key][96],
						'pemberi_tanggal_lahir_debitur' 	   => @$rowData[$key][97] == "" ? null : date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][97],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
						'pemberi_kewarganegaraan_debitur'      => @$rowData[$key][98],
						'pemberi_pekerjaan_debitur' 		   => @$rowData[$key][99],
						'pemberi_alamat_debitur' 			   => @$rowData[$key][100],
						'pemberi_rt_debitur' 				   => @$rowData[$key][101],
						'pemberi_rw_debitur' 				   => @$rowData[$key][102],
						'pemberi_desa_debitur' 				   => @$rowData[$key][103],
						'pemberi_kelurahan_debitur' 		   => @$rowData[$key][104],
						'pemberi_kecamatan_debitur' 		   => @$rowData[$key][105],
						'pemberi_kabupaten_debitur' 		   => @$rowData[$key][106],
						'pemberi_jenis_identitas_debitur'	   => @$rowData[$key][107],
						'pemberi_nik_debitur'		 		   => @$rowData[$key][108],
						'pemberi_ktp_berlaku_debitur'		   => @$rowData[$key][109],
						'pemberi_agama_debitur'		           => @$rowData[$key][110]
					];

					$insertPemberi = $this->m_global->insert('pemberi_fidusia', $dataPemberi);
					$pemberi_id    = $this->db->insert_id();

					$dataTransaksi = [
						'trans_no_kontrak' 	  => @$rowData[$key][17],
						'trans_flag' 		  => '1',
						'trans_akta_id'		  => $akta_id,
						'trans_perjanjian_id' => $perjanjian_id,
						'trans_dk_id'		  => $dk_id,
						'trans_pemberi_id'	  => $pemberi_id,
						'trans_created_at' 	  => date("Y-m-d h:i:s"),
						'trans_created_by'	  => user_data()->user_id
					];

					$this->m_global->insert('transaksi', $dataTransaksi);
				}
		}

		if ( $this->db->trans_status() === FALSE ){
			$this->db->trans_rollback();
			$result['feedback'] = '2';
			$result['msg']      = 'Failed insert data';

			echo json_encode( $result ); exit;
		}
		
		$this->db->trans_commit();
		$result['feedback'] = 1;
		$result['msg'] 		= 'Success insert data';
			
		echo json_encode( $result ); exit;
	}
	
	public function save2(){ 
		$path = $_FILES['file_xls']['name'];
		$ext  = pathinfo($path, PATHINFO_EXTENSION);
		if($_FILES['file_xls']['size'] == "0"){
			$err['feedback'] = 2;
			echo json_encode($err);exit();
		}else{
			if (@strtolower($ext)=='xls' || @strtolower($ext)=='xlsx') {
				$this->load->library("PHPExcel");
				$phpExcel = new PHPExcel();
				/*---Read your Excel workbook---*/
				$inputFileName = $_FILES['file_xls']['tmp_name'];
				try {
					$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$objPHPExcel = $objReader->load($inputFileName);
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
	
				/*---Get worksheet dimensions---*/
				$sheet = $objPHPExcel->getSheet(0);
				$highestRow = $sheet->getHighestRow();
				$highestColumn = $sheet->getHighestColumn();
	
				/*---Loop through each row of the worksheet in turn---*/
				$start = 0;
				$last = date("Y-m-d h:i:sa");
				$last_insert = $last;
	
				$insert_data = array();
				$insert_data_false = array();
				$no_kontrak = array();
				$this->db->trans_begin();
				$result['feedback'] = 0;
				for ($row = 2; $row <= $highestRow; $row++){
					/*---Read	 a row of data into an array---*/
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
					$key = ($start -1);
					$key++;
					if(@$rowData[$key][0] != ""){
						$daerah 	= [
							'daerah_kode_pos' 					=> @$rowData[$key][0],
							'daerah_kelurahan' 					=> @$rowData[$key][1],
							'daerah_kecamatan' 					=> @$rowData[$key][2],
							'daerah_jenis_kota' 				=> @$rowData[$key][3],
							'daerah_kota' 						=> @$rowData[$key][4],
							'daerah_provinsi' 					=> @$rowData[$key][5]
							];
						$this->m_global->insert('daerah', $daerah);
					}
				}
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$result['feedback'] = 1;
				}
				
			}else{
				echo json_encode($result);
			}
		}
		echo json_encode($result);
	}
	
	public function save3(){ 
		$path = $_FILES['file_xls']['name'];
		$ext  = pathinfo($path, PATHINFO_EXTENSION);
		if($_FILES['file_xls']['size'] == "0"){
			$err['feedback'] = 2;
			echo json_encode($err);exit();
		}else{
			if (@strtolower($ext)=='xls' || @strtolower($ext)=='xlsx') {
				$this->load->library("PHPExcel");
				$phpExcel = new PHPExcel();
				/*---Read your Excel workbook---*/
				$inputFileName = $_FILES['file_xls']['tmp_name'];
				try {
					$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$objPHPExcel = $objReader->load($inputFileName);
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
	
				/*---Get worksheet dimensions---*/
				$sheet = $objPHPExcel->getSheet(0);
				$highestRow = $sheet->getHighestRow();
				$highestColumn = $sheet->getHighestColumn();
	
				/*---Loop through each row of the worksheet in turn---*/
				$start = 0;
				$last = date("Y-m-d h:i:sa");
				$last_insert = $last;
	
				$insert_data = array();
				$insert_data_false = array();
				$no_kontrak = array();
				$this->db->trans_begin();
				$result['feedback'] = 0;
				for ($row = 3; $row <= $highestRow; $row++){
					/*---Read	 a row of data into an array---*/
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
					$key = ($start -1);
					$key++;
					if(@$rowData[$key][2] != ""){
						$this->m_global->update('daerah', array('daerah_pengadilan' => @$rowData[$key][2]), ['daerah_jenis_kota' => @$rowData[$key][4], 'daerah_kota' => @$rowData[$key][5]]);
					}
				}
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$result['feedback'] = 1;
				}
				
			}else{
				echo json_encode($result);
			}
		}
		echo json_encode($result);
	}

	public function export(){
		$path = $_FILES['file_xls']['name'];
		$ext  = pathinfo($path, PATHINFO_EXTENSION);
		if($_FILES['file_xls']['size'] == "0"){
			$err['feedback'] = 2;
			echo json_encode($err);exit();
		}else{
			if (@strtolower($ext)=='xls' || @strtolower($ext)=='xlsx') {
				$this->load->library("PHPExcel");
				$phpExcel = new PHPExcel();
				/*---Read your Excel workbook---*/
				$inputFileName = $_FILES['file_xls']['tmp_name'];
				try {
					$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$objPHPExcel = $objReader->load($inputFileName);
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
	
				/*---Get worksheet dimensions---*/
				$sheet = $objPHPExcel->getSheet(0);
				$highestRow = $sheet->getHighestRow();
				$highestColumn = $sheet->getHighestColumn();
	
				/*---Loop through each row of the worksheet in turn---*/
				$start = 0;
				$last = date("Y-m-d h:i:sa");
				$last_insert = $last;
	
				$insert_data = array();
				$insert_data_false = array();
				$no_kontrak = array();
				$result['feedback'] = 0;
				for ($row = 2; $row <= $highestRow; $row++){
					/*---Read	 a row of data into an array---*/
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
					$key = ($start -1);
					$key++;
					$where = ['ed_nomor_kontrak' => $rowData[$key][2]];
					$select = 'ed_nomor_kontrak';
					$data_cek = $this->m_global->get_data_all('entry_data', null, $where, $select);
					$where = ['cabang_name' => $rowData[$key][1]];  
					$select = 'cabang_id';
					$cabang_id = $this->m_global->get_data_all('cabang', null, $where, $select);
					$hitung_no = count($data_cek);
					//31/05/15
					//print_r(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][5],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2));
					//die();  
						if (@$rowData[$key][2] != "" && ($hitung_no > 0 || in_array(@$rowData[$key][2], $no_kontrak, true) || @$cabang_id[0]->cabang_id == "")) {
								$insert_data_false[] 	= [
								'ed_cabang'					=> @$rowData[$key][1],
								'ed_no_kontrak_konsumen'	=> @$rowData[$key][2],
								'ed_nama_konsumen'			=> @$rowData[$key][3],
								'ed_nama_pemilik' 			=> @$rowData[$key][4],
								'ed_tgl_jual' 				=> date('d-m-Y', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][5],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))),
								'ed_nilai_penjaminan' 		=> uang(@$rowData[$key][6],null),
								'ed_nilai_benda_fidusia' 	=> uang(@$rowData[$key][7],null),
								'ed_merk_kendaraan' 		=> @$rowData[$key][8],
								'ed_tipe_kendaraan' 		=> @$rowData[$key][9],
								'ed_thn_pembuatan' 			=> @$rowData[$key][10],
								'ed_no_rangka_kendaraan'	=> @$rowData[$key][11],
								'ed_no_mesin_kendaraan' 	=> @$rowData[$key][12],
								'ed_cover_note' 			=> ((@$rowData[$key][13]!="")?date('d-m-Y', strtotime(PHPExcel_Style_NumberFormat::toFormattedString(@$rowData[$key][13],PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2))):''),
								'ed_bpkb'	 				=> @$rowData[$key][14],
								'ed_last_insert' 			=> $last_insert,
								'ed_created_by'				=> user_data()->user_id,
								'ed_created_date'			=> date("Y-m-d")
								];
						}else if(@$rowData[$key][2] != ""){	
						}else{
						}	
				}
			}else{
			}
		}
		$this->export_real($insert_data_false);
		
	}	
	
	function export_real($insert_data_false){
		ob_clean(); 
		//include('application/libraries/PHPExcel.php');
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="data_gagal.xls"');
		$e = 0;
				$z = 0;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
		$styleArray = array(
                    'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                  );
		$styleArray1 = array(
                     'borders' => array(
                    'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ),
                     ),
                    );
                    //background
                    $styleArray12 = array(
                    'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array(
                    'rgb' => 'FFEC8B',
                    ),
                    ),
									);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
    	$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->mergeCells('A1:O1');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'DATA GAGAL');
		$row = 3;
		$rowawal = $row;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':O'.$row)->applyFromArray($styleArray);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':O'.$row)->applyFromArray($styleArray1);
    	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':O'.$row)->applyFromArray($styleArray12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'No.');
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, 'Cabang');
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, 'No Kontrak');
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, 'Nama Konsumen');
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, 'Nama Pemilik');
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, 'Tgl Jual');
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, 'Nama Penjaminan');
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, 'Nilai Benda Fidusia');
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, 'Merk');
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, 'Type');
		$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, 'Tahun');
		$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, 'No Rangka');
		$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, 'No Mesin');
		$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row, 'Covernote Dealer');
		$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, 'No BPKB');
		$row++;
		$objPHPExcel->getActiveSheet()->freezePane('A'.$row);
		
		
	
								
								  
								
								
							foreach($insert_data_false as $id => $data){
								
								//$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':O'.$row)->applyFromArray($styleArray);
								//$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':O'.$row)->applyFromArray($styleArray1);
								$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, ($row - 3));
								$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, @$data['ed_cabang']);
								$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, @$data['ed_no_kontrak_konsumen']);
								$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, @$data['ed_nama_konsumen']);
								$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, @$data['ed_nama_pemilik']);
								$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, @$data['ed_tgl_jual']);
								$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, @$data['ed_nilai_penjaminan']);
								$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, @$data['ed_nilai_benda_fidusia']);
								$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, @$data['ed_merk_kendaraan']);
								$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, @$data['ed_tipe_kendaraan']);
								$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, @$data['ed_thn_pembuatan']);
								$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, @$data['ed_no_rangka_kendaraan']);
								$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, @$data['ed_no_mesin_kendaraan']);
								$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row, @$data['ed_cover_note']);
								$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, @$data['ed_bpkb']);
								
								
								$row++;
							} 
		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// Write file to the browser
		$writer->save('php://output');
		exit();
	}
}

/* End of file import_data.php */
/* Location: ./application/modules/import_data/controllers/import_data.php */
