<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Non_komersil extends MX_Controller 
{

    private $url        = 'non_komersil';
    private $table_db   = 'data_transaksi';
    private $prefix     = 'data_';

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }

	public function index()
	{

        $data['name']       = "Data konsumen";
        $data['url']        = base_url().$this->url;
        $data['plugin']     = ['datatables', 'datatables_fixcolumns','kendo_ui'];
        
		$this->template->display('index', $data);
    }

    public function select()
	{
		$post 		= $this->input->post();
		$length 	= intval($post['length']);
		$start  	= intval($post['start']);
		$sEcho		= intval($post['draw']);

		$join = [
            ['users', 'users.user_id = data_transaksi.createdby', 'left'],
            ['cabang', 'cabang_id = data_cabang_id', 'left']
        ];

		$where    = null;
		$where_e  = "data_flag not in('3') AND data_tipe_kontrak != 'K'";
		$where    = [];
		$search = [
            'kontrak'   => $this->prefix.'no_kontrak',
            'lastupdate' => 'createdat'
		];

		if (@$post['action'] == 'filter')
		{
			//$where = [];
			foreach ( $search as $key => $value )
			{
				if ( $post[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
                        $tmp = $post['lastupdate'];
                        if($tmp[0] != "" && $tmp[1] != ""){
                            $where_e = $where_e." AND DATE(createdat) BETWEEN '".date('Y-m-d', strtotime($tmp[0]))."' AND '".date('Y-m-d', strtotime($tmp[1]))."'";
						}
					}else {
						$where[$value.' LIKE '] = '%'.$post[$key].'%';
					}
				}
			}
		} else {
			//$where[$this->table_prefix.'status <>'] = '99';
        }

        
        $levelCabangId = '10';
        if( user_data()->user_level == $levelCabangId ) {
            $where['data_cabang_id'] = user_data()->cabang_id;
        }

		$keys 		= array_keys($search);
		@$order 	= [$search[$keys[($post['order'][0]['column']-1)]], $post['order'][0]['dir']];

        $select		= '*';
        $count 		= $this->m_global->count_data_all($this->table_db, $join, $where, $where_e );

		$length 	= $length < 0 ? $count : $length;
		$end 		= $start + $length;
		$end 		= $end > $count ? $count : $end;

		$result['iTotalRecords'] 			= $count;
		$result['iTotalDisplayRecords'] 	= $length;

		$records 			= array();
		$records["data"]	= array();


        $data = $this->m_global->get_data_all($this->table_db, $join, $where, $select, $where_e, ['data_flag', 'ASC'], $start, $length);
        // echo $this->db->last_query();
        // exit;
        $i = 1 + $start;

		foreach ( $data as $rows ) {
			$records["data"][] = array(
				$i,
                $rows->data_no_kontrak,
                '',
                // $rows->data_nama_konsumen,
                data_flag_status($rows->data_flag),
                date('d-m-Y h:i', strtotime($rows->createdat)),
                $this->_fileButton($rows->data_id),
                $this->_button($rows->data_id)
			);
			$i++;
		}   

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $count;
        $records["recordsFiltered"] = $count;

		echo json_encode($records);
    }

    public function show_add()
    {
        $data['name']   = 'Entri Data';
        $data['url']    = base_url().$this->url;
        $data['plugin'] = ['kendo_ui','dropify'];

        $this->template->display('data_transaksi_add', $data);
    }

    public function action_add()
    {
        $post   = $this->input->post();
        $this->form_validation->set_rules('nokontrak', 'No kontrak', 'trim|required');
        $this->form_validation->set_rules('tipe_kontrak', 'Tipe kontrak', 'trim|required');

        if( $this->form_validation->run() == false ) {
           $result['status'] = false;
           $result['message'] = validation_errors();
           
           echo json_encode($result); exit();
        }

        $noKontrak = str_replace(' ', '', $post['nokontrak']);
        $cek       = count($this->m_global->get_data_all('data_transaksi', null, ['data_no_kontrak' => $noKontrak], 'data_no_kontrak'));
        if ($cek > 0) {
            $result['status']  = false;
            $result['message']  = 'No Kontrak ' . $noKontrak . ' sudah terpakai';

            echo json_encode($result); exit;
        }

        $sizePdf   = $_FILES['pdf_path']['size'];
        $emptySize = 0;
        if( $sizePdf == $emptySize ) {
            $result['status']  = false;
            $result['message'] = 'File tidak boleh kosong';

            echo json_encode($result); exit;
        }

        $filePath      = $_FILES['pdf_path']['name'];
        $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);
        $allowedFile   = [
            'pdf'
        ];

        // validation if file not pdf
        if ( !in_array(@strtolower($fileExtension), $allowedFile) ) {
            $result['status']  = false;
            $result['message'] = 'File harus bertipe pdf';

            echo json_encode($result); exit;
        }

        $uploadDirectory = FCPATH."/assets/uploads/data_konsumen/".$noKontrak.'/';
        if( !is_dir($uploadDirectory) ){
            mkdir($uploadDirectory);
        }
        
        chmod($uploadDirectory, 0777);
        $tempName = $_FILES['pdf_path']['tmp_name'];
        $directoryUpload = $uploadDirectory.'/';
        $fileName = date('Y-m-d').'_'.user_data()->user_nik.'_'.rand().'.'.$fileExtension;

        // upload the file
        $doUpload = move_uploaded_file($tempName, $uploadDirectory.$fileName);
        if ( !$doUpload ){
            $result['status']  = false;
            $result['message'] = 'Gagal mengupload file';

            echo json_encode($result); exit;
        }

        $insertDataPdf = [
            'pdf_path'        => $fileName,
            'pdf_createdby'   => user_data()->user_id,
            'pdf_createddate' => date('Y-m-d H:i:s')
        ];
        $insertPdfFile = $this->m_global->insert('pdf_upload', $insertDataPdf);
        $idPdf         = $this->db->insert_id();

        if( user_data()->user_level == '10' ) {
            $cabangId = user_data()->cabang_id;
        } else {
            $cabangId = '1';
        }

        $insertDataTransaksi = [
            'data_no_kontrak'      => $noKontrak,
            'data_nama_konsumen'   => $post['nama_konsumen'],
            'data_tipe_kontrak'    => $post['tipe_kontrak'],
            'data_flag'            => '1',
            'data_cabang_id'       => $cabangId,
            'data_pdf_id'          => $idPdf,
            'createdat'            => date('Y-m-d H:i:s'),
            'createdby'            => user_data()->user_id,
            'updatedat'            => date('Y-m-d H:i:s'),
            'data_tempat_lahir'    => $post['tempat_lahir'],
            'data_tanggal_lahir'   => $post['tanggal_lahir'],
            'data_kelamin'         => $post['jenis_kelamin'],
            'data_alamat'          => $post['alamat'],
            'data_agama'           => $post['agama'],
            'data_pernikahan'      => $post['status_pernikahan'],
            'data_pekerjaan'       => $post['pekerjaan'],
            'data_kewarganegaraan' => $post['kewarganegaraan']
        ];

        if( !$query ) {
            $result['status']  = false;
            $result['message'] = 'Data gagal ditambahkan !';

            echo json_encode($result); exit;
        }

        $result['status']  = true;
        $result['message'] = 'Data berhasil ditambahkan !';

        echo json_encode($result); exit();

    }

    public function show_edit($id)
    {
        $data['pdf'] = "";
        $data['name']    = 'Edit data';
        $data['url']     = base_url().$this->url;
        $data['plugin']  = ['kendo_ui','dropify','pdfobject' => 'js'];
        $data['id']      = $id;

        $joinQuery = [
            ['pdf_upload', 'data_pdf_id = pdf_id', 'left']
        ];
        $data['records'] = $this->m_global->get_data_all($this->table_db, $joinQuery, [strEncrypt($this->prefix.'id', true) => $id])[0];

        $noKontrak = $data['records']->data_no_kontrak;
        $name      = $data['records']->pdf_path;
        $filePath  = '/assets/uploads/data_konsumen/'.$noKontrak.'/'.$name;
        $fullPath  = FCPATH.$filePath;
        if( file_exists($fullPath) ){
            $data['pdf'] = base_url($filePath);
        }


        $this->template->display('data_transaksi_edit', $data);
    }

    public function action_edit($id)
    {
        $post = $this->input->post();
        $this->form_validation->set_rules('data_nokontrak', 'No kontrak', 'trim|required');

        if( $this->form_validation->run() == false ) {
            $response['sts'] = 0;
            $response['msg'] = validation_errors();

            echo json_encode( $response ); exit();
        }

		$filePath    = $_FILES['pdf_path']['name'];
        $ext         = pathinfo($filePath, PATHINFO_EXTENSION);


        $dir = FCPATH.'/assets/uploads/data_konsumen/';
        $tempName = $_FILES['pdf_path']['tmp_name'];
        $fileName = date('Y-m-d').'_'.user_data()->user_nik.'_'.rand().'.pdf';

        $oldNoKontrak = str_replace(' ', '', $post['data_nokontrak_old']);
        $newNoKontrak = str_replace(' ', '', $post['data_nokontrak']);

        $join          = [['pdf_upload', 'data_pdf_id = pdf_id', 'left']];
        $getData       = $this->m_global->get_data_all('data_transaksi', $join, [strEncrypt($this->prefix.'id', true) => $id]);
        $oldDirectory  = FCPATH.'/assets/uploads/data_konsumen/'.$oldNoKontrak;
        $oldFile       = FCPATH.'/assets/uploads/data_konsumen/'.$oldNoKontrak.'/'.$getData[0]->pdf_path;
        $newDirectory  = FCPATH.'/assets/uploads/data_konsumen/'.$newNoKontrak;
        $newFile       = FCPATH.'/assets/uploads/data_konsumen/'.$newNoKontrak.'/'.$getData[0]->pdf_path;
        $pdfFile       = $_FILES['pdf_path'];

        if( $newNoKontrak != $oldNoKontrak ) {
            $cekNoKontrak = $this->m_global->count_data_all('data_transaksi', null, ['data_no_kontrak' => $newNoKontrak]);
            if( $cekNoKontrak > 0 ) {
                $response['sts'] = 0;
                $response['msg'] = 'No kontrak sudah terpakai';

                echo json_encode( $response ); exit;
            }

            if( !is_dir($newDirectory) ){
                mkdir($newDirectory);
                chmod($newDirectory, 0777);
            }

            // if file empty
            if( $pdfFile['size'] == 0 ) {
                rename($oldFile, $newFile);
                rmdir($oldDirectory);

            } else {
                // validation if file not xls or xlsx
                $allowedFile   = [
                    'pdf'
                ];
                if ( !in_array(@strtolower($ext), $allowedFile) ) {
                    $response['sts'] = 0;
                    $response['msg'] = 'File harus bertipe pdf';
        
                    echo json_encode( $response ); exit;
                }
                $tempName = $pdfFile['tmp_name'];
                unlink($oldFile);
                rmdir($oldDirectory);
                move_uploaded_file($tempName, $newFile);
            }

            $paramUpdate = [
                'data_no_kontrak' => $newNoKontrak,
                'updatedat'       => date('Y-m-d H:i:s'),
                'updatedby'       => user_data()->user_id
            ];
            $doUpdate      = $this->m_global->update('data_transaksi', $paramUpdate, [strEncrypt($this->prefix.'id', true) => $id]);
            if( !$doUpdate ) {
                $response['sts'] = 0;
                $response['msg'] = 'Gagal mengupdate data';

                echo json_encode( $response ); exit;
            }

            $response['sts'] = 1;
            $response['msg'] = 'Berhasil mengupdate data';
            
            echo json_encode( $response ); exit;
        }

        // if no kontrak is same
        if( $pdfFile['size'] != 0 ) {
            // validation if file not xls or xlsx
            $allowedFile   = [
                'pdf'
            ];
            if ( !in_array(@strtolower($ext), $allowedFile) ) {
                $response['sts'] = 0;
                $response['msg'] = 'File harus bertipe pdf';

                echo json_encode( $response ); exit;
            }
            $tempName = $pdfFile['tmp_name'];
            $doUpload = move_uploaded_file($tempName, $oldFile);

            if( !$doUpload ) {
                $response['sts'] = 0;
                $response['msg'] = 'Data Gagal di edit';

                echo json_encode( $response ); exit;
            }

            $response['sts'] = 1;
            $response['msg'] = 'Data berhasil di edit';

            echo json_encode ( $response ); exit; 
        }

        $response['sts'] = 1;
        $response['msg'] = 'Data berhasil di edit';

        echo json_encode( $response ); exit;

    }

    public function show_approve($id)
    {
        $data['pdf'] = "";
        $data['name']    = 'Approve data';
        $data['url']     = base_url().$this->url;
        $data['plugin']  = ['kendo_ui','dropify','pdfobject' => 'js'];
        $data['id']      = $id;

        $joinQuery = [
            ['pdf_upload', 'data_pdf_id = pdf_id', 'left']
        ];
        $data['records'] = $this->m_global->get_data_all($this->table_db, $joinQuery, [strEncrypt($this->prefix.'id', true) => $id])[0];

        $noKontrak = $data['records']->data_no_kontrak;
        $name      = $data['records']->pdf_path;
        $filePath  = '/assets/uploads/data_konsumen/'.$noKontrak.'/'.$name;
        $fullPath  = FCPATH.$filePath;
        if( file_exists($fullPath) ){
            $data['pdf'] = base_url($filePath);
        }


        $this->template->display('data_transaksi_approve', $data);
    }

    public function action_approve()
    {
        $id = $this->input->post('id');
        $doUpdate = $this->m_global->update('data_transaksi', ['data_flag' => '2'], [strEncrypt($this->prefix.'id', true) => $id]);

        if( !$doUpdate ) {
            $response['sts'] = 0;
            $response['msg'] = 'Data gagal terupdate';

            echo json_encode( $response ); exit;
        }

        $response['sts'] = 1;
        $response['msg'] = 'Data berhasil terupdate';

        echo json_encode( $response ); exit;
    }

    public function action_delete($id)
    {
        $join = [
            ['pdf_upload', 'data_transaksi.data_pdf_id = pdf_id', 'left']
        ];
        $get_data   = $this->m_global->get_data_all($this->table_db, $join, [strEncrypt($this->prefix.'id', true) => $id])[0];
        $path       = FCPATH . 'assets/uploads/data_konsumen/'.$get_data->data_no_kontrak;
        $filePath = $path.'/'.$get_data->pdf_path;
        if(file_exists($filePath)) {
            unlink($path.'/'.$get_data->pdf_path);
            rmdir($path);
        }
        $query      = $this->m_global->delete($this->table_db, [strEncrypt($this->prefix.'id', true) => $id]);

        if($query) {
            $result['msg'] = 'Data berhasil dihapus !';
            $result['status'] = 1;

            echo json_encode( $result ); exit();
        } 
        
        $result['msg'] = 'Data gagal dihapus !';
        $result['status'] = 0;

        echo json_encode( $result ); exit();
    }

    public function searchNik()
    {
        $nik = $this->input->get('nik');
        $dataKonsumen = $this->m_global->get_data_all('master_konsumen', null, ['konsumen_nik' => $nik]);
        if(count($dataKonsumen) < 1) {
            $result = [
                'status' => false,
                'data'   => []
            ];

            echo json_encode( $result ); exit();
        }

        $result = [
            'status' => true,
            'data'   => $dataKonsumen[0]
        ];
        echo json_encode( $result ); exit();
    }

    public function _button($id)
	{
        $userRole   = user_data()->user_role;
        $button     = null;
		$prev       = $id;
        $id         = strEncrypt($id);
        $getData    = $this->m_global->get_data_all('data_transaksi', null, [strEncrypt($this->prefix.'id', true) => $id]);

        $flag               = $getData[0]->data_flag;
        $edit 	            = '<a data-uk-tooltip title="Edit" class="ajaxify md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="'.base_url($this->url.'/show_edit').'/'.$id.'"><i class="uk-icon-pencil uk-icon-small"></i></a>';
        $delete             = '<a data-uk-tooltip title="Delete" class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" onclick="return f_status(2, this, event);" href="'.base_url($this->url.'/action_delete/'.$id).'"><i class="uk-icon-trash uk-icon-small"></i></a>';
        $preview            = '<a data-uk-tooltip title="Preview" class="md-btn md-btn-primary md-btn-mini md-btn-wave-light md-btn-icon waves-effect waves-button waves-light ajaxify" href="'.base_url('non_komersil/show_approve').'/'.$id.'"><i class="uk-icon-map"></i></a>';
        $tipeKontrak        = $getData[0]->data_tipe_kontrak;
        $prosesFinance = [
            '1',
            '9'
        ];
        $userAuthorize = [
            '40',
            '38',
            '37'
        ];

        if( !in_array($userRole, $userAuthorize) ) {
            $button     = $preview . $edit . $delete;
            return $button;
        }
        
        if( in_array($flag,$prosesFinance) ) {

            $button     = $preview . $edit . $delete;
            return $button;
        }

		
    }

    public function _fileButton( $id )
    {
        $id   = strEncrypt($id);
        $join = [
            ['pdf_upload', 'pdf_id = data_pdf_id', 'left'],
            ['sertifikat_upload', 'data_sertifikat_id = sertifikat_id', 'left'],
            ['invoice_upload', 'data_invoice_id = invoice_id', 'left']
        ];
        $data = $this->m_global->get_data_all('data_transaksi', $join, [strEncrypt($this->prefix.'id', true) => $id]);

        $fileKonsumen 	= '<a href="'.base_url('data/download/'.$id.'/UPF1').'" target="_blank"><span class="uk-badge uk-badge-primary">Data konsumen</span></a> ';
        $fileSertifikat = '<a href="'.base_url('data/download/'.$id.'/UPF2').'" target="_blank" ><span class="uk-badge uk-badge-primary">Sertifikat</span></a> ';
        $fileInvoice    = '<a href="'.base_url('data/download/'.$id.'/UPF3').'" target="_blank" ><span class="uk-badge uk-badge-primary">PNBP</span></a> ';
        $fileAkta       = '<a href="'.base_url('data/download/'.$id.'/UPF4').'" target="_blank" ><span class="uk-badge uk-badge-primary">Akta</span></a> ';
        
        $dataFlag         = $data[0]->data_flag;
        $prosesFinance    = ['1', '2', '9'];
        $prosesSertifikat = ['3'];
        $prosesInvoice    = ['4'];
        $prosesAkta       = ['5'];
        $tipeKontrak      = $data[0]->data_tipe_kontrak;

        // if( $tipeKontrak == 'NK' ) {
        //     return $fileKonsumen;
        // }

        if( in_array($dataFlag, $prosesFinance) ) {
            $button = $fileKonsumen;
            return $button;
        }

        if( in_array($dataFlag, $prosesSertifikat) ) {
            $button = $fileKonsumen.$fileSertifikat;
            return $button;
        }

        if( in_array($dataFlag, $prosesInvoice) ) {
            $button = $fileKonsumen.$fileSertifikat.$fileInvoice;
            return $button;
        }

        if( in_array($dataFlag, $prosesAkta) ) {
            $button = $fileKonsumen.$fileSertifikat.$fileInvoice.$fileAkta;
            return $button;
        }
    }

    public function download($id = '', $type = '')
    {
        $join = [
            ['pdf_upload', 'data_pdf_id = pdf_id', 'left'],
            ['sertifikat_upload', 'data_sertifikat_id = sertifikat_id', 'left'],
            ['invoice_upload', 'data_invoice_id = invoice_id', 'left'],
            ['akta_upload', 'data_akta_id = akta_id', 'left']
        ];
        $data      = $this->m_global->get_data_all('data_transaksi', $join, [strEncrypt($this->prefix.'id', true) => $id]);
        $noKontrak = $data[0]->data_no_kontrak;

        $file = [
            'UPF1' => ['path' => 'data_konsumen', 'file' => $data[0]->pdf_path, 'name' => 'data_konsumen_', 'file_type' => '.pdf'],
            'UPF2' => ['path' => 'sertifikat', 'file' => $data[0]->sertifikat_path, 'name' => 'sertifikat_', 'file_type' => '.pdf'],
            'UPF3' => ['path' => 'invoice', 'file' => $data[0]->invoice_path, 'name' => 'pnbp_', 'file_type' => '.pdf'],
            'UPF4' => ['path' => 'akta', 'file' => $data[0]->akta_path, 'name' => 'akta_', 'file_type' => '.doc'],
        ];

        $filePath = FCPATH . 'assets/uploads/'.$file[$type]['path'].'/'.$noKontrak.'/'.$file[$type]['file'];

        if(file_exists($filePath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.$file[$type]['name'].$noKontrak.$file[$type]['file_type']);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filePath));
            ob_clean();
            flush();
            readfile($filePath);
            exit;
        }
    }

}