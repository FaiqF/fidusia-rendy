<div class="uk-grid">
	<div class="uk-width-medium-1-1">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">Upload Sertifikat</h3>
			</div>
			<div class="md-card-content">
				<div id="alert_error"></div>
				<form method="post" action="<?=$url; ?>/action_add" id="form_add">
					<div class="uk-grid">
						
						<div class="uk-width-medium-2-5 uk-row-first">
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>Nik <span class="required">*</span></label>
											<input name="nokontrak" type="text" class="md-input uk-form-width-medium" id="input-nik">
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
                                    <div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>File PDF </label>
										</div>
									</div>
								</div>
								
								<div class="uk-width-medium-1-2 uk-row-first">
									<button type="button" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light" id="seach-nik">
										<i class="material-icons">search</i>Cari
									</button>
								</div>
                                <div class="uk-margin-medium-top">
									<div class="uk-grid">
										<div class="uk-width-large-1-1" style="width:200%">
											<input accept="application/pdf" data-max-file-size="10000K" name="pdf_path" type="file" id="uploadFile" class="uploadFile"/>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="uk-width-medium-2-5 uk-row first">
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>Nama konsumen <span class="required">*</span></label>
											<input name="nama_konsumen" type="text" class="md-input uk-form-width-medium" readonly>
											<input name="tipe_kontrak" type="hidden" value="NK">
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
								</div>
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>Tempat Lahir <span class="required">*</span></label>
											<input name="tempat_lahir" type="text" class="md-input uk-form-width-medium" readonly>
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>Tanggal Lahir <span class="required">*</span></label>
											<input name="tanggal_lahir" type="text" class="md-input uk-form-width-medium" readonly>
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
								</div>
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>Jenis Kelamin <span class="required">*</span></label>
											<input name="jenis_kelamin" type="text" class="md-input uk-form-width-medium" readonly>
											<input name="tipe_kontrak" type="hidden" value="NK">
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>Alamat <span class="required">*</span></label>
											<input name="alamat" type="text" class="md-input uk-form-width-medium" readonly>
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
								</div>
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>Agama <span class="required">*</span></label>
											<input name="agama" type="text" class="md-input uk-form-width-medium" readonly>
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>Status Pernikahan <span class="required">*</span></label>
											<input name="status_pernikahan" type="text" class="md-input uk-form-width-medium" readonly>
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
								</div>
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>Pekerjaan <span class="required">*</span></label>
											<input name="pekerjaan" type="text" class="md-input uk-form-width-medium" readonly>
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="uk-grid">
								<div class="uk-width-medium-1-2 uk-row-first">
									<div class="uk-form-row">
										<div class="md-input-wrapper md-input-filled">
											<label>Kewarganegaraan <span class="required">*</span></label>
											<input name="kewarganegaraan" type="text" class="md-input uk-form-width-medium" readonly>
											<span class="md-input-bar uk-form-width-medium"></span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-text-center uk-margin-large-top">
						<button type="submit" class="md-btn md-btn-small md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Save</button>
						<a href="<?=$url; ?>" class="md-btn md-btn-small md-btn-warning md-btn-wave-light waves-effect waves-button waves-light ajaxify">Back</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<a href="<?php echo $url; ?>" class="ajaxify reload"></a>
<script>
	$(document).ready(function(){

		// handle form submit
			$('#form_add').on('submit', function(e){
				e.preventDefault();

				var $this 	= $(this),
				url 		= $this.attr('action'),
				data 		= $this.serialize();

				var form = $('form')[0]	;
				var formData = new FormData(form);
				var err_res = 0,
				err_msg = [],
				suc_msg = [];

				$.ajax({
					url: url,
					type: 'post',
					dataType:'json',
					data: formData,
					processData: false,
					contentType: false,
					async: false,
					success : function(res) {
						if (res.status == false) {
							App.notif('error', res.message, 'error');
						} else if (res.status == true) {
							App.notif('Success', res.message, 'success');
							$('.reload').trigger('click');
						}
					},
					error : function(res) {
						App.notif('Error', res.msg , 'error');
					}
				});
			});

		// end form submit

        var fileUpload = $('.uploadFile').dropify({
			messages: {
				'default': 'File Upload',
				'replace': '',
				'remove':  'Batal',
				'error': 'File hanya format PDF dan maksimal 2.5MB !'
			}
		});

		$('#seach-nik').on('click', function() {
			var nik = $('#input-nik').val();
			$.ajax({
				type: "GET",
				url   : "<?php echo base_url('non_komersil/searchNik')?>",
				data  : {nik : nik},
				success: function ( response ) {
					var response = JSON.parse( response );
					if( response.status == false ) {
						alert('Data dengan nik :' + nik + ' tidak ditemukan');
						return;
					}

					alert('Data ditemukan');
					$("input[name='nama_konsumen']").val(response.data.konsumen_nama);
					$("input[name='tempat_lahir']").val(response.data.konsumen_tempat_lahir);
					$("input[name='tanggal_lahir']").val(response.data.konsumen_tanggal_lahir);
					$("input[name='jenis_kelamin']").val(response.data.konsumen_kelamin);
					$("input[name='alamat']").val(response.data.konsumen_alamat);
					$("input[name='agama']").val(response.data.konsumen_agama);
					$("input[name='status_pernikahan']").val(response.data.konsumen_pernikahan);
					$("input[name='pekerjaan']").val(response.data.konsumen_pekerjaan);
					$("input[name='kewarganegaraan']").val(response.data.konsumen_kewarganegaraan);
					return;
				}
			})
		});

	});

</script>