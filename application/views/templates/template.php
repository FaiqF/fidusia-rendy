<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- Remove Tap Highlight on Windows Phone IE -->
		<meta name="msapplication-tap-highlight" content="no"/>

		<link rel="icon" type="image/png" href="<?=base_url(); ?>assets/img/BRIF/favicon.jpg" sizes="16x16">
		<link rel="icon" type="image/png" href="<?=base_url(); ?>assets/img/BRIF/favicon.jpg" sizes="32x32">

		<title>Simulasi Kredit Mobil</title>


		<!-- Custom Style -->
		<link rel="stylesheet" href="<?=base_url(); ?>assets/css/core/style.css" media="all">
		
		<!-- uikit -->
		<link rel="stylesheet" href="<?=base_url(); ?>bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

		<!-- flag icons -->
		<link rel="stylesheet" href="<?=base_url(); ?>assets/icons/flags/flags.min.css" media="all">

		<!-- altair admin -->
		<link rel="stylesheet" href="<?=base_url(); ?>assets/css/main.min.css" media="all">

		<!-- themes -->
		<link rel="stylesheet" href="<?=base_url(); ?>assets/css/themes/themes_combined.min.css" media="all">

		<!-- Sweet Alert -->
		<link rel="stylesheet" href="<?=base_url(); ?>bower_components/sweet-alert/css/sweetalert2.min.css" media="all">

		<!-- Toastr -->
		<link rel="stylesheet" href="<?=base_url(); ?>bower_components/toastr/css/toastr.min.css" media="all">


		<!-- matchMedia polyfill for testing media queries in JS -->
		<!--[if lte IE 9]>
			<script type="text/javascript" src="<?=base_url(); ?>bower_components/matchMedia/matchMedia.js"></script>
			<script type="text/javascript" src="<?=base_url(); ?>bower_components/matchMedia/matchMedia.addListener.js"></script>
			<link rel="stylesheet" href="<?=base_url(); ?>assets/css/ie.css" media="all">
		<![endif]-->

		<!-- common functions -->
		<script src="<?=base_url(); ?>assets/js/common.min.js"></script>
		<script>
			var base_url = "<?=base_url(); ?>";
		</script>

	</head>
	<body class=" sidebar_main_open sidebar_main_swipe">
		<!-- main header -->
		<header id="header_main">
			<?php echo $_header; ?>
		</header><!-- main header end -->
		<!-- main sidebar -->
		<aside id="sidebar_main" class="accordion_mode">
			<?php echo $_sidebar; ?>
		</aside><!-- main sidebar end -->

		<div id="page_content">
			<?php echo $_fullcontent; ?>
		</div>

		<!-- google web fonts -->
		<script src="<?=base_url(); ?>assets/js/webfont.js" async="true" type="text/javascript"></script>
		<!-- <script>
			WebFontConfig = {
				google: {
					families: [
						'Source+Code+Pro:400,700:latin',
						'Roboto:400,300,500,700,400italic:latin'
					]
				}
			};
			(function() {
				var wf = document.createElement('script');
				wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
				'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
				wf.type = 'text/javascript';
				wf.async = 'true';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(wf, s);
			})();
		</script> -->
		<!-- uikit functions -->
		<script src="<?=base_url(); ?>assets/js/uikit_custom.js"></script>
		<!-- altair common functions/helpers -->
		<script src="<?=base_url(); ?>assets/js/altair_admin_common.min.js"></script>
		<!-- page specific plugins -->

		<!-- Sweet Alert -->
		<script src="<?=base_url(); ?>bower_components/sweet-alert/js/sweetalert2.min.js"></script>
		<script src="<?=base_url(); ?>bower_components/sweet-alert/js/core.js"></script>

		<!-- Toastr -->
		<script src="<?=base_url(); ?>bower_components/toastr/js/toastr.min.js"></script>

		<!-- jquery.idle -->
		<script src="<?=base_url(); ?>bower_components/jquery-idletimer/dist/idle-timer.min.js"></script>

		<!-- Custom App.js & Layout.js -->
		<script src="<?=base_url(); ?>assets/js/core/layout.js"></script>
		<script src="<?=base_url(); ?>assets/js/core/app.js"></script>
		
		<!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script> -->
    	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script> -->

    	<!-- <script src="<?php echo base_url()?>assets/js/jquery.dm-uploader.min.js"></script> -->

		<!-- <script>
			$(document).ready(function(){
				checkTab();
			});

			window.onbeforeunload = function() {
				if (localStorage.getItem('tab') == 1 && sessionStorage.getItem('tab') == 1) {
					localStorage.removeItem('tab');
					sessionStorage.removeItem('tab');
					return null;
				}
			};

			$(document).on("idle.idleTimer", function (event, elem, obj) {
				window.location = base_url+'login/out';
			});

			var timeOUTweb = <?=get_config_password('TIMEOUT_SYSTEM')[0]->pass_value; ?>*1000;
			$(document).idleTimer(parseInt(timeOUTweb));

			$(function() {
				if(isHighDensity()) {
					$.getScript( "bower_components/dense/src/dense.js", function() {
						// enable hires images
						altair_helpers.retina_images();
					})
				}
				if(Modernizr.touch) {
					// fastClick (touch devices)
					FastClick.attach(document.body);
				}
			});
			$window.load(function() {
				// ie fixes
				altair_helpers.ie_fix();
			});

			function checkTab() {
				if(typeof(Storage) !== "undefined") {
					if (typeof(localStorage.tab) !== "undefined") {
						if (localStorage.getItem('tab') == 1 && sessionStorage.getItem('tab') == 1) {
							return true;
						} else {
							window.location.href = base_url+'dashboard/newtab';
						}
					} else {
						var Tab = 1;

						localStorage.setItem('tab', Tab);
						sessionStorage.setItem('tab', Tab);
					}
				}
			}
		</script> -->

	</body>
</html>