<div class="sidebar_main_header" style="height:150px;">
	<div class="sidebar_logo" style="height: 150px; line-height: 100px;">
		<a href="<?=base_url('dashboard'); ?>" class="sSidebar_hide sidebar_logo_large">
			<img style="width: 200px; height: 150px;" class="logo_regular" src="<?=base_url(); ?>assets/img/BRIF/sidebar.png" alt="" height="15" width="71"/>
			<img class="logo_light" src="<?=base_url(); ?>assets/img/wom.png" alt="" height="15" width="71"/>
		</a>
		<a href="<?=base_url('dashboard'); ?>" class="sSidebar_show sidebar_logo_small">
			<img class="logo_regular" src="<?=base_url(); ?>assets/img/wom.png" alt="" height="32" width="32"/>
			<img class="logo_light" src="<?=base_url(); ?>assets/img/wom.png" alt="" height="32" width="32"/>
		</a>
	</div>
</div>

<div class="menu_section">
	<ul>
		<?php
			$menu       = menu();
			$url        = $this->uri->segment_array();

			v_sidebar_menu( $menu, $url);
		?>
	</ul>
</div>