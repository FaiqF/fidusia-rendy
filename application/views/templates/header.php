<div class="header_main_content">
	<nav class="uk-navbar">
						
		<!-- main sidebar switch -->
		<a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
			<span class="sSwitchIcon"></span>
		</a>
		
		<div class="uk-navbar-flip">
			<ul class="uk-navbar-nav user_actions">
				<li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
				<li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
					<a href="#" class="user_action_image">
						<b><?=user_data()->user_nik . ' - ' . user_data()->position; ?></b>
						<img class="md-user-image" src="<?=base_url(); ?>assets/img/avatars/user.png" alt=""/>
					</a>
					<div class="uk-dropdown uk-dropdown-small">
						<ul class="uk-nav js-uk-prevent">
							<li><a class="ajaxify" href="<?=base_url('my_profile'); ?>">My profile</a></li>
							<li><a href="<?=base_url('login/out'); ?>">Logout</a></li>
						</ul>
					</div>
				</li>
			</ul>
		</div>
	</nav>
</div>