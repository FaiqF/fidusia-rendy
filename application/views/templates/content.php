<?php if(!isset($breadcrumbs)) : ?>
<div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
	<div class="heading_actions">
		<!-- <a href="#" data-uk-tooltip="{pos:'bottom'}" title="Archive"><i class="md-icon material-icons">&#xE149;</i></a>
		<a href="#" data-uk-tooltip="{pos:'bottom'}" title="Print"><i class="md-icon material-icons">&#xE8AD;</i></a>
		<div data-uk-dropdown>
			<i class="md-icon material-icons">&#xE5D4;</i>
			<div class="uk-dropdown uk-dropdown-small">
				<ul class="uk-nav">
					<li><a href="#">Action</a></li>
					<li><a href="#">Other Action</a></li>
					<li><a href="#">Other Actiona</a></li>
				</ul>
			</div>
		</div> -->
		<?php if(isset($export)) : ?>
			<div data-uk-dropdown>
				<i class="md-icon material-icons">print</i>
				<div class="uk-dropdown uk-dropdown-small">
					<ul class="uk-nav">
						<?php foreach ($export as $key => $val): ?>
							<li><a class="export_data" href="<?=$val['link'] ?>"><?=$val['name']; ?></a></li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>
		<?php endif; ?>
		<?php if(isset($upload)) : ?>
			<div data-uk-dropdown>
				<i class="md-icon material-icons">cloud_upload</i>
				<div class="uk-dropdown uk-dropdown-small">
					<ul class="uk-nav">
						<?php foreach ($upload as $key => $val): ?>
							<li><a class="" href="<?=$val['link'] ?>"><?=$val['name']; ?></a></li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<h1><?=$name; ?></h1>
	<span class="uk-text-upper uk-text-small">Kredit plus finance</span>
</div>
<?php else : ?>
	<?php if(!empty($breadcrumbs)) : ?>
		<div id="top_bar">
			<ul id="breadcrumbs">
				<?php foreach ($breadcrumbs as $key => $val) : ?>
					<li><?php echo ($val !== null) ? '<a class="ajaxify" href="'.$val.'">'.$key.'</a>' : '<span>'.$key.'</span>' ?></li>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>
<?php endif; ?>

<div id="page_content_inner">
	<?php
		/* Additional Plugins */
		get_plugin(@$plugin);
	?>
	<?php echo $_content; ?>
</div>

<script>
	$(document).ready(function(){
		var breadcrumbs = ("<?=is_array(@$breadcrumbs); ?>") ? 'true' : 'false',
			status 		= ("<?=!empty(@$breadcrumbs); ?>") ? 'true' : 'false',
			body 		= $('body');

		if(breadcrumbs === 'true') {
			if (status === 'true') {
				body.removeClass('page_heading_active').addClass('top_bar_active');
			} else {
				body.removeClass('page_heading_active top_bar_active');
			}
		} else {
			body.removeClass('top_bar_active').addClass('page_heading_active');
		}

		$('.export_data').on('click', function(e){
			e.preventDefault();

			var url = $(this).attr('href'),
				data = $('.filter').find('input.form-filter, select.form-filter').serialize();

			window.open(url+'?'+data,'_blank' );
		});
	});
</script>