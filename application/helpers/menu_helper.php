<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function _getUserAccess(){
	$CI =& get_instance();

	$result = $CI->m_global->get_data_all('users', [['roles', 'role_id = user_role']], ['user_id' => user_data()->user_id], 'role_access')[0]->role_access;

	return $result;	
}

function menu($all = '', $access = null)
{
	$CI =& get_instance();

	$in = ($access == null) ? _getUserAccess() : $access;

	$result 	= [];
	$where_e 	= ($all === '') ? '`menu_id` IN ('.$in.')' : null ;

	$menu_db	= $CI->m_global->get_data_all('menus', null, ['menu_status' => '1', 'menu_parent' => '0'], '*', $where_e, ['menu_number', 'ASC']);

	foreach ($menu_db as $key => $val) {
		$result[] = [
						'id'   => $val->menu_id,
						'name' => $val->menu_name,
						'link' => _linkDB($val, $all),
						'icon' => $val->menu_icon,
					];
	}

	return $result;
}

function _linkDB($val, $all)
{
	$CI         =& get_instance();

	$where_e 	= ($all === '') ? '`menu_id` IN ('._getUserAccess().')' : null ;
	$link = $CI->m_global->get_data_all('menus', null, ['menu_status' => '1', 'menu_parent' => $val->menu_id], '*', $where_e, ['menu_number', 'ASC']);

	$resultLink = [];

	if($link) {
		foreach ($link as $k => $v) {
			$resultLink[] = [
								'id'   => $v->menu_id,
								'name' => $v->menu_name,
								'link' => _linkDB($v, $all),
								'icon' => $v->menu_icon,
							];
		}
	} else {
		$resultLink = $val->menu_link;
	}

	return $resultLink;
}

function v_sidebar_menu($menu, $url) {
	if (!isset($url[1])) {
		$url[1] = 'dashboard';
	}

	foreach ($menu as $key => $val) {
		$class_li = '';

		if (!is_array($val['link'])) {
			$x = explode('/', $val['link']);

			$class_li = (end($x) == end($url)) ? 'class="current_section"' : '';
		}
		
		$url_link = (!is_array($val['link']) ? ($val['link'] !== "#" ? (base_url().$val['link']) : "javascript:;" ) : "javascript:;");
		$ajaxify  = (!is_array($val['link']) ? ($val['link'] !== "#" ? 'ajaxify' : "" ) : "");

		echo '<li title="' . $val['name'] . '" ' . $class_li . '>
				<a href="' . $url_link . '" class="' . $ajaxify . '">
					<span class="menu_icon"><i class="material-icons">' . $val['icon'] . '</i></span>
					<span class="menu_title">' . $val['name'] . '</span>
				</a>
				' . (is_array($val['link']) ?
					'<ul>' .
						_submenu($val['link'], $url)
					. '</ul>'
					: ''
				) . '
			</li>';
	}
}

function _submenu($val, $url)
{
	$result = '';

	foreach ($val as $k => $v) {
		$class_li = '';

		if (!is_array($v['link'])) {
			$x = explode('/', $v['link']);

			$class_li = (in_array(end($x), $url)) ? 'act_item' : '';
		} else {
			$x = list_name2($url ,'/', 1);
			$y = [];

			foreach ($v['link'] as $key => $val) {
				$y[] = $val['link'];
			}

			$class_li = (in_array($x, $y)) ? 'current_section act_item' : '';
		}

		$url_link = (!is_array($v['link']) ? ($v['link'] !== "#" ? (base_url().$v['link']) : "javascript:;" ) : "javascript:;");
		$ajaxify  = (!is_array($v['link']) ? ($v['link'] !== "#" ? 'ajaxify' : "" ) : "");

		$result .= '<li title="' . $v['name'] . '" class="' . $class_li . '">
			<a href="' . $url_link . '" class="' . $ajaxify . '">' . $v['name'] . '</a>
			' . (is_array($v['link']) ?
				'<ul>' .
					_submenu($v['link'], $url)
				. '</ul>'
				: ''
			) . '
		</li>';
	}

	return $result;
}

function v_tree_view($menu, $access = null) {
	$data_access = ['1'];

	if ($access) {
		$data_access = explode(', ', $access);
	}

	$html = '<ul id="rolesData" style="display: none;">';
	foreach ($menu as $key => $val) {
		$selected = (in_array($val['id'], $data_access)) ? 'selected' : '' ;

		$exp = (is_array($val['link'])) ? 'expanded' : '';

		$html .= 	'<li data-id="'.$val['id'].'" class="folder '.$selected.' '.$exp.'">'
						.$val['name']
						. (is_array($val['link']) ?
							'<ul>' .
								_tree_submenu($val['link'], $data_access)
							. '</ul>'
							: ''
						) . '
					</li>';
	}

	$html .= '</ul>';

	echo $html;
}

function _tree_submenu($val, $access = null)
{
	$html = '';
	foreach ($val as $k => $v) {
		$selected = (in_array($v['id'], $access)) ? 'selected' : '' ;
		$exp = (is_array($v['link'])) ? 'expanded' : '';

		$html .= 	'<li data-id="'.$v['id'].'" class="folder '.$selected.' '.$exp.'">'
						.$v['name']
						. (is_array($v['link']) ?
							'<ul>' .
								_tree_submenu($v['link'], $access)
							. '</ul>'
							: ''
						) . '
					</li>';
	}

	return $html;
}