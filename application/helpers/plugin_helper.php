<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function get_plugin($plugin = null, $type = null) {
		$arr = [
			'datatables_button' => [
				'css' => [],
				'js'  => [
					'<script src="'.base_url('bower_components/datatables-buttons/js/dataTables.buttons.js').'" type="text/javascript"></script>',
					'<script src="'.base_url('assets/js/custom/datatables/buttons.uikit.js').'" type="text/javascript"></script>',
					'<script src="'.base_url('bower_components/jszip/dist/jszip.min.js').'" type="text/javascript"></script>',
					'<script src="'.base_url('bower_components/pdfmake/build/pdfmake.min.js').'" type="text/javascript"></script>',
					'<script src="'.base_url('bower_components/pdfmake/build/vfs_fonts.js').'" type="text/javascript"></script>',
					'<script src="'.base_url('bower_components/datatables-buttons/js/buttons.colVis.js').'" type="text/javascript"></script>',
					'<script src="'.base_url('bower_components/datatables-buttons/js/buttons.html5.js').'" type="text/javascript"></script>',
					'<script src="'.base_url('bower_components/datatables-buttons/js/buttons.print.js').'" type="text/javascript"></script>',
				]
			],
            'datatables_fixcolumns' => [
				'css' => [],
				'js'  => [
					'<script src="'.base_url('bower_components/datatables_fixcolumns/js/dataTables.fixedColumns.min.js').'" type="text/javascript"></script>',
				]
			],
			'datatables' => [
				'css' => [],
				'js'  => [
					'<script src="'.base_url('bower_components/datatables/media/js/jquery.dataTables.min.js').'" type="text/javascript"></script>',
					'<script src="'.base_url('assets/js/custom/datatables/datatables.uikit.min.js').'" type="text/javascript"></script>',
				]
			],
			'kendo_ui' => [
				'css' => [
					'<link href="'.base_url('bower_components/kendo-ui/styles/kendo.common-material.min.css').'" rel="stylesheet" type="text/css" />',
					'<link href="'.base_url('bower_components/kendo-ui/styles/kendo.material.min.css').'" rel="stylesheet" type="text/css" />',
				],
				'js'  => [
					'<script src="'.base_url('assets/js/kendoui_custom.min.js').'" type="text/javascript"></script>',
					// '<script src="'.base_url('assets/js/pages/kendoui.min.js').'" type="text/javascript"></script>',
				]
			],
			'fancytree' => [
				'css' => [
					'<link href="'.base_url('assets/skins/jquery.fancytree/ui.fancytree.min.css').'" rel="stylesheet" type="text/css" />',
				],
				'js'  => [
					'<script src="'.base_url('bower_components/jquery-ui/jquery-ui.min.js').'" type="text/javascript"></script>',
					'<script src="'.base_url('bower_components/jquery.fancytree/dist/jquery.fancytree-all.min.js').'" type="text/javascript"></script>',
				]
			],
			'dropify' => [
				'css' => [
					'<link href="'.base_url('assets/js/custom/dropify/dist/css/dropify.new.css').'" rel="stylesheet" type="text/css" />',
					// '<link href="'.base_url('assets/skins/dropify/css/dropify.css').'" rel="stylesheet" type="text/css" />',
				],
				'js'  => [
					'<script src="'.base_url('assets/js/custom/dropify/dist/js/dropify.new.js').'" type="text/javascript"></script>',
					// '<script src="'.base_url('assets/js/custom/dropify/dist/js/dropify.min.js').'" type="text/javascript"></script>',
					// '<script src="'.base_url('assets/js/custom/dropify/dist/js/dropify_new.min.js').'" type="text/javascript"></script>',
				]
			],
			'peity' => [
				'css' => [],
				'js'  => [
					'<script src="'.base_url('bower_components/peity/jquery.peity.min.js').'" type="text/javascript"></script>',
				]
			],
			'pdfobject' => [
				'css' => [],
				'js'  => [
					'<script src="'.base_url('assets/js/custom/pdfobject/pdfobject.min.js').'" type="text/javascript"></script>',
				]
			],
			'inputmask' => [
				'css' => [],
				'js'  => [
					'<script src="'.base_url('bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js').'" type="text/javascript"></script>',
				]
			],
		];

		if($plugin == null) {
			return false;
			exit;
		}

		if(is_array($plugin)) {
			foreach ($plugin as $key => $val) {
				if(!in_array($val, ['js', 'css'])) {
					$css = $arr[ $val ]['css'];
					$js  = $arr[ $val ]['js'];

					foreach ($css as $k => $v) {
						echo $v."\n";
					}

					foreach ($js as $k => $v) {
						echo $v."\n";
					}
				} else {
					$each  = $arr[ $key ][ $val ];

					foreach ($each as $k => $v) {
						echo $v."\n";
					}
				}
			}
		} else {
			$css = $arr[ $plugin ]['css'];
			$js  = $arr[ $plugin ]['js'];

			foreach ($css as $key => $val) {
				echo $val."\n";
			}

			foreach ($js as $key => $val) {
				echo $val."\n";
			}
		}
	}

?>
