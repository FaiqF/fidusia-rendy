<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	$CI = &get_instance();
	$CI->load->library( 'session' );

	$url = $CI->uri->segment(1) == '' ? 'dashboard' : $CI->uri->segment(1);

	$ex = array('login','server');

	$user_data 		= $CI->session->userdata('wom_finance');

	$status_link    = @$CI->input->post('status_link');

	if ( ! empty( $user_data ) AND ( ( in_array ( $CI->uri->segment(1), $ex) AND $CI->uri->segment(2) != "out") OR $CI->uri->segment(1) == "" ) )
	{
		redirect( base_url('dashboard') );
	}
	else if ( empty($user_data) AND ! in_array( $CI->uri->segment(1), $ex ) )
	{
		if ( $status_link == 'ajax' )
		{
			echo 'out';
			die();
		} else {
			redirect(base_url('login'));
		}
	}