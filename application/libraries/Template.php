<?php
class Template {
	protected $_ci;

	function __construct()
	{
		$this->_ci =& get_instance();
	}

	function display($template, $data = NULL, $plugin = NULL, $js = NULL, $css = NULL) {
		$url = isset($data['url']) ? $data['url'] : null;

		$sts = $this->_ci->m_global->get_data_all('session', null, ['session_id' => user_data()->session_id], 'session_status');
		if($sts) {
			if ($sts[0]->session_status == '0') {
				$this->_ci->session->sess_destroy();
			}
		} else {
			$this->_ci->session->sess_destroy();
		}

		$this->_update_token();

		$status_link    = @$this->_ci->input->post('status_link');
		$template 		= ($this->_check_link($url) === 'true') ? $template : 'error/error_404' ;

		if ($status_link == 'ajax') {
			// $data['_js']			= $this->_ci->load->view('templates/js', $js, TRUE);
			// $data['_css']			= $this->_ci->load->view('templates/css', $css, TRUE);
			$data['_content']		= $this->_ci->load->view($template, $data, TRUE);

			$this->_ci->load->view('templates/content', $data);
		} else {
			// $data['_js']			= $this->_ci->load->view('templates/js', $js, TRUE);
			// $data['_css']			= $this->_ci->load->view('templates/css', $css, TRUE);
			$data['_content']		= $this->_ci->load->view($template, $data, TRUE);
			$data['_fullcontent']	= $this->_ci->load->view('templates/content', $data, TRUE);
			$data['_header']		= $this->_ci->load->view('templates/header', $data, TRUE);
			$data['_sidebar']		= $this->_ci->load->view('templates/sidebar', $data, TRUE);
			$data['_footer']		= $this->_ci->load->view('templates/footer', $data, TRUE);

			$this->_ci->load->view('templates/template.php', $data);
		}
	}

	function _check_link($data_url) {
		$get_role 	= $this->_ci->m_global->get_data_all('users', [['roles', 'role_id = user_role']], ['user_id' => user_data()->user_id], 'role_access')[0]->role_access;
		$get_menu 	= $this->_ci->m_global->get_data_all('menus', null, ['menu_status' => '1'], 'menu_id, menu_link', '`menu_id` IN ('.$get_role.')');
		$link 		= $this->_check_acc($get_menu)['link'];
		$url 		= str_replace(base_url(), '', $data_url);

		if(in_array($url, $link)) {
			return 'true';
		} else {
			return 'false';
		}
	}

	function _check_acc($data) {
		$link = [];

		foreach ($data as $key => $val) {
			$link['id'][] 	= $val->menu_id;
			$link['link'][] = $val->menu_link;
		}

		return $link;
	}

	function _update_token() {
		$session_data = [
			'session_time' => time(),
		];

		$return = $this->_ci->m_global->update('session', $session_data, ['session_id' => user_data()->session_id]);

		return true;
	}
}

?>