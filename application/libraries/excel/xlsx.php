<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class xlsx{ 
	public function getdata($file)
	{
 		require_once "simplexlsx.class.php";
		$xlsx = new SimpleXLSX($file);
		return $xlsx;
	 }
}