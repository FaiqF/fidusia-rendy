var Layout = function() {
	var handleContent = function() {
		$('.menu_section > ul').on('click', ' li > a.ajaxify', function(e) {
			e.preventDefault();
			App.scrollTop();

			var url 			= $(this).attr("href");
			var pageContent 	= $('#page_content');
			var menuContainer 	= $('.menu_section ul');

			menuContainer.children('li.current_section').removeClass('current_section');
			menuContainer.children('li.act_item').removeClass('act_item');
			menuContainer.children('li.act_section').removeClass('act_section');

			$(this).parents('li').each(function() {
				$(this).addClass('current_section act_section');
			});
			$(this).parents('li').addClass('act_item');

			history.pushState(null, null, url);
			if(url != ajaxify[2]){
				ajaxify.push(url);
			}
			ajaxify = ajaxify.slice(-3, 5);

			$.ajax({
				url 		: url,
				data 		: { status_link : 'ajax'},
				dataType	: 'html',
				type 		: 'POST',
				cache 		: false,
				beforeSend	: function(xhr) {
					App.pageLoading({target: pageContent});
					altair_helpers.content_preloader_show('md');
				},
				error 		: function(xhr, status, error) {
					errorAjaxify();
				},
				success		: function(result, status, xhr) {
					if (result == 'out') {
						window.location = base_url + 'login';
					} else {
						altair_helpers.content_preloader_hide();
						pageContent.html(result);
						pageContent.css('position', '');
						App.initAjax();
					}
				}
			});
		});

		var ajaxify     = [null, null, null];

		$('#header_main').on('click', '.ajaxify', function(e) {
			var ele = $(this);
			functionAjaxify(e, ele);
		});

		$('#page_content').on('click', '.ajaxify', function(e) {
			var ele = $(this);
			functionAjaxify(e, ele);
		});

		// $('#page_content_inner').on('click', '.ajaxify', function(e) {
		// 	var ele = $(this);
		// 	functionAjaxify(e, ele);
		// });

		var functionAjaxify = function (e, ele){
			e.preventDefault();
			App.scrollTop();

			var url 			= $(ele).attr("href");
			var pageContent 	= $('#page_content');
			var menuContainer 	= $('.menu_section ul');
			

			if(url != ajaxify[2]){
				ajaxify.push(url);
				history.pushState(null, null, url);
			}
			ajaxify = ajaxify.slice(-3, 5);

			$.ajax({
				url 		: url,
				data 		: { status_link : 'ajax'},
				dataType	: 'html',
				type 		: 'POST',
				cache 		: false,
				beforeSend	: function(xhr) {
					App.pageLoading({target: pageContent});
					altair_helpers.content_preloader_show('md');
				},
				error 		: function(xhr, status, error) {
					errorAjaxify();
				},
				success		: function(result, status, xhr) {
					if (result == 'out') {
						window.location = base_url + 'login';
					} else {
						altair_helpers.content_preloader_hide();
						pageContent.html(result);
						pageContent.css('position', '');
						App.initAjax();
					}
				}
			});
		}

		var errorAjaxify = function() {
			var pageContent 	= $('#page_content');
			$.ajax({
				type: "POST",
				cache: false,
				url: base_url+'error/error_404', 
				data: { status_link : 'ajax', url: ajaxify[1], url1: ajaxify[2] },
				dataType: "html",
				success: function (html){ 
					if (html == 'out') {
						window.location = base_url + 'login';
					} else {
						pageContent.html(html);
						pageContent.css('position', '');
						altair_helpers.content_preloader_hide();
					}
				}
			});
		}
	}

	return {
		init: function() {
			handleContent();
		},
	}
}();

jQuery(document).ready(function() {
	Layout.init(); // init metronic core componets
});